#ifndef _SHAREDUIXCTRLSELECTOR_H_97B01DFB_8AFE_4B7E_9CC9_B8FC391DEE77_INCLUDED
#define _SHAREDUIXCTRLSELECTOR_H_97B01DFB_8AFE_4B7E_9CC9_B8FC391DEE77_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Nov-2019 at 9:16:09p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is shared UIX library task/view selector control interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.selector.ext.h"

namespace ex_ui { namespace controls {

	using shared::sys_core::CError;

	class CSel_Type {
	public:
		enum _type  {
			e_cmd = 0x0,    // command item type, default;
			e_url = 0x1,    // web link;
		};
	};

	typedef CSel_Type::_type TSelItmType;

	class CSel_Item {
	protected:
		RECT         m_place;
		UINT         m_id   ;
		CAtlString   m_cap  ;
		INT          m_img  ; // image index in image list control, by default is -1, i.e. no image;
		TSelItmType  m_type ;

	public:
		 CSel_Item (void);
		~CSel_Item (void);

	public:
		const
		CAtlString&  Cap (void) const;
		CAtlString&  Cap (void)      ;
		const UINT&  Id  (void) const;
		      UINT&  Id  (void)      ;  // item collection does not control changing identifier this way;
		const  INT&  Img (void) const;
		       INT&  Img (void)      ;
		const RECT&  Rect(void) const;
		      RECT&  Rect(void)      ;
		const
		TSelItmType& Type(void) const;
		TSelItmType& Type(void)      ;
	};

	typedef ::std::vector <CSel_Item>  TSelItems; // selector control supposes unsorted item sequence;

	class CSel_Item_Enum {
	protected:
		TSelItems     m_items;
		CError        m_error;
		INT    m_ndx_selected;    // if less than zero that means no item is selected;

	public:
		 CSel_Item_Enum (void);
		~CSel_Item_Enum (void);

	public:
		HRESULT       Append  (const CSel_Item&);
		TErrorRef     Error   (void) const;
		const
		TSelItems&    Items   (void) const;
		TSelItems&    Items   (void)      ;
		const INT     Selected(void) const;
		HRESULT       Selected(const INT _ndx)  ;

	public:
		operator const TSelItems& (void) const;
	public:
		CSel_Item_Enum& operator += (const CSel_Item&);
	};

	class CSelector   ;
	class CSel_Layout {
	protected:
		CSelector&      m_selector;
		eHorzAlign::_e  m_hz_align; // horizontal alignment in parent window;
		CMargins        m_margins ;
		UINT            m_width   ; // control width; it has a default value on startup, can be re-written;
		mutable CError  m_error   ;

	public:
		 CSel_Layout(CSelector&);
		~CSel_Layout(void);

	public:
		TErrorRef       Error  (void) const ;
		const
		eHorzAlign::_e& HAlign (void) const ;
		eHorzAlign::_e& HAlign (void)       ;
		const
		CMargins&       Margins(void) const ;
		CMargins&       Margins(void)       ;
		UINT            Width  (void) const ;
		HRESULT         Width  (const UINT );          // TODO: input value is checked on 0-value only; minimal value must be defined;
		HRESULT         Update (const RECT& _rc_area); // updates selector window position into an area provided;

	public:
		CSel_Layout&    operator<<(const RECT& _rc_area);        // updates selector window position in accordance with area  ;
		const RECT      operator =(const RECT& _rc_area) const;  // returns calculated rectangle of selector for area provided;
	};

	class CSelector : public ISelector_Events {
	protected:
		ISelector_Events&
		               m_evt_snk;   // control owner event sink reference;
		HANDLE         m_wnd_ptr;
		CError         m_error  ;
		CSel_Item_Enum m_itm_enm;
		CSel_Format    m_format ;
		CSel_Layout    m_layout ;
		UINT           m_ctrl_id;

	public:
		 CSelector(ISelector_Events&);
		~CSelector(void);

	public:
		HRESULT        Create  (const HWND hParent, const RECT& _rc_area, LPCTSTR _lp_sz_cap, const UINT _ctrl_id);
		HRESULT        Destroy (void)      ;
		TErrorRef      Error   (void) const;
		const UINT     Id      (void) const;
		const bool     Is      (void) const; // checks a validity state;
		const
		CSel_Item_Enum& Items  (void) const;
		CSel_Item_Enum& Items  (void)      ;
		const
		CSel_Layout&   Layout  (void) const;
		CSel_Layout&   Layout  (void)      ;
		const
		CSel_Format&   Format  (void) const;
		CSel_Format&   Format  (void)      ;
		const bool     Visible (void) const; // returns control visibility state;
		HRESULT        Visible (const bool); // sets control visibility;
		CWindow        Window  (void) const;

	private: // ISelector_Events
		virtual HRESULT   ISelector_OnBkgChanged (void) override;
		virtual HRESULT   ISelector_OnFontChanged(void) override;
		virtual HRESULT   ISelector_OnItemClick  (const UINT _u_itm_id) override;
		virtual HRESULT   ISelector_OnItemImages (const UINT _u_res_id) override;

	private:
		CSelector(const CSelector&);
		CSelector& operator= (const CSelector&);
	};
}}

#endif/*_SHAREDUIXCTRLSELECTOR_H_97B01DFB_8AFE_4B7E_9CC9_B8FC391DEE77_INCLUDED*/