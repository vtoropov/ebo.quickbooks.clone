#ifndef _SHAREDUIXCTRLSELECTOREXT_H_49A0BB5A_25DB_493B_A79C_C9DACF94AE2F_INCLUDED
#define _SHAREDUIXCTRLSELECTOREXT_H_49A0BB5A_25DB_493B_A79C_C9DACF94AE2F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Nov-2019 at 9:16:09p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is shared UIX library task/view selector control extension interface declaration file.
*/
#include "shared.uix.ctrl.defs.h"

namespace ex_ui { namespace controls {

	interface ISelector_Events {
		virtual HRESULT   ISelector_OnBkgChanged (void) PURE;
		virtual HRESULT   ISelector_OnFontChanged(void) PURE;
		virtual HRESULT   ISelector_OnItemClick  (const UINT _u_itm_id) PURE;
		virtual HRESULT   ISelector_OnItemImages (const UINT _u_res_id) PURE;
	};

	class CSel_Format {
	private:
		ISelector_Events&
		               m_callee   ;
		COLORREF       m_back     ;        // background solid color;
		COLORREF       m_fore     ;
		DWORD          m_size     ;
		CAtlString     m_family   ;
		eHorzAlign::_e m_hz_align ;
		UINT           m_bkg_id   ;        // background image resource identifier; 0 - no background image;
		UINT           m_img_id   ;        // identifier of resource for dreawing item images; 0 - no item images;

	public:
		 CSel_Format(ISelector_Events&);
		~CSel_Format(void);

	public:
		COLORREF       BackColour(void) const;
		HRESULT        BackColour(const COLORREF);
		UINT           BkgImage  (void) const;          // gets background image resource identifier;
		HRESULT        BkgImage  (const UINT u_res_id); // sets background image resource identifier; zero is acceptable;
		UINT           ItmImages (void) const;          // gets item image list resource identifier;
		HRESULT        ItmImages (const UINT u_res_id); // sets item image list resource identifier; zero is acceptable;
		LPCTSTR        Family    (void) const;          // font name, by default is "Verdana"
		HRESULT        Family    (LPCTSTR)   ;
		DWORD          FontSize  (void) const;          // by default is 10pt;
		HRESULT        FontSize  (const DWORD);
		COLORREF       ForeColour(void) const ;         // by defailt is forecolor of current theme;
		HRESULT        ForeColour(const COLORREF);
		eHorzAlign::_e HorzAlign (void) const;          // by default is left;
		VOID           HorzAlign (const eHorzAlign::_e);
	};
}}

#endif/*_SHAREDUIXCTRLSELECTOREXT_H_49A0BB5A_25DB_493B_A79C_C9DACF94AE2F_INCLUDED*/