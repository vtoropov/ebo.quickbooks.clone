#ifndef _SHAREDUIXCTRLLVWCOL_H_ED52FFF6_868D_4EFD_9AEA_AFA574655D05_INCLUDED
#define _SHAREDUIXCTRLLVWCOL_H_ED52FFF6_868D_4EFD_9AEA_AFA574655D05_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Sep-2018 at 9:18:21p, UTC+7, Novosibirsk, Rodniki, Tulenina, Tuesday;
	This is Ebo Pack UIX shared library standard list view control column interface declaration file.
*/

#define CListViewCol_DefMask (LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM)

namespace ex_ui { namespace controls { namespace std {

	class CListViewCol_Vs {

	private:
		LVCOLUMN& m_vs_source;   // a reference to actual column width;
		INT       m_vs_width;    // the last width when a column is visible;
		bool      m_vs_state;    // the visibility state;

	public:
		CListViewCol_Vs(void);   // TODO: this version of the constructor for using visibility independently from column structure; must be reviewed;
		CListViewCol_Vs(LVCOLUMN& _col_width_ref);
		~CListViewCol_Vs(void);

	public:
		VOID      Clear (void)      ;
		bool      Get   (void) const;
		VOID      Set   (const bool);
		VOID      Update(void);       // synchronizes visibility state with current column width (it is used in column assign operator);
		INT       Width (void) const;
		VOID      Width (const INT) ; // is used for setting predefined column width, if necessary;

	public:
		bool operator!= (const CListViewCol_Vs&) const; // compares visibility state only;
		bool operator!= (const bool) const;             // compares visibility state against boolean value provided;
		CListViewCol_Vs& operator= (const bool);
		CListViewCol_Vs& operator= (const CListViewCol_Vs&);
		operator bool (void) const;
	};

	typedef ::std::vector<CListViewCol_Vs>   TListViewCols_Vs; // it is used for control over columns visibility; for example, in settings dialog;

	class CListViewCol_Ex {
		//
		// TODO: a column visibility feature needs to be improved;
		//
	private:
		CAtlString   m_title;
		LVCOLUMN     m_column;
		UINT         m_id;
		CListViewCol_Vs
		             m_col_vs;   // visibility data;

	public:
		CListViewCol_Ex(void);
		CListViewCol_Ex(const CListViewCol_Ex&);
		CListViewCol_Ex(const UINT _format, const UINT _width, const UINT _index, LPCTSTR _title, const UINT _mask = CListViewCol_DefMask);
		~CListViewCol_Ex(void);

	public:
		UINT      Format(void) const;
		VOID      Format(const UINT);
		UINT      Identifier(void)const;    // gets column identifier from internal variable;
		VOID      Identifier(const UINT);   // sets column identifier via header item;
		UINT      Index(void) const;
		VOID      Index(const UINT);
		bool      IsValid(void) const;
		UINT      Mask (void) const;
		VOID      Mask (const UINT = CListViewCol_DefMask);
		LPCTSTR   Title(void) const;
		VOID      Title(LPCTSTR)   ;
		const
		CListViewCol_Vs& Visible(void) const;
		CListViewCol_Vs& Visible(void)      ;
		UINT      Width(void) const;        // gets an actual width of the column; if value is 0, the column is not visible;
		VOID      Width(const UINT);        // sets an actual width of the column; if zero is provided, the column becomes not visible;
	public:
		const LVCOLUMN& ColumnData(void)const;
	public:
		operator  const LVCOLUMN&(void) const;
		operator  LVCOLUMN&(void);
		operator  CAtlString&(void);
	public:
		CListViewCol_Ex& operator= (const CListViewCol_Ex&);
		CListViewCol_Ex& operator= (const LVCOLUMN&);           // does not assign column identifier;
	};

}}}

#endif/*_SHAREDUIXCTRLLVWCOL_H_ED52FFF6_868D_4EFD_9AEA_AFA574655D05_INCLUDED*/