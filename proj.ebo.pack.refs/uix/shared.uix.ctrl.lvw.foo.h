#ifndef _UIXLISTVIEWFOOTER_H_7FC3C1EF_52F8_4a3a_8C9E_BBF2A0B8E025_INCLUDED
#define _UIXLISTVIEWFOOTER_H_7FC3C1EF_52F8_4a3a_8C9E_BBF2A0B8E025_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2018 at 9:32:37p, UTC+7, Bangkok, Suvarnabh, Monday;
	This is UIX Control library extended standard list view control footer interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 11-Aug-2018 at 5:46:03a, UTC+7, Novosibirsk, Rodniki, Tulenina, Saturday;
*/
#include "shared.gen.sys.err.h"

namespace ex_ui { namespace controls { namespace std {

	using shared::sys_core::CError;

	interface IListView_Ex_Footer {

		virtual HRESULT IListView_OnCtrlClick   (const INT nIndex ) PURE;
		virtual HRESULT IListView_OnCtrlDestroy (const INT nIndex ) PURE;
		virtual HRESULT IListView_OnLoadBefore  (const CAtlString&) PURE;
		virtual HRESULT IListView_OnLoadComplete(void) PURE;
	};

	class CListViewFooterItem {

	private:
		CAtlString    m_title;
		LONG          m_data;
		LONG          m_icon;          // icon image index in the list view control image list;
	public:
		CListViewFooterItem(void);
		~CListViewFooterItem(void);

	public:
		LONG          Data (void)const ;
		VOID          Data (const LONG);
		LONG          Icon (void)const ;
		VOID          Icon (const LONG);
		LPCTSTR       Title(void)const ;
		VOID          Title(LPCTSTR)   ;
	};

	typedef ::std::vector<CListViewFooterItem>  TListViewFooterCtrls;

	class CListViewFooterCtrls {

	private:
		TListViewFooterCtrls  m_ctrls;

	public:
		CListViewFooterCtrls(void);
		~CListViewFooterCtrls(void);

	public:
		INT           Active(void)const;
		HRESULT       Active(const INT nIndex);
		HRESULT       Insert(const CListViewFooterItem&);
		HRESULT       Remove(const INT nIndex);
		HRESULT       RemoveAll(void);
	};

	class CListViewFooter  {

	private:
		IListView_Ex_Footer&
		              m_evt_handler;
		CListViewFooterCtrls
		              m_items ;
		HANDLE        m_handle;
		CError        m_error ;
	public:
		CListViewFooter(IListView_Ex_Footer&);
		~CListViewFooter(void);
	public:
		const
		CAxWindow&    Control(void)const;
		CAxWindow&    Control(void);
		HRESULT       Create(const HWND hParent, LPCTSTR lpszURL, const RECT& rcArea);
		HRESULT       Destroy(void);
		TErrorRef     Error (void)const;
		const
		CListViewFooterCtrls& Items(void)const;
		CListViewFooterCtrls& Items(void);
		bool          Visible(void)const;
		VOID          Visible(const bool _val);
		HWND          Window(void)const;
	};

}}}

#endif/*_UIXLISTVIEWFOOTER_H_7FC3C1EF_52F8_4a3a_8C9E_BBF2A0B8E025_INCLUDED*/