#ifndef _SHAREDUIXCTRLCLRS_H_0A281A83_D1DC_4E08_8CD6_3B5DF8D3D7BA_INCLUDED
#define _SHAREDUIXCTRLCLRS_H_0A281A83_D1DC_4E08_8CD6_3B5DF8D3D7BA_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Nov-2019 at 2:23:52a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack shared drawing library complementary colors pairs declaration file.
*/
#include "shared.uix.gdi.draw.defs.h"
namespace ex_ui { namespace draw {
	
	class CCmpClrs {
		// https://looka.com/blog/logo-color-combinations/
		// http://incubar.net/11-best-color-combinations-for-painting-rooms/
	private:
		COLORREF    m_clr[3];
		CAtlString  m_name  ;
		CAtlString  m_desc  ;
	public:
		 CCmpClrs (void);
		 CCmpClrs (const COLORREF _dark, const COLORREF _mid, const COLORREF _lt, LPCTSTR _lp_sz_nm, LPCTSTR _lp_sz_ds);
		~CCmpClrs (void);

	public:
		const DWORD& Dark  (void) const;
		      DWORD& Dark  (void)      ;
		const DWORD& Light (void) const;
		      DWORD& Light (void)      ;
		const DWORD& Middle(void) const;
		      DWORD& Middle(void)      ;
	public:
		const
		CAtlString&  Desc  (void) const;
		CAtlString&  Desc  (void)      ;
		const
		CAtlString&  Name  (void) const;
		CAtlString&  Name  (void)      ;
	};

	typedef ::std::vector<CCmpClrs> TCmpClrs;

	class CCmpClrs_Enum {
	private:
		TCmpClrs     m_clrs;

	public:
		 CCmpClrs_Enum (void);
		~CCmpClrs_Enum (void);

	public:
		const CCmpClrs& Item(const size_t _ndx) const;
		const TCmpClrs& Ref (void) const;
		const size_t    Size(void) const;

	public:
		const CCmpClrs& operator[](const size_t _ndx) const;
	};

}}

#endif/*_SHAREDUIXCTRLCLRS_H_0A281A83_D1DC_4E08_8CD6_3B5DF8D3D7BA_INCLUDED*/