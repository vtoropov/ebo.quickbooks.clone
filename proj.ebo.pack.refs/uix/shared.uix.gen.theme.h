#ifndef _SHAREDUIXGENTHEME_H_00367915_33A3_4982_B1CA_AB21EA6FAC58_INCLUDED
#define _SHAREDUIXGENTHEME_H_00367915_33A3_4982_B1CA_AB21EA6FAC58_INCLUDED
/*
	Created by Tech_dog(VToropov) on 19-Jan-2011 at 11:18:11pm, GMT+3, Rostov-on-Don, 339th Strelkovaya Division, Wednesday;
	This is UIX shared draw library theme support service interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 2-Aug-2018 at 5:55:30p, UTC+7, Novosibirsk, Rodniki, Tulenina, Thursday;
*/
#include "shared.uix.gdi.object.h"

#include <uxTheme.h>

namespace ex_ui { namespace draw {

	class CThemeColourPair {
	private:
		CColour   m_lite;
		CColour   m_dark;

	public:
		CThemeColourPair(void);
		~CThemeColourPair(void);

	public:
		const CColour& Dark(void) const;
		      CColour& Dark(void)      ;
		const CColour& Lite(void) const;
		      CColour& Lite(void)      ;
	public:
		VOID                 Clear(void);  // sets colour pair to invalid state: all colours will become to CLR_NONE;
		const bool           IsValid(void) const;
	};

	class CThemeColours {
	protected:
		CThemeColourPair     m_high; // high color: lite version is a bright; dark one is a less bright color;
		CThemeColourPair     m_med;  // medium color: lite is pure medium; dark is a shallow medium;
		CThemeColourPair     m_low;  // low color: lite is a light low; dark is pure low;
		CThemeColourPair     m_inv;  // inverted color: lite is shallow low inverted, dark is almost low inverted;

	public:
		CThemeColours(void);
		~CThemeColours(void);

	public:
		const
		CThemeColourPair&    High(void) const;
		CThemeColourPair&    High(void)      ;
		const
		CThemeColourPair&    Invert(void) const;
		CThemeColourPair&    Invert(void)      ;
		const
		CThemeColourPair&    Medium(void) const;
		CThemeColourPair&    Medium(void)      ;
		const
		CThemeColourPair&    Low(void) const;
		CThemeColourPair&    Low(void)      ;
	public:
		const bool           IsValid(void) const;
	};

	class CThemePartSupported {
	public:
		enum _e : ULONG {
			eNone            = 0,
			eGlobal          = 1,
			eButton          = 2,
			eTreeView        = 3,
			eCompositeWindow = 0xf
		};
	};

	class CThemeType  {
	public:
		enum _e : ULONG {
			eNative          = 0x0, // OS theme(s) support colors, default
			eNamed           = 0x1, // sets named theme
			eCustom          = 0xf  // user defined color theme
		};
	};

	class CThemeName { // pre-defined named themes
	public:
		enum _e : ULONG {
			eNone            = 0x0,
			eAqua            = 0x1,
			eSky             = 0x2,
			eSteel           = 0x3,
			eSilver          = 0x4,
			eBlackIce        = 0x5
		};
	};

	class CThemeEffect { // theme effects
	public:
		enum _e : ULONG {
			eNone            = 0x0,  // there is no theme effect
			eGlossy          = 0x1,  // glossy effect (enabled by default)
		};
	};

	interface IThemeNotifySink abstract
	{
		virtual VOID OnThemeColorChanged(void) {}
		virtual VOID OnThemeCompositionChanged(void) {}
	};

	class CThemeEvents
	{
	public:
		virtual HRESULT      Advise(IThemeNotifySink*)              = 0;   // subscribes to theme change events
		virtual HRESULT      UnAdvise(IThemeNotifySink*)            = 0;   // removes a subscriber from sink collection
	};

	class CThemeInfo {
	protected:
		HBITMAP         m_hBkgnd;    // background image
		CAtlString      m_szName;    // theme name
		CAtlString      m_szTooltip; // theme screen tip
		HRESULT         m_hResult;   // a state of theme info
		CThemeColours   m_tColors;   // theme color, can be NULL when error state
		UINT            m_nID;       // theme unique identifier

	public:
		CThemeInfo(void);
		~CThemeInfo(void);

	public:
		const
		HBITMAP&        Background(void) const;
		HBITMAP&        Background(void)      ;
		const
		CThemeColours&  Colours(void) const;
		CThemeColours&  Colours(void)      ;
		const
		UINT&           Identifier(void) const;
		UINT&           Identifier(void)      ;
		const
		HRESULT&        LastResult(void) const;
		HRESULT&        LastResult(void)      ;
		const
		CAtlString&     Name(void) const;
		CAtlString&     Name(void)      ;
		const
		CAtlString&     Tooltip(void) const;
		CAtlString&     Tooltip(void)      ;
	public:
		bool            IsValid(void) const;
	};

	typedef ::std::map<UINT, CThemeInfo>  t_ebo_uix_theme_infos;

	class CThemeNamedEnum
	{
	private:
		t_ebo_uix_theme_infos     m_named;
	public:
		CThemeNamedEnum(void);
		~CThemeNamedEnum(void);
	public:
		const
		CThemeColours&    Colours(const UINT nId) const; // gets colors of specified named theme
		const CThemeInfo& Find (const UINT nId)   const; // gets information of specified named theme
		const bool        IsEmpty(void)           const; // gets count of named themes
		const
		t_ebo_uix_theme_infos& Named(void) const;
	};

#ifndef HTHEME
	#define HTHEME HANDLE
#endif

	class CTheme {
	private:
		CThemeNamedEnum    m_named;
		CThemeInfo         m_active;
	public:
		CTheme(void);
		~CTheme(void);
	public:
		bool               IsCompositionEnabled(const bool bReset =false); // checks for composition enabling
		HTHEME             ThemeHandle(const CThemePartSupported::_e);     // gets theme handle for specified UI part
	public:
		static bool        IsEnabled(void);                                // checks a theme is enabled at all
	};
}}
#endif/*_SHAREDUIXGENTHEME_H_00367915_33A3_4982_B1CA_AB21EA6FAC58_INCLUDED*/