#ifndef _SHAREDUIXCTRLSTA_H_F6D64B6A_6CCB_4380_A7E7_9F157DA375FC_INCLUDED
#define _SHAREDUIXCTRLSTA_H_F6D64B6A_6CCB_4380_A7E7_9F157DA375FC_INCLUDED
/*
	Created by Tech_dog (VToropov) on 12-Nov-2015 at 10:29:42pm, GMT+7, Phuket, Rawai, Thursday;
	This is Ebo Pack UIX shared library extended standard status bar control class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 12-Aug-2018 at 4:08:04a, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
*/
#include "shared.uix.ctrl.sta.def.h"

namespace ex_ui { namespace controls { namespace std {
	//
	// TODO: there is no estimation of incorrect index, invalid object can be returned;
	//       the only way to check an object validity is to get its index;
	//
	class CStaPanels {
	public:
		enum _ndx : DWORD {
			eNa   = 0xFF,  // indicates an invalid index of a panel object;
		};
	private:
		TStaPanelMap    m_pans;
		CWindow&        m_sta; 

	public:
		CStaPanels(CWindow& _sta);
		~CStaPanels(void);

	public:
		HRESULT     Apply(void);                   // applies all panels to status bar control;
		HRESULT     Clear(void);                   // destroys panel array; frees all panel pointers;
		HRESULT     Insert(const CStaPanel*);      // a pointer provided will be destroyed automatically;
		HRESULT     Insert(const CStaPanel&);      // creates a copy of an object provided;
		const
		CStaPanel&  Panel (const UINT _ndx) const; // gets a referenece to a panel by index provided (read only);
		CStaPanel&  Panel (const UINT _ndx)      ; // gets a referenece to a panel by index provided (read/write);
		const
		TStaPanelMap& Panes(void) const;
		HRESULT       Panes (const DWORD _ndx, LPCTSTR lpszText); // sets a text to a panel by index provided;
	};

	class CStatusBar {
	private:
		CWindow         m_wnd;           // status bar control object
		CStaPanels      m_panels;
	public:
		CStatusBar(void);
		~CStatusBar(void);
	public:
#if(0)
		HRESULT         Attach  (const HWND _h_tool); // will not work in cases custom draw pane(s); this case must be re-viewed;
#endif
		HRESULT         Create  (const HWND hParent, const UINT ctrlId); // creates a status bar control;
		HRESULT         Destroy (void);       // destroys a status bar control;
		const CStaPanels& Panels(void) const; // gets panels map object reference (read);
		      CStaPanels& Panels(void)      ; // gets panels map object reference (read/write);
		const CWindow&  Window  (void) const; // gets status bar window reference (read);
		      CWindow&  Window  (void)      ; // gets status bar window reference (read/write);
		LRESULT         Update  (const DRAWITEMSTRUCT&); // update status bar on parent window draw operation;

	public:
#if(0)
		CStatusBar& operator=(const HWND);
#endif
		operator const HWND(void) const;
	private:
		CStatusBar(const CStatusBar&);
		CStatusBar& operator= (const CStatusBar&);
	};
	//
	// TODO: it is assumed that status bar control belongs to main window only;
	//       perhaps, it must be changed in order to hold multiple instances,
	//       for example, popup wizard may have its own status bar control;
	//
	CStatusBar&    GetStatusBarObject(void);  // gets a reference to status bar control static object
}}}

#endif/*_SHAREDUIXCTRLSTA_H_F6D64B6A_6CCB_4380_A7E7_9F157DA375FC_INCLUDED*/