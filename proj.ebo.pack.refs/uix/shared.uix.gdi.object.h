#ifndef _UIXDRAWGDIOBJECT_H_758531BE_3252_454f_BDDE_151EB532BF99_INCLUDED
#define _UIXDRAWGDIOBJECT_H_758531BE_3252_454f_BDDE_151EB532BF99_INCLUDED
/*
	Created by Tech_dog (VToropov) on 7-Feb-2015 at 10:27:49pm, GMT+3, Taganrog, Saturday;
	This is Ebo Pack shared UIX draw library GDI related object(s) interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:08:13p, UTC+7, Phuket, Rawai, Monday;
	Adopted to v15a on 11-Aug-2018 at 4:14:58p, UTC+7, Novosibirsk, Tulenina, Saturday;
*/
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw {

	class CColour
	{
	private:
		Gdiplus::Color m_clr;
	public:
		CColour(void);
		CColour(const COLORREF clr, const BYTE alpha = eAlphaValue::eOpaque);
		~CColour(void);
	public:
		VOID     ColorFromRGBA(const COLORREF clr, const BYTE _alpha = eAlphaValue::eOpaque);
		const
		Gdiplus::Color& Data(void) const;
		VOID     Empty  (void);
		bool     IsValid(void) const;
	public:
		operator const Gdiplus::Color&(void) const;
		operator const Gdiplus::Color*(void) const;
		operator const COLORREF(void) const;
	};

	class eCreateFontOption
	{
	public:
		enum _e{
			eNone          = 0x0000, // creates font with default (i.e. system height/bold/italic)
			eExactSize     = 0x0001, // creates font with specified size (irrelative)
			eRelativeSize  = 0x0002, // creates font with relative size (reduction/increasing)
			eBold          = 0x0004,
			eItalic        = 0x0008,
			eUnderline     = 0x0010,
		};
	};

	class CFont
	{
	private:
		HFONT          m_handle;   // font handle
		bool           m_bManaged; // flag that indicates we have to destroy font handle (it the font is not stock one)
	public:
		CFont(LPCTSTR pszFamily = NULL, const DWORD dwOptions = eCreateFontOption::eNone, const LONG lParam = 0);
		~CFont(void);
	public:
		HFONT          Detach(void);
		HFONT          GetHandle(void) const;
	private:
		CFont(const CFont&);
		CFont& operator=(const CFont&);
	};

	class CFontScalable
	{
	private:
		HFONT          m_handle;
	public:
		CFontScalable(const HDC, LPCTSTR lpszFontFamily, const INT nSize, const DWORD dwOptions = eCreateFontOption::eNone);
		~CFontScalable(void);
	public:
		HFONT          Detach(void);
		HFONT          GetHandle(void) const;
		bool           IsValid(void)const;
	public:
		operator HFONT(void)const;
	private:
		CFontScalable(const CFontScalable&);
		CFontScalable& operator=(const CFontScalable&);
	};

	class CText
	{
	private:
		const HDC      m_hdc;               // device context which this text works for;
		const HFONT    m_fnt;               // font handle which this text is drawn by ;
		bool           m_inited;            // initialise status;
	public:
		CText(const HDC hDC, const HFONT hFont);
		~CText(void);
	public:
		HRESULT        GetSize(LPCTSTR lpText, SIZE& szText); // gets a size of the text provided;
		bool           IsValid(void)const;  // checks for a validity of internal objects state;
	};

	class CBitmapInfo : public BITMAP
	{
		typedef BITMAP    _inherited;
	private:
		HRESULT          m_hResult;         // result of getting bitmap info
		UINT             m_UID;             // identifier of this object
		HBITMAP          m_hBitmap;         // bitmap handle
	public:
		CBitmapInfo(void);
		CBitmapInfo(const HBITMAP hBitmap, const UINT UID = ::GetTickCount());
		~CBitmapInfo(void);
	public:
		HRESULT          AttachTo(const HBITMAP);
		HBITMAP          Detach(void);
		UINT             GetID(void) const;
		HRESULT          GetLastResult(void) const;
		HRESULT          GetSize(SIZE&) const;
	private:
		CBitmapInfo(const CBitmapInfo&);
		CBitmapInfo& operator=(const CBitmapInfo&);
	};

	class CDibSection {
	private:
		mutable
		HRESULT       m_hResult;    // last result
		HBITMAP       m_hBitmap;    // encapsulated bitmap descriptor
		PBYTE         m_pData;      // a pointer to bitmap bits
		SIZE          m_sz;         // bitmap size

	public:
		CDibSection(void);
		CDibSection(const HDC hDC, const SIZE& sz);
		~CDibSection(void);

	public:
		HRESULT           Create (const HDC hDC, const SIZE& sz);
		HBITMAP           Detach (void);                   // detaches from encapsulated bitmap descriptor
		const PBYTE       GetBits(void) const;             // gets bitmap data
		HBITMAP           GetHandle(void) const;           // gets a bitmap handle if success, otherwise returns zero
		HRESULT           GetLastResult(void) const;       // gets a last operation result
		bool              IsValid (void) const;            // checks a validity of the encapsulated bitmap descriptor
		Gdiplus::Bitmap*  ToBitmap(void) const;            // creates gdi+ bitmap object from encapsulated bitmap descriptor
	public:
		operator          HBITMAP() const;                 // operator overloading, does the same as GetHandle_Safe()
	};

	class CGdiplusBitmapWrap
	{
		Gdiplus::Bitmap*   m_pBitmap;
		bool               m_bManaged;
	public:
		CGdiplusBitmapWrap(const HBITMAP); // creates GDI+ object from bitmap handle provided
		CGdiplusBitmapWrap(Gdiplus::Bitmap*, const bool bManaged);
		~CGdiplusBitmapWrap(void);
	public:
		HRESULT            Clip(const RECT& rcDest, Gdiplus::Bitmap*& __out_ptr);
		Gdiplus::Bitmap*   Detach(void);
		HRESULT            Reset(void);
	private:
		CGdiplusBitmapWrap(const CGdiplusBitmapWrap&);
		CGdiplusBitmapWrap& operator= (const CGdiplusBitmapWrap&);
	};

	typedef  ::std::map<DWORD, Gdiplus::Bitmap*>  TImageCache;

	class CImageCache {
	private:
		TImageCache        m_cache;
	public:
		CImageCache(void);
		~CImageCache(void);
	public:
		HRESULT            Add(const ULONG uKey, Gdiplus::Bitmap* pb);
		HRESULT            Add(const ULONG uKey, const SIZE& sz_, Gdiplus::Bitmap** ppb = NULL);
		HRESULT            Clear(void);
		INT                Count(void) const;
		Gdiplus::Bitmap*   Get(const ULONG uKey) const;
		bool               Has(const ULONG uKey) const;
		HRESULT            Remove(const ULONG uKey);
		const TImageCache& Raw(void) const;

	public:
		operator const TImageCache& (void) const;

	private:
		CImageCache(const CImageCache&);
		CImageCache& operator=(const CImageCache&);
	};
}}

#endif/*_UIXDRAWGDIOBJECT_H_758531BE_3252_454f_BDDE_151EB532BF99_INCLUDED*/