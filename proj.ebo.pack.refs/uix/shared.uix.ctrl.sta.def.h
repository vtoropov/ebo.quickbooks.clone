#ifndef _SHAREDUIXCTRLSTADEF_H_B20F4A19_85FF_4245_A5AF_35ED654BDA46_INCLUDED
#define _SHAREDUIXCTRLSTADEF_H_B20F4A19_85FF_4245_A5AF_35ED654BDA46_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Aug-2018 at 0:08:32a, UTC+7, Novosibirsk, Rodniki, Tulenina, Monday;
	This is Ebo Pack UIX shared library extended standard status bar panel interface declaration file.
*/

namespace ex_ui { namespace controls { namespace std {

	class CStaMsgType {
	public:
		enum _e : LONG {
			eError     = 0x0,
			eWarning   = 0x1,
			eInfo      = 0x2,
			eWaiting   = 0x3,
		};
	};

	typedef ::std::map<CStaMsgType::_e, INT>  TStaImageMap;   // first element is message type, second one is an index of image list element;

	class CStaImages {
	public:
		enum _e : LONG {
			eNone = -1,
		};
	private:
		HIMAGELIST      m_list;
		TStaImageMap    m_map ;
		INT             m_act ;  // an index of active image; -1 is default and it means no image is set;
	public:
		CStaImages(void);
		CStaImages(const CStaImages&);
		~CStaImages(void);

	public:
		virtual LRESULT Draw(const DRAWITEMSTRUCT&);
	public:
		INT             Active(void) const;   // gets currently active/selected image index for drawing procedure;
		HRESULT         Active(const INT) ;   // sets an index of active/selected image; -1 is not set;
		HRESULT         Create(const UINT nResId);
		HRESULT         Copy  (const HIMAGELIST );
		HRESULT         Destroy(void);
		INT             Index (const CStaMsgType::_e) const;           // gets image list index for a specified type, -1 is not found;
		HRESULT         Index (const CStaMsgType::_e, const INT _ndx); // sets image index for message type provided;
		const
		HIMAGELIST&     List(void) const;
		HIMAGELIST&     List(void);

	public:
		CStaImages& operator= (const CStaImages&);
		CStaImages& operator= (HIMAGELIST);
	};

	class CStaPanel {
	public:
		enum _r : LRESULT {
			eMissed    = 0,
			eHandled   = 1,
		};
	protected:
		CAtlString     m_buffer;
		DWORD          m_style;
		INT            m_width;  // -1 is acceptable for all available width of a status bar;
		DWORD          m_index;  // an index of a panel;
		CWindow        m_owner;  // actually, a status bar window handle; but it can be zero for cases when specific drawing is not necessary;
		CStaImages     m_images; // image list for custom draw support;

	public:
		CStaPanel(void);
		CStaPanel(const CStaPanel&);
		~CStaPanel(void);

	public:
		virtual HRESULT   Destroy(void);               // intended for using of panel instance by a panel container;
		virtual LRESULT   Draw(const DRAWITEMSTRUCT&); // returns zero in case when update event is not handled, otherwise - one;
		virtual HRESULT   Draw(void);                  // forces to re-draw a panel without handling standard message WM_DRAWITEM;

	public:
		const
		CStaImages& Images(void) const;   // gets image list reference (read);
		CStaImages& Images(void)      ;   // gets image list reference (read/write);
		DWORD       Index (void) const;   // gets a panel index in panel array/map ;
		VOID        Index (const DWORD);  // sets a panel index in panel array/map ;
		HWND        Owner (void) const;   // gets owner handle;
		VOID        Owner (const HWND);   // sets owner handle; there is no check for handle validity;
		DWORD       Style (void) const;   // gets a panel style;
		VOID        Style (const DWORD);  // sets a panel style;
		LPCTSTR     Text  (void) const;   // gets a panel caption/text;
		VOID        Text  (LPCTSTR)   ;   // sets a panel caption/text;
		INT         Width (void) const;   // gets a panel width in pixels;
		VOID        Width (const INT) ;   // sets a panel width in pixels;
	public:
		CStaPanel&  operator= (const CStaPanel&);

	public:
		static HRESULT Create(CStaPanel*&);
	};

	typedef ::std::vector<CStaPanel*>     TStaPanelMap;

}}}

#endif/*_SHAREDUIXCTRLSTADEF_H_B20F4A19_85FF_4245_A5AF_35ED654BDA46_INCLUDED*/