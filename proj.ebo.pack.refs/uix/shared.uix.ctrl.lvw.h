#ifndef _UIXLISTVIEWEX_H_9B9CB45D_9EDD_4be7_AD2E_2971B0DD80E4_INCLUDED
#define _UIXLISTVIEWEX_H_9B9CB45D_9EDD_4be7_AD2E_2971B0DD80E4_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Mar-2018 at 10:17:04a, UTC+7, Phuket, Rawai, Tuesday;
	This is Ebo Pack UIX control library extended standard list view control interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 11-Aug-2018 at 5:55:06a, UTC+7, Novosibirsk, Rodniki, Tulenina, Saturday;
*/
#include "shared.uix.ctrl.lvw.gru.h"
#include "shared.uix.ctrl.lvw.foo.h"
#include "shared.uix.ctrl.lvw.col.h"

namespace ex_ui { namespace controls { namespace std {

	interface IListView_Ex_DataSource {

		virtual HRESULT IListView_OnCacheHint ( NMLVCACHEHINT* _pCacheHint) PURE;
		virtual HRESULT IListView_OnDispQuery ( NMLVDISPINFO* _pQueryInfo ) PURE;
		virtual HRESULT IListView_OnFindItem  ( LPNMLVFINDITEM _pFindItem ) PURE;
	};

	typedef ::std::vector<CListViewCol_Ex> TListViewCols_Ex;
	//
	// this class manipulates by column(s) of a list view control directly;
	//
	class CListViewColEnum {
	public:
		enum _r : INT {
			eNa = -1, // means that an element is not found or/and not available;
		};
	private:
		CWindow&     m_ctrlWnd;     // a reference to control window;
		TListViewCols_Ex m_raw;
	public:
		CListViewColEnum(CWindow& _ctrlWnd); // TODO: it is not suitable constructor, it requires a control window reference; must be re-viewed;
		~CListViewColEnum(void);

	public:
		HRESULT     Append (const CListViewCol_Ex&);                   // column index specified is ignored and is substituted by the last one;
		const
		CListViewCol_Ex&   Column(const INT nIndex) const;             // returns a column by an index provided (read-only);
		CListViewCol_Ex&   Column(const INT nIndex)      ;             // returns a column by an index provided (read-write);
		INT         Count  (void) const;                               // gets column count throu header control;
		INT         Find   (const UINT uColId) const;                  // finds column index by an indentifier provided; returns -1 if not found;
		HRESULT     Insert (const CListViewCol_Ex&, const INT nIndex); // index provided is used for placing a column to the specified position;
		bool        IsValid(void) const;                               // checks control window handle for a validity;
		const
		CListViewCol_Ex&   Lookup(const UINT _col_id) const;           // searches a column by identifier provided;
		CListViewCol_Ex&   Lookup(const UINT _col_id)      ;           // if not found, invalid column reference is returned; not thread safe;
		const
		TListViewCols_Ex& RawData(void) const;                         // gets existing column array;
		HRESULT     Remove (const INT nIndex);                         // removes a column at index provided;
		HRESULT     Refresh(void);                                     // updates/refreshes a column list in accordance with attached list view control;
		HRESULT     Update (void);                                     // applies all columns changing to listview control;
		HRESULT     Update (const INT nIndex);                         // updates a control by applying properties of column specified;
		TListViewCols_Vs
		            Visibility(void) const;                            // returns columns visibility array;
	public:
		bool operator!=(const TListViewCols_Vs&) const;                // compares column visibility; if not equal, returns true;
		operator const TListViewCols_Vs  (void ) const;
		operator const TListViewCols_Ex& (void ) const;
		CListViewColEnum& operator= (const TListViewCols_Ex&);         // attaches to raw array of columns;
	};

	//
	// TODO: Unfortunately, group feature does not work in 'report' style mode of a list view control;
	//       it is confirmed by building original sample from:
	//       https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features
	//
	class CListViewCtrl_Ex {

	private:
		UINT             m_ctrlId;
		CWindow          m_wnd;                  // list view control window;
		CListViewGroups  m_groups;               // list item group collection; does not work;
		CListViewFooter  m_footer;               // list view control footer (internal); is not used;
		CListViewColEnum m_cols;                 // list view column enumeration;

	private:
		IListView_Ex_DataSource& m_DataSource;
	public:
		CListViewCtrl_Ex(IListView_Ex_DataSource&, IListView_Ex_Footer&);
		CListViewCtrl_Ex(IListView_Ex_DataSource&, IListView_Ex_Footer&, HWND _list, const UINT nLvwId);
		~CListViewCtrl_Ex(void);
	public:
		HRESULT       Attach  (const HWND hLvCtrl, const UINT nLvwId);
		const
		CListViewColEnum& Columns(void)const;
		CListViewColEnum& Columns(void)     ;
		HRESULT       Create  (const HWND hParent, const UINT nLvwId, const RECT& rcArea, const bool bHasBorder = false);
		HRESULT       Destroy (void);
		const
		CListViewFooter& Footer(void)const;
		CListViewFooter& Footer(void);
		bool          GridLines(void)const;
		HRESULT       GridLines(const bool);
		const
		CListViewGroups& Groups(void)const;
		CListViewGroups& Groups(void);
		HWND          Header  (void) const;
		HRESULT       Insert  (const TListViewCols_Ex&);              // inserts list view column set;
		bool          IsValid (void) const;
		HRESULT       Redraw  (void) const;
		HRESULT       Replace (const HWND hOwner, const UINT nLvwId); // replaces/destroys standard list view control in owner window provided;
		bool          IsThemed(void) const;                           // gets supported theme handle, if failed, false is returned;
		HRESULT       IsThemed(void);                                 // activates a theme if possible;
		const
		CWindow&      Window  (void) const;
		CWindow&      Window  (void);
	public:
		LRESULT       OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	public:
		operator const HWND(void) const;
	private:
		CListViewCtrl_Ex(const CListViewCtrl_Ex&);
		CListViewCtrl_Ex& operator= (const CListViewCtrl_Ex&);
	public:
		// https://docs.microsoft.com/en-us/windows/desktop/menurc/wm-changeuistate
		// it very looks like a listview must be subclassed for handling WM_UPDATEUISTATE appropriately
		// in order to be consistent with settings provided for hiding 'old & classic' focus rectangle;
		// Tech_dog:
		// I am always surprised by Microsoft's attempting to create *new* UI and keeping *old and unnecessary* things at the same time;
		//
		static HRESULT TurnFocusOff(const HWND _lvw);                 // attempts to remove focus dotted rectangle from the list items being selected;
		static HRESULT TurnThemeOn (const HWND _lvw);                 // sets 'Windows Explorer' system theme style on;
	};
}}}

#endif/*_UIXLISTVIEWEX_H_9B9CB45D_9EDD_4be7_AD2E_2971B0DD80E4_INCLUDED*/