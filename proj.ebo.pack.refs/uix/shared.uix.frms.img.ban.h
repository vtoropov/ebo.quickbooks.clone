#ifndef _UIXFRAMEIMAGEHEADER_H_ADF4AA40_1A25_43f7_9ABF_B02BC5319F25_INCLUDED
#define _UIXFRAMEIMAGEHEADER_H_ADF4AA40_1A25_43f7_9ABF_B02BC5319F25_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 3:17:13am, GMT+4, Saint-Petersburg, Monday;
	This is Ebo Pack shared UIX Frame Library Image Header class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 8-Aug-2018 at 11:50:26a, UTC+7, Novosibirsk, Rodniki, Tulenina, Wednesday; 
*/
#include "shared.uix.gdi.provider.h"

namespace ex_ui { namespace frames {
	//
	// TODO: banner image management must be re-viewed;
	//
	class CImageBanner
	{
	private:
		class CImageBannerWnd :
			public  ::ATL::CWindowImpl<CImageBannerWnd>
		{
			typedef ::ATL::CWindowImpl<CImageBannerWnd> TWindow;
			friend class CImageBanner;
		private:
			Gdiplus::Bitmap* m_pImage;        // header image pointer
			HRESULT          m_hResult;       // initialisation result
			mutable SIZE     m_szHeader;      // header size
		public:
			CImageBannerWnd(const ATL::_U_STRINGorID RID);
			~CImageBannerWnd(void);
		public:
			BEGIN_MSG_MAP(CImageBannerWnd)
				MESSAGE_HANDLER(WM_CREATE    , OnCreate )
				MESSAGE_HANDLER(WM_DESTROY   , OnDestroy)
				MESSAGE_HANDLER(WM_ERASEBKGND, OnErase  )
				MESSAGE_HANDLER(WM_PAINT     , OnPaint  )
			END_MSG_MAP()
		private:
			LRESULT OnCreate (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnErase  (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnPaint  (UINT, WPARAM, LPARAM, BOOL&);
		};
	private:
		CImageBannerWnd     m_wnd;
	public: 
		CImageBanner(void);                      // for test purposes only
		CImageBanner(const ATL::_U_STRINGorID RID);
		~CImageBanner(void);
	public:
		HRESULT             Create (const HWND hParent, const UINT ctrlId = 0);
		HRESULT             Destroy(void);
		const SIZE&         GetSize(const bool bAnyState = false) const;
		HRESULT             Image  (const UINT u_res_id);
		bool                IsValid(void) const;
		bool                IsVisible(void) const;
		HRESULT             IsVisible(const bool);
		const CWindow&      Window (void) const; // gets banner window reference (read);
		      CWindow&      Window (void)      ; // gets banner window reference (read/write);
	private:
		CImageBanner(const CImageBanner&);
		CImageBanner& operator= (const CImageBanner&);
	};
}}

#endif/*_UIXFRAMEIMAGEHEADER_H_ADF4AA40_1A25_43f7_9ABF_B02BC5319F25_INCLUDED*/