#ifndef _UIXLISTVIEWEXGROUP_H_9579BD51_9995_467b_AB38_5B038B45D94D_INCLUDED
#define _UIXLISTVIEWEXGROUP_H_9579BD51_9995_467b_AB38_5B038B45D94D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Apr-2018 at 9:39:01p, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is UIX Control library extended standard list view control group interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 11-Aug-2018 at 5:50:23a, UTC+7, Novosibirsk, Rodniki, Tulenina, Saturday;
*/
#include "shared.gen.sys.err.h"

namespace ex_ui { namespace controls { namespace std {

	using shared::sys_core::CError;

	class CListViewGroup  {
	public:
		enum _e {
			eInvalidId    = 0x0,
			eDefaultMask  = LVGF_ALIGN | LVGF_GROUPID | LVGF_HEADER/* | LVGF_ITEMS | LVGF_STATE*/,
		};
	private:
		CAtlString      m_title;
		mutable
		CAtlString      m_title_fmt;
		UINT            m_items;      // expected item count;
		INT             m_n_id;       // object unique identifier;
	public:
		CListViewGroup(void);
		CListViewGroup(LPCTSTR lpszTitle, const UINT nItemCount = 0, const INT nId = CListViewGroup::eInvalidId);
	public:
		INT             Identifier(void)const;
		VOID            Identifier(const INT);
		bool            IsValid(void)const;
		UINT            Items(void)const;
		HRESULT         Items(const UINT);
		LPCTSTR         Title(void)const;
		HRESULT         Title(LPCTSTR);
		LPCTSTR         TitleAsString(void)const;
		LVGROUP         ToObject(void)const;      // header text pointer is set to NULL
	public:
		static INT      GenerateId(void);
	};

	typedef ::std::vector<CListViewGroup>       TListViewGroups;

	class CListViewGroups {
	public:
		enum _e {
			eInvalidIndex = -0x1,
		};
	private:
		::ATL::CWindow& m_lvw_wnd;
		bool            m_enabled;
		TListViewGroups m_groups ;
		CError          m_error  ;
	public:
		CListViewGroups(::ATL::CWindow& _lvw_wnd);
	public:
		HRESULT         Append(const CListViewGroup& _group);
		VOID            Clear(void)     ;
		INT             Count(void)const;
		bool            Enabled(void)const;
		HRESULT         Enabled(const bool bValue);
		TErrorRef       Error(void)const;
		INT             Find (const INT nId)const;    // finds an item by identifier specified, if not found, -1 is returned;
		bool            IsValid(void)const;
		const
		CListViewGroup& Item (const INT nIndex)const;
		CListViewGroup& Item (const INT nIndex);
	};
}}}

#endif/*_UIXLISTVIEWEXGROUP_H_9579BD51_9995_467b_AB38_5B038B45D94D_INCLUDED*/