#ifndef _UIXGDIDRAWSHAPE_H_895270FC_2B30_4EC7_96A2_490C2782EEE6_INCLUDED
#define _UIXGDIDRAWSHAPE_H_895270FC_2B30_4EC7_96A2_490C2782EEE6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jul-2018 at 3:21:50p, UTC+7, Phuket, Rawai, Monday;
	This is UIX Draw library generic shape interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 25-Jul-2018 at 8:16:45p, UTC+7, Novosibirsk, Tulenina, Wednesday;
*/
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw { namespace shape
{
	class CRectEdge
	{
	public:
		enum _e{
			eNone   = 0x0,
			eLeft   = 0x1,
			eTop    = 0x2,
			eRight  = 0x4,
			eBottom = 0x8,
			eAll    = eLeft | eTop | eRight | eBottom
		};

	};

	class CRectEx
	{
	private:
		Gdiplus::Rect  m_rc;
	public:
		CRectEx(const RECT& rc_);
		CRectEx(const INT left_, const INT top_, const INT width_, const INT height_);
		~CRectEx(void);
	public:
		operator Gdiplus::Rect*(void);
		operator Gdiplus::Rect&(void);
	};

	class CRectFEx
	{
	private:
		Gdiplus::RectF m_rc;
	public:
		CRectFEx(const RECT& rc_);
		CRectFEx(const INT left_, const INT top_, const INT width_, const INT height_);
		~CRectFEx(void);
	public:
		operator Gdiplus::RectF*();
		operator Gdiplus::RectF&();
	};
}}}

#endif/*_UIXGDIDRAWSHAPE_H_895270FC_2B30_4EC7_96A2_490C2782EEE6_INCLUDED*/