#ifndef _GENERICSTGLTCFG_H_27268310_F90D_42c2_AAED_E5B128819AA1_INCLUDED
#define _GENERICSTGLTCFG_H_27268310_F90D_42c2_AAED_E5B128819AA1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Apr-2016 at 11:07:24pm, GMT+7, Phuket, Rawai, Friday;
	This is shared generic storage library SQLite database settings interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 4-Jun-2018 at 9:05:00a, UTC+7, Phuket, Rawai, Monday;
	Adopted to v15a on 16-Aug-2018 at 6:14:27a, UTC+7, Novosibirsk, Rodniki, Tulenina, Thursday;
*/
#include "generic.stg.lt.def.h"

namespace shared { namespace stg { namespace lt
{
	using shared::sys_core::CError;

	class CDbSettings
	{
	protected:
		mutable
		CError            m_error;

	protected:
		CAtlString        m_file ;
		CAtlString        m_folder;
		bool              m_create;              // if true a folder must be created if it doesn't exist;

	public:
		CDbSettings(void);

	public:
		bool              IsFolder(void) const;  // gets create folder option (read);
		VOID              IsFolder(const bool);  // sets create folder option (write);
		TErrorRef         Error(void) const;     // gets last error object;
		CAtlString        FileName(void) const;  // gets database file name;
		HRESULT           FileName(LPCTSTR);     // sets database file name;
		CAtlString        FolderPath(const bool bUnwind = false) const; // gets folder path, if unwind is true there is no relative part(s);
		HRESULT           FolderPath(LPCTSTR);   // sets database folder path;
		CAtlString        FullPath(void)const;   // gets absolute path to a database including file name;
		HRESULT           Validate(void)const;   // validates locator object state;
	};

	class CDbSettingsPers : public CDbSettings {
	                       typedef CDbSettings TBase;
	protected:
		HKEY              m_root;        // registry root;
		CAtlString        m_hv_folder;   // registry root folder path;

	public:
		CDbSettingsPers(void);

	public:
		LPCTSTR           Key (void) const;
		HRESULT           Key (LPCTSTR)   ;
		const HKEY&       Root(void) const;
		      HKEY&       Root(void)      ;
	public:
		HRESULT           Load(void);
		HRESULT           Save(void);

	public:
		CDbSettingsPers&   operator= (const CDbSettings& _defaults);
	};
}}}

#endif/*_GENERICSTGLTCFG_H_27268310_F90D_42c2_AAED_E5B128819AA1_INCLUDED*/