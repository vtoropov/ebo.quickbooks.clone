#ifndef _GENERICSTGLTACC_H_2D6E9EBB_126C_4c32_8E26_70694FB1F3E5_INCLUDED
#define _GENERICSTGLTACC_H_2D6E9EBB_126C_4c32_8E26_70694FB1F3E5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Apr-2018 at 7:48:07a, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is Ebo Pack shared generic storage library SQLite database access interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 15-Aug-2018 at 5:24:18p, UTC+7, Novosibirsk, Rodniki, Tulenina, Wednesday;
*/
#include "generic.sql.mdl.h"
#include "generic.stg.lt.def.h"
#include "generic.stg.lt.cfg.h"
#include "generic.stg.lt.loc.h"

namespace shared { namespace stg { namespace lt
{
	using shared::sys_core::CError;
	using shared::sql::CDataSchema;

	class CDbPragma {
	public:
		enum _e {
			eNone            = 0x0,
			eJournalInMemory = 0x1,
			eLockingMode     = 0x2,
			ePageSize        = 0x3,
		};
	private:
		_e              m_id;
		variant_t       m_value;
	public:
		CDbPragma(void);
		CDbPragma(const CDbPragma::_e, const variant_t&);
	public:
		_e              Identifier(void)const;
		HRESULT         Identifier(const _e);
		bool            IsValid(void)const;
		CAtlString      Name(void)const;
		CAtlString      ToString(void)const;
		const
		variant_t&      Value(void)const;
		variant_t&      Value(void);
	public:
		CDbPragma&      operator=(const bool);
		CDbPragma&      operator=(const INT );
	};

	typedef ::std::map<CDbPragma::_e, CDbPragma> TPragmaSet;

	class CDbPragmaSet {
	private:
		TPragmaSet      m_set;
	public:
		CDbPragmaSet(void);
	public:
		HRESULT         Append(const CDbPragma&);
		HRESULT         Append(const CDbPragma::_e, const variant_t&);
		VOID            Clear (void);
		CAtlString      Get   (const CDbPragma::_e) const;
		bool            Has   (const CDbPragma::_e) const;
		HRESULT         Remove(const CDbPragma::_e);
		HRESULT         Set(const CDbPragma::_e, const bool);
		HRESULT         Set(const CDbPragma::_e, const INT);
		HRESULT         Set(const CDbPragma::_e, const variant_t&);
		CAtlString      ToString(void)const;
	};

	class CDbAccessor {
	private:
		mutable
		CError          m_error  ;
		CDbObject&      m_db_obj ;
		CDbLocator      m_locator;
		CDbPragmaSet    m_prg_set;
	public:
		CDbAccessor(CDbObject&);
		~CDbAccessor(void);
	public:
		HRESULT         Close(void);
		TErrorRef       Error(void)const;
		bool            IsOpen(void)const;
		const
		CDbLocator&     Locator(void)const;
		CDbLocator&     Locator(void);
		HRESULT         Open(const bool _no_schema); // opens database via locator settings, applies default data schema if necessary ;
		HRESULT         Open(const CDataSchema&)   ; // opens database via locator settings, applies data schema provided;
		const
		CDbPragmaSet&   Pragma(void)const;
		CDbPragmaSet&   Pragma(void);                // Pragma can be changed before opening a database file;
	};
}}}

#endif/*_GENERICSTGLTACC_H_2D6E9EBB_126C_4c32_8E26_70694FB1F3E5_INCLUDED*/