#ifndef _GENERICSTGSQLITEDEF_H_4C03BDD5_4C4C_4078_87A4_643570250E32_INCLUDED
#define _GENERICSTGSQLITEDEF_H_4C03BDD5_4C4C_4078_87A4_643570250E32_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Aug-2018 at 6:06:51p, UTC+7, Novosibirsk, Rodniki, Tulenina, Wednesday;
	This is Ebo Pack shared library generic storage SQLite database common interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to 15a on 30-Aug-2018 at 5:18:57p, UTC+7, Train #82U, Coatch #19, Place#41, Thursday;
*/
#include "shared.gen.sys.err.h"
#include "generic.sql.sch.h"

namespace shared { namespace stg { namespace lt {

	using shared::sys_core::CError;
	using shared::sql::CDataSchemata;

	class CDbObjectBase {
	protected:
		mutable
		CError        m_error;
	protected:
		CDbObjectBase(LPCTSTR lpszModule);
	public:
		TErrorRef     Error (void) const;
	};

	class CDbConnection : public CDbObjectBase {
	                     typedef CDbObjectBase TBase;
	private:
		volatile
		bool         m_auto_trn;   // indicates of using auto-transaction; by default is on;
		HANDLE       m_handle;

	public:
		 CDbConnection(void);
		~CDbConnection(void);

	public:
		bool         AutoTrn(void) const;   // retursn an auto-transaction mode;
		VOID         AutoTrn(const bool);   // sets auto-transaction mode; if false is set, that means external transaction is used;
		HRESULT      Create (void);
		HRESULT      Destroy(void);
		bool         IsOpen (void) const;
		bool         IsValid(void) const;

	public:
		operator HANDLE& (void);            // provides a connection handler reference for opening a database (read/write);
		operator const HANDLE(void) const;  // provides a connection handler for referencing in other database objects (read);
	};

	class CDbObject : public CDbObjectBase {
	                 typedef CDbObjectBase TBase;
	protected:
		CDataSchemata   m_schemata;
		CDbConnection   m_conn;

	public:
		CDbObject (const DWORD _opts = 0); // provides argument(s) for schema loading;
		~CDbObject(void);

	public:
		const
		CDbConnection&  Connection(void) const;  // gets a reference to connection object (read);
		CDbConnection&  Connection(void)      ;  // gets a reference to connection object (read/write);
		const
		CDataSchemata&  Schemata(void)const;     // gets a reference to schemata object (read);
		CDataSchemata&  Schemata(void);          // gets a reference to schemata object (read/write);
	public:
		operator
		const    CDbConnection&(void) const;
		operator CDbConnection&(void)      ;
	};

}}}

#endif/*_GENERICSTGSQLITEDEF_H_4C03BDD5_4C4C_4078_87A4_643570250E32_INCLUDED*/