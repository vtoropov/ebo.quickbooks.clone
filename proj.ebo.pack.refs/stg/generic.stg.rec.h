#ifndef _GENERICSTGREC_H_711DF957_EB2D_423F_B321_A6659A3C52C5_INCLUDED
#define _GENERICSTGREC_H_711DF957_EB2D_423F_B321_A6659A3C52C5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Nov-2018 at 11:16:41a, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack shared library generic data recordset interface declaration file.
*/
#include "shared.gen.sys.err.h"

namespace shared { namespace stg {

	using shared::sys_core::CError;

#if (0)
	class CRecBufferLen {
	public:
		enum _l : UINT {
			e08   = 0x08,    // 8th byte length;
			e16   = 0x0F,    // sixteen byte length;
			e32   = 0x20,    // 32-byte length;
			e64   = 0x40,    // 64-byte length;
			e96   = 0x60,    // 96-byte length;
			e128  = 0x80,    // 128-byte length;
		};
	public:
		TCHAR     m_small[_l::e08];
	};
#endif

	typedef ::std::vector<CAtlString>       TRecordAsText;
	typedef ::std::vector<_variant_t>       TRecordAsVars;

	typedef ::std::vector<TRecordAsText>    TRecordsetAsText;
	typedef ::std::vector<TRecordAsVars>    TRecordsetAsVars;

	class CRecord {
	protected:
		TRecordAsVars     m_data;

	public:
		CRecord(void);
		CRecord(const CRecord&);
		~CRecord(void);

	public:
		HRESULT   Append (const _variant_t&  );    // appends a variant data to a record;
		HRESULT   Append (const LONG   _value);    // appends a log value to a record;
		HRESULT   Append (LPCTSTR      _value);    // appends a string value to a record;
		HRESULT   Append (const time_t _value);    // appends a unix timestamp to a record;
		VOID      Clear  (void) ;                  // clears  a record;
		const TRecordAsVars&
		          Data   (void) const;             // gets a read-only reference to a raw data;
		bool      IsEmpty(void) const;             // gets an empty property value;
		size_t    Size   (void) const;             // gets a data values count;
		TRecordAsText
		          ToString(void) const;            // gets a record as string value vector;
		const _variant_t&
		          Value  (const size_t _ndx) const;
		UINT      ValueAsUINT(const size_t _ndx) const;
		const CAtlString
		          ValueAsText(const size_t _ndx) const;

	public:
		CRecord&   operator = (const CRecord&);
		CRecord&   operator+= (const _variant_t&);
		CRecord&   operator+= (const LONG);
		CRecord&   operator+= (LPCTSTR);
		CRecord&   operator+= (const time_t);

	public:
		operator const TRecordAsText  (void) const;
		operator const TRecordAsVars& (void) const;
	};

	typedef ::std::vector<CRecord>    TRecordset;

	class CRecordset {
	private:
		TRecordset   m_recs;

	public:
		CRecordset(void);
		~CRecordset(void);

	public:
		HRESULT   Append (const CRecord&);
		HRESULT   Clear  (void)      ;
		size_t    Count  (void) const;
		const
		CRecord&  Row (const size_t _ndx) const;
		CRecord&  Row (const size_t _ndx)      ;
		const
		TRecordset& Set(void) const;
		TRecordset& Set(void)      ;

	public:
		CRecordset& operator+= (const CRecord&);
		operator const TRecordset&(void) const ;
		operator const TRecordsetAsText(void) const;
		operator const TRecordsetAsVars(void) const;
		const
		CRecord& operator[] (const size_t& _ndx) const;
		CRecord& operator[] (const size_t& _ndx)      ;
	};

	class CRecordAdapter {
	public:
		CError        m_error;
	public:
		CRecordAdapter(void);
		~CRecordAdapter(void);
	public:
		TErrorRef     Error (void) const;
	public:
		TRecordsetAsText operator<<(const TRecordsetAsVars&);
	public:
		static HRESULT   VarsToText(const TRecordAsVars&    _in, TRecordAsText&    _out); // TODO: needs more variant data types to look through;
		static HRESULT   VarsToText(const TRecordsetAsVars& _in, TRecordsetAsText& _out);
	};
}}

#endif/*_GENERICSTGREC_H_711DF957_EB2D_423F_B321_A6659A3C52C5_INCLUDED*/