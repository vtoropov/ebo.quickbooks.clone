#ifndef _GENERICSTGLTLOC_H_04077567_42F0_476C_A52E_309C86DB46FC_INCLUDED
#define _GENERICSTGLTLOC_H_04077567_42F0_476C_A52E_309C86DB46FC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Oct-2018 at 6:52:42p, UTC+7, Novosibirsk, Rodniki, Tulenina, Tuesday;
	This is Ebo Pack shared library generic storage SQLite database locatgor interface declaration file.
*/
#include "shared.gen.sys.err.h"

namespace shared { namespace stg { namespace lt {

	using shared::sys_core::CError;

	class CDbLocator {
	protected:
		mutable
		CError         m_error;
		CAtlString     m_path;
	public:
		CDbLocator(void);
		CDbLocator(LPCTSTR lpszPath);
	public:
		TErrorRef      Error  (void) const;
		bool           IsValid(void) const;
		HRESULT        Parse  (LPCTSTR lpszPath);                     // checks database file path for existing and creates a folder(s) if necessary; it uses registry settings;
		HRESULT        Parse  (LPCTSTR lpszFolder, LPCTSTR lpszFile); // checks database folder and file name; it creates a folder if necessary;
		LPCTSTR        Path   (void) const;
	public:
		operator LPCTSTR(void)const;
	public:
		CDbLocator& operator= (LPCTSTR lpszPath);
	};

}}}

#endif/*_GENERICSTGLTLOC_H_04077567_42F0_476C_A52E_309C86DB46FC_INCLUDED*/