#ifndef _GENERICSTGLTPUT_H_3F07F10F_4C4C_491c_BEE5_D92405991826_INCLUDED
#define _GENERICSTGLTPUT_H_3F07F10F_4C4C_491c_BEE5_D92405991826_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Apr-2016 at 11:03:05pm, UTC+7, Phuket, Rawai, Friday;
	This is Ebo Pack shared library generic storage SQLite database writer declaration declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-Aug-2018 at 10:44:09a, UTC+6, Omsk, 5th District, Monday;
	Adopted to v15a on 27-Oct-2018 at 3:07:17p, UTC+7, Novosibirsk, Rodniki, Tulenina, Saturday;
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.sys.tpl.h"

#include "generic.sql.mdl.h"
#include "generic.sql.qry.h"
#include "qry\generic.sql.qry.ins.h"
#include "generic.stg.lt.def.h"

namespace shared { namespace stg { namespace lt {

	using shared::sys_core::CError;
	using shared::sys_core::CThreadBase;
	using shared::sql::CDbQuery;
	using shared::sql::CDbQueryInsert;

	class CDbWriter : public CDbObjectBase {
	                 typedef CDbObjectBase TBase;
	public:
		enum _r : LONG {
			eUndefined = -1,
		};
	private:
		const
		CDbObject&   m_dbo;
		LONG         m_affected;

	public:
		CDbWriter(const CDbObject&);
		~CDbWriter(void);

	public:
		LONG       Affected(void) const;
		HRESULT    Create(const CDbQuery&);   // creates a view object by SQL expression provided;
		HRESULT    Delete(const CDbQuery&);   // deletes records specified by query provided;
		HRESULT    Insert(const CDbQuery&);   // inserts one or more record(s) by base query provided;
	};
}}}

#endif/*_GENERICSTGLTPUT_H_3F07F10F_4C4C_491c_BEE5_D92405991826_INCLUDED*/