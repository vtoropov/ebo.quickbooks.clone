#ifndef _GENERICSTGSQLLTREADER_H_3E9BA3A4_1085_4e8c_9634_0B8C6C272C92_INCLUDED
#define _GENERICSTGSQLLTREADER_H_3E9BA3A4_1085_4e8c_9634_0B8C6C272C92_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Apr-2018 at 7:40:29a, UTC+7, Novosibirsk, Rodniki, Thursday;
	This is Ebo Pack shared library generic storage SQLite reader interface declaration file.
	-----
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.sys.tpl.h"

#include "generic.sql.mdl.h"
#include "generic.sql.qry.h"
#include "generic.stg.rec.h"
#include "generic.stg.lt.def.h"

namespace shared { namespace stg { namespace lt
{
	using shared::sys_core::CError;

	using shared::sys_core::CThreadBase;
	using shared::sql::CDbQuery;
	
	class CDbReader : public CDbObjectBase {
	                 typedef CDbObjectBase TBase;
	public:
		enum _e {
			e_na = -1,
		};
	private:
		const CDbObject& m_db_obj;

	public:
		CDbReader(const CDbObject&);

	public:
		HRESULT       Recordset(const CDbQuery& , CRecordset&) const;       // gets records from generic query; TODO: a first table is used as a source;
		HRESULT       Recordset(const CDbQuery& , TRecordsetAsVars&) const; // gets records from generic query; TODO: a first table is used as a source;
		/*
		HRESULT       Recordset(const CDbQueryAgg&  , UINT& _result) const;     // TODO: query _result must be re-viewed;
		HRESULT       Recordset(const CDbQueryRead& , TRecordsetAsText&) const;
		HRESULT       Recordset(const CDbQueryView& , TRecordsetAsVars&) const;
		*/
	};
}}}

#endif /*_GENERICSTGSQLLTREADER_H_3E9BA3A4_1085_4e8c_9634_0B8C6C272C92_INCLUDED*/