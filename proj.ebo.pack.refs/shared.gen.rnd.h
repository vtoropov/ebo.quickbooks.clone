#ifndef _SHAREDRNDGEN_H_671855FD_3F58_4E42_B445_A1B6A23BA1D2_INCLUDED
#define _SHAREDRNDGEN_H_671855FD_3F58_4E42_B445_A1B6A23BA1D2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Apr-2017 at 02:46:52a, UTC+7, Phuket, Rawai, Sunday;
	This is Shared Lite library pseudo-random number/sequence generator class declaration file.
	-----------------------------------------------------------------------------
	Adopted to sound-bin-trans project on 26-Apr-2019 at 5:42:16p, UTC+7, Phuket, Rawai, Friday;
*/

namespace shared { namespace common
{
	class CRndGen
	{
	public:
		CRndGen(void); // initializes a rendom generator;
	public:
		CAtlString  UniqueSeq (const DWORD  _length , const bool bCaseSensitive = false)const;
		CAtlString  UniqueSeq_Ex(const DWORD _length, const bool bLowCase = true)const;

	public:
		static CAtlString  GuidAsText(const bool _b_simple , const bool bCaseSensitive = false);
		static CAtlString  GuidAsText(const GUID& _guid_ref, const bool _b_simple , const bool bCaseSensitive);
	};
}}

#endif/*_SHAREDRNDGEN_H_671855FD_3F58_4E42_B445_A1B6A23BA1D2_INCLUDED*/