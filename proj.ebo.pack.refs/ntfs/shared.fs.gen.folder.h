#ifndef _SHAREDFILESYSTEM_H_42494520_2884_4ba8_9BBD_450E6620FC22_INCLUDED
#define _SHAREDFILESYSTEM_H_42494520_2884_4ba8_9BBD_450E6620FC22_INCLUDED
/*
	Created by Tech_dog (VToropov) on 11-Jun-2016 at 1:11:46p, GMT+7, Phuket, Rawai, Saturday;
	This is shared NTFS library generic folder class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 9-Jul-2018 at 6:02:31p, UTC+7, Phuket, Rawai, Monday;
*/
#include "shared.fs.defs.h"

namespace shared { namespace ntfs
{
	using shared::sys_core::CError;

	class CGenericFolder : public CBaseObject
	{
		typedef CBaseObject TObject;
	public:
		CGenericFolder(LPCTSTR lpszPath);
		~CGenericFolder(void);
	public:
		HRESULT           Create  (VOID);
		WIN32_FIND_DATA   FindLastModified(VOID)const;
		HRESULT           EnumerateItems(TFileList& _files, TFolderList& _subfolders, LPCTSTR lpszMask = _T("*")); // enumerates items in specific folder only, no recursion
		HRESULT           EnumerateItems(TFolderItemCollection& _items)const;
		bool              SelectObject(LPCTSTR lpszFileName)const;
	};

	class CSpecialFolder
	{
	private:
		CError            m_error;
	public:
		CSpecialFolder(void);
	public:
		TErrorRef         Error(void)const;
		CAtlString        PersonalDocs  (void); // gets user's personal document folder path;
		CAtlString        ProgramFiles  (void); // the program files folder;
		CAtlString        ProgramFiles86(void); // the program files folder for 32-bit programs on 64-bit platform;
		CAtlString        PublicUserData(void); // the file system directory that contains application data for all users;
		CAtlString        UserTemporary (void); // current user temporary folder;
		CAtlString        Windows       (void); // get MS Windows installation folder;
	};

	class CCurrentFolder : public CBaseObject
	{
		typedef CBaseObject TObject;
	public:
		CCurrentFolder(void);
	public:
		CAtlString        GetPath(void)const;
		CAtlString        GetPathFromPattern(LPCTSTR lpszPattern)const;
		bool              IsRelative(LPCTSTR lpszPath)const;
	};
}}

#endif/*_SHAREDFILESYSTEM_H_42494520_2884_4ba8_9BBD_450E6620FC22_INCLUDED*/