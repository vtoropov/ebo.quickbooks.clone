#ifndef _SHAREDGENERICAPPRESOURCE_H_F3703DAD_884A_467b_BB97_A47EB3A50C8A_INCLUDED
#define _SHAREDGENERICAPPRESOURCE_H_F3703DAD_884A_467b_BB97_A47EB3A50C8A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Jun-2016 at 4:57:06p, GMT+7, Phuket, Rawai, Friday;
	This is Shared User32 Wrapper Generic Application Resource class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 5:24:53p, UTC+7, Phuket, Rawai, Monday;
*/
#include <Shellapi.h>
#include "shared.gen.sys.err.h"
#include "shared.gen.raw.buf.h"

namespace shared { namespace user32
{
	class CApplicationIconLoader
	{
	private:
		HICON   m_small;
		HICON   m_large;
	public:
		CApplicationIconLoader(LPCTSTR pszIconFile, const INT nIconIndex);
		CApplicationIconLoader(const UINT nIconId); // it is assumed that resource identifier points to multiframe icon resource
		CApplicationIconLoader(const UINT nSmallIconId, const UINT nLargeIconId);
		~CApplicationIconLoader(void);
	public:
		HICON   DetachLargeIcon(void);
		HICON   DetachSmallIcon(void);
		HICON   GetLargeIcon(void) const;
		HICON   GetSmallIcon(void) const;
	private:
		CApplicationIconLoader(const CApplicationIconLoader&);
		CApplicationIconLoader& operator= (const CApplicationIconLoader&);
	};

	class CApplicationCursor
	{
	private:
		bool    m_resource_owner;
	public:
		CApplicationCursor(LPCTSTR lpstrCursor = IDC_WAIT);
		~CApplicationCursor(void);
	};

	using shared::sys_core::CError;
	using shared::common::CRawData;

	class CGenericResourceLoader
	{
	private:
		CError    m_error;
	public:
		CGenericResourceLoader(void);
	public:
		TErrorRef Error(void)const;
		HRESULT   LoadRcDataAsUtf8(const UINT uResId, CAtlString& _buffer); // loads data as UTF8 character buffer and copies data to string object
		HRESULT   LoadRcDataAsUtf8(const UINT uResId, CRawData& _buffer);   // loads data as UTF8 character buffer
	};
}}

#endif/*_SHAREDGENERICAPPRESOURCE_H_F3703DAD_884A_467b_BB97_A47EB3A50C8A_INCLUDED*/