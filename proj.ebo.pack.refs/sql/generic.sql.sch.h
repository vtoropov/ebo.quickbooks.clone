#ifndef _GENERICSQLSCH_H_8E86CBC9_AEC7_4030_9493_D922222AACDA_INCLUDED
#define _GENERICSQLSCH_H_8E86CBC9_AEC7_4030_9493_D922222AACDA_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Oct-2018 at 7:15:01p, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
	This is Ebo Pack shared library generic SQL database schema related interface(s) declaration file. 
*/
#include "generic.sql.mdl.h"

namespace shared { namespace sql {

	class CDataStream {
	public:
		enum _type {
			ePlainText  = 0x0,
			eXml        = 0x1,
		};
	};

	class CDataSchema : public CDataBase {
	                   typedef CDataBase TBase;
	protected:
		mutable
		CError          m_error ;  // last error object;
		CAtlString      m_source;  // plain text string of data schema source;
		CDataTableEnum  m_tables;

	public:
		CDataSchema(void);
		CDataSchema(LPCTSTR lpszName);
		~CDataSchema(void);

	public:
		virtual bool    IsValid(void) const override sealed;

	public:
		VOID            Clear (void);
		HRESULT         Create(const UINT _u_res_id , const CDataStream::_type);
		HRESULT         Create(LPCTSTR _lp_sz_stream, const CDataStream::_type);
		TErrorRef       Error (void) const;
		const
		CDataTableEnum& Tables  (void)const;
		LPCTSTR         ToString(void)const;
	public:
		CDataSchema&  operator= (const CDataSchema&);
		CDataSchema&  operator<<(LPCTSTR);              // creates a schema from pattern provided; xml data is acceptable;
	public:
		operator const CDataTableEnum&(void) const;
		operator       LPCTSTR  (void) const;           // not implemented yet;
	};

	typedef ::std::vector<CDataSchema> TDataSchemata;

	class CDataSchemata {
	private:
		CError         m_error;
		TDataSchemata  m_schemas;
		CDataSchema    m_default;

	public:
		CDataSchemata(const UINT nResId);
		~CDataSchemata(void);

	public:
		const
		TDataSchemata& All(void)const;                   // no thread safity; no default schema is included;
		HRESULT        Append(const CDataSchema&);       // no finding schema by name in schemata set; 
		const
		CDataSchema&   Default(void)const;
		CDataSchema&   Default(void)     ;
		TErrorRef      Error  (void)const;
		const
		CDataSchema&   SchemaOf(const INT nIndex)const;
		CDataSchema&   SchemaOf(const INT nIndex)     ;
	};

	class CDataSchemaOpt {
	public:
		enum _e {
			eDoNotCare   = 0x0,  // requires later schema loading from a resource by providing specific identifier;
			eLoadDefault = 0x1,  // TODO: it is assumed that schema resource has a value 1; must be reviewed;
		};
	};

}}

#endif/*_GENERICSQLSCH_H_8E86CBC9_AEC7_4030_9493_D922222AACDA_INCLUDED*/