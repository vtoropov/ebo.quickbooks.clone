#ifndef _GENERICSQLQRYDAT_H_2994E9D9_AA54_42F2_9074_8930B1D46FE5_INCLUDED
#define _GENERICSQLQRYDAT_H_2994E9D9_AA54_42F2_9074_8930B1D46FE5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Oct-2018 at 6:35:38a, UTC+7, Novosibirsk, Rodniki, Tulenina, Wednesday;
	This is Ebo Pack shared library generic SQL query common interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "generic.sql.mdl.h"

namespace shared { namespace sql
{
	using shared::sys_core::CError;

	typedef ::std::vector<_variant_t>   TDbQueryData;

	class CDbQueryData {
	private:
		TDbQueryData     m_data;

	public:
		CDbQueryData(void);
		~CDbQueryData(void);

	public:
		HRESULT     Append (const _variant_t&  );    // appends a variant data to a record;
		HRESULT     Append (const LONG   _value);    // appends a log value to a record;
		HRESULT     Append (LPCTSTR      _value);    // appends a string value to a record;
		HRESULT     Append (const time_t _value);    // appends a unix timestamp to a record;
		VOID        Clear  (void) ;                  // clears  a record;
		bool        IsEmpty(void) const;             // gets an empty property value;
		const
		TDbQueryData&  Data(void) const;             // gets a read-obly reference to a raw data;
		size_t      Size   (void) const;             // gets a data values count;
		HRESULT     ToList (CAtlString& _lst) const; // fills value list for a query;

	public:
		CDbQueryData&   operator = (const CDbQueryData&);
		CDbQueryData&   operator = (const TDbQueryData&);
		CDbQueryData&   operator+= (const _variant_t&);
		CDbQueryData&   operator+= (const LONG);
		CDbQueryData&   operator+= (LPCTSTR);
		CDbQueryData&   operator+= (const time_t);
		operator const  TDbQueryData&(void) const ;

	public:
		static HRESULT   IsTyped(const _variant_t&); // checks for supported variant type;
	};

	typedef ::std::vector<CDbQueryData> TDbQuerySet;

	class CDbQuerySet {
	private:
		TDbQuerySet   m_set;
		CError        m_error;

	public:
		CDbQuerySet(void);
		~CDbQuerySet(void);

	public:
		HRESULT       Append (const CDbQueryData&);
		HRESULT       Append (const TDbQueryData&);
		HRESULT       Clear  (void) ;
		size_t        Count  (void) const;
		TErrorRef     Error  (void) const;
		const bool    IsEmpty(void) const;
		const
		CDbQueryData& Data(const size_t _t_ndx) const;

	public:
		CDbQuerySet&   operator = (const CDbQueryData&);
		CDbQuerySet&   operator+= (const CDbQueryData&);
		CDbQuerySet&   operator = (const TDbQueryData&);
		CDbQuerySet&   operator+= (const TDbQueryData&);
		operator const TDbQuerySet& (void) const;
		const
		CDbQueryData&  operator[] (const size_t _t_ndx ) const;
	};

	class CDbParam {
	private:
		CDataType::_e m_type;
		CAtlString    m_fld_name;   //  data field name as param name; an exception can be made for cases when providing arg(s) to stored procedure;
		_variant_t    m_var_val ;   //  parameter value; 
		mutable
		CAtlString    m_val_comp;   //  sign symbol for comparison to value: "="(default), ">=", "<", etc;
	public:
		CDbParam(void);
		CDbParam(LPCTSTR _lp_sz_name);
		~CDbParam(void);
	public:
		LPCTSTR       Compare(void) const;
		VOID          Compare(LPCTSTR)   ;
		bool          IsIdentifiable(LPCTSTR _lpsz_name) const; // checks a param name against value provided;
		bool          IsValid(void)const;                       // returns true if param name is not empty and variant value is valid;
		LPCTSTR       Name(void)const;                          // gets param name;
		HRESULT       Name(LPCTSTR);                            // sets param name;
		CDataType::_e Type(void)const;                          // gets param type;
		VOID          Type(const CDataType::_e);                // sets param type; this mehtod possibly changes a variant value type;
		LONG          ValueAsLong(void)const;                   // returns variant value as long; possibly tries to convert variant to appropriate type;
		VOID          ValueAsLong(const LONG);                  // sets variant value to long; possibly converts variant type;
		CAtlString    ValueAsText(void)const;                   // returns variant value as text; possibly tries to convert variant to appropriate type;
		VOID          ValueAsText(LPCTSTR);                     // sets variant value to text; possibly converts variant type;
		time_t        ValueAsTime(void)const;                   // returns variant value as unix time; possibly tries to convert variant to appropriate type;
		VOID          ValueAsTime(const time_t);                // sets variant value to unix time; possibly converts variant type;
	public:
		CAtlString    ToString(void) const;                     // represents parameter as text of pattern: 'name{compare sign}value';
	public:
		CDbParam&     operator=(const LONG);
		CDbParam&     operator=(const LPCTSTR);
		CDbParam&     operator=(const time_t);
	};

	typedef ::std::vector<CDbParam> TDbQueryParams;

	class CDbParamEnum {
	private:
		mutable
		CError           m_error;
		TDbQueryParams   m_params;
	public:
		CDbParamEnum(void);
		~CDbParamEnum(void);
	public:
		HRESULT   Append (const CDbParam&)  ;
		HRESULT   Append (LPCTSTR _lpsz_name, const LONG _value);
		HRESULT   Append (LPCTSTR _lpsz_name, LPCTSTR _value);
		HRESULT   Append (LPCTSTR _lpsz_name, const time_t _value);
		VOID      Clear  (void)      ;                  // clears parameters;
		TErrorRef Error  (void) const;
		INT       Find   (LPCTSTR _lpsz_name) const;    // finds a parameter by name specified; returns -1 if a param is not found;
		const bool       IsEmpty (void)const;
		const bool       IsValid (void)const;
		const CDbParam&  Item    (const size_t _ndx) const;
		      CDbParam&  Item    (const size_t _ndx)      ;
		const
		TDbQueryParams&  RawData (void)const;
		size_t           Size    (void)const;
		CAtlString       ToString(void)const;           // TODO: this method assumes 'AND' concatination, must be reviewed or deprecated;
	};
}}

#endif/*_GENERICSQLQRYDAT_H_2994E9D9_AA54_42F2_9074_8930B1D46FE5_INCLUDED*/