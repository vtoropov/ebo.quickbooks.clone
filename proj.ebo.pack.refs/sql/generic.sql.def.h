#ifndef _GENERICSTGSQLDEF_H_84A93F23_784A_40EE_BD5B_A59868190FD8_INCLUDED
#define _GENERICSTGSQLDEF_H_84A93F23_784A_40EE_BD5B_A59868190FD8_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Sep-2018 at 12:11:13p, UTC+7, Novosibirsk, Rodniki, Tulenina, Saturday;
	This is Ebo Pack shared library generic SQL common definition interface declaration file;
*/
#include "shared.gen.sys.err.h"

namespace shared { namespace sql {

	using shared::sys_core::CError;
	//
	// TODO: Subsequent select query record amout limit class; perheps, must be moved to separated header file (*done*);
	//
	class CRecordLimit {
	public:
		enum _r : LONG {
			eNa = -1,      // not acceptable value for offset;
		};
	private:
		LONG          m_offset;
		LONG          m_limit;

	public:
		CRecordLimit(void);
		CRecordLimit(const LONG _off, const LONG _lmt);
		CRecordLimit(const CRecordLimit&);
	public:
		void          Clear  (void)      ;
		bool          IsValid(void) const;       // a limit is acceptible when CDataBase::eNa < m_offset and 0 < m_limit; 
		LONG          Limit  (void) const;
		HRESULT       Limit  (const LONG);
		LONG          Offset (void) const;
		HRESULT       Offset (const LONG);
	public:
		CRecordLimit& operator=(const CRecordLimit&);
	};

	class CDataBase {
	public:
		enum _r : LONG {
			eNotFound = -1,
			eNa       = -1, // not available or not applicable;
		};
	protected:
		CAtlString     m_name;

	protected:
		 CDataBase(LPCTSTR lpszName = NULL);
		~CDataBase(void);

	public:
		virtual bool   IsIdentifiable(LPCTSTR lpszName)const;
		virtual bool   IsValid(void)const;

	public:
		LPCTSTR        Name(void)const;
		HRESULT        Name(LPCTSTR);

	public:
		static bool    Identify(LPCTSTR lpszWhat, LPCTSTR lpszWith);
		static bool    Invalidate(LPCTSTR);
		static bool    Validate(LPCTSTR);

	public:
		operator LPCTSTR(void)const;
	};

}}

#endif/*_GENERICSTGSQLDEF_H_84A93F23_784A_40EE_BD5B_A59868190FD8_INCLUDED*/