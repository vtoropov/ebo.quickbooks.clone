#ifndef _GENERICSQLQRY_H_B8A6C03E_6865_47BC_B326_B7ECD2BF7E70_INCLUDED 
#define _GENERICSQLQRY_H_B8A6C03E_6865_47BC_B326_B7ECD2BF7E70_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jun-2018 at 0:35:13a, UTC+7, Phuket, Rawai, Monday;
	This is Ebo Pack shared library generic SQL query interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15a on 26-Oct-2018 at 8:31:50a, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
*/
#include "shared.gen.sys.err.h"
#include "generic.sql.mdl.h"
#include "qry\generic.sql.qry.dat.h"

namespace shared { namespace sql
{
	using shared::sys_core::CError;

	class CDbQuery {
	protected:
		mutable
		CError           m_error;
		CDbParamEnum     m_params;  // these parameters are for where clause only; a query will include a parameter as 'name=value' pair;
		CDataTableEnum   m_tables;  // source/target table(s);
		CRecordLimit     m_limit;
		CAtlString       m_query;   // a query buffered instruction; can be set by a caller directly;

	public:
		 CDbQuery(void);
		 CDbQuery(const UINT _u_res_id);   // initializes a query object from loading resource string;
		 CDbQuery(LPCTSTR _lp_sz_p_qry);   // initializes a query object by providing plain text query;
		~CDbQuery(void);
	public:
		virtual const bool       IsValid (void) const;
		virtual CAtlString       ToString(void) const;
	public:
		TErrorRef        Error  (void)const;
		LPCTSTR          Get    (void)const;    // gets original query text;
		const
		CRecordLimit&    Limit  (void)const;    // query limit of record amount; invalid state is default;
		CRecordLimit&    Limit  (void)     ;
		const
		CDbParamEnum&    Params (void)const;
		CDbParamEnum&    Params (void);
		HRESULT          Raw_Xml(const UINT _u_res_id); // sets raw XML from executable resource by an identifier provided;
		HRESULT          Raw_Xml(LPCTSTR)  ;    // sets a raw XML as a text; it requires an extaction <sql> node for query expression;
		HRESULT          Set    (const UINT _u_res_id); // sets a query from executable resource by an identifier provided;
		HRESULT          Set    (LPCTSTR)  ;    // sets a query instruction; it can contain placeholder pattern(s) for applying parameters provided;
		const
		CDataTableEnum&  Tables (void)const;
		CDataTableEnum&  Tables (void)     ;
	public:
		static bool      IsParamRequired(LPCTSTR _qry, CError&);
		static
		CAtlString       ParamsToQry(const CDbParamEnum&, LPCTSTR _qry, CError&);
	public:
		CDbQuery& operator= (const UINT);
		CDbQuery& operator= (LPCTSTR);
	};
}}

#endif/*_GENERICSQLQRY_H_B8A6C03E_6865_47BC_B326_B7ECD2BF7E70_INCLUDED*/