#ifndef _GENERICSTGSQLMDL_H_EC63EDC3_91A8_4162_863B_593582616188_INCLUDED
#define _GENERICSTGSQLMDL_H_EC63EDC3_91A8_4162_863B_593582616188_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Apr-2018 at 5:48:34p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack shared library generic SQL structure/model interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 15-Aug-2018 at 12:17:35p, UTC+7, Novosibirsk, Rodniki, Tulenina, Wednesday;
*/
#include "generic.sql.def.h"

namespace shared { namespace sql {

	using shared::sys_core::CError;

	class CDataType {
	public:
		enum _e : ULONG {
			eNone    = 0x0, // default value;
			eReal    = 0x1, // http://jakegoulding.com/blog/2011/02/06/sqlite-64-bit-integers/
			eText    = 0x2,
			eInteger = 0x3,
			eDate    = 0x4, // supports unix timestamp and is stored as OLE DATE data type;
		};
	protected:
		_e     m_value;
	public:
		 CDataType (const _e = _e::eNone);
		~CDataType (void);
	public:
		const _e&  Ref   (void) const;  // gets a reference to current type value (ra);
		      _e&  Ref   (void)      ;  // gets a reference to current type value (rw);
		CAtlString ToText(void) const;
	public:
		CDataType& operator= (const _e);
		CDataType& operator= (LPCTSTR );
	public:
		operator _e         (void) const;
		operator CAtlString (void) const;
	public:
		static _e         TextToType(LPCTSTR );
		static CAtlString TypeToText(const _e);
	};

	class CDataField : public CDataBase {
	                  typedef CDataBase TBase;
	private:
		CDataType     m_type;

	public:
		 CDataField(void);
		 CDataField(const CDataType::_e, LPCTSTR lpszName);
		~CDataField(void);

	public:
		bool           IsValid(void)const override sealed; // TODO: must be reviewed;
	public:
		bool           Seq  (LPCTSTR)   ;                  // sets data field from the text sequence: {name:type}
		const
		CDataType&     Type (void) const;
		CDataType&     Type (void)      ;
		HRESULT        Type (const CDataType::_e);

	public:
		CDataField&    operator=  (LPCTSTR _lp_sz_seq );   // sets data field from the text sequence: {name:type}
		CDataField&    operator<< (const CDataType::_e);   // sets field data type;
		CDataField&    operator<< (LPCTSTR _lp_sz_name);   // sets field name;
	};

	typedef ::std::vector<CDataField>  TDataFields;

	class CDataFieldEnum {
	private:
		mutable
		CError         m_error ;
		TDataFields    m_fields;   

	public:
		 CDataFieldEnum(void);
		~CDataFieldEnum(void);

	public:
		HRESULT        Append  (const CDataField&);
		HRESULT        Append  (const CDataType::_e, LPCTSTR lpszName);
		VOID           Clear   (void);
		INT            Count   (void) const;
		const
		TDataFields&   Fields  (void) const;
		TDataFields&   Fields  (void)      ;
		INT            Find    (LPCTSTR lpszName)const; // returns -1 if not found or an error occcurs;
		bool           IsEmpty (void) const;
		CDataField     Item    (const INT _ndx)const;
		const
		TDataFields&   Ref     (void) const;
		TDataFields&   Ref     (void)      ;
		bool           Remove  (const CDataField&  );
		bool           Remove  (const INT  _n_ndx  );
		bool           Remove  (LPCTSTR _lp_sz_name);
		CAtlString     ToList  (LPCTSTR _lp_sz_sep = _T(",")) const;

	public:
		TErrorRef      Error   (void) const;

	public:                                              // sample of string sequence: {field1:integer};...{field2:text}
		CDataFieldEnum& operator<< (LPCTSTR _lp_sz_seq); // parses string as "{name:type};...{name:type}" and appends fields;
		CDataFieldEnum& operator+= (const CDataField& );
		CDataFieldEnum& operator-= (const CDataField& );
		CDataFieldEnum& operator-= (const INT _n_ndx  );
	};

	class CDataIndex : public CDataBase {
	                  typedef CDataBase TBase;
	private:
		CAtlString     m_field;
		CAtlString     m_table;

	public:
		 CDataIndex(void);
		 CDataIndex(LPCTSTR lpszName, LPCTSTR lpszField, LPCTSTR lpszTable);
		~CDataIndex(void);

	public:
		bool           IsIdentifiable(LPCTSTR lpszField)const override sealed;
		bool           IsValid(void)const override sealed;

	public:
		LPCTSTR        Field(void)const;
		HRESULT        Field(LPCTSTR);
		LPCTSTR        Table(void)const;
		HRESULT        Table(LPCTSTR);
	};

	typedef ::std::vector<CDataIndex> TDataIndices;

	class CDataIndexEnum {
	private:
		TDataIndices   m_indices;

	public:
		 CDataIndexEnum(void);
		~CDataIndexEnum(void);

	public:
		HRESULT        Append (const CDataIndex&);      // TODO: is it possible to have more than one index per field?
		HRESULT        Append (LPCTSTR lpszName, LPCTSTR lpszField, LPCTSTR lpszTable);
		VOID           Clear  (void)      ;
		INT            Count  (void) const;
		INT            Find   (LPCTSTR lpszField)const; // returns -1 if not found or an error occcurs;
		bool           IsEmpty(void) const;
		const
		CDataIndex     Item   (const INT _ndx)const;
		CDataIndex     Item   (const INT _ndx);
		const
		TDataIndices&  Ref    (void) const;
		TDataIndices&  Ref    (void)      ;
	};

	class CDataTable : public CDataBase {
	                  typedef CDataBase TBase;
	private:
		CDataFieldEnum    m_fields ;
		CDataIndexEnum    m_indices;

	public:
		 CDataTable(LPCTSTR lpszName = NULL);
		~CDataTable(void);

	public:
		bool              IsValid(void)const override sealed;

	public:
		VOID   Clear(void); // clears internal data; it is necessary if a table is initialized twice or more;
		const
		CDataFieldEnum&   Fields(void)const;
		CDataFieldEnum&   Fields(void);
		const
		CDataIndexEnum&   Indices(void)const;
		CDataIndexEnum&   Indices(void);

	public:
		CDataTable& operator<< (LPCTSTR _lp_sz_nm);  // sets a table name;
	};

	typedef ::std::vector<CDataTable> TDataTableEnum;

	class CDataTableEnum {
	private:
		CError           m_error;
		TDataTableEnum   m_tables;

	public:
		 CDataTableEnum(void);
		~CDataTableEnum(void);

	public:
		HRESULT         Append(const CDataTable&);
		VOID            Clear (void);
		INT             Count (void) const;
		TErrorRef       Error (void) const;
		INT             Find  (LPCTSTR lpszTableName)const; // returns -1, if specified table is not found;
		const
		TDataTableEnum& Raw   (void) const;
		TDataTableEnum& Raw   (void)      ;
		HRESULT         Remove(LPCTSTR lpszTable);
		const
		CDataTable&     Table (const INT _index)const;
		CDataTable&     Table (const INT _index)     ;

	public:
		CDataTableEnum& operator+= (const CDataTable&);
	};

	class CDataJoinType {
	public:
		enum _e {
			eInner  = 0x0,
			eLeft   = 0x1,
			eRight  = 0x2,
		};
	public:
		static _e         StringToType(LPCTSTR);
		static CAtlString TypeToString(const _e);
	};

	class CDataJoin {
	private:
		CDataJoinType::_e
			m_type;
		CAtlString   m_target;
		CAtlString   m_pair;

	public:
		 CDataJoin(void);
		~CDataJoin(void);

	public:
		bool        IsValid(void) const;
		LPCTSTR     Pair   (void) const;
		HRESULT     Pair   (LPCTSTR)   ;
		LPCTSTR     Target (void) const;
		HRESULT     Target (LPCTSTR)   ;
		CDataJoinType::_e  Type(void) const;
		VOID        Type   (const CDataJoinType::_e);
	public:
		CAtlString  ToString(void) const;         // returns a string, which represents a join;
	};

	typedef ::std::vector<CDataJoin>  TDataJoins;

	class CDataJoinEnum {
	private:
		CError         m_error ;
		TDataJoins     m_joins ;
		CAtlString     m_source;

	public:
		 CDataJoinEnum(void);
		~CDataJoinEnum(void);

	public:
		HRESULT        Append (const CDataJoin&);
		VOID           Clear  (void)   ;
		TErrorRef      Error  (void) const;
		bool           IsValid(void) const;
		LPCTSTR        Source (void) const;
		HRESULT        Source (LPCTSTR)   ;
		const
		TDataJoins&    Raw (void) const;
	public:
		CAtlString     ToString(void) const;         // returns a string, which represents all available joins;
	};
	
}}

typedef shared::sql::CDataType::_e TSqlDataType;

#endif/*_GENERICSTGSQLMDL_H_EC63EDC3_91A8_4162_863B_593582616188_INCLUDED*/