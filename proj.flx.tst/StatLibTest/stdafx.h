// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#pragma once

#define WINVER          0x0600
#define _WIN32_WINNT    0x0600


#define _WTL_NO_CSTRING

#include <atlstr.h>

#include <atlbase.h>
#include <atlapp.h>

extern CAppModule _Module;

#pragma comment(lib, "ebo.boo.flex.grid.prior.lib")
