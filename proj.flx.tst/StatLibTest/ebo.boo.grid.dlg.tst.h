// MainDlg.h : interface of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IGridInterface.h"

#pragma comment(lib, "gdiplus.lib")

class CMainDlg : public CDialogImpl<CMainDlg> {
                typedef CDialogImpl<CMainDlg> TDialog;
private:
	UILayer::IGrid* m_pGrid;

public:
	WORD IDD;

public:
	 CMainDlg (void);
	~CMainDlg (void);

	BEGIN_MSG_MAP(CMainDlg)
		MESSAGE_HANDLER   (WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER   (WM_KEYDOWN   , OnKeyDown   )
		COMMAND_ID_HANDLER(IDOK    , OnOK)
		COMMAND_ID_HANDLER(IDCANCEL, OnCancel)
	END_MSG_MAP()

private:
	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnKeyDown   (UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnOK    (WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnCancel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
};
