// stdafx.cpp : source file that includes just the standard includes
//	LibTest.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

::WTL::CMessageLoop* get_MessageLoop(){ return _Module.GetMessageLoop(); }
HINSTANCE get_HINSTANCE() { return _Module.GetResourceInstance(); }