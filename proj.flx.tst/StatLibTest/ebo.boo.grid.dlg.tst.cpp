#include "stdafx.h"
#include "ebo.boo.grid.dlg.tst.h"
#include "ebo.boo.flex.lib.res.h"

/////////////////////////////////////////////////////////////////////////////

namespace _impl {
	
	static HICON Application_LoadIcon(const UINT nIconResId, const bool bTreatAsLargeIcon)
	{
		const SIZE szIcon = {
					::GetSystemMetrics(bTreatAsLargeIcon ? SM_CXICON : SM_CXSMICON), 
					::GetSystemMetrics(bTreatAsLargeIcon ? SM_CYICON : SM_CYSMICON)
				};
		const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
		const HICON hIcon = (HICON) ::LoadImage(hInstance, MAKEINTRESOURCE(nIconResId), 
			IMAGE_ICON, szIcon.cx, szIcon.cy, LR_DEFAULTCOLOR);
		return hIcon;
	}
}
using namespace _impl;
/////////////////////////////////////////////////////////////////////////////

CMainDlg:: CMainDlg(void) : m_pGrid(NULL) , IDD(IDD_MAINDLG) {}
CMainDlg::~CMainDlg(void) {}

/////////////////////////////////////////////////////////////////////////////


LRESULT    CMainDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/) {
	TDialog::CenterWindow();

	TDialog::SetIcon(Application_LoadIcon(IDR_MAINFRAME, false), FALSE);
	TDialog::SetIcon(Application_LoadIcon(IDR_MAINFRAME, true ), TRUE );

	CWindow place_holder = TDialog::GetDlgItem(IDC_EBO_BOO_USR_AVI);
	if (place_holder) {
		place_holder.DestroyWindow();
	}

	m_pGrid = UILayer::CreateGridInstance(m_hWnd, 10, IDC_EBO_BOO_USR_AVI);
	if (m_pGrid == NULL)
		return 0;
	else
		m_pGrid->ShowRowSelector(true);
	// does not work;
	place_holder = TDialog::GetDlgItem(IDC_EBO_BOO_USR_AVI);
	if (place_holder)
		place_holder.SetFocus();

	UILayer::IThemeManager& them_man = m_pGrid->GetThemeManagerRef();

	const UILayer::ePredefinedTheme e_theme = UILayer::ePredefinedTheme::eSolidWhite;

	m_pGrid->ApplyObject(
		&(them_man.GetPredefinedTheme(e_theme))
	);

	UILayer::IRowSelector* p_rs_sel = m_pGrid->GetRowSelector();
	if (p_rs_sel) {
		p_rs_sel->SetVisible(true);
		p_rs_sel->UpdateLayout();

		UILayer::IRowSelectorTheme& rs_them = p_rs_sel->GetTheme();
		UILayer::IColorTheme& rs_cust = rs_them.Customize(e_theme);
		p_rs_sel->ApplyTheme (rs_cust);
	}

	UILayer::ICell* p_act_cell = m_pGrid->GetActiveCell();
	if (p_act_cell) {
		UILayer::IActiveCellTheme& act_them = p_act_cell->GetTheme();
		UILayer::IColorTheme&  act_cust = act_them.Customize(e_theme);
		p_act_cell->ApplyTheme(act_cust);

		p_act_cell->SetOptions(UILayer::eCellOption::eSingleBorder);
	}

	UILayer::IGrid& grid_ = *m_pGrid;
	UILayer::IColumnHeader* pHeader = m_pGrid->GetColumnHeader();
	if (pHeader == NULL)
		return 0;

	static LPCTSTR lp_sz_caps[] = {
		_T(" "), _T("Date"), _T("Type"), _T("Ref#"), _T("Payee"), _T("Category"), _T("Total"), _T("Action")
	};

	static const UILayer::eColumnHAlignTypes e_col_aligns[] = {
		UILayer::e2_clmnHAlignCenter, UILayer::e2_clmnHAlignCenter, UILayer::e2_clmnHAlignLeft , UILayer::e2_clmnHAlignRight,
		UILayer::e2_clmnHAlignLeft  , UILayer::e2_clmnHAlignCenter, UILayer::e2_clmnHAlignRight, UILayer::e2_clmnHAlignRight
	};

	static const UILayer::eColumnDataTypes e_col_types[] = {
		UILayer::e2_clmnDTypeLong , UILayer::e2_clmnDTypeText     , UILayer::e2_clmnDTypeText  , UILayer::e2_clmnDTypeLong ,
		UILayer::e2_clmnDTypeText , UILayer::e2_clmnDTypeText     , UILayer::e2_clmnDTypeDouble, UILayer::e2_clmnDTypeText
	};

	static const UINT u_col_width[] = { 30, 70, 140, 50, 140, 140, 140, 140 };
	static const bool u_col_accss[] = {
		false, false, false, false, false, true, false, false  
	};

	static const DWORD e_grp_style  = UILayer::e2_GroupStyleImmovable|UILayer::e2_GroupStyleNotUpdatable;

	UINT u_grp_id_tag = 0x0C000000;
	UINT u_col_id_tag = 0x10000000;

	CAtlString cs_cap;

	for ( INT i_ = 0; i_ < _countof(lp_sz_caps  ) && i_ < _countof(e_col_aligns)
	               && i_ < _countof(e_col_types ) && i_ < _countof(u_col_width ); i_++ ){

		UILayer::IColumnGroup* pGroup = pHeader->AddGroup(lp_sz_caps[i_]);
		if (pGroup == NULL)
			continue;
		u_grp_id_tag += 1;

		pGroup->SetID   (u_grp_id_tag);
		pGroup->SetStyle(e_grp_style );
		pGroup->SetTag  (u_grp_id_tag);

		UILayer::IColumn* pColumn = pGroup->AddColumn(NULL, ++u_col_id_tag);
		if (pColumn == NULL)
			continue;

		pColumn->SetTag     (u_col_id_tag);
		pColumn->SetHAlign  (e_col_aligns[i_]);
		pColumn->SetDataType(e_col_types [i_]);
		pColumn->SetWidth   (u_col_width [i_]);

		UILayer::__OverlayData& over__  = (*pColumn).AccessOverlay();
		over__.__c__0 = RGB(17, 17, 17);
		over__.__c__1 = RGB(27, 27, 27);

		pColumn->SetLock(true);
		pGroup ->Update();
	}

	grid_.EnableRowGrouping(true);
	grid_.SetRows(10);
	grid_.UpdateLayout(*this);

	return 0;
}

LRESULT    CMainDlg::OnKeyDown   (UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/) {
	return 0;
}

LRESULT    CMainDlg::OnOK(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	TDialog::EndDialog(wID);
	UILayer::DestroyGrid_Safe(&m_pGrid);
	return 0;
}

LRESULT    CMainDlg::OnCancel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	TDialog::EndDialog(wID);
	UILayer::DestroyGrid_Safe(&m_pGrid);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////