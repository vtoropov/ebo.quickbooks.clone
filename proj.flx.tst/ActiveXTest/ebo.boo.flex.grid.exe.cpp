#include "ebo.boo.flex.grid.wtl.h"
#include "ActiveXHost.h"
#include "ActiveXHost.cpp"

CAppModule _Module;

[
	module(name="MCContainer2")
];


int Run(LPTSTR lpstrCmdLine = 0, int nCmdShow = SW_SHOW)
{
	CMessageLoop theLoop;
	_Module.AddMessageLoop(&theLoop);
	int  nRet = 0;
	CActiveXHost* pMcMainFrame = 0;
	HWND hwnParent = NULL;
    pMcMainFrame = new CActiveXHost();
	if(pMcMainFrame)
	{
		HWND hwnParent = NULL;
		pMcMainFrame->Create(hwnParent, 0, 0, WS_OVERLAPPEDWINDOW|WS_VISIBLE);
		pMcMainFrame->CreateFlexGrid(pMcMainFrame->m_hWnd);
		pMcMainFrame->CenterWindow();
		pMcMainFrame->ShowWindow(nCmdShow);
		pMcMainFrame->UpdateWindow();
		nRet = theLoop.Run();
	}
	if (pMcMainFrame)
		delete pMcMainFrame; 
	pMcMainFrame = 0;
	_Module.RemoveMessageLoop();
	return nRet;
}

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE, LPTSTR lpstrCmdLine, int nCmdShow)
{
	ATL::CString strCmdLine(lpstrCmdLine);
	strCmdLine.Replace(_T("\""), _T(""));
	HRESULT hResult = ::CoInitialize(0);
	int nResult = 0;
	if (SUCCEEDED(hResult))
	{	
		::DefWindowProc(0, 0, 0, 0L);
		AtlInitCommonControls(ICC_COOL_CLASSES | ICC_BAR_CLASSES);
		// tries to initialize an application module
		hResult = _Module.Init(0, hInstance);
		if(SUCCEEDED(hResult))
		{
			int nResult = Run(strCmdLine.GetBuffer(), nCmdShow);
			_Module.Term();
		} else
		::CoUninitialize();
	}
	return nResult;
}

