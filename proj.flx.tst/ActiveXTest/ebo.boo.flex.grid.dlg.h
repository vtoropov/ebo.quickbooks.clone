#include "ebo.boo.flex.grid.wtl.h"
#include "ebo.boo.flex.grid.res.h"

class CPropertyDlg : public CDialogImpl<CPropertyDlg>
{
	
public:
	enum { IDD = IDD_PROPERTY };
	// constructor(s) & destructor(s)
public:
	CPropertyDlg(CActiveXHost* pActiveXHost,long lSpeed);
	~CPropertyDlg(void);
	// message map & message handler(s)
public:
	BEGIN_MSG_MAP(CPropertyDlg)
		MESSAGE_HANDLER   (WM_CLOSE     ,     OnClose  )
		MESSAGE_HANDLER   (WM_INITDIALOG,     OnInitDialog)
		COMMAND_ID_HANDLER(IDCANCEL     ,     OnCommand)
		COMMAND_ID_HANDLER(IDC_RUNTRADE ,     OnCommand)
	END_MSG_MAP()
	LRESULT OnClose      (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCommand    (WORD wNotifyCode, WORD wID, HWND hwndCtrl, BOOL& bHandled);
	LRESULT OnInitDialog (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
private:
	CActiveXHost* m_pActiveXHost;
	long m_lSpeed;
};

CPropertyDlg::CPropertyDlg(CActiveXHost* pActiveXHost, long lSpeed):m_pActiveXHost(pActiveXHost),m_lSpeed(lSpeed)
{

}

CPropertyDlg::~CPropertyDlg()
{

}

LRESULT CPropertyDlg::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CenterWindow();
	TCHAR buffRows[512]={0};
	TCHAR buffCols[512]={0};
	TCHAR buffSpeed [512]={0};
	CStatic Rows = static_cast<CStatic>(GetDlgItem(IDC_ROWS )),
	        Cols = static_cast<CStatic>(GetDlgItem(IDC_COLS)),
	       Speed = static_cast<CStatic>(GetDlgItem(IDC_SPEED));
	_itot_s(m_pActiveXHost->GetGridNumRows(), buffRows, _countof(buffRows),10);
	_itot_s(m_pActiveXHost->GetGridNumCols(), buffCols, _countof(buffCols),10);
	_itot_s(m_lSpeed, buffSpeed, _countof(buffSpeed),10);
	Rows.SetWindowText(buffRows);
	Cols.SetWindowText(buffCols);
	Speed.SetWindowText(buffSpeed);
	return 0;
}

LRESULT CPropertyDlg::OnCommand(WORD wNotifyCode, WORD wID, HWND hwndCtrl, BOOL& bHandled)
{
	switch (wID)
	{
	case IDCANCEL:
		EndDialog(wID);
		break;
	case IDC_RUNTRADE:
		TCHAR buffRows[512]={0};
		TCHAR buffCols[512]={0};
		TCHAR buffSpeed [512]={0};
		CStatic Rows = static_cast<CStatic>(GetDlgItem(IDC_ROWS )),
		        Cols = static_cast<CStatic>(GetDlgItem(IDC_COLS)),
		       Speed = static_cast<CStatic>(GetDlgItem(IDC_SPEED));
		Rows.GetWindowText  (buffRows , _countof(buffRows ));
		Cols.GetWindowText  (buffCols , _countof(buffCols ));
		Speed.GetWindowText (buffSpeed, _countof(buffSpeed));
		long lRows   = _ttol(buffRows);
		long lCols   = _ttol(buffCols);
		long lSpeed  = _ttol(buffSpeed);
		m_pActiveXHost->SetGridFixedRows(1);
		m_pActiveXHost->SetGridFixedCols(1);
		m_pActiveXHost->SetGridNumRows(lRows);
		m_pActiveXHost->SetGridNumCols(lCols);
		m_pActiveXHost->StartTrade(lSpeed);
		OnCommand(0, IDCANCEL, 0, bHandled);
	}
    return 0;
}

LRESULT CPropertyDlg::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	OnCommand(0, IDCANCEL, 0, bHandled);
	return 0;
}