//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FlexGridTest.rc
//
#define IDC_RUNTRADE                    3
#define IDR_MENU_GRID                   100
#define IDD_PROPERTY                    102
#define IDC_ROWS                        1001
#define IDC_COLS                        1002
#define IDC_SPEED                       1003
#define IDC_CHECK2                      1007
#define ID_SHOW_DIALOG                  40001
#define ID_FLEXGRIDABOUT                40002

#define IDD_PROPERTY_SORT               1500
#define IDC_SORT_BY                     1501
#define IDC_THEN_BY1                    1502
#define IDC_THEN_BY2                    1503
#define IDC_GROUP                       1504
#define IDC_KEEP_CHECK                  1505
#define IDC_SORT_EVERY_EDIT             1506
#define IDC_SORT_EVERY_SPIN             1507
#define IDC_BUTTON_SORT1                1508
#define IDC_BUTTON_SORT2                1509
#define IDC_BUTTON_SORT3                1510
#define ID_OKEY                         1511
#define ID_CANCEL                       1512
#define IDC_STATIC_SORT_BY              1513
#define  IDC_STATIC_THEN_BY1            1514
#define  IDC_STATIC_THEN_BY2            1515
#define IDC_STATIC_SORTEVERY            1516
#define IDC_STATIC_SECONDS              1517
#define IDR_SORT_ASC                    1518
#define IDR_SORT_DESC                   1519
#define IDR_SORT_ASC_DISABLE            1520
#define IDR_SORT_DESC_DISABLE           1521
#define IDR_SORT_ASC_FOCUS              1522
#define IDR_SORT_DESC_FOCUS             1523
