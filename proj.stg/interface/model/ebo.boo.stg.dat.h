#ifndef _EBOBOOSTGDAT_H_739CF224_9370_4F4D_B10E_0BDE16A4F61F_INCLUDED
#define _EBOBOOSTGDAT_H_739CF224_9370_4F4D_B10E_0BDE16A4F61F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Oct-2019 at 9:32:15p, UTC+7, Novosibirsk, Light Coloured, Monday;
	This is Ebo Pack personal account app local master database entities interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "generic.stg.rec.h"

namespace ebo { namespace boo { namespace data {

	using shared::sys_core::CError;

	class CData_Action {
	public:
		enum _type {
			e_none   = 0x0,
			e_new    = 0x1,
			e_edit   = 0x2,
			e_delete = 0x3,
		};
	private:
		_type    m_current;
	public:
		 CData_Action (const _type = e_none);
		~CData_Action (void);
	public:
		const
		bool     Is   (const _type) const;
		bool     IsNew(void) const;
		const
		_type&   Ref  (void) const;
		_type&   Ref  (void)      ;

	public:
		CData_Action& operator = (const _type);
		operator const _type (void) const;
	};

}}}

typedef ebo::boo::data::CData_Action::_type  TActionType;

#endif/*_EBOBOOSTGDAT_H_739CF224_9370_4F4D_B10E_0BDE16A4F61F_INCLUDED*/