#ifndef _EBOBOOSTGDATUSR_H_948C04CF_7822_4BEA_B4F6_876EB69FEC0F_INCLUDED
#define _EBOBOOSTGDATUSR_H_948C04CF_7822_4BEA_B4F6_876EB69FEC0F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Oct-2019 at 1:41:46p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app user profile interface declaration file.
*/
#include "ebo.boo.stg.dat.h"
#include "ebo.boo.stg.dat.acc.h"

namespace ebo { namespace boo { namespace data { namespace profile {

	using shared::stg::TRecordsetAsText;
	using shared::stg::TRecordAsText;

	using ebo::boo::data::access::CRole ;
	using ebo::boo::data::access::CState;

	class CUser {
		// field sequence in select query is supposed to be as following:
		// 0 - usr_prf_id  |1 - usr_rol_id  |2 - usr_sta_id      |3 - usr_prf_name|
		// 4 - usr_prf_pwd |5 - usr_prf_date|6 - usr_prf_exp_date|7 - usr_prf_exp_pwd
		// 8 - acc_rol_name|9 - acc_sta_name;
	protected:
		mutable
		CError      m_error;
		CAtlString  m_guid ;
		CAtlString  m_name ;
		CAtlString  m_pass ;
		CRole       m_role ;
		CState      m_state;

	public:
		 CUser (void);
		 CUser (const TRecordAsText&);
		~CUser (void);

	public:
		VOID        Clear(void)      ;
		TErrorRef   Error(void) const;
		bool        Is   (void) const;   // returns true if the object is valid;
		LPCTSTR     Guid (void) const;   // gets a user identifier;
		HRESULT     Guid (LPCTSTR _lp_sz_guid);   // sets a user identifier;
		LPCTSTR     Name (void) const;   // gets a user name/nick ;
		HRESULT     Name (LPCTSTR _lp_sz_name);   // sets a user name/nick ;
		LPCTSTR     Pwd  (void) const;
		HRESULT     Pwd  (LPCTSTR)   ;   // empty password is acceptible;
		const
		CRole&      Role (void) const;   // gets user access role reference (ra);
		CRole&      Role (void)      ;   // gets user access role reference (rw);
		const
		CState&     State(void) const;
		CState&     State(void)      ;
		bool        Valid(void) const;
	public:
		CUser&      operator<<(const TRecordAsText&);
	};

	typedef ::std::vector<CUser>  TUsers;

	class CUser_Enum {
	protected:
		CError      m_error;
		TUsers      m_users;

	public:
		 CUser_Enum (void);
		~CUser_Enum (void);

	public:
		TErrorRef   Error (void) const;
		const INT   Find  (const CUser&) const; // returns an index of a user object in vector; -1, if not found;
		const
		TUsers&     Ref   (void) const;
		TUsers&     Ref   (void)      ;
		HRESULT     Set   (const TRecordsetAsText&);
	public:
		CUser_Enum& operator<<(const TRecordsetAsText&);
	};
}}}}

#endif/*_EBOBOOSTGDATUSR_H_948C04CF_7822_4BEA_B4F6_876EB69FEC0F_INCLUDED*/