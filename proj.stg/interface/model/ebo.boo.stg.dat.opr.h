#ifndef _EBOBOOSTGDATOPR_H_F826EABA_FDE5_48BC_A5F3_BE30DC2CD7D6_INCLUDED
#define _EBOBOOSTGDATOPR_H_F826EABA_FDE5_48BC_A5F3_BE30DC2CD7D6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Oct-2019 at 3:23:56p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account bookkeeping operation data model interface declaration file.
*/
#include "shared.gen.sys.date.h"
#include "ebo.boo.stg.dat.h"
#include "ebo.boo.stg.dat.acn.h"

namespace ebo { namespace boo { namespace data { namespace bookee { // boo(k)kee(ping)

	using shared::sys_core::CError;
	using shared::common::CSystemTime;

	using shared::stg::TRecordsetAsText;
	using shared::stg::TRecordAsText;

	class COperType {
		// expected record field sequence: (1) acn_typ_id; (2) acn_typ_name; 
	protected:
		INT          m_id;
		CAtlString   m_name;

	public:
		 COperType (void) ;
		 COperType (const TRecordAsText&);
		~COperType (void) ;

	public:
		const INT&   Id   (void) const;
		      INT&   Id   (void)      ;
		const
		CAtlString&  Name (void) const;
		CAtlString&  Name (void)      ;
		HRESULT      Set  (const TRecordAsText&);

	public:
		COperType& operator = (const INT  _n_id    );
		COperType& operator = (LPCTSTR _lp_sz_name );
		COperType& operator <<(const TRecordAsText&);
	};

	typedef ::std::vector<COperType>  TOperTypes;

	class COperType_Enum {
	protected:
		TOperTypes   m_types;
		CError       m_error;
	public:
		 COperType_Enum (void);
		 COperType_Enum (const TRecordsetAsText&);
		~COperType_Enum (void);

	public:
		TErrorRef   Error (void) const;
		const
		TOperTypes& Ref   (void) const;
		TOperTypes& Ref   (void)      ;
		HRESULT     Set   (const TRecordsetAsText&);

	public:
		COperType_Enum& operator<<(const TRecordsetAsText&);
	};

	class COper {
		// expect record field sequence:
		// (1) acn_opr_id; (2) acn_typ_id; (3) acn_obj_id; (4) acn_cost; (5) acn_timestamp;
		//                 (6) acn_typ_nm; (7) acn_obj_nm; 
	protected:
		INT          m_oper_id;   // this class object identifier;
		COperType    m_type   ;
		CAccount     m_account;
		INT          m_cost   ;   // integer value allows to keep a precision of money in this way: >>2.&0xFF;
		CAtlString   m_date   ;   // operation date as a string;
		CSystemTime  m_tstamp ;   // timestamp in format YYYYMMDDHHMMSS: year>>month>>day>>hour>>mins>>secs;

	public:
		 COper (void) ;
		 COper (const TRecordAsText&);
		~COper (void) ;

	public:
		const
		CAccount &   Account (void) const;
		CAccount &   Account (void)      ;
		const INT&   Cost (void) const;
		      INT&   Cost (void)      ;
		const
		CSystemTime& Date (void) const;
		CSystemTime& Date (void)      ;
		const INT &  Id   (void) const;
		      INT &  Id   (void)      ;
		HRESULT      Set  (const TRecordAsText&);
		const
		COperType&   Type (void) const;
		COperType&   Type (void)      ;

	public:
		COper& operator <<(const TRecordAsText&);
	};

	typedef ::std::vector<COper>   TOpers;

	class COper_Enum {
	protected:
		TOpers       m_opers;
		CError       m_error;
	public:
		 COper_Enum (void);
		 COper_Enum (const TRecordsetAsText&);
		~COper_Enum (void);

	public:
		TErrorRef   Error (void) const;
		const
		TOpers&     Ref   (void) const;
		TOpers&     Ref   (void)      ;
		HRESULT     Set   (const TRecordsetAsText&);

	public:
		COper_Enum& operator<<(const TRecordsetAsText&);
	};

}}}}

#endif/*_EBOBOOSTGDATOPR_H_F826EABA_FDE5_48BC_A5F3_BE30DC2CD7D6_INCLUDED*/