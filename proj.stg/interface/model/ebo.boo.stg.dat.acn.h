#ifndef _EBOBOOSTGDATACN_H_5E6E3E3C_C6CB_4AC2_BC4A_2DFB063E4969_INCLUDED
#define _EBOBOOSTGDATACN_H_5E6E3E3C_C6CB_4AC2_BC4A_2DFB063E4969_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Oct-2019 at 8:22:06a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal bookkeeping account data interface declaration file.
*/
#include "ebo.boo.stg.dat.h"

namespace ebo { namespace boo { namespace data { namespace bookee { // boo(k)kee(ping)

	using shared::sys_core::CError;

	using shared::stg::TRecordsetAsText;
	using shared::stg::TRecordAsText;

	class CAccount  {
		// expected record field sequence: (1) acn_obj_id; (2) acn_obj_name; 
	protected:
		INT          m_id;
		CAtlString   m_name;

	public:
		 CAccount (void) ;
		 CAccount (const TRecordAsText&);
		~CAccount (void) ;

	public:
		const INT &  Id   (void) const;
		      INT &  Id   (void)      ;
		const
		CAtlString&  Name (void) const;
		CAtlString&  Name (void)      ;
		HRESULT      Set  (const TRecordAsText&);

	public:
		CAccount& operator = (const INT  _n_id    );
		CAccount& operator = (LPCTSTR _lp_sz_name );
		CAccount& operator <<(const TRecordAsText&);
	};

	typedef ::std::vector<CAccount>  TAccounts;

	class CAccount_Enum {
	protected:
		TAccounts    m_accounts;
		CError       m_error;
	public:
		 CAccount_Enum (void);
		 CAccount_Enum (const TRecordsetAsText&);
		~CAccount_Enum (void);

	public:
		TErrorRef   Error (void) const;
		const
		TAccounts&  Ref   (void) const;
		TAccounts&  Ref   (void)      ;
		HRESULT     Set   (const TRecordsetAsText&);

	public:
		CAccount_Enum&  operator<<(const TRecordsetAsText&);
	};

}}}}

#endif/*_EBOBOOSTGDATACN_H_5E6E3E3C_C6CB_4AC2_BC4A_2DFB063E4969_INCLUDED*/