#ifndef _EBOBOOSTGDATACC_H_89F7C65A_E713_4ADB_B67E_528329942226_INCLUDED
#define _EBOBOOSTGDATACC_H_89F7C65A_E713_4ADB_B67E_528329942226_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Oct-2019 at 1:31:41p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app user data access interface declaration file.
*/
#include "ebo.boo.stg.dat.h"

namespace ebo { namespace boo { namespace data { namespace access {

	using shared::sys_core::CError;

	using shared::stg::TRecordsetAsText;
	using shared::stg::TRecordAsText;

	class CArea {
	protected:
		INT         m_id;
		CAtlString  m_name;
		CAtlString  m_desc;

	public:
		 CArea (void);
		 CArea (const TRecordAsText&);
		~CArea (void);

	public:
		LPCTSTR     Desc (void) const; // gets access area description;
		HRESULT     Desc (LPCTSTR)   ; // sets access area description; it cannot be NULL or empty;
		const INT&  Id   (void) const; // gets identifier reference (read only);
		      INT&  Id   (void)      ; // gets identifier reference (read-write);
		LPCTSTR     Name (void) const; // gets access area name ;
		HRESULT     Name (LPCTSTR)   ; // sets access area name ; it cannot be NULL or empty;
	public:
		CArea&      operator<<(const TRecordAsText&);
		CArea&      operator= (const INT  _id);
		CArea&      operator= (LPCTSTR _lp_sz_name );
		CArea&      operator<<(LPCTSTR _lp_sz_desc );
	};

	typedef ::std::vector<CArea>   TAccAreas;

	class CArea_Enum {
	protected:
		TAccAreas   m_areas;
		CError      m_error;
	public:
		 CArea_Enum (void);
		 CArea_Enum (const TRecordsetAsText&);
		~CArea_Enum (void);

	public:
		TErrorRef   Error (void) const;
		const
		TAccAreas&  Ref   (void) const;
		TAccAreas&  Ref   (void)      ;
		HRESULT     Set   (const TRecordsetAsText&);

	public:
		CArea_Enum& operator<<(const TRecordsetAsText&);

	};

	class CRole {
	protected:
		INT         m_id  ;
		CAtlString  m_name;
		CAtlString  m_desc;

	public:
		 CRole (void);
		 CRole (const TRecordAsText&);
		~CRole (void);

	public:
		VOID        Clear(void);
		LPCTSTR     Desc (void) const; // gets role description;
		HRESULT     Desc (LPCTSTR)   ; // sets role description; it cannot be NULL or empty;
		const INT&  Id   (void) const; // gets identifier reference (read only);
		      INT&  Id   (void)      ; // gets identifier reference (read-write);
		LPCTSTR     Name (void) const; // gets role name ;
		HRESULT     Name (LPCTSTR)   ; // sets role name ; it cannot be NULL or empty;
	public:
		CRole&      operator<<(const TRecordAsText&);
		CRole&      operator= (const INT  _id);
		CRole&      operator= (LPCTSTR _lp_sz_name );
		CRole&      operator<<(LPCTSTR _lp_sz_desc );
	};

	typedef ::std::vector<CRole>   TAccRoles;

	class CRole_Enum {
	protected:
		TAccRoles   m_roles;
		CError      m_error;
	public:
		 CRole_Enum (void);
		 CRole_Enum (const TRecordsetAsText&);
		~CRole_Enum (void);

	public:
		TErrorRef   Error (void) const;
		const
		CRole&      Item  (const INT _n_ndx) const;
		const
		TAccRoles&  Ref   (void) const;
		TAccRoles&  Ref   (void)      ;
		HRESULT     Set   (const TRecordsetAsText&);

	public:
		CRole_Enum& operator<<(const TRecordsetAsText&);

	};

	class CState {
		// https://stackoverflow.com/questions/1162816/naming-conventions-state-versus-status
	protected:
		INT        m_id ;
		CAtlString m_name;

	public:
		 CState (void) ;
		 CState (const TRecordAsText&);
		~CState (void) ;

	public:
		VOID      Clear(void)      ;
		INT       Id   (void) const;
		HRESULT   Id   (const INT _n_val);
		bool      Is   (void) const;             // returns true if this account is available to work: {Enabled|Restricted}
		LPCTSTR   Name (void) const;
		HRESULT   Name (LPCTSTR)   ;
		HRESULT   Set  (const TRecordAsText&);

	public:
		operator  bool   (void) const;
		operator  INT    (void) const;
		operator  LPCTSTR(void) const;

	public:
		CState&  operator<<(const TRecordAsText&);
		CState&  operator= (const INT _n_val);
		CState&  operator= (LPCTSTR  _lp_sz_name);
	};

	typedef ::std::vector<CState>   TAccStates;

	class CState_Enum {
	protected:
		TAccStates  m_stats;
		CError      m_error;
	public:
		 CState_Enum (void);
		 CState_Enum (const TRecordsetAsText&);
		~CState_Enum (void);

	public:
		TErrorRef   Error (void) const;
		const
		CState&     Item  (const INT _n_ndx) const;
		const
		TAccStates& Ref   (void) const;
		TAccStates& Ref   (void)      ;
		HRESULT     Set   (const TRecordsetAsText&);

	public:
		CState_Enum& operator<<(const TRecordsetAsText&);
	};

}}}}

#endif/*_EBOBOOSTGDATACC_H_89F7C65A_E713_4ADB_B67E_528329942226_INCLUDED*/