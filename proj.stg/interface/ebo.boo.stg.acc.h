#ifndef _EBOBOOSTGACC_H_6149846A_EA67_42EF_AA60_276B6E0F8C5D_INCLUDED
#define _EBOBOOSTGACC_H_6149846A_EA67_42EF_AA60_276B6E0F8C5D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Sep-2018 at 10:12:29p, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
	This is USB Drive Detective (bitsphereinc.com) desktop application SQLite database accessor interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 20-Oct-2019 at 6:15:03p, UTC+7, Novosibirsk, Light Coloured, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "generic.sql.mdl.h"
#include "generic.stg.lt.acc.h"
#include "generic.stg.lt.def.h"
#include "ebo.boo.stg.loc.h"

namespace ebo { namespace boo { namespace data {

	using shared::sys_core::CError;
	using shared::stg::lt::CDbObject;

	class CDbConnector {
	protected:
		mutable
		CError      m_error;

	public:
		 CDbConnector(void);
		~CDbConnector(void);

	public:
		HRESULT     Close (VOID) ;                              // closes database file;
		TErrorRef   Error (void) const;                         // gets last error reference;
		bool        IsOpen(void) const;                         // checks an access to database file;
		HRESULT     Open  (const TDbLocator&) ;                 // opens database file;

	public:
		const
		CDbObject&  DbObject(void) const;
		CDbObject&  DbObject(void);                             // gets SQLite database object;

	public:
		operator       TErrorRef (void) const;
		operator const CDbObject&(void) const;
		operator       CDbObject&(void)      ;
	};

	class CDbStorage {
	private:
		CError      m_error ;
		TDbLocator  m_locator;
	public:
		 CDbStorage(void);
		~CDbStorage(void);

	public:
		TErrorRef   Error  (void) const;
		bool        IsValid(void) const;
		const
		TDbLocator& Locator(void) const;
		TDbLocator& Locator(void)      ;
	};
}}}

#endif/*_UDDDATAPROVACC_H_6149846A_EA67_42EF_AA60_276B6E0F8C5D_INCLUDED*/