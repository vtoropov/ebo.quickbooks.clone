#ifndef _EBOBOOSTGQRYACN_H_5564729A_A3AA_41AA_A743_0D5A0E3CD324_INCLUDED
#define _EBOBOOSTGQRYACN_H_5564729A_A3AA_41AA_A743_0D5A0E3CD324_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Oct-2019 at 2:55:29p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal bookkeeping account data query interface declaration file.
*/
#include "ebo.boo.stg.qry.h"
#include "ebo.boo.stg.dat.acn.h"

namespace ebo { namespace boo { namespace data {

	using ebo::boo::data::bookee::CAccount ;
	using ebo::boo::data::bookee::CAccount_Enum ;

	class CQuery_Accounts  : public CQuery_Generic {
	                        typedef CQuery_Generic TQuery;
	public:
		 CQuery_Accounts(void);
		~CQuery_Accounts(void);

	public:
		HRESULT       Get (CAccount_Enum&); // gets bookkeepping areas list;
	};

}}}

#endif/*_EBOBOOSTGQRYACN_H_5564729A_A3AA_41AA_A743_0D5A0E3CD324_INCLUDED*/