#ifndef _EBOBOOSTGQRYACC_H_C020629B_D2F0_4BF1_A515_4F4B8F2588CB_INCLUDED
#define _EBOBOOSTGQRYACC_H_C020629B_D2F0_4BF1_A515_4F4B8F2588CB_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Oct-2019 at 7:17:51a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app access areas data query interface declaration file.
*/
#include "ebo.boo.stg.qry.h"
#include "ebo.boo.stg.dat.acc.h"

namespace ebo { namespace boo { namespace data {

	using ebo::boo::data::access::CArea_Enum ;
	using ebo::boo::data::access::CRole_Enum ;
	using ebo::boo::data::access::CState_Enum;

	class CQuery_Areas  : public CQuery_Generic {
	                     typedef CQuery_Generic TQuery;
	public:
		 CQuery_Areas(void);
		~CQuery_Areas(void);

	public:
		HRESULT       Get (CArea_Enum&); // gets access areas list;
	};

	class CQuery_Roles  : public CQuery_Generic {
	                     typedef CQuery_Generic TQuery;
	public:
		 CQuery_Roles(void);
		~CQuery_Roles(void);

	public:
		HRESULT       Get (CRole_Enum&); // gets access roles list;
	};

	class CQuery_Stats  : public CQuery_Generic {
	                     typedef CQuery_Generic TQuery;
	public:
		 CQuery_Stats(void);
		~CQuery_Stats(void);

	public:
		HRESULT       Get (CState_Enum&); // gets access status list;
	};

}}}

#endif/*_EBOBOOSTGQRYACC_H_C020629B_D2F0_4BF1_A515_4F4B8F2588CB_INCLUDED*/