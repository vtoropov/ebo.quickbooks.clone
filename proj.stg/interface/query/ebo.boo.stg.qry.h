#ifndef _EBOBOOSTGQRY_H_7F33D1A2_DDD7_47F0_86EB_9B43E5B635CE_INCLUDED
#define _EBOBOOSTGQRY_H_7F33D1A2_DDD7_47F0_86EB_9B43E5B635CE_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Dec-2018 at 9:24:11p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is USB Drive Detective (bitsphereinc.com) desktop app local SQLite database query interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 21-Oct-2019 at 11:37:44a, UTC+7, Novosibirsk, Light Coloured, Monday;
*/
#include "shared.gen.sys.err.h"

#include "generic.sql.qry.h"
#include "generic.stg.rec.h"

#include "ebo.boo.stg.dat.h"
#include "ebo.boo.stg.dat.usr.h"

namespace ebo { namespace boo { namespace data {

	using shared::sys_core::CError;
	using shared::sql::CDbQuery;

	using shared::stg::TRecordsetAsText;
	using shared::stg::TRecordsetAsVars;

	class CQuery_Generic {
	protected:
		CError   m_error;

	public:
		 CQuery_Generic(void);
		~CQuery_Generic(void);

	public:
		TErrorRef     Error  (void) const;
		HRESULT       Insert (const CDbQuery&) ; // actually this query is the same in comparison with Update one;
		HRESULT       Remove (const CDbQuery&) ;
		HRESULT       Select (const CDbQuery&, TRecordsetAsVars& _v_result);
		HRESULT       Update (const CDbQuery&) ;
	};

}}}

#endif/*_EBOBOOSTGQRY_H_7F33D1A2_DDD7_47F0_86EB_9B43E5B635CE_INCLUDED*/