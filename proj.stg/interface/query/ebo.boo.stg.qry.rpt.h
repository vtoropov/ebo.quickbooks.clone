#ifndef _EBOBOOSTGQRYRPT_H_5B82AA67_434C_4D24_9096_0FD6BA9C5B45_INCLUDED
#define _EBOBOOSTGQRYRPT_H_5B82AA67_434C_4D24_9096_0FD6BA9C5B45_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Oct-2019 at 7:17:51a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app report data query interface declaration file.
*/
#include "ebo.boo.stg.qry.h"
#include "ebo.boo.stg.dat.acn.h"

namespace ebo { namespace boo { namespace data {

	using ebo::boo::data::bookee::COperType_Enum;

	class CQuery_Rpt  : public CQuery_Generic {
	                   typedef CQuery_Generic TQuery;
	public:
		 CQuery_Rpt(void);
		~CQuery_Rpt(void);

	public:
		HRESULT       Get (COperType_Enum&); // gets access roles list;
	};


}}}

#endif/*_EBOBOOSTGQRYRPT_H_5B82AA67_434C_4D24_9096_0FD6BA9C5B45_INCLUDED*/