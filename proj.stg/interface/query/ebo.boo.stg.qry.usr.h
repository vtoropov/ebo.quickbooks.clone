#ifndef _EBOBOOSTGQRYUSR_H_721D526B_933E_4C59_9438_67462A55178B_INCLUDED
#define _EBOBOOSTGQRYUSR_H_721D526B_933E_4C59_9438_67462A55178B_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Oct-2019 at 2:21:30p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app user profile data query interface declaration file.
*/
#include "ebo.boo.stg.qry.h"
#include "ebo.boo.stg.dat.usr.h"

namespace ebo { namespace boo { namespace data {

	using ebo::boo::data::profile::CUser;
	using ebo::boo::data::profile::CUser_Enum;

	class CQuery_Users  : public CQuery_Generic {
	                     typedef CQuery_Generic TQuery;
	public:
		 CQuery_Users(void);
		~CQuery_Users(void);

	public:
		HRESULT       Add (const CUser&); // save new user object;
		HRESULT       Edit(const CUser&); // updates existing user object;
		HRESULT       Get (CUser_Enum &); // gets access status list;
		HRESULT       Rem (const CUser&); // deletes a user provided;
	};
}}}

#endif/*_EBOBOOSTGQRYUSR_H_721D526B_933E_4C59_9438_67462A55178B_INCLUDED*/