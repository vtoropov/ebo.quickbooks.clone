#ifndef _EBOBOOSTGLOC_H_F6131209_AA4C_4E6E_9042_B77D87F6D660_INCLUDED
#define _EBOBOOSTGLOC_H_F6131209_AA4C_4E6E_9042_B77D87F6D660_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Nov-2018 at 8:47:07a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop app SQLite database locator interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 20-Oct-2019 at 6:53:56p, UTC+7, Novosibirsk, Light Coloured, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "generic.stg.lt.loc.h"

namespace ebo { namespace boo { namespace data {

	using shared::sys_core::CError;

	class CDbLocator_Cfg  {
	protected:
		CAtlString     m_db_folder;
		CAtlString     m_db_file;

	public:
		CDbLocator_Cfg(void);

	public:
		LPCTSTR        File  (void)const;
		LPCTSTR        Folder(void)const;
	};

	class CDbLocator : public shared::stg::lt::CDbLocator {
	                  typedef shared::stg::lt::CDbLocator TBase;
	protected:
		CAtlString     m_dir  ;
		CAtlString     m_file ;
		bool           m_use_def;

	public:
		 CDbLocator(void);
		~CDbLocator(void);

	public:
		bool           Default (void) const; // gets using default value flag;
		VOID           Default (const bool); // sets using default value flag;
		LPCTSTR        File    (void) const; // gets database file name;
		HRESULT        File    (LPCTSTR)   ; // sets database file name; 
		LPCTSTR        Folder  (void) const; // gets database folder path;
		HRESULT        Folder  (LPCTSTR)   ; // sets database folder path;
		TErrorRef      Error   (void) const; // gets the last error object;
		bool           IsValid (void) const; // checks database path validity;

	public:
		CDbLocator&   operator= (const CDbLocator_Cfg&);
	public:
		operator CAtlString (void) const;  // gets full path to the database;
	};

	class CDbLocator_Master : public CDbLocator {
	                         typedef CDbLocator TBase;
	public:
		CDbLocator_Master (void);
	};

}}}

typedef ebo::boo::data::CDbLocator TDbLocator;

#endif/*_EBOBOOSTGLOC_H_F6131209_AA4C_4E6E_9042_B77D87F6D660_INCLUDED*/