#ifndef _STDAFX_H_DDE18404_1F70_4799_B153_1DE210FC59E2_INCLUDED
#define _STDAFX_H_DDE18404_1F70_4799_B153_1DE210FC59E2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Nov-2018 at 7:57:54p, UTC+7, Novosibirsk, Tulenina, Friday;
	This is USB Drive Detective (bitsphereinc.com) local database shared library precompiled headers declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 20-Oct-2019 at 5:41:19p, UTC+7, Novosibirsk, Light Coloured, Sunday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <comutil.h>
#include <vector>
#include <map>


#endif/*_STDAFX_H_DDE18404_1F70_4799_B153_1DE210FC59E2_INCLUDED*/