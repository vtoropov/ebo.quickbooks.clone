/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Nov-2018 at 8:47:07a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop app SQLite database locator interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 20-Oct-2019 at 6:57:05p, UTC+7, Novosibirsk, Light Coloured, Sunday;
*/
#include "StdAfx.h"
#include "ebo.boo.stg.loc.h"

using namespace ebo::boo::data;

#include "shared.gen.app.obj.h"
#include "shared.fs.gen.folder.h"

using namespace shared::user32;
using namespace shared::ntfs;
////////////////////////////////////////////////////////////////////////////

CDbLocator_Cfg::CDbLocator_Cfg(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR       CDbLocator_Cfg::File  (void) const { return m_db_file.GetString();   }
LPCTSTR       CDbLocator_Cfg::Folder(void) const { return m_db_folder.GetString(); }
/////////////////////////////////////////////////////////////////////////////

CDbLocator:: CDbLocator(void) : m_use_def(true) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CDbLocator::~CDbLocator(void) {}

/////////////////////////////////////////////////////////////////////////////

bool      CDbLocator::Default(void) const    { return m_use_def; }
VOID      CDbLocator::Default(const bool _v) { m_use_def = _v;   }
LPCTSTR   CDbLocator::File   (void) const    { return m_file.GetString(); }
HRESULT   CDbLocator::File   (LPCTSTR _v)    {
	m_error << __MODULE__ << S_OK;
	if (NULL == _v || 1 > ::_tcslen(_v))
		return ((m_error = E_INVALIDARG) = _T("File name is empty or invalid;"));

	m_file = _v;
	return m_error;
}
LPCTSTR   CDbLocator::Folder (void) const    { return m_dir.GetString();  }
HRESULT   CDbLocator::Folder (LPCTSTR _v)    {
	m_error << __MODULE__ << S_OK;
	if (NULL == _v || 1 > ::_tcslen(_v))
		return ((m_error = E_INVALIDARG) = _T("Folder path is empty or invalid;"));

	HRESULT hr_ = TBase::Parse(_v);
	if (FAILED(hr_))
		return hr_;

	const CApplication& the_app = GetAppObjectRef();

	CAtlString cs_dir(_v);

	if (the_app.IsRelatedToAppFolder(_v)) {
		the_app.GetPathFromAppFolder(_v, cs_dir);
		if (cs_dir.IsEmpty() == true)
			return (m_error = the_app.GetLastResult());
	}

	CGenericFolder folder_((LPCTSTR)cs_dir);
	if (folder_.Path().IsExist() == false)
		folder_.Create();
	if (folder_.Error().Is())
		return (m_error = folder_.Error());

	m_dir  = cs_dir;
	return m_error;
}
TErrorRef CDbLocator::Error  (void) const    { return m_error; }
bool      CDbLocator::IsValid(void) const    { return (false == m_file.IsEmpty() && false == m_dir.IsEmpty()); }

/////////////////////////////////////////////////////////////////////////////

CDbLocator&   CDbLocator::operator= (const CDbLocator_Cfg& _def) {
	this->File   (_def.File());
	this->Default(true);
	this->Folder (_def.Folder());
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CDbLocator::operator CAtlString (void) const {
	m_error << __MODULE__ << S_OK;

	CAtlString cs_path = m_dir;
	if (cs_path.Right(cs_path.GetLength() - 1) != _T("\\"))
		cs_path += _T("\\");

	cs_path += m_file;

	return cs_path;
}

/////////////////////////////////////////////////////////////////////////////

CDbLocator_Master::CDbLocator_Master (void) {
	TBase::m_error << __MODULE__ << S_OK >> __MODULE__;

	this->File(_T("qb_ds_master.old.db"));
	this->Folder(_T(".\\data"));
}