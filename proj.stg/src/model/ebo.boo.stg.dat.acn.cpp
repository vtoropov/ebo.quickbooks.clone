/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Oct-2019 at 8:40:42a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal bookkeeping account data interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.stg.dat.acn.h"

using namespace ebo::boo::data::bookee;

/////////////////////////////////////////////////////////////////////////////

CAccount:: CAccount(void) : m_id(0) {}
CAccount:: CAccount (const TRecordAsText& _t_rec) : m_id(0) { this->Set(_t_rec); }
CAccount::~CAccount(void) {}

/////////////////////////////////////////////////////////////////////////////

const INT&   CAccount::Id   (void) const { return m_id; }
      INT&   CAccount::Id   (void)       { return m_id; }
const
CAtlString&  CAccount::Name (void) const { return m_name; }
CAtlString&  CAccount::Name (void)       { return m_name; }
HRESULT      CAccount::Set  (const TRecordAsText& _t_rec) {
	if (_t_rec.size() < 2)
		return E_INVALIDARG;

	m_id   = ::_tstoi(_t_rec[0]);
	m_name = _t_rec[1];

	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CAccount& CAccount::operator = (const INT  _n_id    ) { this->Id() = _n_id; return *this; }
CAccount& CAccount::operator = (LPCTSTR _lp_sz_name ) { this->Name() = _lp_sz_name; return *this; }
CAccount& CAccount::operator <<(const TRecordAsText& _t_rec) { this->Set(_t_rec); return *this; }

/////////////////////////////////////////////////////////////////////////////

CAccount_Enum:: CAccount_Enum(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CAccount_Enum:: CAccount_Enum (const TRecordsetAsText& _t_set) { m_error << __MODULE__ << S_OK >> __MODULE__; *this << _t_set; }
CAccount_Enum::~CAccount_Enum(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CAccount_Enum::Error (void) const { return m_error; }
const
TAccounts&  CAccount_Enum::Ref   (void) const { return m_accounts; }
TAccounts&  CAccount_Enum::Ref   (void)       { return m_accounts; }
HRESULT     CAccount_Enum::Set   (const TRecordsetAsText& _t_set)  {
	m_error << __MODULE__ << S_OK;

	if (m_accounts.empty() == false) m_accounts.clear();

	for (size_t i_ = 0; i_ < _t_set.size(); i_++) {
		const TRecordAsText& t_rec = _t_set[i_];
		CAccount account_(t_rec);

		try {
			m_accounts.push_back(account_);
		}
		catch (const ::std::bad_alloc&) {
			m_error = E_OUTOFMEMORY; break;
		}
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CAccount_Enum& CAccount_Enum::operator<<(const TRecordsetAsText& _t_set) { this->Set(_t_set); return *this; }