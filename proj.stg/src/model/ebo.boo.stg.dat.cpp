/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Oct-2019 at 9:45:28p, UTC+7, Novosibirsk, Light Coloured, Monday;
	This is Ebo Pack personal account app local master database entities interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.stg.dat.h"

using namespace ebo::boo::data;

/////////////////////////////////////////////////////////////////////////////

CData_Action:: CData_Action(const _type _type) : m_current(_type) {}
CData_Action::~CData_Action(void) {}

/////////////////////////////////////////////////////////////////////////////
const
bool     CData_Action::Is   (const _type _e) const { return (_type::e_none != _e && m_current == _e);}
bool     CData_Action::IsNew(void) const {
	return (this->Is(_type::e_new));
}
const
TActionType&   CData_Action::Ref (void) const { return m_current; }
TActionType&   CData_Action::Ref (void)       { return m_current; }

/////////////////////////////////////////////////////////////////////////////

CData_Action& CData_Action::operator = (const _type _e) { this->Ref() = _e; return *this; }
CData_Action::operator const _type (void) const         { return this->Ref(); }