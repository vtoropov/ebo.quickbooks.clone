/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Oct-2019 at 1:37:27p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app user data access interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.stg.dat.acc.h"

using namespace ebo::boo::data::access;

/////////////////////////////////////////////////////////////////////////////

CArea:: CArea(void) : m_id(-1) {}
CArea:: CArea(const TRecordAsText& _t_rec) { *this << _t_rec; }
CArea::~CArea(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR   CArea::Desc (void) const { return m_desc.GetString(); }
HRESULT   CArea::Desc (LPCTSTR _lp_sz_desc) {
	if (NULL == _lp_sz_desc || 0 == ::_tcslen(_lp_sz_desc))
		return E_INVALIDARG;

	m_desc = _lp_sz_desc;
	return S_OK;
}
const
INT&      CArea::Id   (void) const { return m_id; }
INT&      CArea::Id   (void)       { return m_id; }

LPCTSTR   CArea::Name (void) const { return m_name.GetString(); }
HRESULT   CArea::Name (LPCTSTR _lp_sz_name) {
	if (NULL == _lp_sz_name || 0 == ::_tcslen(_lp_sz_name))
		return E_INVALIDARG;

	m_name = _lp_sz_name;
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CArea&    CArea::operator<<(const TRecordAsText& _rec) {
	for (size_t j_ = 0; j_ < _rec.size(); j_++) {
		if (0 == j_) { this->Id() = ::_tstoi((LPCTSTR)_rec[j_]); }
		if (1 == j_) { this->Name((LPCTSTR)_rec[j_]); }
		if (2 == j_) { this->Desc((LPCTSTR)_rec[j_]); }
	}
	return *this;
}
CArea&    CArea::operator= (const INT  _id)      { this->Id  () = _id;      return *this; }
CArea&    CArea::operator= (LPCTSTR _lp_sz_name) { this->Name(_lp_sz_name); return *this; }
CArea&    CArea::operator<<(LPCTSTR _lp_sz_desc) { this->Desc(_lp_sz_desc); return *this; }

/////////////////////////////////////////////////////////////////////////////

CArea_Enum:: CArea_Enum(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CArea_Enum:: CArea_Enum (const TRecordsetAsText& _t_set) { m_error << __MODULE__ << S_OK >> __MODULE__; *this << _t_set; }
CArea_Enum::~CArea_Enum(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CArea_Enum::Error (void) const { return m_error; }
const
TAccAreas&  CArea_Enum::Ref   (void) const { return m_areas; }
TAccAreas&  CArea_Enum::Ref   (void)       { return m_areas; }
HRESULT     CArea_Enum::Set   (const TRecordsetAsText& _t_set) {
	m_error << __MODULE__ << S_OK;

	if (m_areas.empty() == false) m_areas.clear();

	for (size_t i_ = 0; i_ < _t_set.size(); i_++) {
		const TRecordAsText& t_rec = _t_set[i_];
		CArea area_ = t_rec;

		try {
			m_areas.push_back(area_);
		}
		catch (const ::std::bad_alloc&) {
			m_error = E_OUTOFMEMORY; break;
		}
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CArea_Enum&   CArea_Enum::operator<<(const TRecordsetAsText& _t_set) { this->Set(_t_set); return *this; }

/////////////////////////////////////////////////////////////////////////////

CRole:: CRole(void) : m_id(-1) {}
CRole:: CRole(const TRecordAsText& _t_rec) { *this << _t_rec; }
CRole::~CRole(void) {}

/////////////////////////////////////////////////////////////////////////////

VOID      CRole::Clear(void) {
	this->m_id   = 0;
	this->m_name = _T("");
}
LPCTSTR   CRole::Desc (void) const { return m_desc.GetString(); }
HRESULT   CRole::Desc (LPCTSTR _lp_sz_desc) {
	if (NULL == _lp_sz_desc || 0 == ::_tcslen(_lp_sz_desc))
		return E_INVALIDARG;

	m_desc = _lp_sz_desc;
	return S_OK;
}
const
INT&      CRole::Id   (void) const { return m_id; }
INT&      CRole::Id   (void)       { return m_id; }
LPCTSTR   CRole::Name (void) const { return m_name.GetString(); }
HRESULT   CRole::Name (LPCTSTR _lp_sz_name) {
	if (NULL == _lp_sz_name || 0 == ::_tcslen(_lp_sz_name))
		return E_INVALIDARG;

	m_name = _lp_sz_name;
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CRole&    CRole::operator<<(const TRecordAsText& _rec) {
	for (size_t j_ = 0; j_ < _rec.size(); j_++) {
		if (0 == j_) { this->Id() = ::_tstoi((LPCTSTR)_rec[j_]); }
		if (1 == j_) { this->Name((LPCTSTR)_rec[j_]); }
		if (2 == j_) { this->Desc((LPCTSTR)_rec[j_]); }
	}
	return *this;
}
CRole&    CRole::operator= (const INT  _id)      { this->Id  () = _id;      return *this; }
CRole&    CRole::operator= (LPCTSTR _lp_sz_name) { this->Name(_lp_sz_name); return *this; }
CRole&    CRole::operator<<(LPCTSTR _lp_sz_desc) { this->Desc(_lp_sz_desc); return *this; }

/////////////////////////////////////////////////////////////////////////////

CRole_Enum:: CRole_Enum(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CRole_Enum:: CRole_Enum(const TRecordsetAsText& _t_set){ m_error << __MODULE__ << S_OK >> __MODULE__; *this << _t_set; }
CRole_Enum::~CRole_Enum(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CRole_Enum::Error (void) const { return m_error; }
const
CRole&      CRole_Enum::Item  (const INT _n_ndx) const    {
	if (_n_ndx < 0 || _n_ndx > static_cast<INT>(m_roles.size())) {
		static CRole fake_role;
		static bool b_accessed = false;
		if (b_accessed != true) {
			b_accessed  = true;
			fake_role.Id() = -1;
			fake_role.Name(_T("#not_found;"));
		}
		return fake_role;
	}
	else
		return this->Ref()[_n_ndx];
}
const
TAccRoles&  CRole_Enum::Ref   (void) const { return m_roles; }
TAccRoles&  CRole_Enum::Ref   (void)       { return m_roles; }
HRESULT     CRole_Enum::Set   (const TRecordsetAsText& _t_set) {
	m_error << __MODULE__ << S_OK;

	if (m_roles.empty() == false) m_roles.clear();

	for (size_t i_ = 0; i_ < _t_set.size(); i_++) {
		const TRecordAsText& t_rec = _t_set[i_];
		CRole role_ = t_rec;

		try {
			m_roles.push_back(role_);
		}
		catch (const ::std::bad_alloc&) {
			m_error = E_OUTOFMEMORY; break;
		}
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CRole_Enum&   CRole_Enum::operator<<(const TRecordsetAsText& _t_set) { this->Set(_t_set); return *this; }

/////////////////////////////////////////////////////////////////////////////

CState:: CState(void) : m_id(-1) {}
CState:: CState(const TRecordAsText& _t_rec) : m_id(-1) { *this << _t_rec; }
CState::~CState(void) {}

/////////////////////////////////////////////////////////////////////////////

VOID      CState::Clear(void) {
	this->m_id   = 0;
	this->m_name = _T("");
}
INT       CState::Id   (void) const       { return m_id; }
HRESULT   CState::Id   (const INT _n_val) {
	if (0 > _n_val)
		return E_INVALIDARG;
	m_id = _n_val;
	return S_OK;
}
bool      CState::Is   (void) const       { return (this->Id() == 1 || this->Id() == 2);}
LPCTSTR   CState::Name (void) const       { return m_name.GetString(); }
HRESULT   CState::Name (LPCTSTR _lp_sz_nm){
	if (NULL == _lp_sz_nm || 0 == ::_tcslen(_lp_sz_nm))
		return E_INVALIDARG;

	m_name = _lp_sz_nm;
	return S_OK;
}
HRESULT   CState::Set  (const TRecordAsText& _rec) {
	HRESULT hr_ = S_OK;
	for (size_t  i_ = 0; i_ < _rec.size(); i_++) {
		if (0 == i_) { hr_ = this->Id(::_tstoi(_rec[i_])); } if (FAILED(hr_)) return hr_;
		if (1 == i_) { hr_ = this->Name(      (_rec[i_])); } if (FAILED(hr_)) return hr_;
	}
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CState::operator  bool   (void) const { return this->Is()  ; }
CState::operator  INT    (void) const { return this->Id()  ; }
CState::operator  LPCTSTR(void) const { return this->Name(); }

/////////////////////////////////////////////////////////////////////////////

CState&  CState::operator<<(const TRecordAsText& _t_rec) { this->Set(_t_rec);  return *this; }
CState&  CState::operator= (const INT _n_val)      { this->Id(_n_val);         return *this; }
CState&  CState::operator= (LPCTSTR  _lp_sz_name)  { this->Name(_lp_sz_name);  return *this; }

/////////////////////////////////////////////////////////////////////////////

CState_Enum:: CState_Enum(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CState_Enum:: CState_Enum(const TRecordsetAsText& _t_set){ m_error << __MODULE__ << S_OK >> __MODULE__; *this << _t_set; }
CState_Enum::~CState_Enum(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CState_Enum::Error (void) const { return m_error; }
const
CState &    CState_Enum::Item  (const INT _n_ndx) const    {
	if (_n_ndx < 0 || _n_ndx > static_cast<INT>(m_stats.size())) {
		static CState fake_stat;
		static bool b_accessed = false;
		if (b_accessed != true) {
			b_accessed  = true;
			fake_stat.Id(-1);
			fake_stat.Name(_T("#state not found;"));
		}
		return fake_stat;
	}
	else
		return this->Ref()[_n_ndx];
}
const
TAccStates& CState_Enum::Ref   (void) const { return m_stats; }
TAccStates& CState_Enum::Ref   (void)       { return m_stats; }
HRESULT     CState_Enum::Set   (const TRecordsetAsText& _t_set) {
	m_error << __MODULE__ << S_OK;

	if (m_stats.empty() == false) m_stats.clear();

	for (size_t i_ = 0; i_ < _t_set.size(); i_++) {
		const TRecordAsText& t_rec = _t_set[i_];
		CState stat_ = t_rec;

		try {
			m_stats.push_back(stat_);
		}
		catch (const ::std::bad_alloc&) {
			m_error = E_OUTOFMEMORY; break;
		}
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CState_Enum& CState_Enum::operator<<(const TRecordsetAsText& _t_set) { this->Set(_t_set); return *this; }
