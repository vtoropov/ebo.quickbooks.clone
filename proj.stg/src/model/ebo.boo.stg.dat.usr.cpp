/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Oct-2019 at 1:45:46p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app user profile interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.stg.dat.usr.h"

using namespace ebo::boo::data::profile;

#include "shared.gen.rnd.h"

using namespace shared::common;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace data { namespace _impl {

	class CUser_Input {
	private:
		const
		CUser&     m_user ;
		mutable
		CError     m_error;

	public:
		 CUser_Input(const CUser& _usr) : m_user(_usr) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CUser_Input(void) {}
	public:
		HRESULT     Name  (LPCTSTR _lp_sz_name) const {
			m_error << __MODULE__ << S_OK;
			if (NULL == _lp_sz_name || 0 == ::_tcslen(_lp_sz_name))
				return ((m_error = E_INVALIDARG) = _T("User name is empty or invalid;"));
			return m_error;
		}
		TErrorRef   Error (void) const { return m_error; }
	};

}}}}

using namespace ebo::boo::data::_impl;
/////////////////////////////////////////////////////////////////////////////

CUser:: CUser (void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CUser:: CUser (const TRecordAsText& _t_rec){ m_error << __MODULE__ << S_OK >> __MODULE__; *this << _t_rec; }
CUser::~CUser (void) {}

/////////////////////////////////////////////////////////////////////////////

VOID        CUser::Clear(void)       {
	m_error = S_OK;
	if (m_name.IsEmpty() == false) m_name = _T("");
	if (m_pass.IsEmpty() == false) m_pass = _T("");
	if (m_guid.IsEmpty() == true ) {
		m_guid = CRndGen::GuidAsText(false, false);
	}
	m_role .Clear();
	m_state.Clear();
}
TErrorRef   CUser::Error(void) const { return m_error; }
bool        CUser::Is   (void) const {
	return (m_guid.IsEmpty() == false && m_name.IsEmpty() == false);
}
LPCTSTR     CUser::Guid (void) const { return m_guid.GetString(); }
HRESULT     CUser::Guid (LPCTSTR _lp_sz_guid) {
	m_error << __MODULE__ << S_OK;
	if (NULL == _lp_sz_guid || 0 == ::_tcslen(_lp_sz_guid))
		return ((m_error = E_INVALIDARG) = _T("User identifier is empty or invalid;"));

	m_guid = _lp_sz_guid;
	return m_error;
}
LPCTSTR     CUser::Name (void) const { return m_name.GetString(); }
HRESULT     CUser::Name (LPCTSTR _lp_sz_name) {
	CUser_Input input_(*this);
	HRESULT hr_ = input_.Name(_lp_sz_name);
	if (FAILED(hr_))
		m_error = input_.Error();
	else
		m_name = _lp_sz_name;
	return m_error;
}
LPCTSTR     CUser::Pwd  (void) const { return m_pass.GetString(); }
HRESULT     CUser::Pwd  (LPCTSTR _lp_sz_pwd ) {
	m_error << __MODULE__ << S_OK;
	m_pass = _lp_sz_pwd;
	return m_error;
}
const
CRole&      CUser::Role (void) const { return m_role; }
CRole&      CUser::Role (void)       { return m_role; }
const
CState&     CUser::State(void) const { return m_state;}
CState&     CUser::State(void)       { return m_state;}
bool        CUser::Valid(void) const {
	m_error << __MODULE__ << S_OK;
	CUser_Input input_(*this);
	HRESULT hr_ = input_.Name(this->Name());
	if (FAILED(hr_))
		m_error = input_.Error();
	return m_error.Is() != true;
}

/////////////////////////////////////////////////////////////////////////////

CUser&      CUser::operator<<(const TRecordAsText& _t_rec) {
	for (size_t  i_ = 0; i_ < _t_rec.size(); i_++) {
		if (0 == i_) { this->Guid((LPCTSTR)_t_rec[i_]); }                     // usr_prf_id;
		if (1 == i_) { this->Role ().Id() = ::_tstoi((LPCTSTR)_t_rec[i_]); }  // usr_rol_id;
		if (2 == i_) { this->State().Id(::_tstoi((LPCTSTR)_t_rec[i_])); }     // usr_sta_id;
		if (3 == i_) { this->Name ((LPCTSTR)_t_rec[i_]); }                    // usr_prf_name;
		if (4 == i_) { this->Pwd  ((LPCTSTR)_t_rec[i_]); }                    // usr_prf_pwd ;
		if (5 == i_) { continue; } // usr_prf_date;
		if (6 == i_) { continue; } // usr_prf_exp_date;
		if (7 == i_) { continue; } // usr_prf_exp_pwd ;
		if (8 == i_) { this->Role ().Name((LPCTSTR)_t_rec[i_]); }             // acc_rol_name ;
		if (9 == i_) { this->State().Name((LPCTSTR)_t_rec[i_]); }             // acc_sta_name ;
	}
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CUser_Enum:: CUser_Enum (void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CUser_Enum::~CUser_Enum (void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CUser_Enum::Error (void) const { return m_error; }
const INT   CUser_Enum::Find  (const CUser& _usr) const {
	INT n_ndx = -1;
	if (_usr.Is() == false)
		return n_ndx;

	CAtlString cs_guid = _usr.Guid();

	for (size_t i_ = 0; i_ < m_users.size(); i_++) {
		const CUser& user_ = m_users[i_];
		if (0 == cs_guid.CompareNoCase(user_.Guid()))
			return (n_ndx = static_cast<INT>(i_));
	}

	return n_ndx;
}
const
TUsers&     CUser_Enum::Ref   (void) const { return m_users; }
TUsers&     CUser_Enum::Ref   (void)       { return m_users; }
HRESULT     CUser_Enum::Set   (const TRecordsetAsText& _t_set) {
	m_error << __MODULE__ << S_OK;

	if (m_users.empty() == false)
		m_users.clear();

	for (size_t i_ = 0; i_ < _t_set.size(); i_++) {
		const TRecordAsText& t_rec = _t_set[i_];
		CUser user_ = t_rec;

		try {
			m_users.push_back(
					user_
				);
		}
		catch (const ::std::bad_alloc&) {
			m_error = E_OUTOFMEMORY; break;
		}
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CUser_Enum& CUser_Enum::operator<<(const TRecordsetAsText& _t_set) { this->Set(_t_set); return *this; }

/////////////////////////////////////////////////////////////////////////////