/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Oct-2019 at 3:31:13p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account bookkeeping operation data model interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.stg.dat.opr.h"

using namespace ebo::boo::data::bookee;
/////////////////////////////////////////////////////////////////////////////

COperType:: COperType(void) : m_id(0) {}
COperType:: COperType (const TRecordAsText& _t_rec) : m_id(0) { this->Set(_t_rec); }
COperType::~COperType(void) {}

/////////////////////////////////////////////////////////////////////////////

const INT&   COperType::Id   (void) const { return m_id; }
INT&   COperType::Id   (void)       { return m_id; }
const
CAtlString&  COperType::Name (void) const { return m_name; }
CAtlString&  COperType::Name (void)       { return m_name; }
HRESULT      COperType::Set  (const TRecordAsText& _t_rec) {
	if (_t_rec.size() < 2)
		return E_INVALIDARG;

	m_id   = ::_tstoi(_t_rec[0]);
	m_name = _t_rec[1];

	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

COperType& COperType::operator = (const INT  _n_id    ) { this->Id() = _n_id; return *this; }
COperType& COperType::operator = (LPCTSTR _lp_sz_name ) { this->Name() = _lp_sz_name; return *this; }
COperType& COperType::operator <<(const TRecordAsText& _t_rec) { this->Set(_t_rec); return *this; }

/////////////////////////////////////////////////////////////////////////////
COperType_Enum:: COperType_Enum(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
COperType_Enum:: COperType_Enum (const TRecordsetAsText& _t_set) { m_error << __MODULE__ << S_OK >> __MODULE__; *this << _t_set; }
COperType_Enum::~COperType_Enum(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   COperType_Enum::Error (void) const { return m_error; }
const
TOperTypes& COperType_Enum::Ref   (void) const { return m_types; }
TOperTypes& COperType_Enum::Ref   (void)       { return m_types; }
HRESULT     COperType_Enum::Set   (const TRecordsetAsText& _t_set) {
	m_error << __MODULE__ << S_OK;

	if (m_types.empty() == false) m_types.clear();

	for (size_t i_ = 0; i_ < _t_set.size(); i_++) {
		const TRecordAsText& t_rec = _t_set[i_];
		COperType type(t_rec);

		try {
			m_types.push_back(type);
		}
		catch (const ::std::bad_alloc&) {
			m_error = E_OUTOFMEMORY; break;
		}
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

COperType_Enum& COperType_Enum::operator<<(const TRecordsetAsText& _t_set) { this->Set(_t_set); return *this; }

/////////////////////////////////////////////////////////////////////////////

COper:: COper(void) : m_oper_id(0), m_cost(0) {}
COper:: COper(const TRecordAsText& _t_rec) : m_oper_id(0), m_cost(0) { this->Set(_t_rec); }
/////////////////////////////////////////////////////////////////////////////
const
CAccount &   COper::Account (void) const { return m_account; }
CAccount &   COper::Account (void)       { return m_account; }
const INT&   COper::Cost (void) const    { return m_cost   ; }
INT&   COper::Cost (void)          { return m_cost   ; }
const
CSystemTime& COper::Date (void) const    { return m_tstamp ; }
CSystemTime& COper::Date (void)          { return m_tstamp ; }
const INT&   COper::Id   (void) const    { return m_oper_id; }
INT&   COper::Id   (void)          { return m_oper_id; }
HRESULT      COper::Set  (const TRecordAsText& _t_rec) {
	for (size_t  i_ = 0; i_ < _t_rec.size(); i_++) {
		if (0 == i_) { this->Id() = ::_tstoi(_t_rec[i_]); } // acn_opr_id;
		if (1 == i_) { this->Type().Id() = ::_tstoi(_t_rec[i_]); } // acn_typ_id;
		if (2 == i_) { this->Account().Id() = ::_tstoi(_t_rec[i_]); } // acn_obj_id;
		if (3 == i_) { this->Cost() =  ::_tstoi(_t_rec[i_]); }  // acn_cost;
		if (4 == i_) { this->Date() =  ::_tstoi64(_t_rec[i_]);} // acn_timestamp
		if (5 == i_) { this->Type().Name() = _t_rec[i_]; }      // acn_typ_nm
		if (6 == i_) { this->Account().Name() = _t_rec[i_]; }   // acn_obj_nm
	}
	return S_OK;
}
const
COperType&   COper::Type (void) const { return m_type; }
COperType&   COper::Type (void)       { return m_type; }

/////////////////////////////////////////////////////////////////////////////

COper& COper::operator <<(const TRecordAsText& _t_rec) { this->Set(_t_rec); return *this; }

/////////////////////////////////////////////////////////////////////////////

COper_Enum:: COper_Enum(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
COper_Enum:: COper_Enum (const TRecordsetAsText& _t_set) { m_error << __MODULE__ << S_OK >> __MODULE__; *this << _t_set; }
COper_Enum::~COper_Enum(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   COper_Enum::Error (void) const { return m_error; }
const
TOpers&     COper_Enum::Ref   (void) const { return m_opers; }
TOpers&     COper_Enum::Ref   (void)       { return m_opers; }
HRESULT     COper_Enum::Set   (const TRecordsetAsText& _t_set) {
	m_error << __MODULE__ << S_OK;

	if (m_opers.empty() == false) m_opers.clear();

	for (size_t i_ = 0; i_ < _t_set.size(); i_++) {
		const TRecordAsText& t_rec = _t_set[i_];
		COper oper_(t_rec);

		try {
			m_opers.push_back(oper_);
		}
		catch (const ::std::bad_alloc&) {
			m_error = E_OUTOFMEMORY; break;
		}
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

COper_Enum& COper_Enum::operator<<(const TRecordsetAsText& _t_set) { this->Set(_t_set); return *this; }