/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Oct-2019 at 7:23:56a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app access areas data query interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.stg.qry.acc.h"

using namespace ebo::boo::data;

#include "generic.stg.lt.get.h"
#include "generic.stg.lt.put.h"

using namespace shared::sql;
using namespace shared::stg;
using namespace shared::stg::lt;

/////////////////////////////////////////////////////////////////////////////

CQuery_Areas:: CQuery_Areas(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CQuery_Areas::~CQuery_Areas(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CQuery_Areas::Get (CArea_Enum& _areas) {
	m_error << __MODULE__ << S_OK;

	CDbQuery qry_;
	HRESULT hr_ = qry_.Set(_T("SELECT acc_are_id,  acc_are_name, acc_are_desc FROM acc_area WHERE acc_are_id > 0 ORDER BY acc_are_name;"));
	if (FAILED(hr_))
		return (m_error = qry_.Error());

	static LPCTSTR lp_sz_f_nm[] = {
		_T("acc_are_id" ), _T("acc_are_name"), _T("acc_are_desc")
	};
	static CDataType::_e e_types[] = {
		CDataType::eInteger, CDataType::eText, CDataType::eText
	};

	CDataTable tab_(_T("acc_area"));

	for (INT i_ = 0; i_ < _countof(lp_sz_f_nm) && i_ < _countof(e_types); i_++) {
		tab_.Fields().Append(e_types[i_], lp_sz_f_nm[i_]);
	}

	qry_.Tables().Append(tab_);

	TRecordsetAsVars v_set;

	hr_ = TQuery::Select(qry_, v_set);
	if (FAILED(hr_))
		return hr_;

	TRecordsetAsText t_set;
	hr_ = CRecordAdapter::VarsToText(v_set, t_set);
	if (FAILED(hr_))
		m_error = hr_;

	hr_ = _areas.Set(t_set);
	if (FAILED(hr_))
		hr_ = _areas.Error();
	if (_areas.Ref().empty())
		(m_error = __DwordToHresult(ERROR_INVALID_DATA)) = _T("No access area found;");

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CQuery_Roles:: CQuery_Roles(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CQuery_Roles::~CQuery_Roles(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CQuery_Roles::Get (CRole_Enum& _roles) {
	m_error << __MODULE__ << S_OK;

	CDbQuery qry_;
	HRESULT hr_ = qry_.Set(_T("SELECT acc_rol_id,  acc_rol_name, acc_rol_desc FROM acc_role ORDER BY acc_rol_name DESC;"));
	if (FAILED(hr_))
		return (m_error = qry_.Error());

	static LPCTSTR lp_sz_f_nm[] = {
		_T("acc_rol_id" ), _T("acc_rol_name"), _T("acc_rol_desc")
	};
	static CDataType::_e e_types[] = {
		CDataType::eInteger, CDataType::eText, CDataType::eText
	};

	CDataTable tab_(_T("acc_role"));

	for (INT i_ = 0; i_ < _countof(lp_sz_f_nm) && i_ < _countof(e_types); i_++) {
		tab_.Fields().Append(e_types[i_], lp_sz_f_nm[i_]);
	}

	qry_.Tables().Append(tab_);

	TRecordsetAsVars v_set;

	hr_ = TQuery::Select(qry_, v_set);
	if (FAILED(hr_))
		return hr_;

	TRecordsetAsText t_set;
	hr_ = CRecordAdapter::VarsToText(v_set, t_set);
	if (FAILED(hr_))
		m_error = hr_;

	hr_ = _roles.Set(t_set);
	if (FAILED(hr_))
		m_error = _roles.Error();
	if (_roles.Ref().empty())
		(m_error = __DwordToHresult(ERROR_INVALID_DATA)) = _T("No role found;");

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CQuery_Stats:: CQuery_Stats(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CQuery_Stats::~CQuery_Stats(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CQuery_Stats::Get (CState_Enum& _stats) {
	m_error << __MODULE__ << S_OK;

	CDbQuery qry_;
	HRESULT hr_ = qry_.Set(_T("SELECT acc_sta_id,  acc_sta_name FROM acc_state ORDER BY acc_sta_id ASC;"));
	if (FAILED(hr_))
		return (m_error = qry_.Error());

	static LPCTSTR lp_sz_f_nm[] = {
		_T("acc_sta_id" ), _T("acc_sta_name")
	};
	static CDataType::_e e_types[] = {
		CDataType::eInteger, CDataType::eText
	};

	CDataTable tab_(_T("acc_state"));

	for (INT i_ = 0; i_ < _countof(lp_sz_f_nm) && i_ < _countof(e_types); i_++) {
		tab_.Fields().Append(e_types[i_], lp_sz_f_nm[i_]);
	}

	qry_.Tables().Append(tab_);

	TRecordsetAsVars v_set;

	hr_ = TQuery::Select(qry_, v_set);
	if (FAILED(hr_))
		return hr_;

	TRecordsetAsText t_set;
	hr_ = CRecordAdapter::VarsToText(v_set, t_set);
	if (FAILED(hr_))
		m_error = hr_;

	hr_ = _stats.Set(t_set);
	if (FAILED(hr_))
		m_error = _stats.Error();
	if (_stats.Ref().empty())
		(m_error = __DwordToHresult(ERROR_INVALID_DATA)) = _T("No access state found;");

	return m_error;
}