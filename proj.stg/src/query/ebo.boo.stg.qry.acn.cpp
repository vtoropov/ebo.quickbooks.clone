/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Oct-2019 at 3:09:12p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal bookkeeping account data query interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.stg.qry.acn.h"

using namespace ebo::boo::data;

#include "generic.stg.lt.get.h"
#include "generic.stg.lt.put.h"

using namespace shared::sql;
using namespace shared::stg;
using namespace shared::stg::lt;

/////////////////////////////////////////////////////////////////////////////

CQuery_Accounts:: CQuery_Accounts(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CQuery_Accounts::~CQuery_Accounts(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CQuery_Accounts::Get (CAccount_Enum& _account) {
	m_error << __MODULE__ << S_OK;

	CDbQuery qry_;
	HRESULT hr_ = qry_.Set(_T("SELECT acn_obj_id, acn_obj_name FROM acn_account ORDER BY acn_obj_name ASC;"));
	if (FAILED(hr_))
		return (m_error = qry_.Error());

	static LPCTSTR lp_sz_f_nm[] = {
		_T("acn_obj_id" ), _T("acn_obj_name")
	};
	static CDataType::_e e_types[] = {
		CDataType::eInteger, CDataType::eText
	};

	CDataTable tab_(_T("acn_account"));

	for (INT i_ = 0; i_ < _countof(lp_sz_f_nm) && i_ < _countof(e_types); i_++) {
		tab_.Fields().Append(e_types[i_], lp_sz_f_nm[i_]);
	}

	qry_.Tables().Append(tab_);

	TRecordsetAsVars v_set;

	hr_ = TQuery::Select(qry_, v_set);
	if (FAILED(hr_))
		return hr_;

	TRecordsetAsText t_set;
	hr_ = CRecordAdapter::VarsToText(v_set, t_set);
	if (FAILED(hr_))
		m_error = hr_;

	hr_ = _account.Set(t_set);
	if (FAILED(hr_))
		hr_ = _account.Error();
	if (_account.Ref().empty())
	   (m_error = __DwordToHresult(ERROR_INVALID_DATA)) = _T("No accounts found;");

	return m_error;
}