/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Oct-2019 at 2:29:12p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app user profile data query interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.stg.qry.usr.h"

using namespace ebo::boo::data;

#include "generic.stg.lt.get.h"
#include "generic.stg.lt.put.h"

using namespace shared::sql;
using namespace shared::stg;
using namespace shared::stg::lt;

/////////////////////////////////////////////////////////////////////////////

CQuery_Users:: CQuery_Users(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CQuery_Users::~CQuery_Users(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CQuery_Users::Add (const CUser& _usr) {
	m_error << __MODULE__ << S_OK;

	if (_usr.Is() == false)
		return ((m_error = E_INVALIDARG) = _T("User object is invalid or incomplete;"));

	static LPCTSTR lp_sz_pat = _T(
		"INSERT INTO [usr_profile] "
		"(usr_prf_id, usr_rol_id, usr_sta_id, usr_prf_name, usr_prf_pwd, usr_prf_date, usr_prf_exp_date, usr_prf_exp_pwd) "
		"VALUES "
		"('%s', %d, %d, '%s', '%s', '%s', '%s', '%s');"
	);
	CAtlString cs_qry; cs_qry.Format(
		lp_sz_pat,
		_usr.Guid(), _usr.Role().Id(), _usr.State().Id(), _usr.Name(), _usr.Pwd(), _T(""), _T(""), _T("")
	);

	HRESULT hr_ = m_error;

	CDbQuery qry_;
	hr_ = qry_.Set((LPCTSTR)cs_qry);
	if (FAILED(hr_))
		return (m_error = qry_.Error());

	hr_ = TQuery::Insert(qry_);

	return m_error;
}

HRESULT       CQuery_Users::Edit(const CUser& _usr) {
	m_error << __MODULE__ << S_OK;

	if (_usr.Is() == false)
		return ((m_error = E_INVALIDARG) = _T("User object is invalid or incomplete;"));

	static LPCTSTR lp_sz_pat = _T(
		"UPDATE [usr_profile] SET"
		"  usr_rol_id = %d, usr_sta_id = %d, usr_prf_name = '%s', usr_prf_pwd = '%s', "
		"usr_prf_date ='%s', usr_prf_exp_date = '%s', usr_prf_exp_pwd = '%s' "
		"WHERE usr_prf_id = '%s';"
	);
	CAtlString cs_qry; cs_qry.Format(
		lp_sz_pat,
		_usr.Role().Id(), _usr.State().Id(), _usr.Name(), _usr.Pwd(), _T(""), _T(""), _T(""), _usr.Guid()
	);

	HRESULT hr_ = m_error;

	CDbQuery qry_;
	hr_ = qry_.Set((LPCTSTR)cs_qry);
	if (FAILED(hr_))
		return (m_error = qry_.Error());

	hr_ = TQuery::Update(qry_);

	return m_error;
}

HRESULT       CQuery_Users::Get (CUser_Enum& _usrs) {
	m_error << __MODULE__ << S_OK;

	CDbQuery qry_;
	HRESULT hr_ = qry_.Set(_T(
		"SELECT "
		"usr_prf_id  ,  usr_rol_id, usr_sta_id, usr_prf_name, usr_prf_pwd, usr_prf_date, usr_prf_exp_date, usr_prf_exp_pwd, "
		"acc_rol_name,  acc_sta_name "
		"FROM [usr_profile] "
		"INNER JOIN [acc_role] ON usr_profile.usr_rol_id = acc_role.acc_rol_id "
		"INNER JOIN [acc_state] ON usr_profile.usr_sta_id = acc_state.acc_sta_id "
		"ORDER BY 4 ASC;"
	));
	if (FAILED(hr_))
		return (m_error = qry_.Error());

	static LPCTSTR lp_sz_f_nm[] = {
		_T("usr_prf_id" )      , _T("usr_rol_id"), _T("usr_sta_id"), _T("usr_prf_name"), _T("usr_prf_pwd"), _T("usr_prf_date"),
		_T("usr_prf_exp_date" ), _T("usr_prf_exp_pwd" ), _T("acc_rol_name"), _T("acc_sta_name")
	};
	static CDataType::_e e_types[] = {
		CDataType::eText, CDataType::eInteger, CDataType::eInteger, CDataType::eText, CDataType::eText, CDataType::eText,
		CDataType::eText, CDataType::eText   , CDataType::eText   , CDataType::eText
	};

	CDataTable tab_(_T("usr_profile"));

	for (INT i_ = 0; i_ < _countof(lp_sz_f_nm) && i_ < _countof(e_types); i_++) {
		tab_.Fields().Append(e_types[i_], lp_sz_f_nm[i_]);
	}

	qry_.Tables().Append(tab_);

	TRecordsetAsVars v_set;

	hr_ = TQuery::Select(qry_, v_set);
	if (FAILED(hr_))
		return hr_;

	TRecordsetAsText t_set;
	hr_ = CRecordAdapter::VarsToText(v_set, t_set);
	if (FAILED(hr_))
		m_error = hr_;

	hr_ = _usrs.Set(t_set);
	if (FAILED(hr_))
		m_error = _usrs.Error();

	return m_error;
}

HRESULT       CQuery_Users::Rem (const CUser& _usr) {
	m_error << __MODULE__ << S_OK;

	if (_usr.Is() == false)
		return ((m_error = E_INVALIDARG) = _T("User object is invalid or incomplete;"));

	static LPCTSTR lp_sz_pat = _T(
		"DELETE FROM [usr_profile] WHERE usr_prf_id = '%s'";
	);
	CAtlString cs_qry; cs_qry.Format(
		lp_sz_pat, _usr.Guid()
	);

	HRESULT hr_ = m_error;

	CDbQuery qry_;
	hr_ = qry_.Set((LPCTSTR)cs_qry);
	if (FAILED(hr_))
		return (m_error = qry_.Error());

	hr_ = TQuery::Remove(qry_);

	return m_error;
}