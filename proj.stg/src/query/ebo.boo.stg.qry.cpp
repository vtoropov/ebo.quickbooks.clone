/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Dec-2018 at 9:35:34p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is USB Drive Detective (bitsphereinc.com) desktop app local SQLite database query interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 21-Oct-2019 at 11:50:07a, UTC+7, Novosibirsk, Light Coloured, Monday;
*/
#include "StdAfx.h"
#include "ebo.boo.stg.qry.h"
#include "ebo.boo.stg.acc.h"
#include "ebo.boo.stg.loc.h"

using namespace ebo::boo::data;

#include "generic.stg.lt.get.h"
#include "generic.stg.lt.put.h"

using namespace shared::sql;
using namespace shared::stg;
using namespace shared::stg::lt;

/////////////////////////////////////////////////////////////////////////////

CQuery_Generic:: CQuery_Generic(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CQuery_Generic::~CQuery_Generic(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CQuery_Generic::Error  (void) const { return m_error; }
HRESULT       CQuery_Generic::Insert (const CDbQuery& _qry) {
	m_error << __MODULE__ << S_OK;

	if (_qry.IsValid() == false)
		return (m_error = _qry.Error());

	HRESULT hr_ = m_error;

	CDbConnector conn_;
	if (conn_.IsOpen() == false)
		hr_ = conn_.Open(CDbLocator_Master());
	if (FAILED(hr_))
		return (m_error = conn_.Error());

	CDbWriter put_(conn_);
	hr_ = put_.Insert(_qry);
	if (FAILED(hr_))
		return (m_error = put_.Error());

	return m_error;
}
HRESULT       CQuery_Generic::Remove (const CDbQuery& _qry)  {
	m_error << __MODULE__ << S_OK;

	if (_qry.IsValid() == false)
		return (m_error = _qry.Error());

	HRESULT hr_ = m_error;

	CDbConnector conn_;
	if (conn_.IsOpen() == false)
		hr_ = conn_.Open(CDbLocator_Master());
	if (FAILED(hr_))
		return (m_error = conn_.Error());

	CDbWriter put_(conn_);
	hr_ = put_.Delete(_qry);
	if (FAILED(hr_))
		return (m_error = put_.Error());

	return m_error;
}
HRESULT       CQuery_Generic::Select (const CDbQuery& _qry, TRecordsetAsVars& _v_result) {
	m_error << __MODULE__ << S_OK;
	
	if (_qry.IsValid() == false)
		return (m_error = _qry.Error());

	if (_v_result.empty() == false)
		_v_result.clear();

	HRESULT hr_ = m_error;

	CDbConnector conn_;
	if (conn_.IsOpen() == false)
		hr_ = conn_.Open(CDbLocator_Master());
	if (FAILED(hr_))
		return (m_error = conn_.Error());

	CDbReader get_(conn_);
	hr_ = get_.Recordset(_qry, _v_result);
	if (FAILED(hr_))
		return (m_error = get_.Error());

	return m_error;
}
HRESULT       CQuery_Generic::Update (const CDbQuery& _qry) {
	m_error << __MODULE__ << S_OK;

	if (_qry.IsValid() == false)
		return (m_error = _qry.Error());

	HRESULT hr_ = m_error;

	CDbConnector conn_;
	if (conn_.IsOpen() == false)
		hr_ = conn_.Open(CDbLocator_Master());
	if (FAILED(hr_))
		return (m_error = conn_.Error());

	CDbWriter put_(conn_);
	hr_ = put_.Insert(_qry);
	if (FAILED(hr_))
		return (m_error = put_.Error());

	return m_error;
}