/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Sep-2018 at 10:18:12p, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
	This is USB Drive Detective (bitsphereinc.com) desktop application sql data provider base interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 20-Oct-2019 at 6:30:35p, UTC+7, Novosibirsk, Light Coloured, Sunday;
*/
#include "StdAfx.h"
#include "ebo.boo.stg.acc.h"
#include "ebo.boo.stg.loc.h"

using namespace ebo::boo::data;

#include "generic.stg.lt.acc.h"
#include "generic.stg.lt.get.h"
#include "generic.stg.lt.put.h"

using namespace shared::sql;
using namespace shared::stg::lt;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace data { namespace _impl {
	CDbObject  &  CSqlDataLocal_Obj(void) { static CDbObject   db_obj(CDataSchemaOpt::eDoNotCare); return db_obj; }
	CDbAccessor&  CSqlDataLocal_Acc(void) { static CDbAccessor db_acc(CSqlDataLocal_Obj()); return db_acc; }
	CDbStorage &  CSqlDataLocal_Stg(void) { static CDbStorage  db_stg; return db_stg; }
}}}}
using namespace ebo::boo::data::_impl;
/////////////////////////////////////////////////////////////////////////////

CDbConnector:: CDbConnector(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CDbConnector::~CDbConnector(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDbConnector::Close(VOID) {
	m_error << __MODULE__ << S_OK;

	CDbObject&   db_obj = CSqlDataLocal_Obj();
	CDbAccessor& access = CSqlDataLocal_Acc();

	HRESULT hr_ = m_error;

	if (access.IsOpen()) {
		hr_ = access.Close();
		if (FAILED(hr_))
			return (m_error = access.Error());
	}

	hr_ = db_obj.Connection().Destroy();
	if (FAILED(hr_))
		m_error = db_obj.Connection().Error();

	return m_error;
}

TErrorRef     CDbConnector::Error(void) const { return m_error; }

bool          CDbConnector::IsOpen(void) const { return CSqlDataLocal_Acc().IsOpen(); }

HRESULT       CDbConnector::Open  (const ebo::boo::data::CDbLocator& _loc) {
	m_error << __MODULE__ << S_OK;

	if (this->IsOpen())
		return (m_error = (DWORD)ERROR_ALREADY_INITIALIZED);

	CAtlString cs_path  = _loc;
	if (cs_path.IsEmpty() == true)
		return (m_error = _loc.Error());

	HRESULT hr_ = m_error;

	CDbObject  & db_obj = CSqlDataLocal_Obj();
	CDbAccessor& access = CSqlDataLocal_Acc();
	CDbStorage & db_stg = CSqlDataLocal_Stg();

	db_stg.Locator() = _loc;
	//
	// a connection must be checked first before opening;
	//
	if (db_obj.Connection().IsValid() == false)
		hr_ = db_obj.Connection().Create();
	if (FAILED(hr_))
		return (m_error = db_obj.Connection().Error());

	hr_ = access.Locator().Parse(
		_loc.Folder(), _loc.File()
	);
	if (FAILED(hr_))
		return (m_error = access.Locator().Error());
	const bool b_no_schema = true;
	hr_ = access.Open(b_no_schema);
	if (FAILED(hr_))
		return (m_error = access.Error());

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////
const
CDbObject&    CDbConnector::DbObject(void) const { return CSqlDataLocal_Obj(); }
CDbObject&    CDbConnector::DbObject(void)       { return CSqlDataLocal_Obj(); }

/////////////////////////////////////////////////////////////////////////////

CDbConnector::operator       TErrorRef (void) const { return m_error; }
CDbConnector::operator const CDbObject&(void) const { return this->DbObject(); }
CDbConnector::operator       CDbObject&(void)       { return this->DbObject(); }

/////////////////////////////////////////////////////////////////////////////

CDbStorage:: CDbStorage(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CDbStorage::~CDbStorage(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CDbStorage::Error  (void) const { return m_error; }
bool        CDbStorage::IsValid(void) const { return this->Locator().IsValid(); }
const
TDbLocator& CDbStorage::Locator(void) const { return m_locator; }
TDbLocator& CDbStorage::Locator(void)       { return m_locator; }