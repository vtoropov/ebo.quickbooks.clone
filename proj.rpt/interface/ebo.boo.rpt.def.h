#ifndef _EBOBOORPTDEF_H_4ABC6B33_9787_4FF0_988C_E4E7EFE49B80_INCLUDED
#define _EBOBOORPTDEF_H_4ABC6B33_9787_4FF0_988C_E4E7EFE49B80_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Nov-2018 at 7:50:10a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is USB Drive Detective (bitsphereinc.com) desktop app report generic interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 28-Oct-2019 at 5:40:51a, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "shared.gen.sys.err.h"
#include "generic.stg.rec.h"

namespace ebo { namespace boo { namespace reports {

	using shared::sys_core::CError;
	using shared::stg::TRecordAsText;
	using shared::stg::TRecordsetAsText;

	class CHtmRptType {
	public:
		enum _std : DWORD {
			eProfitAndLoss   = 0x0,   // shows HTML content of report in browser component; default;
		};

		enum _alias : DWORD {
			e_pal            = _std::eProfitAndLoss  ,
		};
	};

}}}

#endif/*_EBOBOORPTDEF_H_4ABC6B33_9787_4FF0_988C_E4E7EFE49B80_INCLUDED*/