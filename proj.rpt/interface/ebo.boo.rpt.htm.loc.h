#ifndef _EBOBOORPTHTMLOC_H_1BF85DDB_E030_48AD_8D82_C4602FAA50FB_INCLUDED
#define _EBOBOORPTHTMLOC_H_1BF85DDB_E030_48AD_8D82_C4602FAA50FB_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Nov-2019 at 01:16:12a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Pack personal account HTML-based report locator interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.web.defs.h"
#include "ebo.boo.rpt.def.h"

namespace ebo { namespace boo { namespace reports {

	using shared::sys_core::CError  ;

	using ex_ui::web::CUrlLocator   ;
	using ex_ui::web::CUrlLocateType;

	class CHtmLocator {
	private:
		CError     m_error;

	public:
		 CHtmLocator (void) ;
		~CHtmLocator (void) ;

	public:
		TErrorRef  Error (void) const;
		CAtlString Locate(const CHtmRptType::_std);
	};

}}}

#endif/*_EBOBOORPTHTMLOC_H_1BF85DDB_E030_48AD_8D82_C4602FAA50FB_INCLUDED*/