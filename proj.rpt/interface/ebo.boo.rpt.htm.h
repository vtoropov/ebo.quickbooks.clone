#ifndef _EBOBOORPTHTM_H_3A0E8C9B_1620_4133_89CF_D2E26CF190A5_INCLUDED
#define _EBOBOORPTHTM_H_3A0E8C9B_1620_4133_89CF_D2E26CF190A5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Sep-2018 at 8:49:21a, UTC+7, Novosibirsk, Rodniki, Tulenina, Saturday;
	This is USB Drive Detective (bitsphereinc.com) desktop app HTML data provider interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 28-Oct-2019 at 5:26:40a, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "shared.gen.sys.err.h"
#include "shared.web.browser.h"
#include "shared.web.defs.h"

#include "ebo.boo.rpt.def.h"
#include "ebo.boo.rpt.htm.loc.h"

namespace ebo { namespace boo { namespace reports {

	using shared::sys_core::CError  ;
	using ex_ui::web::CWebBrowser   ;

	typedef ::std::vector<CAtlString>  TRptParams;

	class CHtmlRptParams {
	private:
		TRptParams   m_prms;

	public:
		 CHtmlRptParams(void);
		~CHtmlRptParams(void);

	public:
		INT      Count(void) const;
		LPCTSTR  Param(const INT _ndx) const;

	public:
		CHtmlRptParams& operator+= (LPCTSTR);
		LPCTSTR
		operator [] (const INT _ndx) const;
		operator const TRptParams&  (void) const;
	};

	class CHtmlReportMan {
	private:
		CWebBrowser& m_browser;
		mutable
		CError       m_error;

	public:
		 CHtmlReportMan(CWebBrowser&);
		~CHtmlReportMan(void);

	public:
		HRESULT         Content(const CHtmRptType::_std  , LPCTSTR _lp_sz_param = NULL);
		HRESULT         Content(const CHtmRptType::_std  ,
		                        const CHtmlRptParams&    , const TRecordsetAsText& _data);
		TErrorRef       Error  (void) const;
		CAtlString      URL    (const CHtmRptType::_std) const;
	};
}}}

#endif/*_EBOBOORPTHTM_H_3A0E8C9B_1620_4133_89CF_D2E26CF190A5_INCLUDED*/