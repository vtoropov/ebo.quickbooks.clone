/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Sep-2018 at 8:54:14a, UTC+7, Novosibirsk, Rodniki, Tulenina, Saturday;
	This is USB Drive Detective (bitsphereinc.com) desktop app HTML data provider interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 28-Oct-2019 at 5:50:07a, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include "StdAfx.h"
#include "ebo.boo.rpt.htm.h"

using namespace ebo::boo::reports;
using namespace ex_ui::web;

/////////////////////////////////////////////////////////////////////////////

CHtmlRptParams:: CHtmlRptParams(void) {}
CHtmlRptParams::~CHtmlRptParams(void) {}

/////////////////////////////////////////////////////////////////////////////

INT       CHtmlRptParams::Count(void) const { return static_cast<INT>(m_prms.size()); }
LPCTSTR   CHtmlRptParams::Param(const INT _ndx) const {
	if (0 > _ndx || _ndx > this->Count() - 1)
		return NULL;
	else
		return m_prms[_ndx].GetString();
}

/////////////////////////////////////////////////////////////////////////////

CHtmlRptParams& CHtmlRptParams::operator+= (LPCTSTR _v) {
	CAtlString cs_prm(_v);
	try {
		m_prms.push_back(cs_prm);
	}
	catch (::std::bad_alloc&) {}
	return *this;
}

LPCTSTR
CHtmlRptParams::operator [] (const INT _ndx) const { return this->Param(_ndx); }

CHtmlRptParams::operator const TRptParams&  (void) const { return m_prms; }

/////////////////////////////////////////////////////////////////////////////

CHtmlReportMan:: CHtmlReportMan(CWebBrowser& _browser) : m_browser(_browser) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CHtmlReportMan::~CHtmlReportMan(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CHtmlReportMan::Content(const CHtmRptType::_std _type, LPCTSTR _lp_sz_param) {
	_type; _lp_sz_param;
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = m_error; hr_;

	switch (_type) {
	case CHtmRptType::eProfitAndLoss : {} break;
	default:
		m_error = DISP_E_TYPEMISMATCH;
	}

	return m_error;
}

HRESULT         CHtmlReportMan::Content(const CHtmRptType::_std _type,
                                        const CHtmlRptParams& _params, const TRecordsetAsText& _data) {
	m_error << __MODULE__ << S_OK; _data; _params;

	switch (_type) {
	case CHtmRptType::eProfitAndLoss : {} break;
	default:
		m_error = DISP_E_TYPEMISMATCH;
	}

	return m_error;
}

TErrorRef       CHtmlReportMan::Error  (void) const { return m_error; }

CAtlString      CHtmlReportMan::URL    (const CHtmRptType::_std _type) const {
	_type;
	m_error << __MODULE__ << S_OK;

	CHtmLocator loc_;

	CAtlString cs_url;
	switch (_type) {
	case CHtmRptType::eProfitAndLoss : {
		cs_url = loc_.Locate(_type); if (cs_url.IsEmpty()) m_error = loc_.Error();
		} break;
	default:
		m_error = DISP_E_TYPEMISMATCH;
	}
	return cs_url;
}