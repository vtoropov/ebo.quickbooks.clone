/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Nov-2019 at 01:40:51a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Pack personal account HTML-based report locator interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.rpt.htm.loc.h"
#include "ebo.boo.temp.rpt.res.h"

using namespace ebo::boo::reports;

/////////////////////////////////////////////////////////////////////////////

CHtmLocator:: CHtmLocator(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CHtmLocator::~CHtmLocator(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CHtmLocator::Error (void) const { return m_error; }
CAtlString CHtmLocator::Locate(const CHtmRptType::_std _type){
	m_error << __MODULE__ << S_OK;
	UINT n_res_id = 0;
	switch (_type) {
	case CHtmRptType::eProfitAndLoss: n_res_id = IDS_EBO_BOO_RPT_HTML_URL_PAL; break;
	}
	if (n_res_id == 0) {
		m_error  = __DwordToHresult(ERROR_INVALID_DATATYPE);
		return CAtlString();
	}

	CAtlString cs_url;
	    cs_url.LoadString(n_res_id);
	if (cs_url.IsEmpty() == true) {
		m_error = __DwordToHresult(ERROR_RESOURCE_NAME_NOT_FOUND);
		return cs_url;
	}

	CUrlLocator u_loc_(CUrlLocateType::eHostExecutable);
	HRESULT hr_ = u_loc_.URL((LPCTSTR)cs_url, cs_url);
	if (FAILED(hr_))
		m_error = u_loc_.Error();

	return cs_url;
}

/////////////////////////////////////////////////////////////////////////////