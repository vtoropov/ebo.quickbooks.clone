/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Oct-2018 at 7:59:44a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is USB Drive Detective (bitsphereinc.com) desktop app HTML report data base interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 29-Oct-2019 at 4:39:30p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "ebo.boo.rpt.htm.bas.h"

using namespace ebo::boo::reports;
using namespace ex_ui::web;

#include "shared.gen.str.h"
#include "shared.gen.sys.date.h"
#include "shared.gen.app.obj.h"

using namespace shared::common;
using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

CHtmlUrlLocator:: CHtmlUrlLocator(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CHtmlUrlLocator::~CHtmlUrlLocator(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CHtmlUrlLocator::Error(void) const { return m_error; }

CAtlString    CHtmlUrlLocator::Locate(const CHtmRptType::_std _type) {
	m_error << __MODULE__ << S_OK;
	
	UINT n_res_id = 0;

	switch (_type) {
	case 0 :
	default:
		m_error = DISP_E_MEMBERNOTFOUND;
	}

	if (m_error.Is())
		return CAtlString();

	CAtlString cs_url; cs_url.LoadString(n_res_id);
	if (cs_url.IsEmpty() == true) {
		m_error = (DWORD)ERROR_RESOURCE_NAME_NOT_FOUND;
		return cs_url;
	}
	CUrlLocator u_loc_(CUrlLocateType::eHostExecutable);
	HRESULT hr_ = u_loc_.URL((LPCTSTR)cs_url, cs_url);
	if (FAILED(hr_))
		m_error = u_loc_.Error();

	return cs_url;
}

/////////////////////////////////////////////////////////////////////////////

CHtmlGenBase:: CHtmlGenBase(LPCTSTR _lp_sz_derived) { m_error.Source(_lp_sz_derived); }
CHtmlGenBase::~CHtmlGenBase(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CHtmlGenBase::Error  (void) const { return m_error; }

/////////////////////////////////////////////////////////////////////////////

CHtmlGenParser:: CHtmlGenParser(LPCTSTR _lp_sz_derived) : TBase(_lp_sz_derived) {}
CHtmlGenParser::~CHtmlGenParser(void) {}

/////////////////////////////////////////////////////////////////////////////

CAtlString    CHtmlGenParser::Status(void) const {
	m_error << __MODULE__ << S_OK;

	CAtlString cs_stat;

	if (cs_stat.IsEmpty() == false) {
	}
	else {
		m_error = (DWORD)ERROR_RESOURCE_NAME_NOT_FOUND;
		return cs_stat;
	}

	return cs_stat;
}

/////////////////////////////////////////////////////////////////////////////

CHtmlTableParser:: CHtmlTableParser(void) : TParser(_T("CHtmlTableParser")) {}
CHtmlTableParser::~CHtmlTableParser(void) {}

/////////////////////////////////////////////////////////////////////////////

CAtlString    CHtmlTableParser::Caption(const UINT _u_res) const {
	m_error << __MODULE__ << S_OK;
	CAtlString cs_cap; cs_cap.LoadString(_u_res);
	if (cs_cap.IsEmpty() == true) {
		m_error = (DWORD)ERROR_RESOURCE_NAME_NOT_FOUND;
		return cs_cap;
	}
	cs_cap = this->Caption((LPCTSTR)cs_cap);
	return cs_cap;
}

CAtlString    CHtmlTableParser::Caption(LPCTSTR _lpsz_cap) const {
	m_error << __MODULE__ << S_OK;;
	if (NULL == _lpsz_cap || !::_tcslen(_lpsz_cap)) {
		m_error = E_INVALIDARG;
		return CAtlString();
	}
	CAtlString cs_cap;
	CAtlString cs_pat; cs_pat.LoadString(0);
	if (cs_pat.IsEmpty() == false) {
		cs_cap.Format(
			(LPCTSTR)cs_pat, _lpsz_cap
		);
	}
	else
		m_error =(DWORD)ERROR_RESOURCE_NAME_NOT_FOUND;
	return cs_cap;
}

CAtlString    CHtmlTableParser::Header (const THtmlData& _t_data) const {
	m_error << __MODULE__ << S_OK;

	CAtlString cs_pat_th; cs_pat_th.LoadString(0);
	CAtlString cs_pat_tr; cs_pat_tr.LoadString(0);
	if (cs_pat_th.IsEmpty() == true ||
	    cs_pat_tr.IsEmpty() == true) {
		m_error =(DWORD)ERROR_RESOURCE_NAME_NOT_FOUND;
		return CAtlString();
	}
	CAtlString cs_cols;
	for (size_t i_ = 0; i_ < _t_data.size(); i_++) {
		CAtlString cs_col;
		cs_col.Format(
			(LPCTSTR) cs_pat_th, _t_data[i_].GetString()
		);
		cs_cols += cs_col;
	}

	CAtlString cs_head;
	cs_head.Format(
		(LPCTSTR)cs_pat_tr, cs_cols.GetString()
	);

	return cs_head;
}

CAtlString    CHtmlTableParser::Header (const UINT _u_res) const {
	m_error << __MODULE__ << S_OK;
	CAtlString cs_head; cs_head.LoadString(_u_res);
	if (cs_head.IsEmpty() == true) {
		m_error = (DWORD)ERROR_RESOURCE_NAME_NOT_FOUND;
		return cs_head;
	}

	const TStdStrVec v_cols = CStdString((LPCTSTR)cs_head).Split(_T(";"));
	THtmlData v_head = v_cols;

	cs_head = this->Header(v_head);
	return cs_head;
}

CAtlString    CHtmlTableParser::Header (LPCTSTR _lpsz_data[], const UINT uSize) const {
	m_error << __MODULE__ << S_OK;
	if (NULL == _lpsz_data || uSize < 1) {
		m_error = E_INVALIDARG;
		return CAtlString();
	}

	CAtlString cs_head;
	THtmlData   v_data;
	try {
		for (UINT i_ = 0; i_ < uSize; i_++)
			v_data.push_back(CAtlString(_lpsz_data[i_]));
		cs_head = this->Header(v_data);
	}
	catch(::std::bad_alloc&) {
		m_error = E_OUTOFMEMORY;
	}

	return cs_head;
}

CAtlString    CHtmlTableParser::Outline(void) const {
	m_error << __MODULE__ << S_OK;
	CAtlString cs_out; cs_out.LoadString(0);
	if (cs_out.IsEmpty() == true)
		m_error = (DWORD)ERROR_RESOURCE_NAME_NOT_FOUND;

	return cs_out;
}

CAtlString    CHtmlTableParser::Row    (const TRecordAsText& _rec) const {
	m_error << __MODULE__ << S_OK;

	CAtlString cs_pat_td; cs_pat_td.LoadString(0);
	CAtlString cs_pat_tr; cs_pat_tr.LoadString(0);
	if (cs_pat_td.IsEmpty() == true ||
		cs_pat_tr.IsEmpty() == true) {
		m_error =(DWORD)ERROR_RESOURCE_NAME_NOT_FOUND;
		return CAtlString();
	}
	CAtlString cs_data;
	for (size_t i_ = 0; i_ < _rec.size(); i_++) {
		CAtlString cs_td;
		cs_td.Format(
			(LPCTSTR)cs_pat_td, _rec[i_].GetString()
		);
		cs_data += cs_td;
	}
	CAtlString cs_row;
	cs_row.Format(
		(LPCTSTR)cs_pat_tr, cs_data.GetString()
	);
	return cs_row;
}