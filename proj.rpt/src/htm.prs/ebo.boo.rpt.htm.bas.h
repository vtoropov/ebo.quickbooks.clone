#ifndef _EBOBOORPTHTMBAS_H_C7BC47C5_199C_45CE_A860_076BBC496A7A_INCLUDED
#define _EBOBOORPTHTMBAS_H_C7BC47C5_199C_45CE_A860_076BBC496A7A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Oct-2018 at 7:36:51a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is USB Drive Detective (bitsphereinc.com) desktop app HTML report data base interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 29-Oct-2019 at 4:39:30p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "shared.gen.sys.err.h"
#include "shared.web.browser.h"
#include "shared.web.defs.h"
#include "ebo.boo.rpt.def.h"

namespace ebo { namespace boo { namespace reports {

	using shared::sys_core::CError;
	using ex_ui::web::CWebBrowser ;

	interface IHtmlReport_Func {
		virtual HRESULT      Generate(CWebBrowser&) PURE;
		virtual CAtlString   URL     (void) const   PURE;
	};

	typedef ::std::vector<CAtlString>  THtmlData;

	class CHtmlUrlLocator {
	private:
		CError        m_error;
	public:
		 CHtmlUrlLocator(void);
		~CHtmlUrlLocator(void);

	public:
		TErrorRef     Error(void) const;
		CAtlString    Locate(const CHtmRptType::_std);
	};

	class CHtmlGenBase {
	protected:
		mutable
		CError        m_error;

	protected:
		 CHtmlGenBase(LPCTSTR _lp_sz_derived);
		~CHtmlGenBase(void);

	public:
		TErrorRef     Error  (void) const;    // gets last error of the HTML generic base;
	};

	class CHtmlGenParser : public CHtmlGenBase {
	                      typedef CHtmlGenBase TBase;
	protected:
		 CHtmlGenParser(LPCTSTR _lp_sz_derived);
		~CHtmlGenParser(void);

	public:
		CAtlString    Status (void) const;    // creates report status info section;
	};

	class CHtmlTableParser : public CHtmlGenParser {
                            typedef CHtmlGenParser TParser;
	protected:
		 CHtmlTableParser(void);
		~CHtmlTableParser(void);

	protected:
		CAtlString    Caption(const UINT _u_res) const;                      // creates table caption by resource identifier;
		CAtlString    Caption(LPCTSTR _lpsz_cap) const;                      // creates table caption;
		CAtlString    Header (const UINT _u_res) const;                      // creates table header by resource identifier;
		CAtlString    Header (const THtmlData& _t_data) const;               // creates table header;
		CAtlString    Header (LPCTSTR _lpsz_data[], const UINT uSize) const; // creates table header;
		CAtlString    Outline(void) const;                                   // gets HTML table outer/outline tags pattern;
		CAtlString    Row    (const TRecordAsText& _rec) const;              // creates a table row from text data provided;
	};
}}}

#endif/*_EBOBOORPTHTMBAS_H_C7BC47C5_199C_45CE_A860_076BBC496A7A_INCLUDED*/