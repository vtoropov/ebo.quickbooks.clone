/*
	Created Tech_dog (ebontrop@gmail.com) on 4-Sep-2018 at 10:08:54p, UTC+7, Novosibirsk, Rodniki, Tuesday;
	This is USB Drive Detective (bitsphereinc.com) desktop app status bar control interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 27-Oct-2019 at 1:32:54p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "StdAfx.h"
#include "ebo.boo.frm.main.sta.h"
#include "ebo.boo.res.h"

using namespace ebo::boo::gui;
using namespace ex_ui::controls::std;

#define _idle_message _T("Ready")

/////////////////////////////////////////////////////////////////////////////

CMainForm_Sta:: CMainForm_Sta(void) : m_host(NULL) {}
CMainForm_Sta::~CMainForm_Sta(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CMainForm_Sta::Create (HWND hMainFrame) {

	HRESULT hr_ = S_OK;

	CStatusBar& sta_bar = m_sta;

	if (sta_bar.Window().IsWindow() == TRUE)
		return (hr_ = HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED));

	if (::IsWindow(hMainFrame) == FALSE)
		return (hr_ = OLE_E_INVALIDHWND);

	m_host = hMainFrame;

	hr_ = sta_bar.Create(m_host, IDC_EBO_BOO_MAIN_FRM_STA);
	if (FAILED(hr_))
		return hr_;

	const UINT img_id[] = {
		IDR_EBO_BOO_MAIN_FRM_STAx16, 0, IDR_EBO_BOO_MAIN_FRM_STAx16_pc, 0, IDR_EBO_BOO_MAIN_FRM_STAx16_rs, 0
	};

	const LONG l_width[] = {
		25, 470, 25, 190, 25, 150
	};

	LPCTSTR lpsz_title[] = {
		NULL, _idle_message, NULL, _T("PC: #n/a"), NULL, _T("Rec(s): 0")
	};

	for (INT i_ = 0; i_ < _countof(img_id) && i_ < _countof(l_width) && i_ < _countof(lpsz_title); i_++) {

		CStaPanel* sta_pan = NULL;
		hr_ = CStaPanel::Create(sta_pan);
		if (FAILED(hr_))
			continue;

		if (img_id[i_]) {
			CStaImages& sta_img = sta_pan->Images();
			hr_ = sta_img.Create(img_id[i_]);
			if (SUCCEEDED(hr_) && 0 == i_) {
				sta_img.Index(CStaMsgType::eInfo   , 0);
				sta_img.Index(CStaMsgType::eWarning, 1);
				sta_img.Index(CStaMsgType::eError  , 2);
				sta_img.Index(CStaMsgType::eWaiting, 4);
			}
			sta_img.Active(0);
			sta_pan->Style(SBT_OWNERDRAW);
		}
		if (NULL != lpsz_title[i_]) sta_pan->Text(lpsz_title[i_]);
		sta_pan->Width(l_width[i_]);
		sta_pan->Index(i_);
		sta_pan->Owner(sta_bar); // it's very important for calling pane re-draw event, otherwise, it won't work;

		hr_ = sta_bar.Panels().Insert(sta_pan);
		if (FAILED(hr_)) {
			sta_pan->Destroy(); sta_pan = NULL;
		}
	}
	hr_ = sta_bar.Panels().Apply();
	if (SUCCEEDED(hr_) || true) {
		CWindow w_sta = sta_bar.Window();
		w_sta.RedrawWindow(NULL, NULL, RDW_ERASE|RDW_INVALIDATE|RDW_ERASENOW|RDW_UPDATENOW);
	}

	if (SUCCEEDED(hr_)) {
	}
	return  hr_;
}

HRESULT       CMainForm_Sta::Destroy  (void)       { return m_sta.Destroy(); }
const UINT    CMainForm_Sta::Id       (void) const { return m_sta.Id(); }
bool          CMainForm_Sta::IsValid  (void) const { return NULL != m_sta.Window().m_hWnd && !!m_sta.Window().IsWindow(); }
bool          CMainForm_Sta::IsVisible(void) const { return (this->IsValid() && !!m_sta.Window().IsWindowVisible()); }
HRESULT       CMainForm_Sta::IsVisible(const bool _v) {

	HRESULT hr_ = S_OK;
	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	m_sta.Window().ShowWindow(_v ? SW_SHOW : SW_HIDE);

	return  hr_;
}

HRESULT       CMainForm_Sta::UpdateLayout(void) {

	HRESULT hr_ = S_OK;

	RECT rc_sta = {0};
	m_host.GetClientRect(&rc_sta);

	hr_ = this->UpdateLayout(rc_sta);

	return  hr_;
}

HRESULT       CMainForm_Sta::UpdateLayout(const RECT& rcArea) {

	HRESULT hr_ = S_OK;
	if (m_host.IsWindow() == FALSE)
		return (hr_ = OLE_E_INVALIDHWND);

	if (m_sta.Window().IsWindow() == FALSE)
		return (hr_ = OLE_E_BLANK);

	if (::IsRectEmpty(&rcArea))
		return (hr_ = OLE_E_INVALIDRECT);

	m_sta.Window().MoveWindow(&rcArea);

	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT       CMainForm_Sta::OnDrawEvent(WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	wParam; lParam; bHandled = FALSE;
	if (NULL != lParam) {
		LPDRAWITEMSTRUCT lpDrawItem = (LPDRAWITEMSTRUCT)lParam;
		if (NULL != lpDrawItem && m_sta.Window().IsWindow())
			m_sta.Update(*lpDrawItem);
	}
	return 0;
}

HRESULT       CMainForm_Sta::SetError   (TErrorRef _error) {

	HRESULT hr_ = S_OK;
	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	hr_ = m_sta.Panels().Panes(_ndx::eMessage, _error.Desc());

	CStaPanel& pane_ = m_sta.Panels().Panel(_ndx::eMarker);
	if (CStaPanels::eNa != pane_.Index()) {
		pane_.Images().Active(
			pane_.Images().Index(CStaMsgType::eError)
		);
		pane_.Draw();
	}
	else
		hr_ = DISP_E_BADINDEX;

	return hr_;
} 

HRESULT       CMainForm_Sta::SetInfo (LPCTSTR _lp_sz) {

	HRESULT hr_ = S_OK;
	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);
	if (NULL == _lp_sz) {
		hr_ = m_sta.Panels().Panes(_ndx::eMessage, _idle_message); // TODO: setting the idle message must be re-viewed;
	}
	else
		hr_ = m_sta.Panels().Panes(_ndx::eMessage, _lp_sz);

	CStaPanel& pane_ = m_sta.Panels().Panel(_ndx::eMarker);
	if (CStaPanels::eNa != pane_.Index() &&
		pane_.Images().Index(CStaMsgType::eInfo) != pane_.Images().Active()) {
		pane_.Images().Active(
			pane_.Images().Index(CStaMsgType::eInfo)
		);
		pane_.Draw();
	}

	return  hr_;
}

HRESULT       CMainForm_Sta::SetRecCount(const UINT _recs) {

	HRESULT hr_ = S_OK;
	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	CAtlString cs_msg;
	cs_msg.Format( _T("Rec(s): %u"), _recs);

	hr_ = m_sta.Panels().Panes(_ndx::eRsCount, cs_msg.GetString());
	return hr_;
}

HRESULT       CMainForm_Sta::SetUserUri (LPCTSTR lpszUri) {

	HRESULT hr_ = S_OK;
	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	hr_ = m_sta.Panels().Panes(_ndx::ePcName, lpszUri);
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

CMainForm_Sta::operator const HWND(void) const { return m_sta.Window(); }