#ifndef _EBOBOOFRMMAINLAY_H_B915121B_1007_4EF9_ADE3_EFD8BB4B3169_INCLUDED
#define _EBOBOOFRMMAINLAY_H_B915121B_1007_4EF9_ADE3_EFD8BB4B3169_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Aug-2018 at 8:10:08a, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop application main form layout interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 27-Oct-2019 at 3:05:30p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "ebo.boo.frm.main.h"

namespace ebo { namespace boo { namespace gui { namespace _impl {

	using ebo::boo::gui::CMainForm;

	class CMainForm_Layout {
	private:
		RECT       m_area;
		CMainForm& m_main;
		
	public:
		CMainForm_Layout(CMainForm& _main);
	public:
		RECT    GetIbanRect (const SIZE _ban);
		RECT    GetRbarRect (const SIZE& _ban_sz) const; // calculates re-bar control position that is dependent on main form banner;
		RECT    GetViewRect (void) const;                // calculates main view panel rectangle;
		VOID    RecalcLayout(void);                      // adjust positions of all UI elements of main window;

	public:
		static RECT   AdjustRect(const RECT& _rc);       // checks a rectangle against boundaries of main monitor work screen area;
		static RECT   CenterArea(const RECT& _rc);
		static RECT   FormDefPlace(void);

	private:
		static RECT   GetAvailableArea(void);
	};
}}}}

#endif/*_EBOBOOFRMMAINLAY_H_B915121B_1007_4EF9_ADE3_EFD8BB4B3169_INCLUDED*/