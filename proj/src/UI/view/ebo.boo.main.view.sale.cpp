/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Nov-2019 at 3:46:38p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app sale view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.main.view.sale.h"
#include "ebo.boo.res.h"
#include "ebo.boo.pag.lay.bas.h"
#include "ebo.boo.pro.svc.dlg.h"
#include "ebo.boo.sal.cst.wzd.h"

using namespace ebo::boo::gui;
using namespace ebo::boo::gui::view;

#include "shared.uix.gdi.renderer.h"

using namespace ex_ui::draw::renderers;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl {

	class CSaleView_Layout {
	private:
		CWindow     m_v_ref;


	public:
		CSaleView_Layout (CWindow& _view) : m_v_ref(_view) {}
	};

}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CSaleView::CSaleViewWnd:: CSaleViewWnd(void) : m_head(*this), m_grid(NULL) {}
CSaleView::CSaleViewWnd::~CSaleViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CSaleView::CSaleViewWnd::OnCreate  (UINT, WPARAM, LPARAM, BOOL& _b_hand) {
	_b_hand;
	const RECT rc_ = Lay_(*this);
	m_head.Create(rc_);
	m_grid = UILayer::CreateGridInstance(*this, 10, IDC_EBO_BOO_FLX_GRD_SALE);
	if (m_grid == NULL)
		return 0;
	
	UILayer::IThemeManager& them_man = m_grid->GetThemeManagerRef();

	const UILayer::ePredefinedTheme e_theme = _out::ThemeMan().GrdTheme();

	m_grid->ApplyObject(
		&(them_man.GetPredefinedTheme(e_theme))
	);

	UILayer::IRowSelector* p_rs_sel = m_grid->GetRowSelector();
	if (p_rs_sel) {
		p_rs_sel->SetVisible(true);
		p_rs_sel->UpdateLayout();

		UILayer::IRowSelectorTheme& rs_them = p_rs_sel->GetTheme();
		UILayer::IColorTheme& rs_cust = rs_them.Customize(e_theme);
		p_rs_sel->ApplyTheme (rs_cust);
	}

	UILayer::ICell* p_act_cell = m_grid->GetActiveCell();
	if (p_act_cell) {
		UILayer::IActiveCellTheme& act_them = p_act_cell->GetTheme();
		UILayer::IColorTheme&  act_cust = act_them.Customize(e_theme);
		p_act_cell->ApplyTheme(act_cust);
		p_act_cell->SetOptions(UILayer::eCellOption::eSingleBorder);
	}
	UILayer::IColumnHeader* p_head = m_grid->GetColumnHeader();
	if (p_head == NULL)
		return 0;

	static LPCTSTR lp_sz_caps[] = {
		_T(" "),
		_T("Date")    , _T("Type")  /*, _T("Doc #")*/   , _T("Customer"),
		_T("Due Date"),
		_T("Balance") , _T("Total")   , _T("Status")  , _T("Action")
	};

	static const UILayer::eColumnHAlignTypes e_col_aligns[] = {
		UILayer::e2_clmnHAlignCenter,
		UILayer::e2_clmnHAlignCenter, UILayer::e2_clmnHAlignLeft/*, UILayer::e2_clmnHAlignLeft*/, UILayer::e2_clmnHAlignLeft ,
		UILayer::e2_clmnHAlignCenter,
		UILayer::e2_clmnHAlignRight , UILayer::e2_clmnHAlignRight, UILayer::e2_clmnHAlignLeft , UILayer::e2_clmnHAlignLeft
	};

	static const UILayer::eColumnDataTypes e_col_types[] = {
		UILayer::e2_clmnDTypeLong   ,
		UILayer::e2_clmnDTypeText   , UILayer::e2_clmnDTypeText  , UILayer::e2_clmnDTypeText  , UILayer::e2_clmnDTypeText ,
		UILayer::e2_clmnDTypeText   ,
		UILayer::e2_clmnDTypeText   , UILayer::e2_clmnDTypeText  , UILayer::e2_clmnDTypeText  , UILayer::e2_clmnDTypeText 
	};

	static const UINT u_col_width[] = { 30, 90, 120/*, 90*/, 140, 90, 100, 100, 100, 140 };
	static const bool u_col_accss[] = {
		false, false, false, false, false, false, false, false, false, false, false
	};

	static const DWORD e_grp_style  = UILayer::e2_GroupStyleImmovable|UILayer::e2_GroupStyleNotUpdatable;

	UINT u_grp_id_tag = 0x0C000000;
	UINT u_col_id_tag = 0x10000000;

	CAtlString cs_cap;

	for ( INT i_ = 0; i_ < _countof(lp_sz_caps  ) && i_ < _countof(e_col_aligns)
	               && i_ < _countof(e_col_types ) && i_ < _countof(u_col_width ); i_++ ){

		UILayer::IColumnGroup* pGroup = p_head->AddGroup(lp_sz_caps[i_]);
		if (pGroup == NULL)
			continue;
		u_grp_id_tag += 1;

		pGroup->SetID   (u_grp_id_tag);
		pGroup->SetStyle(e_grp_style );
		pGroup->SetTag  (u_grp_id_tag);

		UILayer::IColumn* pColumn = pGroup->AddColumn(NULL, ++u_col_id_tag);
		if (pColumn == NULL)
			continue;

		pColumn->SetTag     (u_col_id_tag);
		pColumn->SetHAlign  (e_col_aligns[i_]);
		pColumn->SetDataType(e_col_types [i_]);
		pColumn->SetWidth   (u_col_width [i_]);

		UILayer::__OverlayData& over__  = (*pColumn).AccessOverlay();
		over__.__c__0 = RGB(17, 17, 17);
		over__.__c__1 = RGB(27, 27, 27);

		pColumn->SetLock(true);
		pGroup ->Update();
	}

	m_grid->EnableRowGrouping(true);
	m_grid->SetRows(10);
	m_grid->UpdateLayout(*this);

	return 0;
}

LRESULT   CSaleView::CSaleViewWnd::OnDestroy (UINT, WPARAM, LPARAM, BOOL&) {
	m_head.Destroy();
	UILayer::DestroyGrid_Safe(&m_grid); m_grid = NULL;
	return 0;
}

LRESULT   CSaleView::CSaleViewWnd::OnErase   (UINT, WPARAM _w_dc, LPARAM, BOOL& _b_hand) {
	_b_hand = TRUE; _w_dc;
	static const LRESULT l_is_drawn = 1;
	CZBuffer z_buf((HDC)_w_dc, Lay_(*this));

	z_buf.DrawSolidRect(Lay_(*this), ::_out::ThemeMan().Colours().Dark());
	
	return l_is_drawn;
}

LRESULT   CSaleView::CSaleViewWnd::OnSize    (UINT, WPARAM, LPARAM _lp, BOOL&) {
	m_head.Update();
	const SIZE sz_ = m_head.Layout();
	const RECT rc_ = {0, sz_.cy * 1, LOWORD(_lp), HIWORD(_lp)};
	if (m_grid) {
		m_grid->UpdateLayout(rc_);
		CWindow w_grid = m_grid->GetSafeHwnd();
		w_grid.RedrawWindow();
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CSaleView:: CSaleView(void) : TView(m_wnd) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CSaleView::~CSaleView(void) {}

////////////////////////////////////////////////////////////////////////////

bool        CSaleView::CanAccept(const WORD nCmdId ) const {
	switch (nCmdId) {
	case IDC_EBO_BOO_MNU_CMD_SAL_ADD:
	case IDC_EBO_BOO_MNU_CMD_PRO_ADD:
	case IDC_EBO_BOO_MNU_CMD_SVC_ADD: {
		return true;
	}
	default:
	return false;
	}
}

HRESULT     CSaleView::OnCommand(const WORD nCmdId ) {
	TView::m_error << __MODULE__ << S_OK;

	switch (nCmdId) {
	case IDC_EBO_BOO_MNU_CMD_SAL_ADD: {
		CSaleDocWzd wzd_;
		wzd_.DoModal();
	} break;
	case IDC_EBO_BOO_MNU_CMD_PRO_ADD: {
		CProSvcDlg dlg_;
		dlg_.DoModal(CProdSvc_Type(TProSvcType::e_inventory));
	} break;
	case IDC_EBO_BOO_MNU_CMD_SVC_ADD: {
		CProSvcDlg dlg_;
		dlg_.DoModal(CProdSvc_Type(TProSvcType::e_service));
	} break;
	default:
	(m_error = __DwordToHresult(ERROR_NOT_FOUND)) = _T("Command is not found;");
	}

	return TView::m_error;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CSaleView::Refresh  (void) {
	TView::m_error << __MODULE__ << S_OK;

	HRESULT hr_ = TView::Refresh();
	if (m_wnd.m_grid == NULL)
		return (TView::m_error = OLE_E_BLANK);

	m_wnd.m_head.Update();

	UILayer::IThemeManager& them_man = m_wnd.m_grid->GetThemeManagerRef();
	const UILayer::ePredefinedTheme e_theme = _out::ThemeMan().GrdTheme();

	m_wnd.m_grid->ApplyObject(
		&(them_man.GetPredefinedTheme(e_theme))
	);

	UILayer::IRowSelector* p_rs_sel = m_wnd.m_grid->GetRowSelector();
	if (p_rs_sel) {
		
		UILayer::IRowSelectorTheme& rs_them = p_rs_sel->GetTheme();
		UILayer::IColorTheme& rs_cust = rs_them.Customize(e_theme);
		p_rs_sel->ApplyTheme (rs_cust);
		p_rs_sel->Refresh();
	}

	UILayer::ICell* p_act_cell = m_wnd.m_grid->GetActiveCell();
	if (p_act_cell) {
		UILayer::IActiveCellTheme& act_them = p_act_cell->GetTheme();
		UILayer::IColorTheme&  act_cust = act_them.Customize(e_theme);
		p_act_cell->ApplyTheme(act_cust);
	}

	CWindow w_grid = m_wnd.m_grid->GetSafeHwnd();
	w_grid.RedrawWindow();

	return  hr_;
}