#ifndef _EBOBOOMAINVIEWEMP_H_8BAED3F5_E103_4AFA_B1E3_25DA18F4E117_INCLUDED
#define _EBOBOOMAINVIEWEMP_H_8BAED3F5_E103_4AFA_B1E3_25DA18F4E117_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Oct-2019 at 7:58:06p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app employee view interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.boo.main.view.bas.h"
#include "ebo.boo.view.head.emp.h"
#include "IGridInterface.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using shared::sys_core::CError;
	using ex_ui::controls::IControlEvent;

	class CEmpView : public CView_Base , public IControlEvent {
	                typedef CView_Base TView;
	private:
		class CEmpViewWnd : public  CViewWnd_Base {
		                   typedef  CViewWnd_Base TPane;
			friend class CEmpView;
		private:
			UILayer::IGrid* m_grid;
			CHeader_Emp     m_head;
		public:
			 CEmpViewWnd(IControlEvent&);
			~CEmpViewWnd(void);
		private:
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
		};

	private:
		CEmpViewWnd   m_wnd  ;

	public:
		 CEmpView(void);
		~CEmpView(void);

	public:
		HRESULT     Refresh  (void) override sealed;

	private:    // IControlEvent
		HRESULT    IControlEvent_OnClick(const UINT ctrlId) override sealed;
		HRESULT    IControlEvent_OnClick(const UINT ctrlId, const LONG_PTR nData) override sealed;
	};

}}}}

#endif/*_EBOBOOMAINVIEWEMP_H_8BAED3F5_E103_4AFA_B1E3_25DA18F4E117_INCLUDED*/