#ifndef _EBOBOOMAINVIEWSACC_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
#define _EBOBOOMAINVIEWSACC_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Dec-2019 at 5:44:43p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app accountant view interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.boo.main.view.bas.h"
#include "ebo.boo.view.head.acc.h"
#include "IGridInterface.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using shared::sys_core::CError;

	class CAccView : public CView_Base {
	                typedef CView_Base TView;
	private:
		class CAccViewWnd : public  CViewWnd_Base {
		                    typedef CViewWnd_Base TPane;
			friend class CAccView;
		private:
			UILayer::IGrid* m_grid;
			CHeader_Acc     m_head;
		public:
			 CAccViewWnd(void);
			~CAccViewWnd(void);
		private:
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
		};

	private:
		CAccViewWnd   m_wnd  ;

	public:
		 CAccView(void);
		~CAccView(void);

	public:
		HRESULT     Refresh  (void) override sealed;
	};

}}}}

#endif/*_EBOBOOMAINVIEWSACC_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED*/