/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Sep-2018 at 10:13:44p, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
	This is USB Drive Detective (bitsphereinc.com) desktop app web-based report view interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 29-Oct-2019 at 6:03:45p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "ebo.boo.main.view.rpt.h"
#include "ebo.boo.res.h"
#include "ebo.boo.pag.lay.bas.h"

using namespace ebo::boo::gui::view;

#include <shellapi.h>
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace view { namespace _impl {

	using ex_ui::web::CWebBrowser;

	class CRptView_Layout {
	private:
		CWebBrowser& m_brw;
		CRptRebar  & m_bar;
		CWindow    & m_wnd;
	public:
		CRptView_Layout(CWindow& _wnd, CRptRebar& _bar, CWebBrowser& _brw) : m_wnd(_wnd), m_bar(_bar), m_brw(_brw) {}

	public:
		HRESULT     OnSize(const RECT _rc_area) {

			m_bar.UpdateLayout(_rc_area);
			const RECT rc_bar = Lay_(m_wnd) = m_bar.Identifier();
			const RECT rc_web = {
				_rc_area.left, rc_bar.bottom, _rc_area.right, _rc_area.bottom
			};

			if (m_brw.IsValid()){
				m_brw.GetAxWindowRef().SetWindowPos(NULL, &rc_web, SWP_NOZORDER|SWP_NOACTIVATE);
			//	m_brw.Refresh();
			}
			return S_OK;
		}
	};

}}}}}
using namespace ebo::boo::gui::view::_impl;
/////////////////////////////////////////////////////////////////////////////

CRptView::CRptViewWnd:: CRptViewWnd(CRptView& _view) : m_browser(*this), m_htm_man(m_browser), m_view(_view) {
	m_error << __MODULE__ << S_OK >> __MODULE__; }
CRptView::CRptViewWnd::~CRptViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CRptView::CRptViewWnd::OnCommand (UINT, WPARAM wParam, LPARAM, BOOL&) {
	LRESULT lResult   = 0;
	const WORD wCmdId = static_cast<WORD>(wParam);
	m_view.OnCommand(wCmdId);
	return  lResult;
}

LRESULT   CRptView::CRptViewWnd::OnCreate  (UINT, WPARAM, LPARAM, BOOL&)
{
	m_error << __MODULE__ << S_OK;

	RECT rc_ = {0};
	TPane::GetClientRect(&rc_);
	
	HRESULT hr_ = m_cmd_bar.Create(*this, rc_);
	if (FAILED(hr_)) {
		(m_error = hr_).Show();
		_out::MainFrmSta().SetError(m_error);
		return 0;
	}

	m_cmd_bar.RecalcLayout();
	const RECT rc_bar = Lay_(*this) = m_cmd_bar.Identifier();
	rc_.top  = rc_bar.bottom;
		
	hr_ = m_browser.Create(*this, rc_);
	if (FAILED(hr_))
		_out::MainFrmSta().SetError(m_browser.Error());
	else
	{
		m_browser.Open(_T(""));
		m_browser.SetInitialized();
	}
	LRESULT lResult = 0;
	return  lResult;
}

LRESULT   CRptView::CRptViewWnd::OnDestroy (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	m_browser.Destroy();

	LRESULT lResult = 0;
	return  lResult;
}

LRESULT   CRptView::CRptViewWnd::OnSize    (UINT, WPARAM, LPARAM lParam, BOOL&)
{
	const RECT area_ = {
		0, 0, LOWORD(lParam), HIWORD(lParam)
	};
	CRptView_Layout(*this, m_cmd_bar, m_browser).OnSize(area_);
	LRESULT lResult = 0;
	return  lResult;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CRptView::CRptViewWnd::BrowserEvent_BeforeNavigate(LPCTSTR lpszUrl) {
	lpszUrl;
	HRESULT hr_ = S_OK;

	CAtlString cs_url(lpszUrl);

	static LPCTSTR lpsz_cmd_pfx = _T("cmd:");

	if (0 == cs_url.Find(lpsz_cmd_pfx)) {
		cs_url = cs_url.Right(cs_url.GetLength() - static_cast<INT>(::_tcslen(lpsz_cmd_pfx)));
		ShellExecute(
			NULL,
			_T("open"),
			cs_url.GetString(),
			NULL   ,
			NULL   ,
			SW_SHOW
		);
		return (hr_ = S_FALSE);
	}
		
	return  hr_;
}

HRESULT   CRptView::CRptViewWnd::BrowserEvent_DocumentComplete(LPCTSTR lpszUrl, const bool bStreamObject) {
	lpszUrl; bStreamObject;
	HRESULT hr_ = S_FALSE;
	if (CHtmRptType::eProfitAndLoss  == m_htm_std) hr_ = m_htm_man.Content(m_htm_std, NULL);
	return  hr_;
}

HRESULT   CRptView::CRptViewWnd::BrowserEvent_OnMouseMessage(const MSG&) {
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CRptView:: CRptView(void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CRptView::~CRptView(void) {}

/////////////////////////////////////////////////////////////////////////////

bool      CRptView::CanAccept(const WORD wCmdId ) const {
	if (this->IsVisible() == false)
		return false;

	return m_wnd.m_cmd_bar.CanAccept(wCmdId);
}

bool      CRptView::IsChecked(const UINT nCmdId) const {
	return m_wnd.m_cmd_bar.IsChecked(nCmdId);
}

bool      CRptView::IsValid  (void) const { return (NULL != m_wnd.m_hWnd && !!m_wnd.IsWindow() && m_wnd.m_browser.IsValid()); }

HRESULT   CRptView::OnCommand(const WORD cmdId) {
	cmdId;
	m_error << __MODULE__ << S_OK;

	_out::SetIdleMessage();

	if (m_wnd.m_cmd_bar.CanAccept(cmdId)) {
		HRESULT hr_ = m_wnd.m_cmd_bar.OnCommand(cmdId);
		if (SUCCEEDED(hr_)) {
			const RECT rc_cli = Lay_(m_wnd);
			hr_ = CRptView_Layout(m_wnd, m_wnd.m_cmd_bar, m_wnd.m_browser).OnSize(rc_cli);
		}
		else {
			m_error = hr_;
		}
		return m_error;
	}

	switch (cmdId) {
	case IDC_EBO_BOO_CMD_VIEW_PRN: {
			HRESULT hr_ = m_wnd.m_browser.Print();
			if (FAILED(hr_)) {
				(m_error = hr_) = _T("Internal error: the report cannot be printed. No report is open.");
				::_out::MainFrmSta().SetError(m_error);
				m_error.Show();
			}
			return (m_error);
		} break;
	case IDC_EBO_BOO_CMD_VIEW_PRE  : {
			HRESULT hr_ = m_wnd.m_browser.PrintPreview();
			if (FAILED(hr_)) {
				m_error = m_wnd.m_browser.Error();
			}
			return (m_error);
		} break;
	case IDC_EBO_BOO_MNU_CMD_RPT_PAL: m_wnd.m_htm_std = CHtmRptType::eProfitAndLoss; break;
	}
	CAtlString cs_rpt_url = m_wnd.m_htm_man.URL(m_wnd.m_htm_std);

	if (SUCCEEDED(m_wnd.m_htm_man.Error().Result())) {

		m_wnd.m_browser.Open(cs_rpt_url);
	}

	return  m_error;
}

/////////////////////////////////////////////////////////////////////////////

const
CRptRebar&  CRptView::Rebar  (void) const { return m_wnd.m_cmd_bar; }
CRptRebar&  CRptView::Rebar  (void)       { return m_wnd.m_cmd_bar; }