/*
	Created Tech_dog (ebontrop@gmail.com) on 5-Sep-2018 at 6:09:15p, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is USB Drive Detective (bitsphereinc.com) desktop app re-bar component interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 31-Oct-2019 at 10:53:12a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "StdAfx.h"
#include "ebo.boo.view.rpt.rba.h"
#include "ebo.boo.res.h"

using namespace ebo::boo::gui;

/////////////////////////////////////////////////////////////////////////////

CRptRebar:: CRptRebar(void) {}
CRptRebar::~CRptRebar(void) {}

/////////////////////////////////////////////////////////////////////////////
bool        CRptRebar::CanAccept (const WORD wCmdId)  const {
	return m_tlb.CanAccept(wCmdId);
}
HRESULT     CRptRebar::Create    (const HWND hParent, const RECT& rcArea) {

	HRESULT hr_ = S_OK;
	if (m_bar.IsValid())
		return (hr_ = HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED));

	if (::IsWindow(hParent) == FALSE)
		return (hr_ = OLE_E_INVALIDHWND);

	if (::IsRectEmpty(&rcArea))
		return (hr_ = OLE_E_INVALIDRECT);

	hr_ = m_bar.Create(hParent, rcArea, this->Identifier());
	if (FAILED(hr_))
		return hr_;

	m_host = hParent;

	hr_ = m_tlb.Create(m_bar);
	if (FAILED(hr_))
		return hr_;

	hr_ = this->RecalcLayout();

	return  hr_;
}

HRESULT     CRptRebar::Destroy   (void) {

	HRESULT hr_ = S_OK;
	//
	// TODO: this requires additional consideration - child toolbar can be still alive; (not sure about that);
	//
	if (m_bar.IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	if (m_tlb.IsValid())
		m_tlb.Destroy();

	hr_ = m_bar.Destroy();

	m_host = NULL;

	return  hr_;
}
UINT        CRptRebar::Identifier(void) const { return IDC_EBO_BOO_RPT_CTRL_RBAR; }
bool        CRptRebar::IsChecked (const UINT nCmdId ) const {
	return m_tlb.IsChecked(nCmdId);
}
bool        CRptRebar::IsValid   (void) const { return m_bar.IsValid(); }
bool        CRptRebar::IsVisible (void) const { return (m_bar.IsValid() && !!m_bar.Window().IsWindowVisible()); }
HRESULT     CRptRebar::IsVisible (const bool _v) {

	HRESULT hr_ = S_OK;
	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	m_bar.Window().ShowWindow(_v ? SW_SHOW : SW_HIDE);

	return  hr_;
}
HRESULT     CRptRebar::OnCommand (const WORD cmdId  ) {
	if (m_tlb.CanAccept(cmdId) == false)
		return (DISP_E_TYPEMISMATCH);
	HRESULT hr_ = m_tlb.OnCommand(cmdId);
	if (SUCCEEDED(hr_))
		hr_ = RecalcLayout();
	return  hr_;
}
HRESULT     CRptRebar::RecalcLayout(void) {

	HRESULT hr_ = S_OK;
	if (m_bar.IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	if (m_tlb.IsValid() == false)
		return (hr_ = HRESULT_FROM_WIN32(ERROR_OBJECT_NOT_FOUND));

	RECT rc_tools = {0};
	CWindow tools = m_tlb;
	tools.GetWindowRect(&rc_tools);

	CReband band_;
	band_.Sizes().Ideal(__W(rc_tools));
	const SIZE sz_min ={__W(rc_tools), __H(rc_tools)};
	band_.Sizes().Length(sz_min.cx);
	band_.Child().MinSize(sz_min);
	band_.Child().Handle(tools);
	band_.Style().Modify(RBBS_GRIPPERALWAYS, true);

	const UINT u_cnt = (UINT) ::SendMessage(m_bar, RB_GETBANDCOUNT, 0, 0);
	if (0 == u_cnt)
		hr_ = m_bar.AppendBand(band_);
	else
		hr_ = m_bar.UpdateBand(0, band_);

	return  hr_;
}

const
CRptTools&  CRptRebar::Toolbar(void) const { return m_tlb; }
CRptTools&  CRptRebar::Toolbar(void)       { return m_tlb; }

HRESULT     CRptRebar::UpdateLayout(const RECT& rcArea) {

	HRESULT hr_ = S_OK;
	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	m_bar.Window().MoveWindow(&rcArea);

	return  hr_;
}

CWindow&    CRptRebar::Window (void) { return m_bar.Window(); }

/////////////////////////////////////////////////////////////////////////////

CRptRebar::operator const HWND(void) const { return m_bar.Window(); }