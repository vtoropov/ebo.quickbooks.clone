#ifndef _EBOBOOVIEWRPTRBA_H_1AD283BC_1C4E_48B3_980A_2FDCCEC61450_INCLUDED
#define _EBOBOOVIEWRPTRBA_H_1AD283BC_1C4E_48B3_980A_2FDCCEC61450_INCLUDED
/*
	Created Tech_dog (ebontrop@gmail.com) on 5-Sep-2018 at 10:48:33a, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is USB Drive Detective (bitsphereinc.com) desktop app re-bar component interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 31-Oct-2019 at 10:52:56a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "shared.uix.ctrl.rba.h"
#include "ebo.boo.view.rpt.tlb.h"

namespace ebo { namespace boo { namespace gui {

	using ex_ui::controls::std::CRebarCtrl;
	using ex_ui::controls::std::CRebarPos;
	using ex_ui::controls::std::CReband;

	//
	// *important* this class takes an ownership over all child toolbar(s);
	//             it is not necessary to create and to destroy all child toolbars;
	//
	class CRptRebar {

	private:
		CWindow       m_host;  // main frame window handle;
		CRebarCtrl    m_bar ;  // re-bar control instance;
		CRptTools     m_tlb ;  // child toolbar control;

	public:
		 CRptRebar(void);
		~CRptRebar(void);

	public:
		bool          CanAccept (const WORD wCmdId)  const;
		HRESULT       Create    (const HWND hParent, const RECT& rcArea); // creates rebar control and assign child toolbar;
		HRESULT       Destroy   (void)      ;
		UINT          Identifier(void) const;           // TODO: it's necessary to be placed in re-bar control;
		bool          IsChecked (const UINT nCmdId ) const;
		bool          IsValid   (void) const;           // checks re-bar window handle for getting validity state;
		bool          IsVisible (void) const;           // gets current state of re-bar visibility;
		HRESULT       IsVisible (const bool);           // sets current state of re-bar visibility;
		HRESULT       OnCommand (const WORD cmdId  );
		HRESULT       RecalcLayout(void) ;              // updates re-bar band size in accordance with toolbar height (images are changed);
		const
		CRptTools&    Toolbar(void) const;              // gets toolbar instance reference (read-only);
		CRptTools&    Toolbar(void)      ;              // gets toolbar instance reference (read-write);
		HRESULT       UpdateLayout(const RECT& rcArea); // updates re-bar position by available area rectangle provided;
		CWindow&      Window (void)      ;

	public:
		operator const HWND(void) const;
	};
}}}

#endif/*_EBOBOOVIEWRPTRBA_H_1AD283BC_1C4E_48B3_980A_2FDCCEC61450_INCLUDED*/