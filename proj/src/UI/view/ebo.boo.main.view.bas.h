#ifndef _EBOBOOMAINVIEWBAS_H_4EAFFCB6_B092_4F4E_9F32_9B057D1B7713_INCLUDED
#define _EBOBOOMAINVIEWBAS_H_4EAFFCB6_B092_4F4E_9F32_9B057D1B7713_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Oct-2019 at 8:51:42p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app base view interface declaration file.
*/
#include "shared.gen.sys.err.h"
namespace ebo { namespace boo { namespace gui { namespace view {

	using shared::sys_core::CError;

	class CViewWnd_Base : public  ATL::CWindowImpl<CViewWnd_Base> {
		                 typedef  ATL::CWindowImpl<CViewWnd_Base> TPaneBase;
	protected:
		 CViewWnd_Base(void);
		~CViewWnd_Base(void);
	
	public:
#define WM_ERASE WM_ERASEBKGND
	//	DECLARE_WND_CLASS(_T("ebo::boo::main::view::base"));
		DECLARE_WND_CLASS_EX(_T("ebo::boo::main::view::base"), 0, 0);
		BEGIN_MSG_MAP(CViewWnd_Base)
			MESSAGE_HANDLER (WM_COMMAND,     OnCommand)
			MESSAGE_HANDLER (WM_CREATE ,     OnCreate )
			MESSAGE_HANDLER (WM_DESTROY,     OnDestroy)
			MESSAGE_HANDLER (WM_ERASE  ,     OnErase  )
			MESSAGE_HANDLER (WM_SIZE   ,     OnSize   )
		END_MSG_MAP()

	protected:
		virtual LRESULT   OnCommand (UINT, WPARAM, LPARAM, BOOL&);
		virtual LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&);
		virtual LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&);
		virtual LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&);
		virtual LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&);
	};

	class CView_Base {
	
	protected:
		CViewWnd_Base&  m_wnd  ;
		CError          m_error;

	protected:
		 CView_Base(CViewWnd_Base&);
		~CView_Base(void);

	public:
		virtual bool        CanAccept(const WORD nCmdId ) const;
		virtual HRESULT     Create   (const HWND hParent, const RECT& _area, const bool bVisible);
		virtual HRESULT     Destroy  (void);
		virtual TErrorRef   Error    (void) const;
		virtual bool        IsChecked(const UINT nCmdId) const;
		virtual bool        IsValid  (void) const;
		virtual bool        IsVisible(void) const;
		virtual HRESULT     IsVisible(const bool);
		virtual HRESULT     OnCommand(const WORD cmdId);
		virtual HRESULT     OnPrepare(const WORD cmdId);
		virtual HRESULT     Refresh  (void);
		virtual HRESULT     UpdateLayout(const RECT& _area);

	private:
		CView_Base (const CView_Base&);
		CView_Base& operator= (const CView_Base&);
	};

}}}}

#endif/*_EBOBOOMAINVIEWBAS_H_4EAFFCB6_B092_4F4E_9F32_9B057D1B7713_INCLUDED*/