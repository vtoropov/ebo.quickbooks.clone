#ifndef _EBOBOOMAINVIEWTAX_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
#define _EBOBOOMAINVIEWTAX_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Dec-2019 at 5:20:08a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app tax view interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.boo.main.view.bas.h"
#include "ebo.boo.view.head.tax.h"
#include "IGridInterface.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using shared::sys_core::CError;

	class CTaxView : public CView_Base {
	                typedef CView_Base TView;
	private:
		class CTaxViewWnd : public  CViewWnd_Base {
		                   typedef  CViewWnd_Base TPane;
			friend class CTaxView;
		private:
			UILayer::IGrid* m_grid;
			CHeader_Tax     m_head;
		public:
			 CTaxViewWnd(void);
			~CTaxViewWnd(void);
		private:
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
		};

	private:
		CTaxViewWnd   m_wnd  ;

	public:
		 CTaxView(void);
		~CTaxView(void);

	public: // TView
		bool        CanAccept(const WORD nCmdId ) const override sealed;
		HRESULT     OnCommand(const WORD nCmdId )       override sealed;

	public:
		HRESULT     Refresh  (void) override sealed;
	};

}}}}

#endif/*_EBOBOOMAINVIEWTAX_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED*/