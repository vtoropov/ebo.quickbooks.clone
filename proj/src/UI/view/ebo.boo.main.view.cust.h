#ifndef _EBOBOOMAINVIEWCUST_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
#define _EBOBOOMAINVIEWCUST_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Nov-2019 at 11:46:31p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app customer view interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.boo.main.view.bas.h"
#include "ebo.boo.view.head.cust.h"
#include "IGridInterface.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using shared::sys_core::CError;

	class CCustView : public CView_Base {
	                 typedef CView_Base TView;
	private:
		class CCustViewWnd : public  CViewWnd_Base {
		                    typedef  CViewWnd_Base TPane;
			friend class CCustView;
		private:
			UILayer::IGrid* m_grid;
			CHeader_Cust    m_head;
		public:
			 CCustViewWnd(void);
			~CCustViewWnd(void);
		private:
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
		};

	private:
		CCustViewWnd   m_wnd  ;

	public:
		 CCustView(void);
		~CCustView(void);

	public: // TView
		bool        CanAccept(const WORD nCmdId ) const override sealed;
		HRESULT     OnCommand(const WORD nCmdId )       override sealed;

	public:
		HRESULT     Refresh  (void) override sealed;
	};

}}}}

#endif/*_EBOBOOMAINVIEWCUST_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED*/