#ifndef _EBOBOOMAINVIEWSALE_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
#define _EBOBOOMAINVIEWSALE_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Nov-2019 at 3:46:00p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app sale view interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.boo.main.view.bas.h"
#include "ebo.boo.view.head.sale.h"
#include "IGridInterface.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using shared::sys_core::CError;

	class CSaleView : public CView_Base {
	                 typedef CView_Base TView;
	private:
		class CSaleViewWnd : public  CViewWnd_Base {
		                    typedef  CViewWnd_Base TPane;
			friend class CSaleView;
		private:
			UILayer::IGrid* m_grid;
			CHeader_Sale    m_head;
		public:
			 CSaleViewWnd(void);
			~CSaleViewWnd(void);
		private:
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
		};

	private:
		CSaleViewWnd   m_wnd  ;

	public:
		 CSaleView(void);
		~CSaleView(void);

	public: // TView
		bool        CanAccept(const WORD nCmdId ) const override sealed;
		HRESULT     OnCommand(const WORD nCmdId )       override sealed;

	public:
		HRESULT     Refresh  (void) override sealed;
	};

}}}}

#endif/*_EBOBOOMAINVIEWSALE_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED*/