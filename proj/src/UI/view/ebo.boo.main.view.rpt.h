#ifndef _EBOBOOMAINVIEWRPT_H_88564FC4_95B8_4E4C_9E83_48617423D592_INCLUDED
#define _EBOBOOMAINVIEWRPT_H_88564FC4_95B8_4E4C_9E83_48617423D592_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Sep-2018 at 8:55:00p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is USB Drive Detective (bitsphereinc.com) desktop app web-based report view interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 27-Oct-2019 at 11:41:49p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "shared.web.browser.h"
#include "ebo.boo.rpt.def.h"
#include "ebo.boo.rpt.htm.h"
#include "ebo.boo.main.view.bas.h"
#include "ebo.boo.view.rpt.rba.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using shared::sys_core::CError;

	using ex_ui::web::CWebBrowser;
	using ex_ui::web::IWebBrowserEventHandler ;

	using ebo::boo::reports::CHtmRptType;
	using ebo::boo::reports::CHtmlReportMan;

	using ebo::boo::gui::CRptRebar;

	class CRptView : public CView_Base {
	                typedef CView_Base TView;
	private:
		class CRptViewWnd : public  CViewWnd_Base, public IWebBrowserEventHandler {
		                   typedef  CViewWnd_Base TPane;
			friend class CRptView;
		private:
			CRptView&           m_view   ;
			CWebBrowser         m_browser;
			CHtmlReportMan      m_htm_man;
			CHtmRptType::_std   m_htm_std;
			CRptRebar           m_cmd_bar;
			CError              m_error  ;

		public:
			 CRptViewWnd(CRptView&);
			~CRptViewWnd(void);

		private:
			LRESULT   OnCommand (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;

		private: // IWebBrowserEventHandler
			virtual HRESULT BrowserEvent_BeforeNavigate  (LPCTSTR lpszUrl) override sealed;
			virtual HRESULT BrowserEvent_DocumentComplete(LPCTSTR lpszUrl, const bool bStreamObject) override sealed;
			virtual HRESULT BrowserEvent_OnMouseMessage  (const MSG&) override sealed;
		};

	private:
		CRptViewWnd   m_wnd  ;

	public:
		 CRptView(void);
		~CRptView(void);

	public:
		bool        CanAccept(const WORD wCmdId ) const override sealed;
		bool        IsChecked(const UINT nCmdId ) const override sealed;
		bool        IsValid  (void) const override sealed;
		HRESULT     OnCommand(const WORD cmdId  ) override sealed;
	public:
		const
		CRptRebar&  Rebar    (void) const;
		CRptRebar&  Rebar    (void)      ;
	};
}}}}

#endif/*_EBOBOOMAINVIEWRPT_H_88564FC4_95B8_4E4C_9E83_48617423D592_INCLUDED*/