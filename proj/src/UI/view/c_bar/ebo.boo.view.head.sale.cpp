/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Dec-2019 at 10:59:15a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Pack personal account app sales view header interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.view.head.sale.h"

using namespace ebo::boo::gui::view;

using namespace ex_ui::controls;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace view { namespace _impl {

	class CHeader_Sale_Layout {
	public:
		enum _sz {
			e_gap = 5,
			e_cap = 2
		};
	private:
		CWindow&     m_wnd_ref;
		RECT         m_cli_rec;
		RECT         m_cap_rec;
		RECT         m_lab_rec[0xa];

	public:
		CHeader_Sale_Layout ( CWindow& _wnd ) : m_wnd_ref(_wnd) {
			if (m_wnd_ref.IsWindow())
				m_wnd_ref.GetClientRect(&m_cli_rec);
			else
				::SetRectEmpty(&m_cli_rec);

			for (INT i_ = 0; i_ < _countof(m_lab_rec);  i_++)
				::SetRectEmpty(&m_lab_rec[i_]);

			::SetRectEmpty(&m_cap_rec);
		}
	public:
		RECT     Area  (const INT _ndx) const {
			RECT rc_ ={0};
			if (_ndx < 0 || _ndx > _countof(m_lab_rec) - 1)
				return rc_;
			else
				return m_lab_rec[_ndx];
		}
		RECT     Area_Cap(void) const { return m_cap_rec; }
		VOID     Update(void) {

			static LONG l_height_c = 26;
			static LONG l_height_0 = 25;
			static LONG l_height_1 = 18;

			m_wnd_ref.GetClientRect(&m_cli_rec);

			m_cap_rec.top    = m_cap_rec.left = 0;
			m_cap_rec.bottom = m_cap_rec.top  + l_height_c;
			m_cap_rec.right  = m_cap_rec.left +(__W(m_cli_rec)) / 2;

			LONG l_midl = m_cap_rec.bottom + l_height_0 + _sz::e_gap;
			LONG l_wdth = __W(m_cli_rec) / 5;
			LONG l_left =     m_cli_rec.left;
			for (INT i_ = 0;  i_ < _countof(m_lab_rec); i_ += 2) {
				m_lab_rec[0 + i_].top = l_midl - l_height_0; m_lab_rec[0 + i_].bottom = l_midl;
				m_lab_rec[1 + i_].top = l_midl;              m_lab_rec[1 + i_].bottom = l_midl + l_height_1;

				m_lab_rec[0 + i_].left = l_left; m_lab_rec[0 + i_].right = l_left + l_wdth;
				m_lab_rec[1 + i_].left = l_left; m_lab_rec[1 + i_].right = l_left + l_wdth;

				l_left += l_wdth; if (i_== 2 || i_ == 6) l_left += _sz::e_cap;
			}
		}
	};

}}}}}
using namespace ebo::boo::gui::view::_impl;
/////////////////////////////////////////////////////////////////////////////

CHeader_Sale:: CHeader_Sale(CWindow& _view) : m_view(_view) {}
CHeader_Sale::~CHeader_Sale(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CHeader_Sale::Create (const RECT& _rc_area) {
	if (::IsRectEmpty(&_rc_area))
		return OLE_E_INVALIDRECT;
	if (m_view.IsWindow() == FALSE)
		return OLE_E_INVALIDHWND;

	CHeader_Sale_Layout lay_(m_view);
	lay_.Update();
	/////////////////////////////////////////////////////////////////////////////
	m_laba.Format().FontSize(24); 
	m_laba.Format().ForeColour(_out::ThemeMan().Colours().Light());
	m_laba.Format().BkgColour (_out::ThemeMan().Colours().Dark ());
	m_laba.Format().Margins().Left() = 10;
	m_laba.Create(m_view, lay_.Area_Cap(), _T("Sales Transactions"));
	/////////////////////////////////////////////////////////////////////////////
	m_lab_e[0].Format().FontSize(24);  m_lab_e[0].Format().ForeColour(RGB(0xff, 0xff, 0xff));
	m_lab_e[1].Format().FontSize(14);  m_lab_e[1].Format().ForeColour(RGB(0xff, 0xff, 0xff));

	m_lab_e[0].Format().BkgColour(RGB( 75,  92, 135)); m_lab_e[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_e[1].Format().BkgColour(RGB( 75,  92, 135)); m_lab_e[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_e[0].Format().Margins().Left() = 15;
	m_lab_e[1].Format().Margins().Left() = 15;

	m_lab_e[0].Create(m_view, lay_.Area(0), _T("$0"));
	m_lab_e[1].Create(m_view, lay_.Area(1), _T("Estimate(s)"));
	/////////////////////////////////////////////////////////////////////////////
	m_lab_u[0].Format().FontSize(24);  m_lab_u[0].Format().ForeColour(RGB(0xff, 0xff, 0xff));
	m_lab_u[1].Format().FontSize(14);  m_lab_u[1].Format().ForeColour(RGB(0xff, 0xff, 0xff));

	m_lab_u[0].Format().BkgColour(RGB(106, 139, 172)); m_lab_u[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_u[1].Format().BkgColour(RGB(106, 139, 172)); m_lab_u[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_u[0].Format().Margins().Left() = 15;
	m_lab_u[1].Format().Margins().Left() = 15;

	m_lab_u[0].Create(m_view, lay_.Area(2), _T("$0"));
	m_lab_u[1].Create(m_view, lay_.Area(3), _T("Unbilled Activity"));
	/////////////////////////////////////////////////////////////////////////////
	m_lab_i[0].Format().FontSize(24);  m_lab_i[0].Format().ForeColour(RGB(0xff, 0xff, 0xff));
	m_lab_i[1].Format().FontSize(14);  m_lab_i[1].Format().ForeColour(RGB(0xff, 0xff, 0xff));

	m_lab_i[0].Format().BkgColour(RGB(237, 180,  51)); m_lab_i[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_i[1].Format().BkgColour(RGB(237, 180,  51)); m_lab_i[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_i[0].Format().Margins().Left() = 15;
	m_lab_i[1].Format().Margins().Left() = 15;

	m_lab_i[0].Create(m_view, lay_.Area(4), _T("$0"));
	m_lab_i[1].Create(m_view, lay_.Area(5), _T("Open Invoices"));
	/////////////////////////////////////////////////////////////////////////////
	m_lab_o[0].Format().FontSize(24);  m_lab_o[0].Format().ForeColour(RGB(0xff, 0xff, 0xff));
	m_lab_o[1].Format().FontSize(14);  m_lab_o[1].Format().ForeColour(RGB(0xff, 0xff, 0xff));

	m_lab_o[0].Format().BkgColour(RGB(202,  78,  50)); m_lab_o[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_o[1].Format().BkgColour(RGB(202,  78,  50)); m_lab_o[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_o[0].Format().Margins().Left() = 15;
	m_lab_o[1].Format().Margins().Left() = 15;

	m_lab_o[0].Create(m_view, lay_.Area(4), _T("$0"));
	m_lab_o[1].Create(m_view, lay_.Area(5), _T("Overdue"));
	/////////////////////////////////////////////////////////////////////////////
	m_lab_p[0].Format().FontSize(24);  m_lab_p[0].Format().ForeColour(RGB(0xff, 0xff, 0xff));
	m_lab_p[1].Format().FontSize(14);  m_lab_p[1].Format().ForeColour(RGB(0xff, 0xff, 0xff));

	m_lab_p[0].Format().BkgColour(RGB(123, 202,   0)); m_lab_p[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_p[1].Format().BkgColour(RGB(123, 202,   0)); m_lab_p[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_p[0].Format().Margins().Left() = 15;
	m_lab_p[1].Format().Margins().Left() = 15;

	m_lab_p[0].Create(m_view, lay_.Area(6), _T("$0"));
	m_lab_p[1].Create(m_view, lay_.Area(7), _T("Paid"));

	return S_OK;
}

HRESULT    CHeader_Sale::Destroy(void) {
	m_laba.Destroy();

	m_lab_e[0].Destroy();
	m_lab_e[1].Destroy();
	m_lab_o[0].Destroy();
	m_lab_o[1].Destroy();
	m_lab_i[0].Destroy();
	m_lab_i[1].Destroy();
	m_lab_p[0].Destroy();
	m_lab_p[1].Destroy();
	return S_OK;
}

const SIZE CHeader_Sale::Layout (void) const {

	CHeader_Sale_Layout lay_(m_view);
	lay_.Update();

	SIZE sz_ = {
		0, lay_.Area(1).bottom + CHeader_Sale_Layout::e_gap
	};
	return sz_;
}

HRESULT    CHeader_Sale::Update (void) {
	CHeader_Sale_Layout lay_(m_view);
	lay_.Update();

	const RECT rc_c = lay_.Area_Cap();
	m_laba.Window ().MoveWindow(&rc_c);
	m_laba.Format ().ForeColour(_out::ThemeMan().Colours().Light());
	m_laba.Format ().BkgColour (_out::ThemeMan().Colours().Dark ());
	m_laba.Refresh();

	const RECT rc_0 = lay_.Area(0); m_lab_e[0].Window().MoveWindow(&rc_0);
	const RECT rc_1 = lay_.Area(1); m_lab_e[1].Window().MoveWindow(&rc_1);

	const RECT rc_2 = lay_.Area(2); m_lab_u[0].Window().MoveWindow(&rc_2);
	const RECT rc_3 = lay_.Area(3); m_lab_u[1].Window().MoveWindow(&rc_3);

	const RECT rc_4 = lay_.Area(4); m_lab_i[0].Window().MoveWindow(&rc_4);
	const RECT rc_5 = lay_.Area(5); m_lab_i[1].Window().MoveWindow(&rc_5);

	const RECT rc_6 = lay_.Area(6); m_lab_o[0].Window().MoveWindow(&rc_6);
	const RECT rc_7 = lay_.Area(7); m_lab_o[1].Window().MoveWindow(&rc_7);

	const RECT rc_8 = lay_.Area(8); m_lab_p[0].Window().MoveWindow(&rc_8);
	const RECT rc_9 = lay_.Area(9); m_lab_p[1].Window().MoveWindow(&rc_9);

	return S_OK;
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////