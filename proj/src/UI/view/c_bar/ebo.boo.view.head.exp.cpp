/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Nov-2019 at 8:57:39p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app expense view header interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.view.head.exp.h"

using namespace ebo::boo::gui::view;

using namespace ex_ui::controls;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace view { namespace _impl {

	class CHeader_Ex_Layout {
	private:
		CWindow      m_wnd_ref;
		RECT         m_cli_rec;

	public:
		CHeader_Ex_Layout ( CWindow& _wnd ) : m_wnd_ref(_wnd) {
			if (m_wnd_ref.IsWindow())
				m_wnd_ref.GetClientRect(&m_cli_rec);
			else
				::SetRectEmpty(&m_cli_rec);
		}
	public:
		RECT     Area  (void) {
			static LONG l_height = 26;
			RECT rc_    = {0};
			LONG l_midl = m_cli_rec.top + l_height + 10; l_midl;
			LONG l_wdth = __W(m_cli_rec) / 2;
			LONG l_left =     m_cli_rec.left;
			
			rc_.top    = l_midl - l_height;
			rc_.bottom = l_midl;
			rc_.left   = l_left; rc_.right = l_left + l_wdth;

			return rc_;
		}
	};

}}}}}
using namespace ebo::boo::gui::view::_impl;
/////////////////////////////////////////////////////////////////////////////

CHeader_Exp:: CHeader_Exp(CWindow& _view) : m_view(_view) {}
CHeader_Exp::~CHeader_Exp(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CHeader_Exp::Create (const RECT& _rc_area) {
	if (::IsRectEmpty(&_rc_area))
		return OLE_E_INVALIDRECT;
	if (m_view.IsWindow() == FALSE)
		return OLE_E_INVALIDHWND;

	CHeader_Ex_Layout lay_( m_view);
	const RECT rc_  = lay_.Area();

	/////////////////////////////////////////////////////////////////////////////
	m_laba.Format().FontSize(24); 
	m_laba.Format().ForeColour(_out::ThemeMan().Colours().Light());
	m_laba.Format().BkgColour (_out::ThemeMan().Colours().Dark ());
	m_laba.Format().Margins().Left() = 10;
	m_laba.Create(m_view, rc_, _T("Expense Transactions"));

	return S_OK;
}

HRESULT    CHeader_Exp::Destroy(void) {
	m_laba.Destroy();
	return S_OK;
}

const SIZE CHeader_Exp::Layout (void) const {

	CHeader_Ex_Layout lay_(m_view);

	SIZE sz_ = {
		0, lay_.Area().bottom
	};
	return sz_;
}

HRESULT    CHeader_Exp::Update (void) {
	CHeader_Ex_Layout lay_(m_view);

	const RECT rc_0 = lay_.Area();
	m_laba.Window ().MoveWindow(&rc_0);
	m_laba.Format ().ForeColour(_out::ThemeMan().Colours().Light());
	m_laba.Format ().BkgColour (_out::ThemeMan().Colours().Dark ());
	m_laba.Refresh();

	return S_OK;
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////