#ifndef _EBOBOOVIEWHEADEXP_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED
#define _EBOBOOVIEWHEADEXP_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Nov-2019 at 8:57:30p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app expense view header interface declaration file.
*/
#include "shared.uix.ctrl.label.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using ex_ui::controls::CLabel_Ex;
	using ex_ui::controls::CLab_Format;

	class CHeader_Exp {
	private:
		CWindow&   m_view;
		CLabel_Ex  m_laba;

	public:
		 CHeader_Exp (CWindow& _view);
		~CHeader_Exp (void);

	public:
		HRESULT    Create (const RECT& _rc_area);
		HRESULT    Destroy(void);
		const SIZE Layout (void) const;
		HRESULT    Update (void);
	};

}}}}

#endif/*_EBOBOOVIEWHEADEXP_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED*/