#ifndef _EBOBOOVIEWHEADEMP_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED
#define _EBOBOOVIEWHEADEMP_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Nov-2019 at 3:34:35a, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is Ebo Pack personal account app employee view header interface declaration file.
*/
#include "shared.uix.ctrl.label.h"
#include "shared.uix.ctrl.button.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using ex_ui::controls::CLabel_Ex  ;
	using ex_ui::controls::CLab_Format;
	using ex_ui::controls::CButton_Ex ;
	using ex_ui::controls::IControlEvent;

	class CHeader_Emp : public IControlEvent {
	private:
		CWindow&   m_view;
		CLabel_Ex  m_laba;
		CButton_Ex m_bt_0;
		CButton_Ex m_bt_1;
		CButton_Ex m_bt_2;

	public:
		 CHeader_Emp (CWindow& _view, IControlEvent&);
		~CHeader_Emp (void);

	private:    // IControlEvent
		HRESULT    IControlEvent_OnClick(const UINT ctrlId) override sealed;
		HRESULT    IControlEvent_OnClick(const UINT ctrlId, const LONG_PTR nData) override sealed;
	public:
		HRESULT    Create (const RECT& _rc_area);
		HRESULT    Destroy(void);
		const SIZE Layout (void) const;
		HRESULT    Update (void);

	public:
		static
		LONG       ReqHeight(void);
	};

}}}}

#endif/*_EBOBOOVIEWHEADEMP_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED*/