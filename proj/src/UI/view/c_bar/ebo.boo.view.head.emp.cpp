/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Nov-2019 at 3:34:49a, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is Ebo Pack personal account app employee view header interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.view.head.emp.h"
#include "ebo.boo.res.h"

using namespace ebo::boo::gui::view;

using namespace ex_ui::controls;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace view { namespace _impl {

	class CHeader_Em_Initer {
	private:
		CWindow   m_header;
		RECT      m_rc_cli;

	public:
		CHeader_Em_Initer(const CWindow& _head) : m_header(_head) {
			if (m_header.IsWindow())
				m_header.GetClientRect(&m_rc_cli);
			else
				::SetRectEmpty(&m_rc_cli);
		}

	public:
		HRESULT   OnCreate(CButton_Ex** _buts, const UINT _count, const RECT& _rc_lab) {
			if (_buts == NULL || _count == 0)
				return E_INVALIDARG;

			static const INT n_gap = 5;

			HRESULT hr_ = S_OK;
			LONG l_left = _rc_lab.right + n_gap;
			const LONG l_md = m_rc_cli.top + CHeader_Emp::ReqHeight() / 2;

			for (UINT i_ = 0; i_ < _count; i_++) {
				CButton_Ex* p_but = _buts[ i_ ];
				if (NULL == p_but)
					continue;
				CButton_Ex& b_ref = *p_but;
				switch (i_) {
				case 0: {
					b_ref.Runtime ().CtrlId(IDC_EBO_BOO_VIEW_EMP_NEW);
					b_ref.Renderer().Images().Add(CControlState::eDisabled, IDR_EBO_BOO_VIEW_EMP_NEW);
					b_ref.Renderer().Images().Add(CControlState::eHovered , IDR_EBO_BOO_VIEW_EMP_NEW);
					b_ref.Renderer().Images().Add(CControlState::eNormal  , IDR_EBO_BOO_VIEW_EMP_NEW);
					b_ref.Renderer().Images().Add(CControlState::ePressed , IDR_EBO_BOO_VIEW_EMP_NEW);
					b_ref.Runtime ().BackColor(::_out::ThemeMan().Colours().Dark());
					} break;
				case 1: {
					b_ref.Runtime ().CtrlId(IDC_EBO_BOO_VIEW_EMP_EDT);
					b_ref.Renderer().Images().Add(CControlState::eDisabled, IDR_EBO_BOO_VIEW_EMP_EDT);
					b_ref.Renderer().Images().Add(CControlState::eHovered , IDR_EBO_BOO_VIEW_EMP_EDT);
					b_ref.Renderer().Images().Add(CControlState::eNormal  , IDR_EBO_BOO_VIEW_EMP_EDT);
					b_ref.Renderer().Images().Add(CControlState::ePressed , IDR_EBO_BOO_VIEW_EMP_EDT);
					b_ref.Runtime ().BackColor(::_out::ThemeMan().Colours().Dark());
					} break;
				case 2: {
					b_ref.Runtime ().CtrlId(IDC_EBO_BOO_VIEW_EMP_REM);
					b_ref.Renderer().Images().Add(CControlState::eDisabled, IDR_EBO_BOO_VIEW_EMP_REM);
					b_ref.Renderer().Images().Add(CControlState::eHovered , IDR_EBO_BOO_VIEW_EMP_REM);
					b_ref.Renderer().Images().Add(CControlState::eNormal  , IDR_EBO_BOO_VIEW_EMP_REM);
					b_ref.Renderer().Images().Add(CControlState::ePressed , IDR_EBO_BOO_VIEW_EMP_REM);
					b_ref.Runtime ().BackColor(::_out::ThemeMan().Colours().Dark());
					} break;
				}
				const SIZE sz_req = b_ref.Renderer().RequiredSize();
				RECT rc_req  = { 0 };
				rc_req.left  = l_left; rc_req.top = l_md - sz_req.cy / 2;
				rc_req.right = rc_req.left + sz_req.cx; rc_req.bottom = rc_req.top + sz_req.cy;
				hr_ = b_ref.Create(m_header, &rc_req, false);
				if (FAILED(hr_))
					break;
				else
					l_left += (sz_req.cx + n_gap);
			}

			return  hr_;
		}
	};

	class CHeader_Em_Layout {
	private:
		CWindow      m_wnd_ref;
		RECT         m_cli_rec;

	public:
		CHeader_Em_Layout ( CWindow& _wnd ) : m_wnd_ref(_wnd) {
			if (m_wnd_ref.IsWindow())
				m_wnd_ref.GetClientRect(&m_cli_rec);
			else
				::SetRectEmpty(&m_cli_rec);
		}
	public:
		RECT     Lab_Area  (LPCTSTR _lp_sz_txt, const CLab_Format& _fmt) {
			static LONG l_req_h = CHeader_Emp::ReqHeight();
			RECT rc_ = {
				m_cli_rec.left, m_cli_rec.top,
				m_cli_rec.left + CLabel_Ex::ReqSize(_lp_sz_txt, _fmt).cx, m_cli_rec.top + l_req_h
			};
			return rc_;
		}
	};

}}}}}
using namespace ebo::boo::gui::view::_impl;
/////////////////////////////////////////////////////////////////////////////

CHeader_Emp:: CHeader_Emp(CWindow& _view, IControlEvent& _snk) : m_view(_view), m_bt_0(_snk), m_bt_1(_snk), m_bt_2(_snk) {}
CHeader_Emp::~CHeader_Emp(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CHeader_Emp::Create (const RECT& _rc_area) {

	HRESULT hr_ = S_OK;

	if (::IsRectEmpty(&_rc_area))    return (hr_ = OLE_E_INVALIDRECT);
	if (m_view.IsWindow() == FALSE)  return (hr_ = OLE_E_INVALIDHWND);

	static LPCTSTR lp_sz_cap = _T("Employees");

	CHeader_Em_Layout lay_( m_view);

	/////////////////////////////////////////////////////////////////////////////
	m_laba.Format().FontSize(24); 
	m_laba.Format().ForeColour(_out::ThemeMan().Colours().Light());
	m_laba.Format().BkgColour (_out::ThemeMan().Colours().Dark ());
	m_laba.Format().Margins().Left () = 10;
	m_laba.Format().Margins().Right() = 10;

	const RECT rc_l = lay_.Lab_Area(lp_sz_cap, m_laba.Format());

	m_laba.Create(m_view, rc_l, lp_sz_cap);

	/////////////////////////////////////////////////////////////////////////////
	CHeader_Em_Initer initer(m_view);

	CButton_Ex* buts[3] = {
		&m_bt_0, &m_bt_1, &m_bt_2
	};
	initer.OnCreate(&buts[0], _countof(buts), rc_l);

	return hr_;
}

HRESULT    CHeader_Emp::Destroy(void) {
	m_laba.Destroy();
	m_bt_0.Destroy();
	m_bt_1.Destroy();
	m_bt_2.Destroy();
	return S_OK;
}

const SIZE CHeader_Emp::Layout (void) const {

	CHeader_Em_Layout lay_(m_view);

	SIZE sz_ = {
		0, CHeader_Emp::ReqHeight()
	};
	return sz_;
}

HRESULT    CHeader_Emp::Update (void) {
	
	m_laba.Format().ForeColour(_out::ThemeMan().Colours().Light());
	m_laba.Format().BkgColour (_out::ThemeMan().Colours().Dark ());
	m_laba.Refresh();

	m_bt_0.Runtime().BackColor(_out::ThemeMan().Colours().Dark ());
	m_bt_0.Refresh();

	m_bt_1.Runtime().BackColor(_out::ThemeMan().Colours().Dark ());
	m_bt_1.Refresh();

	m_bt_2.Runtime().BackColor(_out::ThemeMan().Colours().Dark ());
	m_bt_2.Refresh();

	return S_OK;
}
/////////////////////////////////////////////////////////////////////////////

HRESULT    CHeader_Emp::IControlEvent_OnClick(const UINT ctrlId) { ctrlId; return S_OK; }
HRESULT    CHeader_Emp::IControlEvent_OnClick(const UINT ctrlId, const LONG_PTR nData) { ctrlId; nData; return S_OK; }

/////////////////////////////////////////////////////////////////////////////

LONG       CHeader_Emp::ReqHeight(void) {
	static LONG l_gap =  3;
	static LONG l_but = 48; // images that are used for buttons are expected to have 48x48px size;
	return l_gap * 2 + l_but;
}