/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Dec-2019 at 7:01:13a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app taxes view header interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.view.head.tax.h"

using namespace ebo::boo::gui::view;

using namespace ex_ui::controls;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace view { namespace _impl {

	class CHeader_Tax_Layout {
	private:
		CWindow&     m_wnd_ref;
		RECT         m_cli_rec;
		RECT         m_lab_rec[8];

	public:
		CHeader_Tax_Layout ( CWindow& _wnd ) : m_wnd_ref(_wnd) {
			if (m_wnd_ref.IsWindow())
				m_wnd_ref.GetClientRect(&m_cli_rec);
			else
				::SetRectEmpty(&m_cli_rec);

			for (INT i_ = 0; i_ < _countof(m_lab_rec);  i_++)
				::SetRectEmpty(&m_lab_rec[i_]);
		}
	public:
		RECT     Area  (const INT _ndx) {
			RECT rc_ ={0};
			if (_ndx < 0 || _ndx > _countof(m_lab_rec) - 1)
				return rc_;
			else
				return m_lab_rec[_ndx];
		}
		VOID     Update(void) {

			static LONG l_height = 26;

			m_wnd_ref.GetClientRect(&m_cli_rec);

			LONG l_midl = m_cli_rec.top + l_height + 10;
			LONG l_wdth = __W(m_cli_rec) / 4;
			LONG l_left =     m_cli_rec.left;
			for (INT i_ = 0;  i_ < _countof(m_lab_rec); i_ += 2) {
				m_lab_rec[0 + i_].top = l_midl - l_height; m_lab_rec[0 + i_].bottom = l_midl;
				m_lab_rec[1 + i_].top = l_midl;            m_lab_rec[1 + i_].bottom = l_midl + l_height;

				m_lab_rec[0 + i_].left = l_left; m_lab_rec[0 + i_].right = l_left + l_wdth;
				m_lab_rec[1 + i_].left = l_left; m_lab_rec[1 + i_].right = l_left + l_wdth;

				l_left += l_wdth;
			}
		}
	};

}}}}}
using namespace ebo::boo::gui::view::_impl;
/////////////////////////////////////////////////////////////////////////////

CHeader_Tax:: CHeader_Tax(CWindow& _view) : m_view(_view) {}
CHeader_Tax::~CHeader_Tax(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CHeader_Tax::Create (const RECT& _rc_area) {
	if (::IsRectEmpty(&_rc_area))
		return OLE_E_INVALIDRECT;
	if (m_view.IsWindow() == FALSE)
		return OLE_E_INVALIDHWND;

	CHeader_Tax_Layout lay_(m_view);
	lay_.Update();

	/////////////////////////////////////////////////////////////////////////////
	m_lab_s[0].Format().FontSize(24);  m_lab_s[0].Format().ForeColour(RGB(0x00, 0x00, 0x00));
	m_lab_s[1].Format().FontSize(14);  m_lab_s[1].Format().ForeColour(RGB(0x00, 0x00, 0x00));

	m_lab_s[0].Format().BkgColour(RGB(244, 245, 248)); m_lab_s[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_s[1].Format().BkgColour(RGB(244, 245, 248)); m_lab_s[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_s[0].Format().Margins().Left() = 15;
	m_lab_s[1].Format().Margins().Left() = 15;

	m_lab_s[0].Create(m_view, lay_.Area(0), _T("$0"));
	m_lab_s[1].Create(m_view, lay_.Area(1), _T("Collected on Sales"));
	/////////////////////////////////////////////////////////////////////////////
	m_lab_p[0].Format().FontSize(24);  m_lab_p[0].Format().ForeColour(RGB(0x00, 0x00, 0x00));
	m_lab_p[1].Format().FontSize(14);  m_lab_p[1].Format().ForeColour(RGB(0x00, 0x00, 0x00));

	m_lab_p[0].Format().BkgColour(RGB(244, 245, 248)); m_lab_p[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_p[1].Format().BkgColour(RGB(244, 245, 248)); m_lab_p[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_p[0].Format().Margins().Left() = 15;
	m_lab_p[1].Format().Margins().Left() = 15;

	m_lab_p[0].Create(m_view, lay_.Area(2), _T("$0"));
	m_lab_p[1].Create(m_view, lay_.Area(3), _T("Paid on Purchases"));
	/////////////////////////////////////////////////////////////////////////////
	m_lab_a[0].Format().FontSize(24);  m_lab_a[0].Format().ForeColour(RGB(0x00, 0x00, 0x00));
	m_lab_a[1].Format().FontSize(14);  m_lab_a[1].Format().ForeColour(RGB(0x00, 0x00, 0x00));

	m_lab_a[0].Format().BkgColour(RGB(244, 245, 248)); m_lab_a[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_a[1].Format().BkgColour(RGB(244, 245, 248)); m_lab_a[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_a[0].Format().Margins().Left() = 15;
	m_lab_a[1].Format().Margins().Left() = 15;

	m_lab_a[0].Create(m_view, lay_.Area(4), _T("$0"));
	m_lab_a[1].Create(m_view, lay_.Area(5), _T("Adjustments"));
	/////////////////////////////////////////////////////////////////////////////
	m_lab_t[0].Format().FontSize(24);  m_lab_t[0].Format().ForeColour(RGB(0x00, 0x00, 0x00));
	m_lab_t[1].Format().FontSize(14);  m_lab_t[1].Format().ForeColour(RGB(0x00, 0x00, 0x00));

	m_lab_t[0].Format().BkgColour(RGB(244, 245, 248)); m_lab_t[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_t[1].Format().BkgColour(RGB(244, 245, 248)); m_lab_t[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_t[0].Format().Margins().Left() = 15;
	m_lab_t[1].Format().Margins().Left() = 15;

	m_lab_t[0].Create(m_view, lay_.Area(6), _T("$0"));
	m_lab_t[1].Create(m_view, lay_.Area(7), _T("Total"));

	return S_OK;
}

HRESULT    CHeader_Tax::Destroy(void) {
	m_lab_s[0].Destroy();
	m_lab_s[1].Destroy();
	m_lab_p[0].Destroy();
	m_lab_p[1].Destroy();
	m_lab_a[0].Destroy();
	m_lab_a[1].Destroy();
	m_lab_t[0].Destroy();
	m_lab_t[1].Destroy();
	return S_OK;
}

const SIZE CHeader_Tax::Layout (void) const {

	CHeader_Tax_Layout lay_(m_view);
	lay_.Update();

	SIZE sz_ = {
		0, lay_.Area(0).bottom
	};
	return sz_;
}

HRESULT    CHeader_Tax::Update (void) {
	CHeader_Tax_Layout lay_(m_view);
	lay_.Update();

	const RECT rc_0 = lay_.Area(0); m_lab_s[0].Window().MoveWindow(&rc_0);
	const RECT rc_1 = lay_.Area(1); m_lab_s[1].Window().MoveWindow(&rc_1);

	const RECT rc_2 = lay_.Area(2); m_lab_p[0].Window().MoveWindow(&rc_2);
	const RECT rc_3 = lay_.Area(3); m_lab_p[1].Window().MoveWindow(&rc_3);

	const RECT rc_4 = lay_.Area(4); m_lab_a[0].Window().MoveWindow(&rc_4);
	const RECT rc_5 = lay_.Area(5); m_lab_a[1].Window().MoveWindow(&rc_5);

	const RECT rc_6 = lay_.Area(6); m_lab_t[0].Window().MoveWindow(&rc_6);
	const RECT rc_7 = lay_.Area(7); m_lab_t[1].Window().MoveWindow(&rc_7);

	return S_OK;
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////