#ifndef _EBOBOOVIEWHEAD_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED
#define _EBOBOOVIEWHEAD_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Nov-2019 at 12:53:59p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app generic view header interface declaration file.
*/
#include "shared.uix.ctrl.label.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using ex_ui::controls::CLabel_Ex;
	using ex_ui::controls::CLab_Format;

	class CHeader {
	private:
		CWindow&   m_view;
		CLabel_Ex  m_lab_e[2]; // estimate;
		CLabel_Ex  m_lab_o[2]; // overdue ;
		CLabel_Ex  m_lab_i[2]; // open invoice;
		CLabel_Ex  m_lab_p[2]; // paid;

	public:
		 CHeader (CWindow& _view);
		~CHeader (void);

	public:
		HRESULT    Create (const RECT& _rc_area);
		HRESULT    Destroy(void);
		const SIZE Layout (void) const;
		HRESULT    Update (void);
	};

}}}}

#endif/*_EBOBOOVIEWHEAD_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED*/