/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Nov-2019 at 01:00:55p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app generic view header interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.view.head.h"

using namespace ebo::boo::gui::view;

using namespace ex_ui::controls;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace view { namespace _impl {

	class CHeader_Layout {
	private:
		CWindow&     m_wnd_ref;
		RECT         m_cli_rec;
		RECT         m_lab_rec[8];

	public:
		CHeader_Layout ( CWindow& _wnd ) : m_wnd_ref(_wnd) {
			if (m_wnd_ref.IsWindow())
				m_wnd_ref.GetClientRect(&m_cli_rec);
			else
				::SetRectEmpty(&m_cli_rec);

			for (INT i_ = 0; i_ < _countof(m_lab_rec);  i_++)
				::SetRectEmpty(&m_lab_rec[i_]);
		}
	public:
		RECT     Area  (const INT _ndx) {
			RECT rc_ ={0};
			if (_ndx < 0 || _ndx > _countof(m_lab_rec) - 1)
				return rc_;
			else
				return m_lab_rec[_ndx];
		}
		VOID     Update(void) {

			static LONG l_height = 26;

			m_wnd_ref.GetClientRect(&m_cli_rec);

			LONG l_midl = m_cli_rec.top + l_height + 10;
			LONG l_wdth = __W(m_cli_rec) / 4;
			LONG l_left =     m_cli_rec.left;
			for (INT i_ = 0;  i_ < _countof(m_lab_rec); i_ += 2) {
				m_lab_rec[0 + i_].top = l_midl - l_height; m_lab_rec[0 + i_].bottom = l_midl;
				m_lab_rec[1 + i_].top = l_midl;            m_lab_rec[1 + i_].bottom = l_midl + l_height;

				m_lab_rec[0 + i_].left = l_left; m_lab_rec[0 + i_].right = l_left + l_wdth;
				m_lab_rec[1 + i_].left = l_left; m_lab_rec[1 + i_].right = l_left + l_wdth;

				l_left += l_wdth;
			}
		}
	};

}}}}}
using namespace ebo::boo::gui::view::_impl;
/////////////////////////////////////////////////////////////////////////////

CHeader:: CHeader(CWindow& _view) : m_view(_view) {}
CHeader::~CHeader(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CHeader::Create (const RECT& _rc_area) {
	if (::IsRectEmpty(&_rc_area))
		return OLE_E_INVALIDRECT;
	if (m_view.IsWindow() == FALSE)
		return OLE_E_INVALIDHWND;

	CHeader_Layout lay_(m_view);
	lay_.Update();

	/////////////////////////////////////////////////////////////////////////////
	m_lab_e[0].Format().FontSize(24);  m_lab_e[0].Format().ForeColour(RGB(0xff, 0xff, 0xff));
	m_lab_e[1].Format().FontSize(14);  m_lab_e[1].Format().ForeColour(RGB(0xff, 0xff, 0xff));

	m_lab_e[0].Format().BkgColour(RGB( 33, 171, 246)); m_lab_e[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_e[1].Format().BkgColour(RGB( 33, 171, 246)); m_lab_e[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_e[0].Format().Margins().Left() = 15;
	m_lab_e[1].Format().Margins().Left() = 15;

	m_lab_e[0].Create(m_view, lay_.Area(0), _T("$0"));
	m_lab_e[1].Create(m_view, lay_.Area(1), _T("Estimate"));
	/////////////////////////////////////////////////////////////////////////////
	m_lab_o[0].Format().FontSize(24);  m_lab_o[0].Format().ForeColour(RGB(0xff, 0xff, 0xff));
	m_lab_o[1].Format().FontSize(14);  m_lab_o[1].Format().ForeColour(RGB(0xff, 0xff, 0xff));

	m_lab_o[0].Format().BkgColour(RGB(254, 127,   0)); m_lab_o[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_o[1].Format().BkgColour(RGB(254, 127,   0)); m_lab_o[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_o[0].Format().Margins().Left() = 15;
	m_lab_o[1].Format().Margins().Left() = 15;

	m_lab_o[0].Create(m_view, lay_.Area(2), _T("$0"));
	m_lab_o[1].Create(m_view, lay_.Area(3), _T("Overdue"));
	/////////////////////////////////////////////////////////////////////////////
	m_lab_i[0].Format().FontSize(24);  m_lab_i[0].Format().ForeColour(RGB(0xff, 0xff, 0xff));
	m_lab_i[1].Format().FontSize(14);  m_lab_i[1].Format().ForeColour(RGB(0xff, 0xff, 0xff));

	m_lab_i[0].Format().BkgColour(RGB(180, 184, 191)); m_lab_i[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_i[1].Format().BkgColour(RGB(180, 184, 191)); m_lab_i[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_i[0].Format().Margins().Left() = 15;
	m_lab_i[1].Format().Margins().Left() = 15;

	m_lab_i[0].Create(m_view, lay_.Area(4), _T("$0"));
	m_lab_i[1].Create(m_view, lay_.Area(5), _T("Open Invoice"));
	/////////////////////////////////////////////////////////////////////////////
	m_lab_p[0].Format().FontSize(24);  m_lab_p[0].Format().ForeColour(RGB(0xff, 0xff, 0xff));
	m_lab_p[1].Format().FontSize(14);  m_lab_p[1].Format().ForeColour(RGB(0xff, 0xff, 0xff));

	m_lab_p[0].Format().BkgColour(RGB(123, 202,   0)); m_lab_p[0].Format().VertAlign(eVertAlign::eBottom);
	m_lab_p[1].Format().BkgColour(RGB(123, 202,   0)); m_lab_p[1].Format().VertAlign(eVertAlign::eTop);

	m_lab_p[0].Format().Margins().Left() = 15;
	m_lab_p[1].Format().Margins().Left() = 15;

	m_lab_p[0].Create(m_view, lay_.Area(6), _T("$0"));
	m_lab_p[1].Create(m_view, lay_.Area(7), _T("Paid Last 30 Days"));

	return S_OK;
}

HRESULT    CHeader::Destroy(void) {
	m_lab_e[0].Destroy();
	m_lab_e[1].Destroy();
	m_lab_o[0].Destroy();
	m_lab_o[1].Destroy();
	m_lab_i[0].Destroy();
	m_lab_i[1].Destroy();
	m_lab_p[0].Destroy();
	m_lab_p[1].Destroy();
	return S_OK;
}

const SIZE CHeader::Layout (void) const {

	CHeader_Layout lay_(m_view);
	lay_.Update();

	SIZE sz_ = {
		0, lay_.Area(0).bottom
	};
	return sz_;
}

HRESULT    CHeader::Update (void) {
	CHeader_Layout lay_(m_view);
	lay_.Update();

	const RECT rc_0 = lay_.Area(0); m_lab_e[0].Window().MoveWindow(&rc_0);
	const RECT rc_1 = lay_.Area(1); m_lab_e[1].Window().MoveWindow(&rc_1);

	const RECT rc_2 = lay_.Area(2); m_lab_o[0].Window().MoveWindow(&rc_2);
	const RECT rc_3 = lay_.Area(3); m_lab_o[1].Window().MoveWindow(&rc_3);

	const RECT rc_4 = lay_.Area(4); m_lab_i[0].Window().MoveWindow(&rc_4);
	const RECT rc_5 = lay_.Area(5); m_lab_i[1].Window().MoveWindow(&rc_5);

	const RECT rc_6 = lay_.Area(6); m_lab_p[0].Window().MoveWindow(&rc_6);
	const RECT rc_7 = lay_.Area(7); m_lab_p[1].Window().MoveWindow(&rc_7);

	return S_OK;
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////