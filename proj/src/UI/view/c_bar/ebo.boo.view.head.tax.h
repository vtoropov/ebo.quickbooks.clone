#ifndef _EBOBOOVIEWHEADTAX_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED
#define _EBOBOOVIEWHEADTAX_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Dec-2019 at 6:09:57a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app taxes view header interface declaration file.
*/
#include "shared.uix.ctrl.label.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using ex_ui::controls::CLabel_Ex;
	using ex_ui::controls::CLab_Format;

	class CHeader_Tax {
	private:
		CWindow&   m_view;
		CLabel_Ex  m_lab_s[2]; // collected on sales;
		CLabel_Ex  m_lab_p[2]; // purchased;
		CLabel_Ex  m_lab_a[2]; // adjusment;
		CLabel_Ex  m_lab_t[2]; // total;

	public:
		 CHeader_Tax (CWindow& _view);
		~CHeader_Tax (void);

	public:
		HRESULT    Create (const RECT& _rc_area);
		HRESULT    Destroy(void);
		const SIZE Layout(void) const;
		HRESULT    Update (void);
	};

}}}}

#endif/*_EBOBOOVIEWHEADTAX_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED*/