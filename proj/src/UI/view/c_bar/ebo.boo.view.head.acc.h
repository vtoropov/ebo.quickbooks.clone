#ifndef _EBOBOOVIEWHEADACC_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED
#define _EBOBOOVIEWHEADACC_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Dec-2019 at 5:37:34p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app accountant view header interface declaration file.
*/
#include "shared.uix.ctrl.label.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using ex_ui::controls::CLabel_Ex;
	using ex_ui::controls::CLab_Format;

	class CHeader_Acc {
	private:
		CWindow&   m_view;
		CLabel_Ex  m_laba;

	public:
		 CHeader_Acc (CWindow& _view);
		~CHeader_Acc (void);

	public:
		HRESULT    Create (const RECT& _rc_area);
		HRESULT    Destroy(void);
		const SIZE Layout (void) const;
		HRESULT    Update (void);
	};

}}}}

#endif/*_EBOBOOVIEWHEADACC_H_577B776A_A980_4D76_9A70_E073986F3AF5_INCLUDED*/