#ifndef _EBOBOOMAINVIEWSUPL_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
#define _EBOBOOMAINVIEWSUPL_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Dec-2019 at 4:19:09p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Ebo Pack personal account app supplier view interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.boo.main.view.bas.h"
#include "ebo.boo.view.head.h"
#include "IGridInterface.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using shared::sys_core::CError;

	class CSuplView : public CView_Base {
	                 typedef CView_Base TView;
	private:
		class CSuplViewWnd : public  CViewWnd_Base {
		                    typedef  CViewWnd_Base TPane;
			friend class CSuplView;
		private:
			UILayer::IGrid*
			          m_grid;
			CHeader   m_head;
		public:
			 CSuplViewWnd(void);
			~CSuplViewWnd(void);
		private:
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
		};

	private:
		CSuplViewWnd   m_wnd  ;

	public:
		 CSuplView(void);
		~CSuplView(void);

	public: // TView
		bool        CanAccept(const WORD nCmdId ) const override sealed;
		HRESULT     OnCommand(const WORD nCmdId )       override sealed;

	public:
		HRESULT     Refresh  (void) override sealed;
	};

}}}}

#endif/*_EBOBOOMAINVIEWSUPL_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED*/