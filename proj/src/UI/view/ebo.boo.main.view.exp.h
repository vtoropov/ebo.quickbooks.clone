#ifndef _EBOBOOMAINVIEWSEXP_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
#define _EBOBOOMAINVIEWSEXP_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Nov-2019 at 8:08:05p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app expense view interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.boo.main.view.bas.h"
#include "ebo.boo.view.head.exp.h"
#include "IGridInterface.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using shared::sys_core::CError;

	class CExpView : public CView_Base {
	                typedef CView_Base TView;
	private:
		class CExpViewWnd : public  CViewWnd_Base {
		                    typedef CViewWnd_Base TPane;
			friend class CExpView;
		private:
			UILayer::IGrid* m_grid;
			CHeader_Exp     m_head;
		public:
			 CExpViewWnd(void);
			~CExpViewWnd(void);
		private:
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
		};

	private:
		CExpViewWnd   m_wnd  ;

	public:
		 CExpView(void);
		~CExpView(void);

	public:
		HRESULT     Refresh  (void) override sealed;
	};

}}}}

#endif/*_EBOBOOMAINVIEWSEXP_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED*/