/*
	Created Tech_dog (ebontrop@gmail.com) on 5-Sep-2018 at 7:55:28p, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is USB Drive Detective (bitsphereinc.com) desktop report toolbar component interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 31-Oct-2019 at 9:26:33a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "StdAfx.h"
#include "ebo.boo.view.rpt.tlb.h"
#include "ebo.boo.res.too.bar.h"
#include "ebo.boo.frm.main.mnu.h"
#include "ebo.boo.res.h"

using namespace ebo::boo::gui;

#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.gdi.provider.h"
#include "shared.uix.gdi.object.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

CRptTools:: CRptTools(void) : m_images(*this), m_buttons(m_tools) {}
CRptTools::~CRptTools(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT CRptTools::IToolImage_OnStyleChanged(void) {
	__empty_ln;
	HRESULT hr_ = S_OK;

#define TB_SETDISIMAGES TB_SETDISABLEDIMAGELIST
	//
	// TODO: setting buttons size dynamically must be reviewed;
	//
	if (NULL == m_tools)
		return (hr_ = __HRESULT_FROM_LASTERROR());

	const SIZE sz_ = m_images.Size();
	const HDC hdc_ = m_tools.GetDC();

	const TToolButtonIds& ids_ = m_buttons;

	WTL::CToolBarCtrl ctl_ = m_tools;
	SIZE sz_pad = {0};
	ctl_.GetPadding(&sz_pad);
	const INT n_btn_count  = ctl_.GetButtonCount();
	for ( INT i_ = 0; i_ < n_btn_count &&
	                  i_ < static_cast<INT>(ids_.size()); i_++) {

		TCHAR sz_buf[32]  = {0};
		const INT n_sz_ln = ctl_.GetButtonText(ids_[i_], sz_buf);
		if (n_sz_ln < 1)
			continue;
		SIZE sz_bt_tx = {0};
		CText(hdc_, m_tools.GetFont()).GetSize(sz_buf, sz_bt_tx);

		INT n_factor = 3;
		switch (i_) {
		case 0: case 3: case 5: { n_factor = 5; } break;
		}

		TBBUTTONINFO bt_nfo = {0};
		bt_nfo.cbSize = sizeof (TBBUTTONINFO) ;
		bt_nfo.dwMask = TBIF_SIZE;
		bt_nfo.cx     = static_cast<WORD>(sz_bt_tx.cx + sz_.cx + sz_pad.cx * n_factor);

		if (FALSE == ctl_.SetButtonInfo(ids_[i_], &bt_nfo)) {
			hr_ = HRESULT_FROM_WIN32(::GetLastError());
		}
	}
	m_tools.ReleaseDC(hdc_);

	m_tools.SendMessage (TB_SETIMAGELIST , 0, (LPARAM)m_images.List(false));
	m_tools.SendMessage (TB_SETDISIMAGES , 0, (LPARAM)m_images.List(true ));
#if (0) // sets equal size for all buttons, but some of them have a text of specific length;
	m_tools.SendMessage (TB_SETBUTTONSIZE, 0, MAKELPARAM( sz_.cx, sz_.cy) );
#endif
	m_tools.SendMessage (TB_AUTOSIZE     , 0, 0);
	m_tools.SendMessage (TB_SETINDENT    , 3, 0);
	m_tools.RedrawWindow(NULL, NULL, RDW_ERASENOW|RDW_UPDATENOW);
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

bool      CRptTools::CanAccept(const WORD wCmdId) const {
	switch (wCmdId) {
	case IDC_EBO_BOO_MNU_CMD_TLS_EXT:
	case IDC_EBO_BOO_MNU_CMD_TLS_LRG:
	case IDC_EBO_BOO_MNU_CMD_TLS_MED:
	case IDC_EBO_BOO_MNU_CMD_TLS_SML:
	return true;
	}
	return false;
}

HRESULT   CRptTools::Create (const HWND hParent) {
	hParent;
	HRESULT hr_ = S_OK;

	if (m_tools.IsWindow() == TRUE)
		return (hr_ = HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS));

	static const DWORD dwStyle = WS_VISIBLE|WS_CHILD|TBSTYLE_LIST|TBSTYLE_TOOLTIPS|TBSTYLE_TRANSPARENT;

	RECT rc_ = {0};

	_U_MENUorID mnu_id = IDC_EBO_BOO_TOO_RPT;

	m_tools.Create(
		TOOLBARCLASSNAME, hParent, rc_, NULL, dwStyle, 0, mnu_id, NULL
	);
	if (NULL == m_tools)
		return (hr_ = __HRESULT_FROM_LASTERROR());

	WTL::CToolBarCtrl tool_ = m_tools;
	tool_.SetExtendedStyle(TBSTYLE_EX_DRAWDDARROWS);

	//
	// TODO: toolbar button enumeration class must be used here;
	//

	m_tools.SendMessage(TB_BUTTONSTRUCTSIZE, (WPARAM)sizeof(TBBUTTON), 0);
	const TToolButtonIds& ids_ = m_buttons;
	size_t ndx_ = 0;

	TBBUTTON buttons_[] = {
		{ 0, (ndx_ < ids_.size() ? ids_[ndx_++] : 0) , TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_DROPDOWN },   // print
		{ 1, (ndx_ < ids_.size() ? ids_[ndx_++] : 0) , TBSTATE_ENABLED, TBSTYLE_BUTTON | NULL __nothing   },   // email
		{ 2, (ndx_ < ids_.size() ? ids_[ndx_++] : 0) , TBSTATE_ENABLED, TBSTYLE_BUTTON | NULL __nothing   },   // excel
	};

	static LPCTSTR lp_sz_[] = {
		_T("Print"       ), _T("Send..."     ), _T("Convert...")
	};

	for (size_t  i_ = 0; i_ < ids_.size()  &&  i_ < _countof(buttons_); i_++) {
		buttons_[i_].iString = (INT_PTR)lp_sz_[i_];
	}

	m_tools.SendMessage(TB_ADDBUTTONS, _countof(buttons_), (LPARAM)&buttons_);
	m_tools.SendMessage(TB_AUTOSIZE  ,  0, 0);
	m_tools.SendMessage(TB_SETINDENT ,  3, 0);

	hr_ = m_images.Create();

	return  hr_;
}

HRESULT   CRptTools::Destroy(void) {
	__no_args;
	HRESULT hr_ = S_OK;
	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	m_tools.SendMessage(WM_CLOSE);

	return  hr_;
}
bool      CRptTools::IsChecked (const UINT nCmdId ) const {
	switch (nCmdId) {
	case IDC_EBO_BOO_MNU_CMD_TLS_EXT: return m_images.Is(CToolImages::eExtra );
	case IDC_EBO_BOO_MNU_CMD_TLS_LRG: return m_images.Is(CToolImages::eLarge );
	case IDC_EBO_BOO_MNU_CMD_TLS_MED: return m_images.Is(CToolImages::eMedium);
	case IDC_EBO_BOO_MNU_CMD_TLS_SML: return m_images.Is(CToolImages::eSmall );
	}
	return false;
}
bool      CRptTools::IsValid   (void) const { return !!m_tools.IsWindow(); }

/////////////////////////////////////////////////////////////////////////////

HRESULT   CRptTools::OnCommand (const WORD cmdId) {

	HRESULT hr_ = S_OK;
	CToolImages::_e style_ = CToolImages::eLarge;

	if (IDC_EBO_BOO_MNU_CMD_TLS_EXT == cmdId) style_ = CToolImages::eExtra  ;
	if (IDC_EBO_BOO_MNU_CMD_TLS_LRG == cmdId) style_ = CToolImages::eLarge  ;
	if (IDC_EBO_BOO_MNU_CMD_TLS_MED == cmdId) style_ = CToolImages::eMedium ;
	if (IDC_EBO_BOO_MNU_CMD_TLS_SML == cmdId) style_ = CToolImages::eSmall  ;

	hr_ = this->Images().Style(style_);
	return  hr_;
}

HRESULT   CRptTools::OnDropdown(const WORD btnId, const LPARAM lpData) {

	HRESULT hr_ = S_OK;
	LPNMTOOLBAR pTool = reinterpret_cast<LPNMTOOLBAR>(lpData); btnId;

	if (NULL == pTool)
		return (hr_ = TYPE_E_WRONGTYPEKIND);

	HMENU hPopup = NULL;

	switch (pTool->iItem) {
	case IDC_EBO_BOO_CMD_VIEW_PRN : { hPopup = CMenuBar::GetPrintPrw(); } break;
	case IDC_EBO_BOO_CMD_VIEW_PRE : { hPopup = CMenuBar::GetExpMenu (); } break;
	default:
		hr_ = DISP_E_MEMBERNOTFOUND;
	}

	if (SUCCEEDED(hr_) && hPopup) {

		RECT rc_menu = pTool->rcButton;
		::MapWindowPoints(m_tools, HWND_DESKTOP, (LPPOINT)&rc_menu, 0x2);
		::TrackPopupMenu(
			hPopup, TPM_RIGHTALIGN, rc_menu.right, rc_menu.bottom, 0, m_tools, NULL
		);
		::DestroyMenu(hPopup); hPopup = NULL;
	}

	return  hr_;
}

HRESULT   CRptTools::OnTooltip (const WORD btnId, const LPARAM lpData) {
	btnId; lpData;
	HRESULT hr_ = S_OK;
	NMTTDISPINFO* pTipInfo = reinterpret_cast<NMTTDISPINFO*>(lpData);
	if ( NULL ==  pTipInfo )
		return hr_;
	
	const CAtlString& cs_tip = m_buttons.Tooltip(btnId);
	const size_t t_sz_len  = _countof(pTipInfo->szText);

	::_tcsncpy_s(
	pTipInfo->szText, t_sz_len, (LPCTSTR)cs_tip, t_sz_len );
	pTipInfo->uFlags   = TTF_IDISHWND;
	pTipInfo->lpszText = pTipInfo->szText;

	return  hr_;
}
/////////////////////////////////////////////////////////////////////////////
const
CToolButtons& CRptTools::Buttons(void) const { return m_buttons; }
CToolButtons& CRptTools::Buttons(void)       { return m_buttons; }
const
CToolImages & CRptTools::Images (void) const { return m_images ; }
CToolImages & CRptTools::Images (void)       { return m_images ; }

/////////////////////////////////////////////////////////////////////////////

CRptTools::operator const HWND  (void) const { return m_tools  ; }