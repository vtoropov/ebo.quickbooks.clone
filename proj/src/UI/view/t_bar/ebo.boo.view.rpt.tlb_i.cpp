/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Dec-2018 at 7:04:18p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is USB Drive Detective (bitsphereinc.com) desktop app toolbar image list management interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 30-Oct-2019 at 4:30:00p, UTC+7, Novosibirsk, Tulenina, Wednesday;
*/
#include "StdAfx.h"
#include "ebo.boo.view.rpt.tlb_i.h"
#include "ebo.boo.res.too.bar.h"

using namespace ebo::boo::gui;

#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl {

	class CTool_I_Helper {
	public:
		static UINT   Disabled (const CToolImages::_e _sz) {  // gets resource identifier for disable image list;

			UINT eResId = IDR_EBO_BOO_RPT_IMG_x32_d;

			switch (_sz) {
			case CToolImages::eImg_x24: { eResId = IDR_EBO_BOO_RPT_IMG_x24_d; } break;
			case CToolImages::eImg_x32: { eResId = IDR_EBO_BOO_RPT_IMG_x32_d; } break;
			case CToolImages::eImg_x48: { eResId = IDR_EBO_BOO_RPT_IMG_x48_d; } break;
			case CToolImages::eImg_x64: { eResId = IDR_EBO_BOO_RPT_IMG_x64_d; } break;
			}
			return eResId;
		}
		static UINT   Enabled (const CToolImages::_e _sz) { // gets resource identifier for enable image list;

			UINT eResId = IDR_EBO_BOO_RPT_IMG_x32;

			switch (_sz) {
			case CToolImages::eImg_x24: { eResId = IDR_EBO_BOO_RPT_IMG_x24; } break;
			case CToolImages::eImg_x32: { eResId = IDR_EBO_BOO_RPT_IMG_x32; } break;
			case CToolImages::eImg_x48: { eResId = IDR_EBO_BOO_RPT_IMG_x48; } break;
			case CToolImages::eImg_x64: { eResId = IDR_EBO_BOO_RPT_IMG_x64; } break;
			}
			return eResId;
		}
	};

}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CToolImages:: CToolImages(IToolImage_Evts& _snk) : m_e_lst(NULL), m_d_lst(NULL), m_style(_e::eSmall), m_sink(_snk) {
	m_error << __MODULE__ << S_OK >> __MODULE__;
}
CToolImages::~CToolImages(void) {
	this->Destroy();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CToolImages::Create  (void) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = m_error;

	const CToolImages::_e default_ = CToolImages::eMedium;

	if (default_ == CToolImages::eExtra )  hr_ = this->Style(_e::eExtra );
	if (default_ == CToolImages::eLarge )  hr_ = this->Style(_e::eLarge );
	if (default_ == CToolImages::eMedium)  hr_ = this->Style(_e::eMedium);
	if (default_ == CToolImages::eSmall )  hr_ = this->Style(_e::eSmall );

	return m_error;
}

HRESULT       CToolImages::Destroy (void) {
	m_error << __MODULE__ << S_OK;

	if (NULL != m_d_lst) { ::ImageList_Destroy(m_d_lst); m_d_lst = NULL; } // no error evaluating here;
	if (NULL != m_e_lst) { ::ImageList_Destroy(m_e_lst); m_e_lst = NULL; } // no error evaluating here;

	return m_error;
}

TErrorRef     CToolImages::Error   (void) const { return m_error; }
bool          CToolImages::Is      (const _e _e) const {
	return (_e == m_style);
}

const
HIMAGELIST&   CToolImages::List (const bool _b_disabled) const { return (_b_disabled ? m_d_lst : m_e_lst); }

SIZE          CToolImages::Size (void) const {
	SIZE sz_not_found = {0};
	switch (m_style) {
	case _e::eImg_x64: { return SIZE{/*148*/64, 64}; } break;
	case _e::eImg_x48: { return SIZE{/*128*/48, 48}; } break;
	case _e::eImg_x32: { return SIZE{/*114*/32, 32}; } break;
	case _e::eImg_x24: { return SIZE{/*148*/24, 24}; } break;
	}
	return sz_not_found;
}

const CToolImages::_e
      CToolImages::Style (void) const { return m_style; }

HRESULT       CToolImages::Style (const _e _sz) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = this->Destroy();

	const UINT n_e_res = CTool_I_Helper::Enabled (_sz);
	const UINT n_d_res = CTool_I_Helper::Disabled(_sz);

	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();

	hr_ = CGdiPlusPngLoader::CreateImages( n_d_res, hInstance, m_d_lst ); if (FAILED(hr_)) return (m_error = hr_);
	hr_ = CGdiPlusPngLoader::CreateImages( n_e_res, hInstance, m_e_lst ); if (FAILED(hr_)) return (m_error = hr_);

	m_style = _sz;
	m_sink.IToolImage_OnStyleChanged();

	return m_error;
}