#ifndef _EBOBOOVIEWRPTTLB_H_A50B2159_873F_441C_9444_18FE65B8A289_INCLUDED
#define _EBOBOOVIEWRPTTLB_H_A50B2159_873F_441C_9444_18FE65B8A289_INCLUDED
/*
	Created Tech_dog (ebontrop@gmail.com) on 5-Sep-2018 at 6:34:00p, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is USB Drive Detective (bitsphereinc.com) desktop report toolbar component interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 31-Oct-2019 at 8:54:24a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "ebo.boo.view.rpt.tlb_b.h"
#include "ebo.boo.view.rpt.tlb_i.h"

namespace ebo { namespace boo { namespace gui {

	class CRptTools : public IToolImage_Evts {
	private:
		CToolButtons   m_buttons;
		CToolImages    m_images;
		CWindow        m_host ;
		CWindow        m_tools;

	public:
		 CRptTools(void);
		~CRptTools(void);

	private: // IToolImage_Evts
		virtual HRESULT IToolImage_OnStyleChanged(void) override sealed;

	public:
		bool        CanAccept(const WORD wCmdId ) const;
		HRESULT     Create   (const HWND hParent);
		HRESULT     Destroy  (void);
		bool        IsChecked(const UINT nCmdId ) const;
		bool        IsValid  (void) const;
		HRESULT     Update   (void)      ; // updates toolbar layout;
	public: // accessor(s)
		const
		CToolButtons& Buttons(void) const;
		CToolButtons& Buttons(void)      ;
		const
		CToolImages & Images (void) const;
		CToolImages & Images (void)      ;

	public: // message handler(s)
		HRESULT     OnCommand (const WORD cmdId);
		HRESULT     OnDropdown(const WORD btnId, const LPARAM lpData);
		HRESULT     OnTooltip (const WORD btnId, const LPARAM lpData);

	public:
		operator const HWND(void) const;
	};

}}}

#endif/*_EBOBOOVIEWRPTTLB_H_A50B2159_873F_441C_9444_18FE65B8A289_INCLUDED*/