/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Dec-2018 at 7:41:58p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is USB Drive Detective (bitsphereinc.com) desktop app toolbar button collection interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 31-Oct-2019 at 6:57:07a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "StdAfx.h"
#include "ebo.boo.view.rpt.tlb_b.h"
#include "ebo.boo.res.rpt.cmd.h"
#include "ebo.boo.res.too.bar.h"

using namespace ebo::boo::gui;

/////////////////////////////////////////////////////////////////////////////

CToolButtons::CToolButtons(const CWindow& _tbar) : m_tbar(_tbar) { m_error << __MODULE__ << S_OK >> __MODULE__;

	static WORD w_cmd_ids[] = {
		IDC_EBO_BOO_CMD_VIEW_PRN, IDC_EBO_BOO_CMD_VIEW_MAIL, IDC_EBO_BOO_CMD_VIEW_XCEL, IDC_EBO_BOO_CMD_VIEW_PRE
	};
	static WORD w_tip_ids[] = {
		IDS_EBO_BOO_CMD_VIEW_PRN, IDS_EBO_BOO_CMD_VIEW_MAIL, IDS_EBO_BOO_CMD_VIEW_XCEL, IDS_EBO_BOO_CMD_VIEW_PRE
	};
	CAtlString cs_tip;
	try {
		for (size_t i_ = 0; i_ < _countof(w_cmd_ids) && i_ < _countof(w_tip_ids); i_++) {
			cs_tip.LoadString(w_tip_ids[i_]);
			m_tips.insert(
				::std::make_pair(w_cmd_ids[i_] , cs_tip)
			);
			m_t_ids.push_back(w_cmd_ids[i_]);
		}
	}
	catch (::std::bad_alloc&) {
		m_error = E_OUTOFMEMORY;
	}
}
CToolButtons::~CToolButtons(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CToolButtons::Check(const UINT cmdId, const bool _b_check) {
	cmdId; _b_check;
	HRESULT hr_ = S_OK;
	if (m_tbar.IsWindow() == FALSE)
		return (hr_ = OLE_E_INVALIDHWND);

	CToolBarCtrl t_bar = m_tbar;
	if (FALSE == t_bar.CheckButton(cmdId, static_cast<BOOL>(_b_check)))
		hr_ = HRESULT_FROM_WIN32(::GetLastError());

	return  hr_;
}

HRESULT       CToolButtons::Enable (const UINT cmdId, const bool _b_enable) {
	cmdId; _b_enable;
	HRESULT hr_ = S_OK;
	if (m_tbar.IsWindow() == FALSE)
		return (hr_ = OLE_E_INVALIDHWND);

	CToolBarCtrl t_bar = m_tbar;
	if (!t_bar.EnableButton(cmdId, static_cast<BOOL>(_b_enable)))
		hr_ = HRESULT_FROM_WIN32(::GetLastError());

	return  hr_;
}

const
CAtlString&   CToolButtons::Tooltip(const UINT btnId) const {
	btnId;
	TToolTips::const_iterator it_ = m_tips.find(btnId);
	if (it_ == m_tips.end()) {
		static CAtlString cs_na;
		return cs_na;
	}
	else
		return it_->second;
}

/////////////////////////////////////////////////////////////////////////////

CToolButtons::operator const TToolButtonIds& (void) const { return m_t_ids; }