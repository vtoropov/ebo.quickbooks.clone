#ifndef _EBOBOOVIEWRPTTLB_I_H_51211D78_EB0A_4330_8D0D_9024BA053A46_INCLUDED
#define _EBOBOOVIEWRPTTLB_I_H_51211D78_EB0A_4330_8D0D_9024BA053A46_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Dec-2018 at 6:47:04p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is USB Drive Detective (bitsphereinc.com) desktop app toolbar image list management interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 30-Oct-2019 at 3:58:46p, UTC+7, Novosibirsk, Tulenina, Wednesday;
*/
#include "shared.gen.sys.err.h"

namespace ebo { namespace boo { namespace gui {

	using shared::sys_core::CError;

	interface IToolImage_Evts {
		virtual HRESULT IToolImage_OnStyleChanged(void) PURE;
	};

	class CToolImages {
	public:
		enum _e {
			eImg_x64 = 0x0,
			eImg_x48 = 0x1,
			eImg_x32 = 0x2,
			eImg_x24 = 0x3,
			eExtra   = eImg_x64,
			eLarge   = eImg_x48,
			eMedium  = eImg_x32,
			eSmall   = eImg_x24,
		};
	private:
		IToolImage_Evts&
		              m_sink ;    // event sink;
		_e            m_style;    // an image frame style;
		HIMAGELIST    m_e_lst;    // an image list for enable state of the toolbar buttons;
		HIMAGELIST    m_d_lst;    // an image list for disable state of the toolbar buttons;
		CError        m_error;

	public:
		 CToolImages(IToolImage_Evts&);
		~CToolImages(void);

	public:
		HRESULT       Create  (void);
		                                     // https://docs.microsoft.com/en-us/windows/desktop/controls/tb-setimagelist
		HRESULT       Destroy (void);        // destroys both image lists; a toolbar is affected;
		TErrorRef     Error   (void) const;
		bool          Is      (const _e) const;
		const
		HIMAGELIST&   List    (const bool _b_disabled) const;
		SIZE          Size    (void) const;  // required toolbar button size that is based on image list style selected;
		const _e      Style   (void) const;
		HRESULT       Style   (const _e);
	};
}}}

#endif/*_EBOBOOVIEWRPTTLB_I_H_51211D78_EB0A_4330_8D0D_9024BA053A46_INCLUDED*/