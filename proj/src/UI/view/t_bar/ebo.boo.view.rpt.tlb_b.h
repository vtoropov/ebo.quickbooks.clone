#ifndef _EBOBOOVIEWRPTTLB_B_H_F9857037_9C1C_4F50_B9D4_796AFF419F48_INCLUDED
#define _EBOBOOVIEWRPTTLB_B_H_F9857037_9C1C_4F50_B9D4_796AFF419F48_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Dec-2018 at 7:30:54p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is USB Drive Detective (bitsphereinc.com) desktop app toolbar button collection interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 31-Oct-2019 at 6:57:07a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include "shared.gen.sys.err.h"

namespace ebo { namespace boo { namespace gui {

	using shared::sys_core::CError;

	typedef ::std::map<UINT, CAtlString>   TToolTips;   // a key is command/button identifier, a value is a tooltip text;

	interface IToolBtn_Evts {
		virtual VOID IToolBtn_OnCheck (const UINT btnId, const bool) PURE;
		virtual VOID IToolBtn_OnEnable(const UINT btnId, const bool) PURE;
	};
#if(0)
	class CToolButton {
	private:
		IToolBtn_Evts&
		           m_sink;
		UINT       m_u_id;
		CAtlString m_c_tip;

	protected:
		CToolButton(IToolBtn_Evts&);
	};
#endif

	typedef ::std::vector<WORD> TToolButtonIds;

	class CToolButtons {
	private:
		const
		CWindow&      m_tbar ;
		CError        m_error;
		TToolTips     m_tips ;
		TToolButtonIds
		              m_t_ids;

	public:
		 CToolButtons(const CWindow& _tbar);
		~CToolButtons(void);

	public:
		HRESULT       Check  (const UINT cmdId, const bool);
		HRESULT       Enable (const UINT cmdId, const bool);  // enables/disables a button by command identifier provided;
		const
		CAtlString&   Tooltip(const UINT btnId) const;

	public:
		operator const TToolButtonIds& (void) const;
	};
}}}

#endif/*_EBOBOOVIEWRPTTLB_B_H_F9857037_9C1C_4F50_B9D4_796AFF419F48_INCLUDED*/