	/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jul-2018 on 10:31:47a, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Bot HTTP/NET test desktop application main form interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 7:19:46a, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "StdAfx.h"
#include "ebo.boo.frm.main.h"
#include "ebo.boo.res.h"
#include "ebo.boo.usr.man.wzd.h"
#include "ebo.boo.frm.main.lay.h"
#include "ebo.boo.frm.main.mnu.h"
#include "ebo.boo.pwd.dlg.h"

using namespace ebo::boo::gui;

#include "shared.gen.app.res.h"
#include "shared.gen.app.obj.h"

using namespace shared::user32;

#include "shared.web.defs.h"
using namespace ex_ui::web;

#include "shared.uix.gen.clrs.h"
#include "shared.uix.gdi.renderer.h"

using namespace ex_ui::draw;
using namespace ex_ui::draw::renderers;
using namespace ex_ui::controls;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl {

	class CMainForm_Init {
	private:
		CMainForm&  m_main ;
		CError      m_error;

	public:
		CMainForm_Init(CMainForm& _main_frm) : m_main(_main_frm) { m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT OnCreate(void){
			m_error << __MODULE__ << S_OK;

			CSelector& sel_ = m_main.Selector();
			CCmpClrs_Enum clrs_;
			const CCmpClrs& pair_ = _out::ThemeMan();

			sel_.Format().BackColour(pair_.Dark());
			sel_.Format().ForeColour(pair_.Light());
			sel_.Format().ItmImages (IDR_EBO_BOO_MAIN_FRM_SEL);
			sel_.Layout().Width(150);
			sel_.Layout().HAlign() = eHorzAlign::eLeft;
			sel_.Layout().Margins().Left() = 5;

			static LPCTSTR lp_sz_caps[] = {
				_T("Home")      , _T("Customers" ), _T("Sales")     , _T("Expenses")  ,
				_T("Employees" ), _T("Reports")   , _T("Suppliers" ),
				_T("Taxes")     , _T("Accounting")
			};
			static UINT  u_itm_cmds[] = {
				IDC_EBO_BOO_SEL_CMD_HOME,IDC_EBO_BOO_SEL_CMD_CUST,IDC_EBO_BOO_SEL_CMD_SALE,IDC_EBO_BOO_SEL_CMD_EXPS,
				IDC_EBO_BOO_SEL_CMD_EMPL,IDC_EBO_BOO_SEL_CMD_REPS,IDC_EBO_BOO_SEL_CMD_SUPL,
				IDC_EBO_BOO_SEL_CMD_TAXS,IDC_EBO_BOO_SEL_CMD_ACNT
			};

			CSel_Item_Enum& items_= sel_.Items();
			for (UINT i_ = 0; i_ < _countof(lp_sz_caps) && i_ < _countof(u_itm_cmds); i_ ++) {
				CSel_Item itm_;
				itm_.Id () = u_itm_cmds[i_];
				itm_.Cap() = lp_sz_caps[i_];

				switch (i_) {
				case 0: { itm_.Img() = 0;} break;
				case 7: { itm_.Img() = 3;} break;
				}

				items_.Append(itm_);
			}
			RECT rc_ = {0}; m_main.Window().GetClientRect(&rc_);
			HRESULT hr_ = sel_.Create(m_main.Window(), rc_, _T("ebo_pack::controls::selector"), IDC_EBO_BOO_MAIN_FRM_SEL);
			if (FAILED(hr_)) {
				m_error = sel_.Error();
				m_error.Show();
			}
			items_.Selected(0);

			return m_error;
		}
		VOID    SetMenu(void) { CMenuBar(m_main).Initialize(); }
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CMainForm::CMainFormWnd:: CMainFormWnd(CMainForm& _frm) : m_form(_frm), m_selector(*this)
	{ m_error << __MODULE__ << S_OK >> __MODULE__;
	HRESULT hr_ = m_master.Open();
	if (FAILED(hr_)) {
		m_error = m_master.Error();
		m_error.Show();
	}
}

CMainForm::CMainFormWnd::~CMainFormWnd(void) {
	HRESULT hr_ =  m_master.Close();
	if (FAILED(hr_)) {
		m_error = m_master.Error();
		m_error.Show();
	}
}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CMainForm::CMainFormWnd::OnCreate (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	wParam; lParam; bHandled = TRUE;

	CApplicationIconLoader loader_(IDR_EBO_BOO_ICO);
	{
		TWindow::SetIcon(loader_.DetachLargeIcon(), TRUE );
		TWindow::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	const CApplication& the_app = GetAppObjectRef();

	CAtlString cs_title;
	cs_title.Format (
		_T("%s [ver.%s]"),
		(LPCTSTR)the_app.Version().CustomValue(_T("Comments"   )),
		(LPCTSTR)the_app.Version().CustomValue(_T("FileVersion"))
	);

	TWindow::SetWindowText(cs_title.GetString());

	HRESULT hr_ = _out::MainFrmSta().Create(*this); hr_;

	CMainForm_Init init_(m_form);
	init_.SetMenu ();
	init_.OnCreate();
	const RECT rc_view = CMainForm_Layout(m_form).GetViewRect();

	hr_ = m_v_man.Create(*this, rc_view);

	CBkgSegmentMap segs_;
	hr_ = segs_.Append(CBkgSegment::e_middle, IDR_EBO_BOO_MAIN_FRM_GND_EMP);
	if (FAILED(hr_)) {
		segs_.Error().Show();
		return 0;
	}
	hr_ = m_render.Images().Create(segs_);
	if (FAILED(hr_)) {
		m_render.Images().Error().Show();
	}

	CMainForm_Layout(m_form).RecalcLayout();
	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnDestroy(UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = FALSE;
	m_v_man.Destroy();

	if (m_selector.Window ().IsWindow()) {
		m_selector.Destroy();
	}
	_out::MainFrmSta ().Destroy();

	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnDraw   (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	return _out::MainFrmSta().OnDrawEvent(wParam, lParam, bHandled);
}

LRESULT   CMainForm::CMainFormWnd::OnErase  (UINT, WPARAM wParam, LPARAM, BOOL& b_hand) {
	b_hand = TRUE;
	static bool  b_fst_time = false;
	if (false == b_fst_time) {
		HBRUSH brush = ::CreateSolidBrush(RGB(0, 0, 0));
		::SetClassLongPtr(*this, GCLP_HBRBACKGROUND, (LONG_PTR)brush);
		b_fst_time = true;
	}

	static const LRESULT l_is_drawn = 1;
	if (m_v_man.Visible() == false) {
		RECT rc_ = CMainForm_Layout(m_form).GetViewRect();
		m_render.Draw((HDC)wParam, rc_);
	}
	return l_is_drawn;
}

LRESULT   CMainForm::CMainFormWnd::OnMnuInit(UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = TRUE;
	const INT bSubMenuIndex  = LOWORD(lParam);
	const HMENU hSubMenu = reinterpret_cast<HMENU>(wParam);

	CMenuBar(m_form).UpdateState(
		hSubMenu, bSubMenuIndex
	);
	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnSize   (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = FALSE;
	RECT rc_ = {0, 0, LOWORD(lParam), HIWORD(lParam)}; rc_;

	CMainForm_Layout(m_form).RecalcLayout();
	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnSysCmd (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE: {
		::PostQuitMessage(0);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CMainForm::CMainFormWnd::ISelector_OnBkgChanged (void) { return S_OK;  }
HRESULT   CMainForm::CMainFormWnd::ISelector_OnFontChanged(void) { return S_OK;  }
HRESULT   CMainForm::CMainFormWnd::ISelector_OnItemClick  (const UINT _u_itm_id) {
	switch (_u_itm_id) {
	case IDC_EBO_BOO_SEL_CMD_HOME: {
			view::CViewType::_tp t_v_ = view::CViewType::e_none ;
			m_v_man.Trigger(t_v_);
		} break;
	case IDC_EBO_BOO_SEL_CMD_CUST: {
			view::CViewType::_tp t_v_ = view::CViewType::e_cust;
			m_v_man.Trigger(t_v_);
		} break;
	case IDC_EBO_BOO_SEL_CMD_SALE: {
			view::CViewType::_tp t_v_ = view::CViewType::e_sale;
			m_v_man.Trigger(t_v_);
		} break;
	case IDC_EBO_BOO_SEL_CMD_EXPS: {
			view::CViewType::_tp t_v_ = view::CViewType::e_exp ;
			m_v_man.Trigger(t_v_);
		} break;
	case IDC_EBO_BOO_SEL_CMD_EMPL: {
			view::CViewType::_tp t_v_ = view::CViewType::e_emp ;
			m_v_man.Trigger(t_v_);
		} break;
	case IDC_EBO_BOO_SEL_CMD_REPS: {
			m_v_man.Report().OnCommand(IDC_EBO_BOO_MNU_CMD_RPT_PAL);
			view::CViewType::_tp t_v_ = view::CViewType::e_rpt;
			m_v_man.Trigger(t_v_);
		} break;
	case IDC_EBO_BOO_SEL_CMD_TAXS: {
			view::CViewType::_tp t_v_ = view::CViewType::e_tax;
			m_v_man.Trigger(t_v_);
		} break;
	case IDC_EBO_BOO_SEL_CMD_SUPL: {
			view::CViewType::_tp t_v_ = view::CViewType::e_supl;
			m_v_man.Trigger(t_v_);
		} break;
	case IDC_EBO_BOO_SEL_CMD_ACNT: {
			view::CViewType::_tp t_v_ = view::CViewType::e_acnt;
			m_v_man.Trigger(t_v_);
		} break;
	}
	return S_OK;
}
HRESULT   CMainForm::CMainFormWnd::ISelector_OnItemImages (const UINT _u_res_id) { _u_res_id; return S_OK; }

/////////////////////////////////////////////////////////////////////////////

LRESULT   CMainForm::CMainFormWnd::OnCommand(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled) {
	bHandled = TRUE; wNotifyCode; hWndCtl;
	if (m_v_man.Handler().Accept(wID)) {
		m_v_man.Handler().Do(wID); return 0;
	}
	switch (wID) {
	case IDC_EBO_BOO_MNU_CMD_CST_LST : {
			ISelector_OnItemClick(IDC_EBO_BOO_SEL_CMD_CUST);
			m_selector.Items() << IDC_EBO_BOO_SEL_CMD_CUST ;
		} break;
	case IDC_EBO_BOO_MNU_CMD_RPT_PAL : {
			m_v_man.Report().OnCommand(wID);
			ISelector_OnItemClick(IDC_EBO_BOO_SEL_CMD_REPS);
			m_selector.Items() << IDC_EBO_BOO_SEL_CMD_REPS ;
		} break;
	case IDC_EBO_BOO_MNU_CMD_SAL_LST : {
			ISelector_OnItemClick(IDC_EBO_BOO_SEL_CMD_SALE);
			m_selector.Items() << IDC_EBO_BOO_SEL_CMD_SALE ;
		} break;
	case IDC_EBO_BOO_MNU_CMD_EXP_LST : {
			ISelector_OnItemClick(IDC_EBO_BOO_SEL_CMD_EXPS);
			m_selector.Items() << IDC_EBO_BOO_SEL_CMD_EXPS ;
		} break;
	case IDC_EBO_BOO_MNU_CMD_SUP_LST : {
			ISelector_OnItemClick(IDC_EBO_BOO_SEL_CMD_SUPL);
			m_selector.Items() << IDC_EBO_BOO_SEL_CMD_SUPL ;
		} break;
	case IDC_EBO_BOO_MNU_CMD_TAX_LST : {
			ISelector_OnItemClick(IDC_EBO_BOO_SEL_CMD_TAXS);
			m_selector.Items() << IDC_EBO_BOO_SEL_CMD_TAXS ;
		} break;
	case IDC_EBO_BOO_MNU_CMD_USR_MAN : { CUserManWzd man_wzd; man_wzd.DoModal(); } break;
	case IDC_EBO_BOO_MNU_CMD_SEL_LFT : { m_selector.Layout().HAlign() =  eHorzAlign::eLeft ; CMainForm_Layout(m_form).RecalcLayout();} break;
	case IDC_EBO_BOO_MNU_CMD_SEL_RIT : { m_selector.Layout().HAlign() =  eHorzAlign::eRight; CMainForm_Layout(m_form).RecalcLayout();} break;
	case IDC_EBO_BOO_MNU_CMD_APP_EXIT: { TWindow::SendMessage(WM_CLOSE); ::PostQuitMessage(0); } break;
	case IDC_EBO_BOO_MNU_CMD_USR_PWD : {
			CPwdDlg dlg_pwd(CPwdDlg::e_change); dlg_pwd.DoModal();
		} break;
	default:
		if (SUCCEEDED(_out::ThemeMan().Select(wID))) {
			const CCmpClrs& pair_ = _out::ThemeMan();

			m_selector.Format().BackColour(pair_.Dark());
			m_selector.Format().ForeColour(pair_.Light());

			m_v_man.Redraw();
		}
		else
		bHandled = FALSE;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CMainForm:: CMainForm(void) : m_wnd(*this) { }
CMainForm::~CMainForm(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CMainForm::Create  (const HWND hParent, const RECT& rcArea)
{
	if (m_wnd.IsWindow())
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;

	RECT rc_ = CMainForm_Layout::CenterArea(rcArea);
	HWND hView = m_wnd.Create(hParent, &rc_, NULL, /*WS_VISIBLE|*/WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN);
	if (!hView)
		return HRESULT_FROM_WIN32(::GetLastError());
	else
		return S_OK;
}

HRESULT       CMainForm::Destroy (void) {
	if (m_wnd.IsWindow())
		m_wnd.SendMessage(WM_CLOSE);
	return S_OK;
}
const bool    CMainForm::IsTheme (const WORD _w_cmd_id) const {
	return _out::ThemeMan().Is(_w_cmd_id);
}
const
CSelector&    CMainForm::Selector(void) const { return m_wnd.m_selector; }
CSelector&    CMainForm::Selector(void)       { return m_wnd.m_selector; }
HRESULT       CMainForm::UpdateLayout(const RECT& rcArea)
{
	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;
	if (!m_wnd.IsWindow())
		return OLE_E_BLANK;
	RECT rc_ = rcArea;
	m_wnd.SetWindowPos(NULL, &rc_, SWP_NOZORDER|SWP_NOACTIVATE);
	return S_OK;
}
const
CViewManager& CMainForm::Views   (void) const { return m_wnd.m_v_man; }
CViewManager& CMainForm::Views   (void)       { return m_wnd.m_v_man; }
HRESULT       CMainForm::Visible (const bool _vis) {

	HRESULT hr_ = S_OK;
	if (m_wnd.IsWindow())
		m_wnd.ShowWindow(_vis ? SW_SHOW : SW_HIDE);
	else
		hr_ = OLE_E_BLANK;

	return hr_;
}

const
CWindow&      CMainForm::Window  (void) const { return m_wnd; }
CWindow&      CMainForm::Window  (void)       { return m_wnd; }

/////////////////////////////////////////////////////////////////////////////

CMainForm::operator  CWindow&    (void) { return this->Window(); }