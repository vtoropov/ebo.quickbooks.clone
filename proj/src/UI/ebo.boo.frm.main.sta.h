#ifndef _EBOBOOFRMMAINSTA_H_3AAA6350_66A0_4E4A_81F0_9B8E2711BA84_INCLUDED
#define _EBOBOOFRMMAINSTA_H_3AAA6350_66A0_4E4A_81F0_9B8E2711BA84_INCLUDED
/*
	Created Tech_dog (ebontrop@gmail.com) on 4-Sep-2018 at 8:59:03p, UTC+7, Novosibirsk, Rodniki, Tuesday;
	This is USB Drive Detective (bitsphereinc.com) desktop app status bar control interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 27-Oct-2019 at 1:27:55p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "shared.uix.ctrl.sta.h"
#include "shared.gen.sys.err.h"

namespace ebo { namespace boo { namespace gui {

	using ex_ui::controls::std::CStatusBar;
	using shared::sys_core::CError;

	class CMainForm_Sta {
	public:
		enum _ndx {
			eMarker  = 0x0,  // indicates a message type, i.e. error, warning, wait, info;
			eMessage = 0x1,  // message body text;
			ePcIcon  = 0x2,  // personal computer icon;
			ePcName  = 0x3,  // personal computer name;
			eRsIcon  = 0x4,  // record count icon;
			eRsCount = 0x5,  // record count indication;
		};
	private:
		CWindow          m_host; // main frame window handle;
		CStatusBar       m_sta ; // status bar control instance;

	public:
		 CMainForm_Sta(void);
		~CMainForm_Sta(void);

	public:
		HRESULT     Create      (HWND hMainFrame);
		HRESULT     Destroy     (void);
		const UINT  Id          (void) const;
		bool        IsValid     (void) const;          // checks status bar window handle for getting validity state;
		bool        IsVisible   (void) const;          // gets current state of status bar visibility;
		HRESULT     IsVisible   (const bool);         // sets current state of status bar visibility;
		HRESULT     UpdateLayout(void);               // updates status bar position by getting host window client rectangle;
		HRESULT     UpdateLayout(const RECT& rcArea); // updates status bar position by available area rectangle provided;

	public:
		LRESULT     OnDrawEvent (WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		HRESULT     SetError    (TErrorRef) ;
		HRESULT     SetInfo     (LPCTSTR)   ;         // sets standard info message;
		HRESULT     SetRecCount (const UINT);
		HRESULT     SetUserUri  (LPCTSTR)   ;         // sets host name and user name as URI

	public:
		operator const HWND(void) const;
	};
}}}

#endif/*_EBOBOOFRMMAINSTA_H_3AAA6350_66A0_4E4A_81F0_9B8E2711BA84_INCLUDED*/