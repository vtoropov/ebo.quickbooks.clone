/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Aug-2018 at 4:23:18p, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop application main form layout interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 27-Oct-2019 at 3:09:59p, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "StdAfx.h"
#include "ebo.boo.frm.main.lay.h"
#include "ebo.boo.res.h"
#include "ebo.boo.pag.lay.bas.h"

using namespace ebo::boo::gui;
using namespace ebo::boo::gui::_impl;

#include "shared.uix.gdi.provider.h"
#include "shared.uix.frms.img.ban.h"

using namespace ex_ui::draw;
using namespace ex_ui::controls::std;
using namespace ex_ui::frames;

/////////////////////////////////////////////////////////////////////////////

CMainForm_Layout::CMainForm_Layout(CMainForm& _main) : m_main(_main) {

	::SetRectEmpty(&m_area);
	if (m_main.Window().IsWindow())
		m_main.Window().GetClientRect(&m_area);
}

/////////////////////////////////////////////////////////////////////////////

RECT    CMainForm_Layout::GetIbanRect(const SIZE _ban) {
	RECT rc_ban = m_area;
	rc_ban.bottom = _ban.cy;
	return rc_ban;
}

RECT    CMainForm_Layout::GetRbarRect(const SIZE& _ban_sz) const {

	RECT rc_bar = m_area;
	rc_bar.top  = m_area.top + _ban_sz.cy;
	return rc_bar;
}

RECT    CMainForm_Layout::GetViewRect(void) const {
	
	RECT rc_view = m_area;

	const CMainForm_Sta& sta_ = _out::MainFrmSta();
	const CSelector& sel_     = m_main.Selector ();

	if (sta_.IsValid() == false)
		return rc_view;

	if (sta_.IsValid() && sta_.IsVisible()) {
		RECT rc_sta = Lay_(m_main.Window()) = sta_.Id();
		rc_view.bottom = rc_sta.top;
	}

	if (sel_.Window().IsWindow() && sel_.Window().IsWindowVisible()) {
		RECT rc_sel = Lay_(m_main.Window()) = sel_.Id();
		if ( rc_sel.left == 0) // attached to the left side;
			rc_view.left  = rc_sel.right;
		else
			rc_view.right = rc_sel.left;
	}


	return rc_view;
}

VOID    CMainForm_Layout::RecalcLayout(void) {

	CWindow main_frm = m_main.Window();

	if (main_frm.IsWindow() == FALSE)
		return;

	RECT rc_area = m_area;
#if(1)
	if (FALSE == main_frm.GetClientRect(&rc_area)) // is already assigned via class constructor;
		return;
#endif
	if (::IsRectEmpty(&rc_area))
		return;
	//
	// (1) gets banner size;
	//
	const SIZE sz_ban = {0};//m_main.Banner().GetSize();
	//
	// (2) gets rebar rectangle and moves the rebar control;
	//
	// (3) gets status bar rectangle and moves the control;
	//
	CMainForm_Sta& sta_bar = _out::MainFrmSta();
	if (sta_bar.IsValid()){
		sta_bar.UpdateLayout(m_area);

		const RECT rc_sta = Lay_(m_main.Window()) = sta_bar.Id();
		rc_area.bottom = rc_sta.top;
	}
	// (4) selector placement;
	m_main.Selector().Layout() << rc_area;
	//
	// (5) gets main frame views' rectangle and moves them;
	//
	const RECT rc_view = this->GetViewRect();

	if (m_main.Views().Customer().IsValid()) { m_main.Views().Customer().UpdateLayout(rc_view); }
	if (m_main.Views().Report  ().IsValid()) { m_main.Views().Report  ().UpdateLayout(rc_view); }
	if (m_main.Views().Sales   ().IsValid()) { m_main.Views().Sales   ().UpdateLayout(rc_view); }
	if (m_main.Views().Expenses().IsValid()) { m_main.Views().Expenses().UpdateLayout(rc_view); }
	if (m_main.Views().Employee().IsValid()) { m_main.Views().Employee().UpdateLayout(rc_view); }
	if (m_main.Views().Taxes   ().IsValid()) { m_main.Views().Taxes   ().UpdateLayout(rc_view); }
	if (m_main.Views().Supplier().IsValid()) { m_main.Views().Supplier().UpdateLayout(rc_view); }
	if (m_main.Views().Accounts().IsValid()) { m_main.Views().Accounts().UpdateLayout(rc_view); }
}

/////////////////////////////////////////////////////////////////////////////

RECT   CMainForm_Layout::AdjustRect(const RECT& _rc) {

	RECT rc_adjusted = _rc;
	const RECT rc_area = CMainForm_Layout::GetAvailableArea();

	if (_rc.left < rc_area.left)
		::OffsetRect(&rc_adjusted, rc_area.left - _rc.left, 0);

	if (_rc.top  < rc_area.top )
		::OffsetRect(&rc_adjusted, 0, rc_area.top - _rc.top);

	const LONG x_delta = rc_adjusted.right  - rc_area.right ;  if (0 < x_delta) rc_adjusted.right  = rc_area.right ;
	const LONG y_delta = rc_adjusted.bottom - rc_area.bottom;  if (0 < y_delta) rc_adjusted.bottom = rc_area.bottom;

	return rc_adjusted;
}

RECT   CMainForm_Layout::CenterArea(const RECT& _rc) {

	const RECT  screen_ = CMainForm_Layout::GetAvailableArea();
	const POINT left_top = {
		(screen_.right - screen_.left) / 2  - (_rc.right - _rc.left) / 2,
		(screen_.bottom - screen_.top) / 2  - (_rc.bottom - _rc.top) / 2,
	};
	RECT center_ = {
		left_top.x,
		left_top.y,
		left_top.x + (_rc.right - _rc.left),
		left_top.y + (_rc.bottom - _rc.top)
	};

	return center_;
}

RECT   CMainForm_Layout::FormDefPlace(void) {

	RECT rc_ = {0, 0, 700, 400};

	return CMainForm_Layout::CenterArea(rc_);
}

/////////////////////////////////////////////////////////////////////////////

RECT   CMainForm_Layout::GetAvailableArea(void)
{
	const POINT ptZero = {0};
	const HMONITOR hMonitor = ::MonitorFromPoint(ptZero, MONITOR_DEFAULTTOPRIMARY);
	MONITORINFO mInfo  = {0};
	mInfo.cbSize = sizeof(MONITORINFO);
	::GetMonitorInfo(hMonitor, &mInfo);
	return mInfo.rcWork;
}