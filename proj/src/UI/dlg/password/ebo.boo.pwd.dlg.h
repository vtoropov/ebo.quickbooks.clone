#ifndef _EBOBOOPWDDLG_H_DADB01B9_2D3C_428b_9F8E_3F527A43ED5D_INCLUDED
#define _EBOBOOPWDDLG_H_DADB01B9_2D3C_428b_9F8E_3F527A43ED5D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jan-2016 at 0:28:24a, UTC+7, Phuket, Rawai, Monday;
	This is File Watcher(thefileguardian.com) desktop password dialog interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Boo project on 7-Jan-2020 at 11:21:43a, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.frms.img.ban.h"
#include "ebo.boo.pwd.pag.1st.h"
#include "ebo.boo.pwd.pag.2nd.h"

namespace ebo { namespace boo { namespace gui {

	using shared::sys_core::CError;
	using ex_ui::frames::CImageBanner ;
	using ex_ui::draw::defs::IRenderer;

	class CPwdDlg {
	public :
		enum _mode {
			e_auth   = 0x0, // user authentication mode; default;
			e_change = 0x1, // user password change mode;
		};
	private:
		class CPwdDlgImpl : public  ::ATL::CDialogImpl<CPwdDlgImpl>, IRenderer, ITabSetCallback {
		                    typedef ::ATL::CDialogImpl<CPwdDlgImpl>  TDialog;
		friend class CPwdDlg;
		private:
			CImageBanner     m_banner;
			CPwdDlg::_mode   m_mode  ;
			CPagePwd_1st     m_page_1;
			CPagePwd_2nd     m_page_2;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CPwdDlgImpl)
				MESSAGE_HANDLER     (WM_COMMAND   ,   OnCommand)
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy)
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDlg)
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCmd )
				COMMAND_ID_HANDLER  (IDOK         ,   OnButDown)
				COMMAND_ID_HANDLER  (IDCANCEL     ,   OnButDown)
			END_MSG_MAP()
		public:
			 CPwdDlgImpl(const CPwdDlg::_mode);
			~CPwdDlgImpl(void);
		private:
			LRESULT OnCommand(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnButDown(WORD , WORD   , HWND   , BOOL&);
			LRESULT OnDestroy(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnInitDlg(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnSysCmd (UINT , WPARAM , LPARAM , BOOL&);

		private: // IRenderer
			HRESULT DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const override sealed;
		};
	private:
		CPwdDlgImpl  m_dlg;
	public:
		 CPwdDlg(const CPwdDlg::_mode);
		~CPwdDlg(void);
	public:
		INT_PTR    DoModal(void);
	private:
		CPwdDlg(const CPwdDlg&);
		CPwdDlg& operator= (const CPwdDlg&);
	};

}}}

#endif/*_EBOBOOPWDDLG_H_DADB01B9_2D3C_428b_9F8E_3F527A43ED5D_INCLUDED*/