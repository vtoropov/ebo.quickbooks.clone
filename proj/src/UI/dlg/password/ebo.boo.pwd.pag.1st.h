#ifndef _EBOBOOPWDPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOBOOPWDPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Jan-2020 at 7:01:45p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app user password dialog authentication page interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.stg.dat.acc.h"

namespace ebo { namespace boo { namespace gui {

	class CPagePwd_1st : public CTabPageBase, public  ITabPageEvents {
	                    typedef CTabPageBase  TBasePage;
	private:
	public :
		 CPagePwd_1st(ITabSetCallback&);
		~CPagePwd_1st(void);

	private:
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TBasePage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed;

	public:
	};

}}}

#endif/*_EBOBOOPWDPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/