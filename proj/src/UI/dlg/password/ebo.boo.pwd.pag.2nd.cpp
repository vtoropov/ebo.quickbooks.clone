/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jan-2020 at 4:58:01a, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app user password dialog change page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.pwd.pag.2nd.h"
#include "ebo.boo.res.dlg.pwd.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPagePwd_2nd_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDR_EBO_BOO_PWD_2ND_WRN  ,  // page warning image   ;
			e_pwd_old = IDC_EBO_BOO_PWD_2ND_PWD_0,  // old password edit box;
			e_pwd_new = IDC_EBO_BOO_PWD_2ND_PWD_1,  // new password edit box;
			e_pwd_wrn = IDC_EBO_BOO_PWD_2ND_MSG  ,  // warning message ctrl ;
		};
	};
	typedef CPagePwd_2nd_Ctl This_Ctl;

	class CPagePwd_2nd_Layout {
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPagePwd_2nd_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );

		}
		VOID   OnSize  (void) {
		}
	};

	class CPagePwd_2nd_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPagePwd_2nd_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;

			::WTL::CEdit pwd_old = Lay_(m_page_ref) << This_Ctl::e_pwd_old;
			if (pwd_old)
				pwd_old.SetCueBannerText(_T("Type old password"), TRUE);

			::WTL::CEdit pwd_new = Lay_(m_page_ref) << This_Ctl::e_pwd_new;
			if (pwd_new)
				pwd_new.SetCueBannerText(_T("Type new password"), TRUE);

			return m_error;
		}

	};

	class CPagePwd_2nd_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPagePwd_2nd_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		bool   OnPassword(void) {

			CAtlString cs_pwd_old;
			CWindow pwd_old = Lay_(m_page_ref) << This_Ctl::e_pwd_old;
			if (0!= pwd_old) {
				pwd_old.GetWindowTextW(cs_pwd_old);
				return true ;
			}
			else {
				CWindow pwd_wrn = Lay_(m_page_ref) << This_Ctl::e_pwd_wrn;
				if (0!= pwd_wrn)
					pwd_wrn.SetWindowTextW(_T("Invalid password."));
				return false;
			}
		}
		bool   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPagePwd_2nd:: CPagePwd_2nd(ITabSetCallback& _set_snk):
       TBasePage(IDD_EBO_OUT_PWD_2ND_PAG, *this, _set_snk) {
}

CPagePwd_2nd::~CPagePwd_2nd(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPagePwd_2nd::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPagePwd_2nd::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPagePwd_2nd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPagePwd_2nd_Init init_(*this);
	init_.OnCreate();
	
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPagePwd_2nd::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPagePwd_2nd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPagePwd_2nd::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPagePwd_2nd_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE ) {
				TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPagePwd_2nd::IsChanged   (void) const {
	return false;
}

CAtlString CPagePwd_2nd::GetPageTitle(void) const {  static CAtlString cs_title(_T("Authentication")); return cs_title; }

VOID       CPagePwd_2nd::UpdateData  (const DWORD _opt) {
	_opt;
}