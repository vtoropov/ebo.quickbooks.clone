/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jan-2016 at 0:48:31a, GMT+7, Phuket, Rawai, Monday;
	This is File Watcher(thefileguardian.com) desktop password dialog interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Boo project on 7-Jan-2020 at 11:23:21a, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "ebo.boo.pwd.dlg.h"
#include "ebo.boo.res.dlg.pwd.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.gen.app.res.h"
#include "shared.uix.gdi.provider.h"

using namespace shared::user32;
using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl {

	class CPagePwd_Ctl {
	public:
		enum _ctl : WORD {
			e_but_yes = IDOK    ,    // dialog okay   ;
			e_but_can = IDCANCEL,    // dialog cancel ;
		};
	};
	typedef CPagePwd_Ctl This_Ctl;


	class CPwdDlg_Layout {
	private:
		enum _size {
			e_gap_bottom = 0x7,
		};
	private:
		RECT           m_client_area;
		const CWindow& m_dlg_ref;
	public:
		CPwdDlg_Layout(const CWindow& dlg_ref) : m_dlg_ref(dlg_ref) {
			if (m_dlg_ref.IsWindow())
				m_dlg_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID    OnCreate(void) {
		}
		RECT    OnSize  (const CPwdDlg::_mode, const CImageBanner& _ban) {

			const SIZE sz_ban = _ban.ReqSize();
			const CWindow  but_ok = Lay_(m_dlg_ref) << This_Ctl::e_but_yes;
			const RECT rec_but_ok = Lay_(m_dlg_ref)  = This_Ctl::e_but_yes;

			RECT rc_area = m_client_area;
			if (0 != but_ok) {
				rc_area.bottom = rec_but_ok.top - _size::e_gap_bottom;
			}
			rc_area.top = sz_ban.cy;

			return rc_area;
		}
	};

	class CPwdDlg_Init {
	private:
		const CWindow& m_page_ref;
	public:
		CPwdDlg_Init(const CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		VOID   OnCreate(void) {
		}
	};

	class CPwdDlg_Handler
	{
	private:
		const CWindow& m_page_ref;
	public:
		CPwdDlg_Handler(const CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		bool   OnPassword(void) {
			return (true);
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPwdDlg::CPwdDlgImpl::CPwdDlgImpl(const CPwdDlg::_mode e_mode):
	IDD     (CPwdDlg::e_auth == e_mode ? IDD_EBO_BOO_PWD_DLG_0 : IDD_EBO_BOO_PWD_DLG_1),
	m_banner(IDC_EBO_BOO_PWD_DLG_BAN),
	m_mode  (e_mode)                 ,
	m_page_1(*this ), m_page_2(*this )   { m_banner.Renderer(this); }
CPwdDlg::CPwdDlgImpl::~CPwdDlgImpl(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT CPwdDlg::CPwdDlgImpl::OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam , BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = TRUE;
	switch (wParam) {
	case IDOK     : {
			CPwdDlg_Handler handler_(*this);
			bHandled = handler_.OnPassword();
		} break;
	case IDCANCEL : {
			
		} break;
	default:
		bHandled = FALSE;
	}

	if (bHandled) {
		TDialog::EndDialog(static_cast<INT>(wParam));
	}
	return 0;
}

LRESULT CPwdDlg::CPwdDlgImpl::OnButDown(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled) {
	wNotifyCode; wID; hWndCtl; bHandled = TRUE;
	switch (wID) {
	case IDOK :  {
			CPwdDlg_Handler handler_(*this);
			if (!handler_.OnPassword())
				return 0;
		} break;
	default:
	bHandled = FALSE;
	}
	if (bHandled)
		TDialog::EndDialog(wID);
	return 0;
}

LRESULT CPwdDlg::CPwdDlgImpl::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam , BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	if (m_page_1.IsWindow()) { m_page_1.DestroyWindow(); m_page_1.m_hWnd = NULL; }
	if (m_page_2.IsWindow()) { m_page_2.DestroyWindow(); m_page_2.m_hWnd = NULL; }
	m_banner.Destroy();
	return 0;
}

LRESULT CPwdDlg::CPwdDlgImpl::OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam , BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = TRUE;

	HRESULT hr_ = m_banner.Create(TDialog::m_hWnd); hr_;

	CPwdDlg_Layout layout_(*this);
	layout_.OnCreate();

	CPwdDlg_Init init_(*this);
	init_.OnCreate();

	RECT rc_page = layout_.OnSize(m_mode, m_banner);

	if (_mode::e_auth == m_mode ) {
		m_page_1.Create(*this);
		m_page_1.MoveWindow(&rc_page); m_page_1.ShowWindow(SW_SHOW);
	}
	if (_mode::e_change == m_mode){
		m_page_2.Create(*this);
		m_page_2.MoveWindow(&rc_page); m_page_2.ShowWindow(SW_SHOW);
	}

	TDialog::CenterWindow();
	{
		CApplicationIconLoader loader_(IDR_EBO_BOO_PWD_DLG_ICO);
		TDialog::SetIcon(loader_.DetachLargeIcon(), TRUE );
		TDialog::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}

	if (CPwdDlg::e_change == m_mode)
		TDialog::SetWindowTextW(_T("User Password Change"));

	return 0;
}

LRESULT CPwdDlg::CPwdDlgImpl::OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam , BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam) {
	case SC_CLOSE:  {
			TDialog::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT CPwdDlg::CPwdDlgImpl::DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& _rc_area) const {
	hChild;
	if (NULL == hSurface)
		return __DwordToHresult(ERROR_INVALID_HANDLE);
	if (::IsRectEmpty(&_rc_area))
		return OLE_E_INVALIDRECT;
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getsyscolor
	CZBuffer dc_(hSurface, _rc_area);
	dc_.FillSolidRect(
		&_rc_area, ::GetSysColor(COLOR_3DFACE)
	);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CPwdDlg:: CPwdDlg(const CPwdDlg::_mode e_mode) : m_dlg(e_mode) {}
CPwdDlg::~CPwdDlg(void) {}

/////////////////////////////////////////////////////////////////////////////

INT_PTR    CPwdDlg::DoModal(void) {
	const INT_PTR result_ = m_dlg.DoModal();
	return result_;
}