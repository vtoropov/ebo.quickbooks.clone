#ifndef _EBOBOOEMPDATTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _EBOBOOEMPDATTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Nov-2019 at 11:20:24a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app employee dialog personal data tabset interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.emp.dat.pag.1st.h"
#include "ebo.boo.emp.dat.pag.2nd.h"
#include "ebo.boo.emp.dat.pag.3rd.h"
#include "ebo.boo.emp.dat.pag.4th.h"
#include "ebo.boo.emp.dat.pag.5th.h"

namespace ebo { namespace boo { namespace gui {

	class CEmpTabSet
	{
	public:
		enum _pages : INT {
			e_pers  = 0,  // employee personal information page;
			e_empl  = 1,  // employee data page;
			e_cont  = 2,  // employee contacts page;
			e_avat  = 3,  // employee avatar page;
			e_addr  = 4,  // employee current address page;
		};
	private:
		WTL::CTabCtrl m_cTabCtrl;
		_pages        m_e_active;

	private: //tab page(s)
		CPageEmp_1st  m_1st_page;
		CPageEmp_2nd  m_2nd_page;
		CPageEmp_3rd  m_3rd_page;
		CPageEmp_4th  m_4th_page;
		CPageEmp_5th  m_5th_page;

	public:
		 CEmpTabSet (ITabSetCallback&);
		~CEmpTabSet (void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy (void)        ;
		_pages        Selected(void)   const;
		HRESULT       Selected(const _pages);
		VOID          UpdateLayout(void);
	public:
	};
}}}

#endif/*_EBOBOOEMPDATTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/