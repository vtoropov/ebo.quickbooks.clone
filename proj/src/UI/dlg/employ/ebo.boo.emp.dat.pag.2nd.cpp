/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Dec-2019 at 8:32:29p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app employee dialog employment data page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.emp.dat.pag.2nd.h"
#include "ebo.boo.res.dlg.emp.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageEmp_2nd_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_BOO_EMP_PAG_2ND_AVA,    // page avatar of tax;
			e_ava_cap = IDC_EBO_BOO_EMP_PAG_2ND_CAP,    // page caption of tax;
			e_ava_ct1 = IDC_EBO_BOO_EMP_PAG_2ND_AV1,    // page avatar of period;
			e_ava_ca1 = IDC_EBO_BOO_EMP_PAG_2ND_CA1,    // page caption of period;
			e_ava_ct2 = IDC_EBO_BOO_EMP_PAG_2ND_AV2,    // page avatar of auxiliary data;
			e_ava_ca2 = IDC_EBO_BOO_EMP_PAG_2ND_CA2,    // page caption of auxiliary data;
		};
	};
	typedef CPageEmp_2nd_Ctl This_Ctl;

	class CPageEmp_2nd_Layout {
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageEmp_2nd_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_cap;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}

			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ct1, This_Ctl::e_ava_ct1) );

			CWindow  cap_av1 = Lay_(m_page_ref) << This_Ctl::e_ava_ca1;
			if (0 != cap_av1) {
				cap_av1.SetFont(CTabPageBase::SectionCapFont());
			}

			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ct2, This_Ctl::e_ava_ct2) );

			CWindow  cap_av2 = Lay_(m_page_ref) << This_Ctl::e_ava_ca2;
			if (0 != cap_av2) {
				cap_av2.SetFont(CTabPageBase::SectionCapFont());
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPageEmp_2nd_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;

	public:
		CPageEmp_2nd_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;
			return m_error;
		}

	};

	class CPageEmp_2nd_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPageEmp_2nd_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageEmp_2nd:: CPageEmp_2nd(ITabSetCallback& _set_snk):
	TBasePage(IDD_EBO_BOO_EMP_PAG_2ND, *this, _set_snk) {
}

CPageEmp_2nd::~CPageEmp_2nd(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageEmp_2nd::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageEmp_2nd::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageEmp_2nd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPageEmp_2nd_Init init_(*this);

	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageEmp_2nd::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageEmp_2nd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageEmp_2nd::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
		if (!TBasePage::m_bInited)
			break;
		const WORD wNotify = HIWORD(wParam);
		const WORD ctrlId  = LOWORD(wParam);

		CPageEmp_2nd_Handler handler_(*this);

		if (bHandled == FALSE)
			bHandled = handler_.OnCommand(ctrlId, wNotify);
		if (bHandled == TRUE ) {
			TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
		}
	} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageEmp_2nd::IsChanged   (void) const {
	return false;
}

CAtlString CPageEmp_2nd::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Employment ")); return cs_title; }

VOID       CPageEmp_2nd::UpdateData  (const DWORD _opt) {
	_opt;
}