/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Nov-2019 at 11:21:23a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app employee data dialog interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.emp.dat.dlg.h"
#include "ebo.boo.res.dlg.emp.h"
#include "ebo.boo.pag.lay.bas.h"

using namespace ebo::boo::data;
using namespace ebo::boo::gui;

#include "shared.gen.app.res.h"
#include "shared.uix.gdi.provider.h"

using namespace shared::user32;
using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl {

	class CEmpDlg_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_bann  = IDC_EBO_BOO_EMP_DLG_BAN,
			e_ctl_sepa  = IDC_EBO_BOO_EMP_DLG_SEP,
			e_ctl_save  = IDOK      ,    // 'Save' command button;
			e_ctl_close = IDCANCEL  
		};
	};
	typedef CEmpDlg_Ctl This_Ctl;

	class CEmpDlg_Initer {
	private:
		CWindow&     m_dlg_ref;
	public:
		 CEmpDlg_Initer (CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {}
		~CEmpDlg_Initer (void) {}

	public:
		HRESULT      OnCreate(void) {
			HRESULT hr_ = S_OK;
			CAtlString cs_cap;

			CData_Action act_ = CData_Action::e_new;

			switch (act_) {
			case TActionType::e_new  : { cs_cap = _T("Creating New Employee"); } break;
			case TActionType::e_edit : { cs_cap.Format(
						_T("Editing Employee: %s"), _T("#n/a")
					);
				} break;
			default :
				cs_cap = _T("#Error");
				hr_ = __DwordToHresult(ERROR_NOT_SUPPORTED);
			}
			m_dlg_ref.SetWindowText((LPCTSTR)cs_cap);

			return  hr_;
		}
	};

	class CEmpDlg_Layout {
	private:
		enum _e {
			e_gap = 0x04,
			e_ban = 0x3c,  // 60x750px is taken from PNG banner file; this is a hight of the banner;
		};

	private:
		CWindow&     m_dlg_ref;
		RECT         m_dlg_rec;

	public:
		CEmpDlg_Layout(CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {

			if (m_dlg_ref.GetClientRect(&m_dlg_rec) == FALSE)
				::SetRectEmpty(&m_dlg_rec);
		}

	public:
		RECT     BanArea(VOID) {
			__no_args;
			RECT rc_ban  = {0};
			CWindow ban_ = m_dlg_ref.GetDlgItem(This_Ctl::e_ctl_bann);
			if (NULL == ban_)
				return rc_ban;

			rc_ban = (Lay_(m_dlg_ref) = This_Ctl::e_ctl_bann);
			return rc_ban;
		}

		RECT     TabArea(VOID) {
			const
			RECT rc_ban = this->BanArea();
			RECT rc_sep = (Lay_(m_dlg_ref) = This_Ctl::e_ctl_sepa);

			RECT rc_tab = {0};
			::SetRect(
				&rc_tab, m_dlg_rec.left + _e::e_gap, rc_ban.bottom - _e::e_gap/2, m_dlg_rec.right - _e::e_gap, rc_sep.top - _e::e_gap
			);

			return rc_tab;
		}
	};

	class CEmpDlg_Handler {
	private:
		CWindow&     m_dlg_ref;

	public:
		CEmpDlg_Handler(CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {}

	public:
		VOID   OnCommand(WORD wNotify, WORD ctrlId) {
			wNotify; ctrlId;
			if (This_Ctl::e_ctl_close == ctrlId) {
				::EndDialog (m_dlg_ref,  ctrlId);
			}
			if (This_Ctl::e_ctl_save  == ctrlId) {
				::EndDialog (m_dlg_ref,  ctrlId);
			}
		}

	};

}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CEmpDlg::CEmpDlgImpl:: CEmpDlgImpl(void) : 
	IDD(IDD_EBO_BOO_EMP_DLG), m_banner(IDR_EBO_BOO_EMP_DLG_BAN), m_tabset(*this) {
	m_banner.Renderer(this);	
}
CEmpDlg::CEmpDlgImpl::~CEmpDlgImpl(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT CEmpDlg::CEmpDlgImpl::OnBtnCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const WORD wNotify = HIWORD(wParam); wNotify;
	const WORD ctrlId  = LOWORD(wParam); ctrlId ;

	CEmpDlg_Handler hand_(*this);
	hand_.OnCommand(wNotify, ctrlId);

	return 0;
}

LRESULT CEmpDlg::CEmpDlgImpl::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	m_tabset.Destroy();
	m_banner.Destroy();
	return 0;
}

LRESULT CEmpDlg::CEmpDlgImpl::OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	TDialog::CenterWindow();
	{
		CApplicationIconLoader   loader_(IDR_EBO_BOO_EMP_DLG_ICO);
		TDialog::SetIcon(loader_.DetachLargeIcon(), TRUE );
		TDialog::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	HRESULT hr_ = m_banner.Create(TDialog::m_hWnd, This_Ctl::e_ctl_bann);
	if (SUCCEEDED(hr_)){}

	CEmpDlg_Initer   initer_(*this); initer_.OnCreate();

	CEmpDlg_Layout   layout_(*this);
	const RECT rc_tabs = layout_.TabArea();

	hr_ = m_tabset.Create(*this, rc_tabs);

	return 0;
}

LRESULT CEmpDlg::CEmpDlgImpl::OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	switch (wParam)  {
	case VK_RETURN : {
			CWindow sav_but = (Lay_(*this)) << This_Ctl::e_ctl_save;
			if (sav_but.IsWindowEnabled()) {
				CEmpDlg_Handler hand_(*this); hand_.OnCommand(0, This_Ctl::e_ctl_save);
			}
		} break;
	}
	return 0;
}

LRESULT CEmpDlg::CEmpDlgImpl::OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			TDialog::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CEmpDlg::CEmpDlgImpl::OnTabNotify (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled) {
	idCtrl; pnmh; bHandled = TRUE;
	m_tabset.UpdateLayout();
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CEmpDlg::CEmpDlgImpl::TabSet_OnDataChanged (const UINT pageId, const bool bChanged ) {
	pageId; bChanged;
	HRESULT hr_ = S_OK;

	CWindow bt_app = TDialog::GetDlgItem(IDOK);
	if (NULL != bt_app) {
		bt_app.EnableWindow(bChanged);
	}

	return  hr_;
}

HRESULT   CEmpDlg::CEmpDlgImpl::TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) {
	ctrlId; _rc_ctrl;
	HRESULT hr_ = S_OK;
	switch (ctrlId) {
	case IDYES   : case IDOK:
	case IDCANCEL:
	{
		CWindow ctl_btn = TDialog::GetDlgItem(ctrlId);
		if (NULL != ctl_btn) {
			ctl_btn.GetWindowRect(&_rc_ctrl);
		}
	} break;
	default:
	hr_ = DISP_E_PARAMNOTFOUND;
	}
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CEmpDlg::CEmpDlgImpl::DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& _rc_area) const {
	hChild;
	if (NULL == hSurface)
		return __DwordToHresult(ERROR_INVALID_HANDLE);
	if (::IsRectEmpty(&_rc_area))
		return OLE_E_INVALIDRECT;
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getsyscolor
	CZBuffer dc_(hSurface, _rc_area);
	dc_.FillSolidRect(
		&_rc_area, ::GetSysColor(COLOR_3DFACE)
	);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CEmpDlg:: CEmpDlg(void) {}
CEmpDlg::~CEmpDlg(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CEmpDlg::DoModal(void) {

	const INT_PTR result = m_dlg.DoModal();

	return (result == IDCANCEL ? S_FALSE : S_OK);
}