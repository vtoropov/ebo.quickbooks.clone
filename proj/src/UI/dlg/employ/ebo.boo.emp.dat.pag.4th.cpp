/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Dec-2019 at 2:32:51p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app employee dialog avatar data page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.emp.dat.pag.4th.h"
#include "ebo.boo.res.dlg.emp.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageEmp_4th_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_BOO_EMP_PAG_4TH_AVA,    // page avatar ;
			e_ava_cap = IDC_EBO_BOO_EMP_PAG_4TH_CAP,    // page caption;
			e_ava_but = IDC_EBO_BOO_EMP_PAG_4TH_BUT,    // avatar browsing button;
		};
	};
	typedef CPageEmp_4th_Ctl This_Ctl;

	class CPageEmp_4th_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageEmp_4th_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_cap;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPageEmp_4th_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;

	public:
		CPageEmp_4th_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;
			return m_error;
		}

	};

	class CPageEmp_4th_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPageEmp_4th_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageEmp_4th:: CPageEmp_4th(ITabSetCallback& _set_snk):
	TBasePage(IDD_EBO_BOO_EMP_PAG_4TH, *this, _set_snk) {
}

CPageEmp_4th::~CPageEmp_4th(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageEmp_4th::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageEmp_4th::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageEmp_4th_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPageEmp_4th_Init init_(*this);

	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageEmp_4th::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageEmp_4th_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageEmp_4th::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
		if (!TBasePage::m_bInited)
			break;
		const WORD wNotify = HIWORD(wParam);
		const WORD ctrlId  = LOWORD(wParam);

		CPageEmp_4th_Handler handler_(*this);

		if (bHandled == FALSE)
			bHandled = handler_.OnCommand(ctrlId, wNotify);
		if (bHandled == TRUE ) {
			TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
		}
	} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageEmp_4th::IsChanged   (void) const {
	return false;
}

CAtlString CPageEmp_4th::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Avatar ")); return cs_title; }

VOID       CPageEmp_4th::UpdateData  (const DWORD _opt) {
	_opt;
}