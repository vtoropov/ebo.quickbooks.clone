/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Nov-2019 at 11:20:43a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app employee dialog personal data tabset interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.emp.dat.tab.set.h"
#include "ebo.boo.res.dlg.emp.h"

using namespace ebo::boo::gui;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CEmpTabSet_Layout
	{
	private:
		RECT    m_area;
	public:
		CEmpTabSet_Layout(const RECT& rcArea) : m_area(rcArea) {}

	public:
		RECT    TabsArea (void) const {
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};
}}}}

using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CEmpTabSet:: CEmpTabSet(ITabSetCallback& _snk) :
	m_e_active(_pages::e_pers), m_1st_page(_snk), m_2nd_page(_snk), m_3rd_page(_snk), m_4th_page(_snk), m_5th_page(_snk) { _snk; }
CEmpTabSet::~CEmpTabSet(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CEmpTabSet::Create(const HWND hParent, const RECT& rcArea) {
	hParent; rcArea;
	HRESULT hr_ = S_OK;

	if(FALSE == ::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return (hr_ = E_INVALIDARG);

	CEmpTabSet_Layout layout(rcArea);
	RECT rcTabs  = layout.TabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs ,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_EBO_BOO_EMP_DLG_TAB
		);
	const SIZE szPadding = {
		5, 2
	};
	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());
	m_cTabCtrl.SetPadding(szPadding);
	m_cTabCtrl.SetItemSize(128,  24);

	INT nIndex = 0;
	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW; // auto-placement is made by default implementation of create/initialize page handler;

	m_cTabCtrl.AddItem(m_1st_page.GetPageTitle());
	{
		m_1st_page.Create(m_cTabCtrl.m_hWnd);
		m_1st_page.SetWindowPos(
				HWND_TOP,
				0, 0, 0 , 0,
				dwFlags
			);
		if (m_1st_page.IsWindow()) m_1st_page.Index(nIndex++);
	}
	m_cTabCtrl.AddItem(m_2nd_page.GetPageTitle());
	{
		m_2nd_page.Create(m_cTabCtrl.m_hWnd);
		m_2nd_page.SetWindowPos(
			HWND_TOP,
			0, 0, 0 , 0,
			dwFlags
		);
		if (m_2nd_page.IsWindow()) m_2nd_page.Index(nIndex++);
	}
	m_cTabCtrl.AddItem(m_3rd_page.GetPageTitle());
	{
		m_3rd_page.Create(m_cTabCtrl.m_hWnd);
		m_3rd_page.SetWindowPos(
			HWND_TOP,
			0, 0, 0 , 0,
			dwFlags
		);
		if (m_3rd_page.IsWindow()) m_3rd_page.Index(nIndex++);
	}
	m_cTabCtrl.AddItem(m_4th_page.GetPageTitle());
	{
		m_4th_page.Create(m_cTabCtrl.m_hWnd);
		m_4th_page.SetWindowPos(
			HWND_TOP,
			0, 0, 0 , 0,
			dwFlags
		);
		if (m_4th_page.IsWindow()) m_4th_page.Index(nIndex++);
	}
	m_cTabCtrl.AddItem(m_5th_page.GetPageTitle());
	{
		m_5th_page.Create(m_cTabCtrl.m_hWnd);
		m_5th_page.SetWindowPos(
			HWND_TOP,
			0, 0, 0 , 0,
			dwFlags
		);
		if (m_5th_page.IsWindow()) m_5th_page.Index(nIndex++);
	}

	m_cTabCtrl.SetCurSel(m_e_active);

	this->UpdateLayout();

	return S_OK;
}

HRESULT       CEmpTabSet::Destroy(void)
{
	if (m_1st_page.IsWindow()){
		m_1st_page.DestroyWindow();  m_1st_page.m_hWnd = NULL;
	}
	if (m_2nd_page.IsWindow()){
		m_2nd_page.DestroyWindow();  m_2nd_page.m_hWnd = NULL;
	}
	if (m_3rd_page.IsWindow()){
		m_3rd_page.DestroyWindow();  m_3rd_page.m_hWnd = NULL;
	}
	if (m_4th_page.IsWindow()){
		m_4th_page.DestroyWindow();  m_4th_page.m_hWnd = NULL;
	}
	if (m_5th_page.IsWindow()){
		m_5th_page.DestroyWindow();  m_5th_page.m_hWnd = NULL;
	}
	return S_OK;
}

CEmpTabSet::_pages
              CEmpTabSet::Selected(void) const      { return m_e_active; }
HRESULT       CEmpTabSet::Selected(const _pages _v) {
	HRESULT hr_ = S_OK;
	const bool b_changed = (_v != m_e_active);
	m_e_active  = _v;
	if (b_changed && m_cTabCtrl.IsWindow()) {
		m_cTabCtrl.SetCurSel(m_e_active);
		this->UpdateLayout();
	}
	return  hr_;
}

void          CEmpTabSet::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	if (m_1st_page.IsWindow()) m_1st_page.ShowWindow(nTabIndex == m_1st_page.Index() ? SW_SHOW : SW_HIDE);
	if (m_2nd_page.IsWindow()) m_2nd_page.ShowWindow(nTabIndex == m_2nd_page.Index() ? SW_SHOW : SW_HIDE);
	if (m_3rd_page.IsWindow()) m_3rd_page.ShowWindow(nTabIndex == m_3rd_page.Index() ? SW_SHOW : SW_HIDE);
	if (m_4th_page.IsWindow()) m_4th_page.ShowWindow(nTabIndex == m_4th_page.Index() ? SW_SHOW : SW_HIDE);
	if (m_5th_page.IsWindow()) m_5th_page.ShowWindow(nTabIndex == m_5th_page.Index() ? SW_SHOW : SW_HIDE);
}