/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Nov-2019 at 11:19:28a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app employee dialog personal data page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.emp.dat.pag.1st.h"
#include "ebo.boo.res.dlg.emp.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageEmp_1st_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_BOO_EMP_PAG_1ST_AVA,    // page avatar ;
			e_ava_cap = IDC_EBO_BOO_EMP_PAG_1ST_CAP,    // page caption;
			e_ava_sub = IDC_EBO_BOO_EMP_PAG_1ST_DSP,    // page sub-section title;
			e_ava_sim = IDC_EBO_BOO_EMP_PAG_1ST_AV2,    // page sub-avatar control;
		};
	};
	typedef CPageEmp_1st_Ctl This_Ctl;

	class CPageEmp_1st_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageEmp_1st_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_sim, This_Ctl::e_ava_sim) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_cap;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}
			CWindow  cap_dsp = Lay_(m_page_ref) << This_Ctl::e_ava_sub;
			if (0 != cap_dsp) {
				cap_dsp.SetFont(CTabPageBase::SectionCapFont());
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPageEmp_1st_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPageEmp_1st_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;
			return m_error;
		}

	};

	class CPageEmp_1st_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPageEmp_1st_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageEmp_1st:: CPageEmp_1st(ITabSetCallback& _set_snk):
       TBasePage(IDD_EBO_BOO_EMP_PAG_1ST, *this, _set_snk) {
}

CPageEmp_1st::~CPageEmp_1st(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageEmp_1st::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageEmp_1st::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageEmp_1st_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPageEmp_1st_Init init_(*this);
	
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageEmp_1st::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageEmp_1st_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageEmp_1st::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPageEmp_1st_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE ) {
				TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageEmp_1st::IsChanged   (void) const {
	return false;
}

CAtlString CPageEmp_1st::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Personal Data ")); return cs_title; }

VOID       CPageEmp_1st::UpdateData  (const DWORD _opt) {
	_opt;
}