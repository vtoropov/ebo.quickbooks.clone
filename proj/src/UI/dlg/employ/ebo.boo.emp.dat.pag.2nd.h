#ifndef _EBOBOOEMPDATPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOBOOEMPDATPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Dec-2019 at 8:30:18p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account app employee dialog employment data page interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.stg.dat.acc.h"

namespace ebo { namespace boo { namespace gui {

	class CPageEmp_2nd : public CTabPageBase, public  ITabPageEvents {
	                    typedef CTabPageBase  TBasePage;
	private:
	public :
		 CPageEmp_2nd(ITabSetCallback&);
		~CPageEmp_2nd(void);

	private:
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TBasePage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed;

	public:
	};

}}}

#endif/*_EBOBOOEMPDATPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/