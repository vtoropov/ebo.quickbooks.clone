#ifndef _EBOBOOUSRPRFPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOBOOUSRPRFPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Sep-2018 at 11:38:45a, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
	This is USB Drive Detective (bitsphereinc.com) main view options dialog page interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 15-Sep-2019 at 1:26:03p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to Ebo Pack personal account app on 22-Oct-2019 at 8:19:34a, UTC+7, Novosibirsk, Light Coloured, Tuesday;
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.stg.dat.acc.h"

namespace ebo { namespace boo { namespace gui {

	using ebo::boo::data::access::CRole_Enum ;
	using ebo::boo::data::access::TAccRoles  ;
	using ebo::boo::data::access::CRole      ;
	using ebo::boo::data::access::CState     ;
	using ebo::boo::data::access::CState_Enum;
	using ebo::boo::data::access::TAccStates ;

	class CPageProfile_1st : public CTabPageBase, public  ITabPageEvents {
	                        typedef CTabPageBase  TBasePage;
	private:
		CRole_Enum    m_roles;
		CState_Enum   m_stats;
	public :
		 CPageProfile_1st(ITabSetCallback&);
		~CPageProfile_1st(void);

	private:
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TBasePage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed; // temporarily disabled due to possible invocation after control(s) destroying;

	public:
	};

}}}

#endif/*_EBOBOOUSRPRFPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/