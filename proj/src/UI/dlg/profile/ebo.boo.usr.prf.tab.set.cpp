/*
	Created by Tech_dog (VToropov) on 16-Jan-2016 at 3:58:22pm, GMT+7, Phuket, Rawai, Saturday;
	This is USB Drive Detective (bitsphereinc.com) app database option tab set interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 17-Aug-2018 at 6:57:00p, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	Adopted to Ebo Pack Google push notification app on 15-Sep-2019 at 1:12:36p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to Ebo Pack personal account app on 22-Oct-2019 at 11:48:25a, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "ebo.boo.usr.prf.tab.set.h"
#include "ebo.boo.res.dlg.prf.h"

using namespace ebo::boo::gui;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CProfileTabSet_Layout
	{
	private:
		RECT    m_area;
	public:
		CProfileTabSet_Layout(const RECT& rcArea) : m_area(rcArea) {}

	public:
		RECT    TabsArea (void) const {
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};
}}}}

using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CProfileTabSet:: CProfileTabSet(ITabSetCallback& _snk) : m_e_active(_pages::e_auth), m_login(_snk) { _snk; }
CProfileTabSet::~CProfileTabSet(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CProfileTabSet::Create(const HWND hParent, const RECT& rcArea) {
	hParent; rcArea;
	HRESULT hr_ = S_OK;

	if(FALSE == ::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return (hr_ = E_INVALIDARG);

	CProfileTabSet_Layout layout(rcArea);
	RECT rcTabs  = layout.TabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs ,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_EBO_BOO_USR_PRF_DLG_TAB
		);
	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());

	INT nIndex = 0; nIndex;
	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW; // auto-placement is made by default implementation of create/initialize page handler;

	m_cTabCtrl.AddItem(m_login.GetPageTitle());
	{
		m_login.Create(m_cTabCtrl.m_hWnd);
		m_login.SetWindowPos(
				HWND_TOP,
				0, 0, 0 , 0,
				dwFlags
			);
		if (m_login.IsWindow()) m_login.Index(nIndex++);
	}
	m_cTabCtrl.AddItem(_T(" Closing Date "));
	{
	}
	m_cTabCtrl.SetCurSel(m_e_active);

	this->UpdateLayout();

	return S_OK;
}

HRESULT       CProfileTabSet::Destroy(void)
{
	if (m_login.IsWindow()){
		m_login.DestroyWindow();  m_login.m_hWnd = NULL;
	}
	return S_OK;
}

CProfileTabSet::_pages
              CProfileTabSet::Selected(void) const      { return m_e_active; }
HRESULT       CProfileTabSet::Selected(const _pages _v) {
	HRESULT hr_ = S_OK;
	const bool b_changed = (_v != m_e_active);
	m_e_active  = _v;
	if (b_changed && m_cTabCtrl.IsWindow()) {
		m_cTabCtrl.SetCurSel(m_e_active);
		this->UpdateLayout();
	}
	return  hr_;
}

void          CProfileTabSet::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	if (m_login.IsWindow()) m_login.ShowWindow(nTabIndex == m_login.Index() ? SW_SHOW : SW_HIDE);
}