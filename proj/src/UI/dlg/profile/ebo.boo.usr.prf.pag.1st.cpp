/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Sep-2018 at 11:44:40a, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
	This is USB Drive Detective (bitsphereinc.com) main view options dialog page interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 15-Sep-2019 at 2:43:52p, UTC+7, Novosibirsk, Tulenina, Sunday;
	Adopted to Ebo Pack personal account app on 22-Oct-2019 at 8:29:40a, UTC+7, Novosibirsk, Light Coloured, Tuesday;
*/
#include "StdAfx.h"
#include "ebo.boo.usr.prf.pag.1st.h"
#include "ebo.boo.res.dlg.prf.h"
#include "ebo.boo.wzd.pag.lay.h"
#include "ebo.boo.stg.qry.acc.h"
#include "ebo.boo.data.cache.h"

using namespace ebo::boo::data;
using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageProfile_1st_Ctl {
	public:
		enum _ctl : WORD {
			e_sta_lst = IDC_EBO_BOO_USR_PRF_1ST_STA,    // access state dropdown;
			e_ava_ctl = IDC_EBO_BOO_USR_PRF_1ST_AVA,    // page avatar ;
			e_ava_txt = IDC_EBO_BOO_USR_PRF_1ST_CAP,    // page caption;
			e_btn_sav = IDOK,
			e_edt_nam = IDC_EBO_BOO_USR_PRF_1ST_NAM,    // user name edit box;
			e_edt_pwd = IDC_EBO_BOO_USR_PRF_1ST_PWD,    // user password;
			e_inf_img = IDC_EBO_BOO_USR_PRF_1ST_IMG,    // hype information sign;
			e_inf_txt = IDC_EBO_BOO_USR_PRF_1ST_INF,    // hype information text;
			e_rol_lst = IDC_EBO_BOO_USR_PRF_1ST_ROL,    // access role dropdown;
			e_prv_cap = IDC_EBO_BOO_USR_PRF_1ST_CP1,    // privileges section caption;
		};
	};
	typedef CPageProfile_1st_Ctl This_Ctl;

	class CPageProfile_1st_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageProfile_1st_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );

			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_inf_img, This_Ctl::e_inf_img) );
			CWindow  inf_ctl = Lay_(m_page_ref) << This_Ctl::e_inf_txt;
			if (0 != inf_ctl) {
				CAtlString cs_info;   cs_info.LoadString(This_Ctl::e_inf_txt);
				inf_ctl.SetWindowText(cs_info);
			}
			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_txt;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}
			CWindow  cap_ctl = Lay_(m_page_ref) << This_Ctl::e_prv_cap;
			if (0 != cap_ctl) {
				cap_ctl.SetFont(CTabPageBase::SectionCapFont());
			}
		}
		VOID   OnSize  (void) {
			CPage_Ban pan_(This_Ctl::e_inf_img, This_Ctl::e_inf_txt);
			pan_.Image().Resource() = This_Ctl::e_inf_img;
			pan_.Label().Resource() = This_Ctl::e_inf_txt;
			CPage_Layout(m_page_ref).AdjustBan(
				pan_
			);
		}
	};

	class CPageProfile_1st_Init
	{
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPageProfile_1st_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (CRole_Enum& _roles, CState_Enum& _stats) {
			m_error << __MODULE__ << S_OK;
			WTL::CComboBox rol_lst = Lay_(m_page_ref) << This_Ctl::e_rol_lst;
			if (NULL == rol_lst)
				return ((m_error = __DwordToHresult(ERROR_OBJECT_NOT_FOUND)) = _T("Access roles control is not found;"));
			CQuery_Roles  qry_rls;
			HRESULT hr_ = qry_rls.Get(_roles);
			if (FAILED(hr_)) {
				m_error = qry_rls.Error();
				rol_lst.ResetContent(); rol_lst.AddString(_T("#error")); rol_lst.SetCurSel(0); rol_lst.EnableWindow(FALSE);
			}
			else {
				rol_lst.ResetContent(); rol_lst.EnableWindow(TRUE);
				const TAccRoles& ref_ = _roles.Ref();
				for (size_t i_ = 0; i_ < ref_.size(); i_++) {
					const CRole& role_ = ref_[i_];
					rol_lst.AddString  (role_.Name());
					rol_lst.SetItemData(static_cast<INT>(i_), role_.Id());
				}
				rol_lst.SetCurSel(0);
				this->UpdateInfo (_roles);
			}
			WTL::CComboBox sta_lst = Lay_(m_page_ref) << This_Ctl::e_sta_lst;
			if (NULL == sta_lst)
				return ((m_error = __DwordToHresult(ERROR_OBJECT_NOT_FOUND)) = _T("Access state control is not found;"));

			CQuery_Stats  qry_sta;
			hr_ = qry_sta.Get(_stats);
			if (FAILED(hr_)) {
				m_error = qry_sta.Error();
				sta_lst.ResetContent(); sta_lst.AddString(_T("#error")); sta_lst.SetCurSel(0); sta_lst.EnableWindow(FALSE);
			}
			else {
				sta_lst.ResetContent(); sta_lst.EnableWindow(TRUE);
				const TAccStates& ref_ = _stats.Ref();
				for (size_t i_ = 0; i_ < ref_.size(); i_++) {
					const CState& sta_= ref_[i_];
					sta_lst.AddString(sta_.Name());
					sta_lst.SetItemData(static_cast<INT>(i_), sta_.Id());
				}
				sta_lst.SetCurSel(0);
			} 

			return m_error;
		}

		HRESULT   SetUserData(void) {
			m_error << __MODULE__ << S_OK;
			if (TActionType::e_edit != TDataCache().User().Action())
				return m_error;
			const CUser& user_ = TDataCache().User().Ref();
			CWindow edt_name   = Lay_(m_page_ref) << This_Ctl::e_edt_nam;
			if (0!= edt_name)
				edt_name.SetWindowText(user_.Name());
			CWindow edt_pwd    = Lay_(m_page_ref) << This_Ctl::e_edt_pwd;
			if (0!= edt_pwd)
				edt_pwd.SetWindowTextW(user_.Pwd());
			WTL::CComboBox rol_combo = Lay_(m_page_ref) << This_Ctl::e_rol_lst;
			if (NULL != rol_combo) {
				const INT rol_id = user_.Role().Id();
				for (INT i_ = 0; i_ < rol_combo.GetCount(); i_ ++) {
					if (rol_combo.GetItemData(i_) == rol_id) {
						rol_combo.SetCurSel(i_); break;
					}
				}
			}
			WTL::CComboBox sta_combo = Lay_(m_page_ref) << This_Ctl::e_sta_lst;
			if (NULL != sta_combo) {
				const INT rol_id = user_.State().Id();
				for (INT i_ = 0; i_ < sta_combo.GetCount(); i_ ++) {
					if (sta_combo.GetItemData(i_) == rol_id) {
						sta_combo.SetCurSel(i_); break;
					}
				}
			}
			return m_error;
		}

		HRESULT   UpdateInfo (const CRole_Enum& _roles) {
			m_error << __MODULE__ << S_OK;
			CWindow  inf_txt = Lay_(m_page_ref) << This_Ctl::e_inf_txt;
			if (0 == inf_txt)
				return ((m_error = __DwordToHresult(ERROR_OBJECT_NOT_FOUND)) = _T("Information control is not found;"));

			WTL::CComboBox rol_lst = Lay_(m_page_ref) << This_Ctl::e_rol_lst;
			if (NULL == rol_lst)
				return ((m_error = __DwordToHresult(ERROR_OBJECT_NOT_FOUND)) = _T("Roles list control is not found;"));

			static LPCTSTR lp_sz_pat = _T("%s\n%s");
			CAtlString cs_inf_def; cs_inf_def.LoadString(IDS_EBO_BOO_USR_PRF_1ST_INF);
			CAtlString cs_inf_cap;

			if (rol_lst.IsWindowEnabled() == FALSE) {
				cs_inf_cap.Format(
					lp_sz_pat, (LPCTSTR)cs_inf_def, _T("#error of data loading;")
				);
			}
			else {
				const INT n_ndx = rol_lst.GetCurSel();
				if (CB_ERR == n_ndx){
					cs_inf_cap.Format(
						lp_sz_pat, (LPCTSTR)cs_inf_def, _T("#warning of role selection;")
					);
				}
				else {
					cs_inf_cap.Format(
						lp_sz_pat, (LPCTSTR)cs_inf_def, _roles.Ref()[n_ndx].Desc() // TODO: combo current index may be out of range;
					);
				}
				inf_txt.SetWindowTextW((LPCTSTR)cs_inf_cap);
			}

			return m_error;
		}
	};

	class CPageProfile_1st_Handler
	{
	private:
		const CRole_Enum & m_roles ;
		const CState_Enum& m_stats ;
		CWindow&     m_page_ref;

	public:
		CPageProfile_1st_Handler(CWindow& dlg_ref, const CRole_Enum& _roles, const CState_Enum& _states)
			: m_page_ref(dlg_ref), m_roles(_roles), m_stats(_states) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			if (This_Ctl::e_rol_lst == ctrlId) {
				if (CBN_SELCHANGE  == wNotify) {

					WTL::CComboBox rol_lst = ((Lay_(m_page_ref)) << This_Ctl::e_rol_lst);
					if (rol_lst){
						const INT n_ndx = rol_lst.GetCurSel();
						if (CB_ERR != n_ndx) {
							const CRole& selected_ = m_roles.Item(n_ndx);
							TDataCache().User().Ref().Role() = selected_;
						}
					}

					CPageProfile_1st_Init init_(m_page_ref);
					init_.UpdateInfo(m_roles);
					bHandled = TRUE;
				}
			}
			if (This_Ctl::e_sta_lst == ctrlId) {
				if (CBN_SELCHANGE  == wNotify) {

					WTL::CComboBox sta_lst = ((Lay_(m_page_ref)) << This_Ctl::e_sta_lst);
					if (sta_lst){
						const INT n_ndx = sta_lst.GetCurSel();
						if (CB_ERR != n_ndx) {
							const CState& selected_ = m_stats.Item(n_ndx);
							TDataCache().User().Ref().State() = selected_;
						}
					}
					bHandled = TRUE;
				}
			}
			if (This_Ctl::e_edt_nam == ctrlId ) {
				if (EN_CHANGE  == wNotify) {
					TDataCache().User().Ref().Name((LPCTSTR)((Lay_(m_page_ref)) <= ctrlId));
					bHandled = TRUE;
				}
			}
			if (This_Ctl::e_edt_pwd == ctrlId) {
				if (EN_CHANGE  == wNotify) {
					TDataCache().User().Ref().Pwd((LPCTSTR)((Lay_(m_page_ref))  <= ctrlId));
					bHandled = TRUE;
				}
			}
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageProfile_1st:: CPageProfile_1st(ITabSetCallback& _set_snk):
       TBasePage(IDD_EBO_OUT_USR_PRF_1ST_REG, *this, _set_snk) {
}

CPageProfile_1st::~CPageProfile_1st(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageProfile_1st::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageProfile_1st::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageProfile_1st_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPageProfile_1st_Init init_(*this);
	init_.OnCreate(m_roles, m_stats);
	init_.SetUserData();
	
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageProfile_1st::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageProfile_1st_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageProfile_1st::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPageProfile_1st_Handler handler_(*this, m_roles, m_stats);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE ) {
				TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageProfile_1st::IsChanged   (void) const {
#if(0)
	CWindow lvw_wnd(Lay_(*this) << This_Ctl::e_col_vis);
	CListViewColEnum cols_ex(lvw_wnd);
#endif
	return false;
}

CAtlString CPageProfile_1st::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Authentication ")); return cs_title; }

VOID       CPageProfile_1st::UpdateData  (const DWORD _opt) {
	_opt;
}