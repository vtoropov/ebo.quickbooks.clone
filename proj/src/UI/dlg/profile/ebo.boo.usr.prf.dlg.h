#ifndef _EBOBOOUSREPRFDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
#define _EBOBOOUSREPRFDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Dec-2018 at 5:05:16p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is USB Drive Detective (bitsphereinc.com) desktop app scan dialog interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google notify service on 14-Sep-2019 at 3:53:05p, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Pack personal account app on 22-Oct-2019 at 11:53:25a, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.frms.img.ban.h"
#include "ebo.boo.usr.prf.tab.set.h"

namespace ebo { namespace boo { namespace gui {

	using shared::sys_core::CError;
	using ex_ui::frames::CImageBanner;
	using ex_ui::draw::defs::IRenderer;

	class CProfileDlg {
	private:
		class CProfileDlgImpl : public ::ATL::CDialogImpl<CProfileDlgImpl>, ITabSetCallback, IRenderer {
		                       typedef ::ATL::CDialogImpl<CProfileDlgImpl>  TDialog;
			friend class CProfileDlg;
		private:
			CImageBanner       m_banner;
			CProfileTabSet     m_tabset;

		public :
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CProfileDlgImpl)
				MESSAGE_HANDLER     (WM_COMMAND   ,   OnBtnCmd )
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy)
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDlg)
				MESSAGE_HANDLER     (WM_KEYDOWN   ,   OnKeyDown) // does not work in modeless dialogs;
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCmd )
			END_MSG_MAP()

		public:
			 CProfileDlgImpl (void) ;
			~CProfileDlgImpl (void) ;

		private:
			LRESULT OnBtnCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

		private: // ITabSetCallback
			virtual HRESULT TabSet_OnDataChanged (const UINT pageId, const bool bChanged) override sealed;
			virtual HRESULT TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) override sealed;
		private: // IRenderer
			HRESULT DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const override sealed;
		};
	private:
		CProfileDlgImpl   m_dlg;

	public:
		 CProfileDlg (void);
		~CProfileDlg (void);

	public:
		HRESULT    DoModal(void);

	private:
		CProfileDlg (const CProfileDlg&);
		CProfileDlg& operator= (const CProfileDlg&);
	};

}}}

#endif/*_EBOBOOUSREPRFDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED*/