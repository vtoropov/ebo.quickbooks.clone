#ifndef _EBOBOOTAXDATTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _EBOBOOTAXDATTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Dec-2019 at 0:06:07a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app tax rate dialog tabset interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.tax.dat.pag.1st.h"
#include "ebo.boo.tax.dat.pag.2nd.h"
#include "ebo.boo.tax.dat.pag.3rd.h"

namespace ebo { namespace boo { namespace gui {

	class CTaxTabSet {
	public:
		enum _show_page {
			e_all = 0,
			e_1st = 1,
			e_2nd = 2,
			e_3rd = 3,
		};
		enum _pages : INT {
			e_rate  = 0,  // tax rate page;
			e_group = 1,  // tax group rate page;
			e_cstm  = 2,  // custom tax page;
		};
	private:
		WTL::CTabCtrl m_cTabCtrl;
		_pages        m_e_active;

	private: //tab page(s)
		CPageTax_1st  m_1st_page;
		CPageTax_2nd  m_2nd_page;
		CPageTax_3rd  m_3rd_page;

	public:
		 CTaxTabSet (ITabSetCallback&);
		~CTaxTabSet (void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& rcArea, const _show_page = _show_page::e_all);
		HRESULT       Destroy (void)        ;
		_pages        Selected(void)   const;
		HRESULT       Selected(const _pages);
		VOID          UpdateLayout(void);
	public:
	};
}}}

#endif/*_EBOBOOTAXDATTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/