/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Dec-2019 at 2:49:15a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app group tax rate dialog page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.tax.dat.pag.2nd.h"
#include "ebo.boo.res.dlg.tax.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageTax_2nd_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_BOO_TAX_PAG_2ND_AVA,    // page avatar of tax;
			e_ava_cap = IDC_EBO_BOO_TAX_PAG_2ND_CAP,    // page caption of tax;
			e_app_lst = IDC_EBO_BOO_TAX_PAG_2ND_APP,
			e_com_lst = IDC_EBO_BOO_TAX_PAG_2ND_RAT,
		};
	};
	typedef CPageTax_2nd_Ctl This_Ctl;

	class CPageTax_2nd_Layout {
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageTax_2nd_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_cap;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPageTax_2nd_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;

	public:
		CPageTax_2nd_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;

			static LPCTSTR lp_sz_app[] = {
				_T("Net Amount"), _T("Tax Amount"), _T("Tax + Net Amount")
			};

			CComboBox app_lst = Lay_(m_page_ref) << This_Ctl::e_app_lst;
			if (NULL != app_lst) {
				for (INT i_ = 0; i_ < _countof(lp_sz_app); i_++) {
					app_lst.AddString(lp_sz_app[i_]);
				}
				app_lst.SetCurSel(0);
			}

			static LPCTSTR lp_sz_com[] = {
				_T("Current company::Sales"), _T("Current company::Purchases")
			};

			CComboBox com_lst = Lay_(m_page_ref) << This_Ctl::e_com_lst;
			if (NULL != com_lst) {
				for (INT i_ = 0; i_ < _countof(lp_sz_com); i_++) {
					com_lst.AddString(lp_sz_com[i_]);
				}
				com_lst.SetCurSel(0);
			}
			return m_error;
		}

	};

	class CPageTax_2nd_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPageTax_2nd_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageTax_2nd:: CPageTax_2nd(ITabSetCallback& _set_snk):
	TBasePage(IDD_EBO_BOO_TAX_PAG_2ND, *this, _set_snk) {
}

CPageTax_2nd::~CPageTax_2nd(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageTax_2nd::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageTax_2nd::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageTax_2nd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPageTax_2nd_Init init_(*this);
	init_.OnCreate();

	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageTax_2nd::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageTax_2nd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageTax_2nd::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
		if (!TBasePage::m_bInited)
			break;
		const WORD wNotify = HIWORD(wParam);
		const WORD ctrlId  = LOWORD(wParam);

		CPageTax_2nd_Handler handler_(*this);

		if (bHandled == FALSE)
			bHandled = handler_.OnCommand(ctrlId, wNotify);
		if (bHandled == TRUE ) {
			TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
		}
	} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageTax_2nd::IsChanged   (void) const {
	return false;
}

CAtlString CPageTax_2nd::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Group Rate ")); return cs_title; }

VOID       CPageTax_2nd::UpdateData  (const DWORD _opt) {
	_opt;
}