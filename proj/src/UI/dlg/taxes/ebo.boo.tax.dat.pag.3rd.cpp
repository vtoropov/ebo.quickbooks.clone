/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Dec-2019 at 3:09:12a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app custom tax rate dialog page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.tax.dat.pag.3rd.h"
#include "ebo.boo.res.dlg.tax.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageTax_3rd_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_BOO_TAX_PAG_3RD_AVA,    // page avatar ;
			e_ava_cap = IDC_EBO_BOO_TAX_PAG_3RD_CAP,    // page caption;
			e_prd_lst = IDC_EBO_BOO_TAX_PAG_3RD_PRD,
			e_frq_lst = IDC_EBO_BOO_TAX_PAG_3RD_FRQ,
			e_rpt_lst = IDC_EBO_BOO_TAX_PAG_3RD_RPT,
		};
	};
	typedef CPageTax_3rd_Ctl This_Ctl;

	class CPageTax_3rd_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageTax_3rd_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_cap;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}

			static LPCTSTR lp_sz_mn[] = {
				_T("January"), _T("February"), _T("March")    , _T("April")  , _T("May")     , _T("June"),
				_T("July")   , _T("August")  , _T("September"), _T("October"), _T("November"), _T("December")
			};

			SYSTEMTIME sys_st = {0};
			::GetLocalTime(&sys_st);

			CComboBox prd_lst = Lay_(m_page_ref) << This_Ctl::e_prd_lst;
			if (NULL != prd_lst) {
				for (INT i_ = 0; i_ < _countof(lp_sz_mn); i_++) {
					prd_lst.AddString(lp_sz_mn[i_]);
				}
				if (prd_lst.GetCount()>=sys_st.wMonth )
					prd_lst.SetCurSel(sys_st.wMonth -1);
			}

			CComboBox frq_lst = Lay_(m_page_ref) << This_Ctl::e_frq_lst;
			if (frq_lst) {
				frq_lst.AddString(_T("Monthly"));
				frq_lst.AddString(_T("Quarterly"));
				frq_lst.AddString(_T("Half-yearly"));
				frq_lst.AddString(_T("Yearly"));
				frq_lst.SetCurSel(0);
			}

			CComboBox rpt_lst = Lay_(m_page_ref) << This_Ctl::e_rpt_lst;
			if (rpt_lst) {
				rpt_lst.AddString(_T("Accrual"));
				rpt_lst.AddString(_T("Cash"));
				rpt_lst.SetCurSel(0);
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPageTax_3rd_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;

	public:
		CPageTax_3rd_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;
			return m_error;
		}

	};

	class CPageTax_3rd_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPageTax_3rd_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageTax_3rd:: CPageTax_3rd(ITabSetCallback& _set_snk):
	TBasePage(IDD_EBO_BOO_TAX_PAG_3RD, *this, _set_snk) {
}

CPageTax_3rd::~CPageTax_3rd(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageTax_3rd::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageTax_3rd::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageTax_3rd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPageTax_3rd_Init init_(*this);

	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageTax_3rd::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageTax_3rd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageTax_3rd::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
		if (!TBasePage::m_bInited)
			break;
		const WORD wNotify = HIWORD(wParam);
		const WORD ctrlId  = LOWORD(wParam);

		CPageTax_3rd_Handler handler_(*this);

		if (bHandled == FALSE)
			bHandled = handler_.OnCommand(ctrlId, wNotify);
		if (bHandled == TRUE ) {
			TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
		}
	} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageTax_3rd::IsChanged   (void) const {
	return false;
}

CAtlString CPageTax_3rd::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Custom Tax ")); return cs_title; }

VOID       CPageTax_3rd::UpdateData  (const DWORD _opt) {
	_opt;
}