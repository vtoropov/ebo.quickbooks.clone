/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Dec-2019 at 0:56:56a, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is Ebo Pack personal account app customer dialog payment & terms page implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.cst.dat.pag.5th.h"
#include "ebo.boo.res.dlg.cst.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageCust_5th_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_BOO_CST_PAG_5TH_AVA,    // prefer payment avatar ;
			e_ava_cap = IDC_EBO_BOO_CST_PAG_5TH_CAP,    // prefer payment caption;
			e_bal_ctl = IDC_EBO_BOO_CST_PAG_5TH_AV1,    // balance section avatar ;
			e_bal_cap = IDC_EBO_BOO_CST_PAG_5TH_CA1,    // balance section caption;
			e_lng_ctl = IDC_EBO_BOO_CST_PAG_5TH_AV2,    // language section avatar ;
			e_lng_cap = IDC_EBO_BOO_CST_PAG_5TH_CA2,    // language section caption;
			e_lng_lst = IDC_EBO_BOO_CST_PAG_5TH_LNG,    // language combo control;
		};
	};
	typedef CPageCust_5th_Ctl This_Ctl;

	class CPageCust_5th_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageCust_5th_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_bal_ctl, This_Ctl::e_bal_ctl) );
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_lng_ctl, This_Ctl::e_lng_ctl) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_cap;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}
			CWindow  cap_bal = Lay_(m_page_ref) << This_Ctl::e_bal_cap;
			if (0 != cap_bal) {
				cap_bal.SetFont(CTabPageBase::SectionCapFont());
			}
			CWindow  cap_lng = Lay_(m_page_ref) << This_Ctl::e_lng_cap;
			if (0 != cap_lng) {
				cap_lng.SetFont(CTabPageBase::SectionCapFont());
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPageCust_5th_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPageCust_5th_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;
#if (1)
			WTL::CComboBox cbo_lng = Lay_(m_page_ref) << This_Ctl::e_lng_lst;
			if (NULL != cbo_lng) {
				cbo_lng.AddString(_T("English"));
			}
#endif
			return m_error;
		}

	};

	class CPageCust_5th_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPageCust_5th_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageCust_5th:: CPageCust_5th(ITabSetCallback& _set_snk):
       TBasePage(IDD_EBO_BOO_CST_PAG_5TH, *this, _set_snk) {
}

CPageCust_5th::~CPageCust_5th(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageCust_5th::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageCust_5th::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageCust_5th_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPageCust_5th_Init init_(*this);
	init_.OnCreate();
	
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageCust_5th::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageCust_5th_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageCust_5th::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPageCust_5th_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE ) {
				TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageCust_5th::IsChanged   (void) const {
	return false;
}

CAtlString CPageCust_5th::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Payment && Billing ")); return cs_title; }

VOID       CPageCust_5th::UpdateData  (const DWORD _opt) {
	_opt;
}