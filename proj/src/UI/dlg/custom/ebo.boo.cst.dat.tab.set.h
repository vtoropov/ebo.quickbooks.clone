#ifndef _EBOBOOCSTDATTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _EBOBOOCSTDATTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Dec-2019 at 1:26:41a, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app customer dialog personal data tabset interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.cst.dat.pag.1st.h"
#include "ebo.boo.cst.dat.pag.2nd.h"
#include "ebo.boo.cst.dat.pag.3rd.h"
#include "ebo.boo.cst.dat.pag.4th.h"
#include "ebo.boo.cst.dat.pag.5th.h"
#include "ebo.boo.cst.dat.pag.6th.h"

namespace ebo { namespace boo { namespace gui {

	class CCustTabSet
	{
	public:
		enum _pages : INT {
			e_gnrl  = 0,  // customer general information page;
			e_prnt  = 1,  // parent control data page;
		};
	private:
		WTL::CTabCtrl m_cTabCtrl;
		_pages        m_e_active;

	private: //tab page(s)
		CPageCust_1st  m_1st_page;
		CPageCust_2nd  m_2nd_page;
		CPageCust_3rd  m_3rd_page;
		CPageCust_4th  m_4th_page;
		CPageCust_5th  m_5th_page;
		CPageCust_6th  m_6th_page;

	public:
		 CCustTabSet (ITabSetCallback&);
		~CCustTabSet (void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy (void)        ;
		_pages        Selected(void)   const;
		HRESULT       Selected(const _pages);
		VOID          UpdateLayout(void);
	public:
	};
}}}

#endif/*_EBOBOOCSTDATTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/