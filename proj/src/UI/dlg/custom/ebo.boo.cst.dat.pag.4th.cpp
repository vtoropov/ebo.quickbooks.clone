/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Dec-2019 at 8:00:37p, UTC+7, Novosibirsk, Tulenina, Wednesday;
	This is Ebo Pack personal account app customer dialog shipping address page implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.cst.dat.pag.4th.h"
#include "ebo.boo.res.dlg.cst.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageCust_4th_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_BOO_CST_PAG_4TH_AVA,    // page billing address avatar ;
			e_ava_cap = IDC_EBO_BOO_CST_PAG_4TH_CAP,    // page billing address caption;
			e_shp_ctl = IDC_EBO_BOO_CST_PAG_4TH_AV1,    // page shipping address avatar ;
			e_shp_cap = IDC_EBO_BOO_CST_PAG_4TH_CA1,    // page shipping address caption;
		};
	};
	typedef CPageCust_4th_Ctl This_Ctl;

	class CPageCust_4th_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageCust_4th_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_cap;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}

			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_shp_ctl, This_Ctl::e_shp_ctl) );

			CWindow  cap_shp = Lay_(m_page_ref) << This_Ctl::e_shp_cap;
			if (0 != cap_shp) {
				cap_shp.SetFont(CTabPageBase::SectionCapFont());
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPageCust_4th_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPageCust_4th_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;
			return m_error;
		}

	};

	class CPageCust_4th_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPageCust_4th_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageCust_4th:: CPageCust_4th(ITabSetCallback& _set_snk):
       TBasePage(IDD_EBO_BOO_CST_PAG_4TH, *this, _set_snk) {
}

CPageCust_4th::~CPageCust_4th(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageCust_4th::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageCust_4th::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageCust_4th_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPageCust_4th_Init init_(*this);
	
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageCust_4th::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageCust_4th_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageCust_4th::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPageCust_4th_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE ) {
				TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageCust_4th::IsChanged   (void) const {
	return false;
}

CAtlString CPageCust_4th::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Address ")); return cs_title; }

VOID       CPageCust_4th::UpdateData  (const DWORD _opt) {
	_opt;
}