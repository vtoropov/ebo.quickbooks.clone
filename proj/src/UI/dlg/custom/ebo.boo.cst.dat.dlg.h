#ifndef _EBOBOOCSTDATDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
#define _EBOBOOCSTDATDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Dec-2019 at 8:58:29p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app customer data dialog interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.frms.img.ban.h"
#include "ebo.boo.cst.dat.tab.set.h"

namespace ebo { namespace boo { namespace gui {

	using shared::sys_core::CError;
	using ex_ui::frames::CImageBanner;
	using ex_ui::draw::defs::IRenderer;

	class CCustDlg {
	private:
		class CCustDlgImpl : public ::ATL::CDialogImpl<CCustDlgImpl>, ITabSetCallback, IRenderer {
		                    typedef ::ATL::CDialogImpl<CCustDlgImpl>  TDialog;
			friend class CCustDlg;
		private:
			CImageBanner   m_banner;
			CCustTabSet    m_tabset;

		public :
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CCustDlgImpl)
				MESSAGE_HANDLER     (WM_COMMAND    ,  OnBtnCmd   )
				MESSAGE_HANDLER     (WM_DESTROY    ,  OnDestroy  )
				MESSAGE_HANDLER     (WM_INITDIALOG ,  OnInitDlg  )
				MESSAGE_HANDLER     (WM_KEYDOWN    ,  OnKeyDown  ) // does not work in modeless dialogs;
				MESSAGE_HANDLER     (WM_SYSCOMMAND ,  OnSysCmd   )
				NOTIFY_CODE_HANDLER (TCN_SELCHANGE ,  OnTabNotify)
			END_MSG_MAP()

		public:
			 CCustDlgImpl (void) ;
			~CCustDlgImpl (void) ;

		private:
			LRESULT OnBtnCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		private:
			LRESULT OnTabNotify (INT  , LPNMHDR, BOOL&);

		private: // ITabSetCallback
			virtual HRESULT TabSet_OnDataChanged (const UINT pageId, const bool bChanged) override sealed;
			virtual HRESULT TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) override sealed;
		private: // IRenderer
			HRESULT DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const override sealed;
		};
	private:
		CCustDlgImpl   m_dlg;

	public:
		 CCustDlg (void);
		~CCustDlg (void);

	public:
		HRESULT    DoModal(void);

	private:
		CCustDlg (const CCustDlg&);
		CCustDlg& operator= (const CCustDlg&);
	};

}}}

#endif/*_EBOBOOCSTDATDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED*/