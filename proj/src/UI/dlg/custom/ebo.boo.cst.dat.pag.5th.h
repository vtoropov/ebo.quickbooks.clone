#ifndef _EBOBOOCSTDATPAG5TH_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOBOOCSTDATPAG5TH_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Dec-2019 at 0:52:56a, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is Ebo Pack personal account app customer dialog payment & terms page interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.stg.dat.acc.h"

namespace ebo { namespace boo { namespace gui {

	class CPageCust_5th : public CTabPageBase, public  ITabPageEvents {
	                     typedef CTabPageBase  TBasePage;
	private:
	public :
		 CPageCust_5th(ITabSetCallback&);
		~CPageCust_5th(void);

	private:
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TBasePage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed;

	public:
	};

}}}

#endif/*_EBOBOOCSTDATPAG5TH_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/