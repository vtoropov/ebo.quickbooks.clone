#ifndef _EBOBOOPROSVCPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOBOOPROSVCPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Dec-2019 at 5:21:41a, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack personal account product and service info 1st dialog page interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"

namespace ebo { namespace boo { namespace gui {

	class CPagePaS_1st : public CTabPageBase, public  ITabPageEvents {
	                    typedef CTabPageBase  TBasePage;
	private:
		
	public :
		 CPagePaS_1st(ITabSetCallback&);
		~CPagePaS_1st(void);

	private:
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TBasePage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed;

	public:
	};

}}}

#endif/*_EBOBOOPROSVCPAG1ST_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/