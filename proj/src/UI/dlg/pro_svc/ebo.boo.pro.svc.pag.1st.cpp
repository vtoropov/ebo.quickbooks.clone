/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Dec-2019 at 5:21:41a, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack personal account product and service info 1st dialog page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.pro.svc.pag.1st.h"
#include "ebo.boo.res.dlg.pas.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPagePaS_1st_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_BOO_PRO_SVC_1ST_AVA,    // product section avatar ;
			e_ava_txt = IDC_EBO_BOO_PRO_SVC_1ST_CAP,    // product section caption;
			e_edt_nam = IDC_EBO_BOO_PRO_SVC_1ST_NAM,    // product name edit box;
			e_edt_sku = IDC_EBO_BOO_PRO_SVC_1ST_SKU,    // product SKU;
			e_lst_cat = IDC_EBO_BOO_PRO_SVC_1ST_CAT,    // product category list;
			e_av1_ctl = IDC_EBO_BOO_PRO_SVC_1ST_AVA_1,  // sale section avatar ;
			e_av1_txt = IDC_EBO_BOO_PRO_SVC_1ST_CAP_1,  // sale section caption;
			e_lst_acc = IDC_EBO_BOO_PRO_SVC_1ST_ACC,    // sale account list;
		};
	};
	typedef CPagePaS_1st_Ctl This_Ctl;

	class CPagePaS_1st_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPagePaS_1st_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_av1_ctl, This_Ctl::e_av1_ctl) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_txt;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}
			CWindow  cap_av1 = Lay_(m_page_ref) << This_Ctl::e_av1_txt;
			if (0 != cap_av1) {
				cap_av1.SetFont(CTabPageBase::SectionCapFont());
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPagePaS_1st_Init
	{
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPagePaS_1st_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;

			CComboBox lst_cat = Lay_(m_page_ref) << This_Ctl::e_lst_cat;
			if (NULL!=lst_cat) {
				lst_cat.AddString(_T("[Add New...]"));
				lst_cat.SetCurSel(0);
			}

			CComboBox lst_acc = Lay_(m_page_ref) << This_Ctl::e_lst_acc;
			if (NULL!=lst_acc) {
				lst_acc.AddString(_T("[Add New...]"));
				lst_acc.SetCurSel(0);
			}

			return m_error;
		}

	};

	class CPagePaS_1st_Handler
	{
	private:
#if (0)
		const CRole_Enum & m_roles ;
		const CState_Enum& m_stats ;
#endif
		CWindow&     m_page_ref;

	public:
		CPagePaS_1st_Handler(CWindow& dlg_ref) : m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;

			if (This_Ctl::e_edt_nam == ctrlId ) {
				if (EN_CHANGE  == wNotify) {
					bHandled = TRUE;
				}
			}
			if (This_Ctl::e_edt_sku == ctrlId) {
				if (EN_CHANGE  == wNotify) {
					bHandled = TRUE;
				}
			}
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPagePaS_1st:: CPagePaS_1st(ITabSetCallback& _set_snk):
       TBasePage(IDD_EBO_BOO_PRO_SVC_1ST_GEN, *this, _set_snk) {
}

CPagePaS_1st::~CPagePaS_1st(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPagePaS_1st::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPagePaS_1st::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPagePaS_1st_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPagePaS_1st_Init init_(*this);
	init_.OnCreate();
	
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPagePaS_1st::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPagePaS_1st_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPagePaS_1st::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPagePaS_1st_Handler handler_(*this/*, m_roles, m_stats*/);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE ) {
				TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPagePaS_1st::IsChanged   (void) const {
#if(0)
	CWindow lvw_wnd(Lay_(*this) << This_Ctl::e_col_vis);
	CListViewColEnum cols_ex(lvw_wnd);
#endif
	return false;
}

CAtlString CPagePaS_1st::GetPageTitle(void) const {  static CAtlString cs_title(_T(" General ")); return cs_title; }

VOID       CPagePaS_1st::UpdateData  (const DWORD _opt) {
	_opt;
}