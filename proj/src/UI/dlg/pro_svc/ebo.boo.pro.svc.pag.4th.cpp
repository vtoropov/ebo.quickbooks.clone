/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2019 at 3:41:20p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Ebo Pack personal account product and service info 4th dialog page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.pro.svc.pag.4th.h"
#include "ebo.boo.res.dlg.pas.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPagePaS_4th_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_BOO_PRO_SVC_4TH_AVA,    // service page avatar ;
			e_ava_cap = IDC_EBO_BOO_PRO_SVC_4TH_CAP,    // service page caption;
			e_ava_but = IDC_EBO_BOO_PRO_SVC_4TH_BUT,    // service thumbs browsing button;
		};
	};
	typedef CPagePaS_4th_Ctl This_Ctl;

	class CPagePaS_4th_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPagePaS_4th_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_cap;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPagePaS_4th_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;

	public:
		CPagePaS_4th_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;
			return m_error;
		}

	};

	class CPagePaS_4th_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPagePaS_4th_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPagePaS_4th:: CPagePaS_4th(ITabSetCallback& _set_snk):
	TBasePage(IDD_EBO_BOO_PRO_SVC_4TH_THU, *this, _set_snk) {
}

CPagePaS_4th::~CPagePaS_4th(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPagePaS_4th::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPagePaS_4th::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPagePaS_4th_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPagePaS_4th_Init init_(*this);

	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPagePaS_4th::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPagePaS_4th_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPagePaS_4th::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
		if (!TBasePage::m_bInited)
			break;
		const WORD wNotify = HIWORD(wParam);
		const WORD ctrlId  = LOWORD(wParam);

		CPagePaS_4th_Handler handler_(*this);

		if (bHandled == FALSE)
			bHandled = handler_.OnCommand(ctrlId, wNotify);
		if (bHandled == TRUE ) {
			TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
		}
	} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPagePaS_4th::IsChanged   (void) const {
	return false;
}

CAtlString CPagePaS_4th::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Thumbs ")); return cs_title; }

VOID       CPagePaS_4th::UpdateData  (const DWORD _opt) {
	_opt;
}