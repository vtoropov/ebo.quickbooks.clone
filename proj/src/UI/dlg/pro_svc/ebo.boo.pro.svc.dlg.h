#ifndef _EBOBOOPROSVCDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
#define _EBOBOOPROSVCDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Dec-2019 at 5:54:24a, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack personal account product and service info dialog interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.frms.img.ban.h"
#include "ebo.boo.pro.svc.tab.set.h"
#include "ebo.boo.pro.svc.typ.set.h"

namespace ebo { namespace boo { namespace gui {

	using shared::sys_core::CError;
	using ex_ui::frames::CImageBanner;
	using ex_ui::draw::defs::IRenderer;

	class CProSvcDlg {
	private:
		class CProSvcDlgImpl : public ::ATL::CDialogImpl<CProSvcDlgImpl>, ITabSetCallback, IRenderer {
		                      typedef ::ATL::CDialogImpl<CProSvcDlgImpl>  TDialog;
			friend class CProSvcDlg;
		private:
			CImageBanner       m_banner;
			CProSvcTabSet      m_tabset;
			CProdSvc_Type      m_type  ;

		public :
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CProSvcDlgImpl)
				MESSAGE_HANDLER     (WM_COMMAND    ,   OnBtnCmd   )
				MESSAGE_HANDLER     (WM_DESTROY    ,   OnDestroy  )
				MESSAGE_HANDLER     (WM_INITDIALOG ,   OnInitDlg  )
				MESSAGE_HANDLER     (WM_KEYDOWN    ,   OnKeyDown  ) // does not work in modeless dialogs;
				MESSAGE_HANDLER     (WM_SYSCOMMAND ,   OnSysCmd   )
				NOTIFY_CODE_HANDLER (TCN_SELCHANGE ,   OnTabNotify)
			END_MSG_MAP()

		public:
			 CProSvcDlgImpl (void) ;
			~CProSvcDlgImpl (void) ;

		private:
			LRESULT OnBtnCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		private:
			LRESULT OnTabNotify (INT  , LPNMHDR, BOOL&);

		private: // ITabSetCallback
			virtual HRESULT TabSet_OnDataChanged (const UINT pageId, const bool bChanged) override sealed;
			virtual HRESULT TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) override sealed;
		private: // IRenderer
			HRESULT DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const override sealed;
		};
	private:
		CProSvcDlgImpl   m_dlg;

	public:
		 CProSvcDlg (void);
		~CProSvcDlg (void);

	public:
		HRESULT    DoModal(const CProdSvc_Type&);

	private:
		CProSvcDlg (const CProSvcDlg&);
		CProSvcDlg& operator= (const CProSvcDlg&);
	};

}}}

#endif/*_EBOBOOPROSVCDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED*/