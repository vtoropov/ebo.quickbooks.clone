/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Dec-2019 at 9:36:54a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account product and service info 3rd dialog page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.pro.svc.pag.3rd.h"
#include "ebo.boo.res.dlg.pas.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPagePaS_3rd_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_BOO_PRO_SVC_3RD_AVA,    // service section avatar ;
			e_ava_txt = IDC_EBO_BOO_PRO_SVC_3RD_CAP,    // service section caption;
			e_lst_cat = IDC_EBO_BOO_PRO_SVC_3RD_CAT,    // service category list;
			e_edt_nam = IDC_EBO_BOO_PRO_SVC_3RD_NAM,    // service name edit box;
			e_av1_ctl = IDC_EBO_BOO_PRO_SVC_3RD_AVA_1,  // service section avatar ;
			e_av1_txt = IDC_EBO_BOO_PRO_SVC_3RD_CAP_1,  // service section caption;
			e_lst_acc = IDC_EBO_BOO_PRO_SVC_3RD_ACC,    // sale account list;
		};
	};
	typedef CPagePaS_3rd_Ctl This_Ctl;

	class CPagePaS_3rd_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPagePaS_3rd_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_av1_ctl, This_Ctl::e_av1_ctl) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_txt;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}
			CWindow  cap_av1 = Lay_(m_page_ref) << This_Ctl::e_av1_txt;
			if (0 != cap_av1) {
				cap_av1.SetFont(CTabPageBase::SectionCapFont());
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPagePaS_3rd_Init
	{
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPagePaS_3rd_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;

			CComboBox lst_cat = Lay_(m_page_ref) << This_Ctl::e_lst_cat;
			if (NULL!=lst_cat) {
				lst_cat.AddString(_T("[Add New...]"));
				lst_cat.SetCurSel(0);
			}

			CComboBox lst_acc = Lay_(m_page_ref) << This_Ctl::e_lst_acc;
			if (NULL!=lst_acc) {
				lst_acc.AddString(_T("[Add New...]"));
				lst_acc.SetCurSel(0);
			}

			return m_error;
		}

	};

	class CPagePaS_3rd_Handler
	{
	private:
#if (0)
		const CRole_Enum & m_roles ;
		const CState_Enum& m_stats ;
#endif
		CWindow&     m_page_ref;

	public:
		CPagePaS_3rd_Handler(CWindow& dlg_ref) : m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;

			if (This_Ctl::e_edt_nam == ctrlId ) {
				if (EN_CHANGE  == wNotify) {
					bHandled = TRUE;
				}
			}
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPagePaS_3rd:: CPagePaS_3rd(ITabSetCallback& _set_snk):
       TBasePage(IDD_EBO_BOO_PRO_SVC_3RD_GEN, *this, _set_snk) {
}

CPagePaS_3rd::~CPagePaS_3rd(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPagePaS_3rd::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPagePaS_3rd::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPagePaS_3rd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPagePaS_3rd_Init init_(*this);
	init_.OnCreate();
	
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPagePaS_3rd::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPagePaS_3rd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPagePaS_3rd::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPagePaS_3rd_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE ) {
				TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPagePaS_3rd::IsChanged   (void) const { return false; }

CAtlString CPagePaS_3rd::GetPageTitle(void) const {  static CAtlString cs_title(_T(" General ")); return cs_title; }

VOID       CPagePaS_3rd::UpdateData  (const DWORD _opt) {
	_opt;
}