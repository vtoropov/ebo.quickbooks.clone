#ifndef _EBOBOOPROSVCPAG4TH_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOBOOPROSVCPAG4TH_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2019 at 3:39:14p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Ebo Pack personal account product and service info 4th dialog page interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.stg.dat.acc.h"

namespace ebo { namespace boo { namespace gui {

	class CPagePaS_4th : public CTabPageBase, public  ITabPageEvents {
	                    typedef CTabPageBase  TBasePage;
	private:
	public :
		 CPagePaS_4th(ITabSetCallback&);
		~CPagePaS_4th(void);

	private:
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TBasePage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed;

	public:
	};

}}}

#endif/*_EBOBOOPROSVCPAG4TH_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/