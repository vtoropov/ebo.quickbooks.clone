#ifndef _EBOBOOPROSVCTYPSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _EBOBOOPROSVCTYPSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Dec-2019 at 3:42:44p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack personal account product and service item type set interface declaration file.
*/

namespace ebo { namespace boo { namespace gui {

	class CProdSvc_Type {
	public:
		enum _type {
			e_none       = 0x0,
			e_inventory  = 0x1,
			e_nonstored  = 0x2,
			e_service    = 0x3,
			e_assembly   = 0x4,
		};
	private:
		mutable
		CAtlString  m_desc ;
		_type       m_value;	

	public:
		 CProdSvc_Type (void);
		 CProdSvc_Type (const _type);
		~CProdSvc_Type (void);

	public:
		LPCTSTR     Desc (void) const;
		const
		_type       Type (void) const;
		HRESULT     Type (const _type);

	};
}}}

typedef ebo::boo::gui::CProdSvc_Type::_type TProSvcType;

#endif/*_EBOBOOPROSVCTYPSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/