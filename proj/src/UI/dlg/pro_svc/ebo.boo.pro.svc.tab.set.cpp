/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Dec-2019 at 5:47:51a, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack personal account product and service info tab set interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.pro.svc.tab.set.h"
#include "ebo.boo.res.dlg.pas.h"

using namespace ebo::boo::gui;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CProSvcTabSet_Layout
	{
	private:
		RECT    m_area;
	public:
		CProSvcTabSet_Layout(const RECT& rcArea) : m_area(rcArea) {}

	public:
		RECT    TabsArea (void) const {
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};
}}}}

using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CProSvcTabSet:: CProSvcTabSet(ITabSetCallback& _snk) :
	m_e_active(_pages::e_general), m_page_1st(_snk), m_page_2nd(_snk), m_page_3rd(_snk), m_page_4th(_snk) { _snk; }
CProSvcTabSet::~CProSvcTabSet(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CProSvcTabSet::Create(const HWND hParent, const RECT& rcArea, const CProdSvc_Type& _tp) {
	hParent; rcArea;
	HRESULT hr_ = S_OK; m_e_type = _tp;

	if(FALSE == ::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return (hr_ = E_INVALIDARG);

	CProSvcTabSet_Layout layout(rcArea);
	RECT rcTabs  = layout.TabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs ,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_EBO_BOO_PRO_SVC_DLG_TAB
		);
	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());

	INT nIndex = 0; nIndex;
	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW; // auto-placement is made by default implementation of create/initialize page handler;

	if (m_e_type.Type() == TProSvcType::e_service) {
		m_cTabCtrl.AddItem(m_page_3rd.GetPageTitle());
		{
			m_page_3rd.Create(m_cTabCtrl.m_hWnd);
			m_page_3rd.SetWindowPos(
				HWND_TOP,
				0, 0, 0 , 0,
				dwFlags
			);
			if (m_page_3rd.IsWindow()) m_page_3rd.Index(nIndex++);
		}
	}
	else {
		m_cTabCtrl.AddItem(m_page_1st.GetPageTitle());
		{
			m_page_1st.Create(m_cTabCtrl.m_hWnd);
			m_page_1st.SetWindowPos(
					HWND_TOP,
					0, 0, 0 , 0,
					dwFlags
				);
			if (m_page_1st.IsWindow()) m_page_1st.Index(nIndex++);
		}
	}
	if (m_e_type.Type() == TProSvcType::e_service) {
		m_cTabCtrl.AddItem(m_page_4th.GetPageTitle());
		{
			m_page_4th.Create(m_cTabCtrl.m_hWnd);
			m_page_4th.SetWindowPos(
				HWND_TOP,
				0, 0, 0 , 0,
				dwFlags
			);
			if (m_page_4th.IsWindow()) m_page_4th.Index(nIndex++);
		}
	}
	else {
		m_cTabCtrl.AddItem(m_page_2nd.GetPageTitle());
		{
			m_page_2nd.Create(m_cTabCtrl.m_hWnd);
			m_page_2nd.SetWindowPos(
				HWND_TOP,
				0, 0, 0 , 0,
				dwFlags
			);
			if (m_page_2nd.IsWindow()) m_page_2nd.Index(nIndex++);
		}
	}
	m_cTabCtrl.SetCurSel(m_e_active);

	this->UpdateLayout();

	return S_OK;
}

HRESULT       CProSvcTabSet::Destroy(void)
{
	if (m_page_1st.IsWindow()){
		m_page_1st.DestroyWindow();  m_page_1st.m_hWnd = NULL;
	}
	if (m_page_2nd.IsWindow()){
		m_page_2nd.DestroyWindow();  m_page_2nd.m_hWnd = NULL;
	}
	if (m_page_3rd.IsWindow()){
		m_page_3rd.DestroyWindow();  m_page_3rd.m_hWnd = NULL;
	}
	if (m_page_4th.IsWindow()){
		m_page_4th.DestroyWindow();  m_page_4th.m_hWnd = NULL;
	}
	return S_OK;
}

CProSvcTabSet::_pages
              CProSvcTabSet::Selected(void) const      { return m_e_active; }
HRESULT       CProSvcTabSet::Selected(const _pages _v) {
	HRESULT hr_ = S_OK;
	const bool b_changed = (_v != m_e_active);
	m_e_active  = _v;
	if (b_changed && m_cTabCtrl.IsWindow()) {
		m_cTabCtrl.SetCurSel(m_e_active);
		this->UpdateLayout();
	}
	return  hr_;
}

void          CProSvcTabSet::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	if (m_page_1st.IsWindow()) m_page_1st.ShowWindow(nTabIndex == m_page_1st.Index() ? SW_SHOW : SW_HIDE);
	if (m_page_2nd.IsWindow()) m_page_2nd.ShowWindow(nTabIndex == m_page_2nd.Index() ? SW_SHOW : SW_HIDE);
	if (m_page_3rd.IsWindow()) m_page_3rd.ShowWindow(nTabIndex == m_page_3rd.Index() ? SW_SHOW : SW_HIDE);
	if (m_page_4th.IsWindow()) m_page_4th.ShowWindow(nTabIndex == m_page_4th.Index() ? SW_SHOW : SW_HIDE);
}