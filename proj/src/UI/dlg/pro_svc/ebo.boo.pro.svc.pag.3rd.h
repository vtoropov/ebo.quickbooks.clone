#ifndef _EBOBOOPROSVCPAG3RD_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOBOOPROSVCPAG3RD_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Dec-2019 at 9:33:39a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack personal account product and service info 3rd dialog page interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"

namespace ebo { namespace boo { namespace gui {

	class CPagePaS_3rd : public CTabPageBase, public  ITabPageEvents {
	                    typedef CTabPageBase  TBasePage;
	private:
		
	public :
		 CPagePaS_3rd(ITabSetCallback&);
		~CPagePaS_3rd(void);

	private:
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TBasePage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed;

	public:
	};

}}}

#endif/*_EBOBOOPROSVCPAG3RD_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/