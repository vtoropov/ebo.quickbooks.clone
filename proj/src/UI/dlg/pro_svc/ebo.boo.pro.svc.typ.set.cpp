/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Dec-2019 at 3:48:45p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack personal account product and service item type set interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.pro.svc.typ.set.h"
#include "ebo.boo.res.dlg.pas.h"

using namespace ebo::boo::gui;

/////////////////////////////////////////////////////////////////////////////

CProdSvc_Type:: CProdSvc_Type(void) : m_value(_type::e_none) {}
CProdSvc_Type:: CProdSvc_Type(const _type _tp) : m_value(_tp){}
CProdSvc_Type::~CProdSvc_Type(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR     CProdSvc_Type::Desc (void) const {
	m_desc = _T("#n/a");

	switch (this->Type()) {
	case _type::e_assembly   :  { m_desc.LoadStringW(IDS_EBO_BOO_PRO_SVC_TYP_BUN); } break;
	case _type::e_inventory  :  { m_desc.LoadStringW(IDS_EBO_BOO_PRO_SVC_TYP_INV); } break;
	case _type::e_nonstored  :  { m_desc.LoadStringW(IDS_EBO_BOO_PRO_SVC_TYP_NON); } break;
	case _type::e_service    :  { m_desc.LoadStringW(IDS_EBO_BOO_PRO_SVC_TYP_SVC); } break;
	}

	return m_desc.GetString();
}
const
TProSvcType CProdSvc_Type::Type (void) const     { return this->m_value; }
HRESULT     CProdSvc_Type::Type (const _type _v) { this->m_value = _v; return S_OK; }
/////////////////////////////////////////////////////////////////////////////