#ifndef _EBOBOOPROSVCTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _EBOBOOPROSVCTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Dec-2019 at 5:43:29a, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack personal account product and service info tab set interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.pro.svc.pag.1st.h"
#include "ebo.boo.pro.svc.pag.2nd.h"
#include "ebo.boo.pro.svc.pag.3rd.h"
#include "ebo.boo.pro.svc.pag.4th.h"
#include "ebo.boo.pro.svc.typ.set.h"

namespace ebo { namespace boo { namespace gui {

	class CProSvcTabSet
	{
	public:
		enum _pages : INT {
			e_general  = 0,  // general information page;
			e_thumbs   = 1
		};
	private:
		WTL::CTabCtrl m_cTabCtrl;
		_pages        m_e_active;
		CProdSvc_Type m_e_type  ;

	private: //tab page(s)
		CPagePaS_1st  m_page_1st;
		CPagePaS_2nd  m_page_2nd;
		CPagePaS_3rd  m_page_3rd;
		CPagePaS_4th  m_page_4th;

	public:
		 CProSvcTabSet (ITabSetCallback&);
		~CProSvcTabSet (void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& rcArea, const CProdSvc_Type&);
		HRESULT       Destroy (void)        ;
		_pages        Selected(void)   const;
		HRESULT       Selected(const _pages);
		VOID          UpdateLayout(void);
	public:
	};
}}}

#endif/*_EBOBOOPROSVCTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/