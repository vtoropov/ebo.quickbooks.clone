/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Dec-2019 at 9:33:31p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Ebo Pack personal account app supplier dialog notes & attachments page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.spl.dat.pag.6th.h"
#include "ebo.boo.res.dlg.spl.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageSupl_6th_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_BOO_SPL_PAG_6TH_AVA,    // notes avatar ;
			e_ava_cap = IDC_EBO_BOO_SPL_PAG_6TH_CAP,    // notes caption;
			e_att_lst = IDC_EBO_BOO_SPL_PAG_6TH_LST,    // attachments list view control;
		};
	};
	typedef CPageSupl_6th_Ctl This_Ctl;

	class CPageSupl_6th_Layout {
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageSupl_6th_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_cap;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPageSupl_6th_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPageSupl_6th_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (HIMAGELIST _images) {
			m_error << __MODULE__ << S_OK;

			CListViewCtrl att_lst = Lay_(m_page_ref) << This_Ctl::e_att_lst;
			if ( NULL  != att_lst ) {
				att_lst.AddColumn(_T(" ")          , 0); att_lst.SetColumnWidth(0,  32);
				att_lst.AddColumn(_T("Reference")  , 1); att_lst.SetColumnWidth(1, 296);
				att_lst.AddColumn(_T("Type")       , 2); att_lst.SetColumnWidth(2,  64);
			}
			att_lst.SetExtendedListViewStyle(LVS_EX_FULLROWSELECT, 0);
			att_lst.SetImageList(_images, LVSIL_SMALL);
#if (1)
			att_lst.AddItem(0, 0, _T(""), 1);
			att_lst.AddItem(0, 1, _T("https://drive.google.com/open?id=1uUJnfMar3KzXnq4y6OA-sM0KFavTWGGt"));
			att_lst.AddItem(0, 2, _T("Web Storage"));
#endif
			return m_error;
		}

	};

	class CPageSupl_6th_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPageSupl_6th_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageSupl_6th:: CPageSupl_6th(ITabSetCallback& _set_snk):
       TBasePage(IDD_EBO_BOO_SPL_PAG_6TH, *this, _set_snk), m_lv_img(NULL) {
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	CGdiPlusPngLoader::CreateImages(
		IDR_EBO_BOO_SPL_PAG_6TH_LST, hInstance, m_lv_img
	);
}

CPageSupl_6th::~CPageSupl_6th(void) {
	if (m_lv_img) {
		::ImageList_Destroy(m_lv_img); m_lv_img = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageSupl_6th::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageSupl_6th::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageSupl_6th_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPageSupl_6th_Init init_(*this);
	init_.OnCreate(m_lv_img);
	
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageSupl_6th::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageSupl_6th_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageSupl_6th::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPageSupl_6th_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE ) {
				TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageSupl_6th::IsChanged   (void) const {
	return false;
}

CAtlString CPageSupl_6th::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Notes && Attachments ")); return cs_title; }

VOID       CPageSupl_6th::UpdateData  (const DWORD _opt) {
	_opt;
}