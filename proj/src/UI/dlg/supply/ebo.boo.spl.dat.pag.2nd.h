#ifndef _EBOBOOSPLDATPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOBOOSPLDATPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Dec-2019 at 6:34:12p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Ebo Pack personal account app supplier dialog parent control data page interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.stg.dat.acc.h"

namespace ebo { namespace boo { namespace gui {

	class CPageSupl_2nd : public CTabPageBase, public  ITabPageEvents {
	                     typedef CTabPageBase  TBasePage;
	private:
	public :
		 CPageSupl_2nd(ITabSetCallback&);
		~CPageSupl_2nd(void);

	private:
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TBasePage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed;

	public:
	};

}}}

#endif/*_EBOBOOSPLDATPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/