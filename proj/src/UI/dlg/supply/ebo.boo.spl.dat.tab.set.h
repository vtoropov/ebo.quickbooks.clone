#ifndef _EBOBOOSTLDATTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _EBOBOOSTLDATTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Dec-2019 at 6:09:42p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Ebo Pack personal account app supplier dialog page tabset interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.spl.dat.pag.1st.h"
#include "ebo.boo.spl.dat.pag.2nd.h"
#include "ebo.boo.spl.dat.pag.3rd.h"
#include "ebo.boo.spl.dat.pag.4th.h"
#include "ebo.boo.spl.dat.pag.5th.h"
#include "ebo.boo.spl.dat.pag.6th.h"

namespace ebo { namespace boo { namespace gui {

	class CSuplTabSet
	{
	public:
		enum _pages : INT {
			e_gnrl  = 0,  // customer general information page;
			e_prnt  = 1,  // parent control data page;
		};
	private:
		WTL::CTabCtrl m_cTabCtrl;
		_pages        m_e_active;

	private: //tab page(s)
		CPageSupl_1st  m_1st_page;
		CPageSupl_2nd  m_2nd_page;
		CPageSupl_3rd  m_3rd_page;
		CPageSupl_4th  m_4th_page;
#if (0)
		CPageSupl_5th  m_5th_page;
#endif
		CPageSupl_6th  m_6th_page;

	public:
		 CSuplTabSet (ITabSetCallback&);
		~CSuplTabSet (void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy (void)        ;
		_pages        Selected(void)   const;
		HRESULT       Selected(const _pages);
		VOID          UpdateLayout(void);
	public:
	};
}}}

#endif/*_EBOBOOSTLDATTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/