/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Dec-2019 at 6:40:43p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Ebo Pack personal account app supplier dialog parent control data page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.spl.dat.pag.2nd.h"
#include "ebo.boo.res.dlg.spl.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageSupl_2nd_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_BOO_SPL_PAG_2ND_AVA,    // page parent control data avatar ;
			e_ava_cap = IDC_EBO_BOO_SPL_PAG_2ND_CAP,    // page parent control data caption;
		};
	};
	typedef CPageSupl_2nd_Ctl This_Ctl;

	class CPageSupl_2nd_Layout {
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageSupl_2nd_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_cap;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPageSupl_2nd_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPageSupl_2nd_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;

			return m_error;
		}

	};

	class CPageSupl_2nd_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPageSupl_2nd_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageSupl_2nd:: CPageSupl_2nd(ITabSetCallback& _set_snk):
       TBasePage(IDD_EBO_BOO_SPL_PAG_2ND, *this, _set_snk) {
}

CPageSupl_2nd::~CPageSupl_2nd(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageSupl_2nd::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageSupl_2nd::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageSupl_2nd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPageSupl_2nd_Init init_(*this);
	init_.OnCreate();
	
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageSupl_2nd::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageSupl_2nd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageSupl_2nd::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPageSupl_2nd_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE ) {
				TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageSupl_2nd::IsChanged   (void) const {
	return false;
}

CAtlString CPageSupl_2nd::GetPageTitle(void) const {  static CAtlString cs_title(_T(" Account ")); return cs_title; }

VOID       CPageSupl_2nd::UpdateData  (const DWORD _opt) {
	_opt;
}