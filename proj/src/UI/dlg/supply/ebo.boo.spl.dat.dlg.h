#ifndef _EBOBOOSPLDATDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
#define _EBOBOOSPLDATDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Dec-2019 at 6:01:02p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is Ebo Pack personal account app supplier data dialog interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.frms.img.ban.h"
#include "ebo.boo.spl.dat.tab.set.h"

namespace ebo { namespace boo { namespace gui {

	using shared::sys_core::CError;
	using ex_ui::frames::CImageBanner;
	using ex_ui::draw::defs::IRenderer;

	class CSuplDlg {
	private:
		class CSuplDlgImpl : public ::ATL::CDialogImpl<CSuplDlgImpl>, ITabSetCallback, IRenderer {
		                    typedef ::ATL::CDialogImpl<CSuplDlgImpl>  TDialog;
			friend class CSuplDlg;
		private:
			CImageBanner   m_banner;
			CSuplTabSet    m_tabset;

		public :
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CSuplDlgImpl)
				MESSAGE_HANDLER     (WM_COMMAND    ,  OnBtnCmd   )
				MESSAGE_HANDLER     (WM_DESTROY    ,  OnDestroy  )
				MESSAGE_HANDLER     (WM_INITDIALOG ,  OnInitDlg  )
				MESSAGE_HANDLER     (WM_KEYDOWN    ,  OnKeyDown  ) // does not work in modeless dialogs;
				MESSAGE_HANDLER     (WM_SYSCOMMAND ,  OnSysCmd   )
				NOTIFY_CODE_HANDLER (TCN_SELCHANGE ,  OnTabNotify)
			END_MSG_MAP()

		public:
			 CSuplDlgImpl (void) ;
			~CSuplDlgImpl (void) ;

		private:
			LRESULT OnBtnCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		private:
			LRESULT OnTabNotify (INT  , LPNMHDR, BOOL&);

		private: // ITabSetCallback
			virtual HRESULT TabSet_OnDataChanged (const UINT pageId, const bool bChanged) override sealed;
			virtual HRESULT TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) override sealed;
		private: // IRenderer
			HRESULT DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const override sealed;
		};
	private:
		CSuplDlgImpl   m_dlg;

	public:
		 CSuplDlg (void);
		~CSuplDlg (void);

	public:
		HRESULT    DoModal(void);

	private:
		CSuplDlg (const CSuplDlg&);
		CSuplDlg& operator= (const CSuplDlg&);
	};

}}}

#endif/*_EBOBOOSPLDATDLG_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED*/