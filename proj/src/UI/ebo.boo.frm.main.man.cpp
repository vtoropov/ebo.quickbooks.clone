/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Dec-2018 at 12:15:54p, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is USB Drive Detective desktop (bitsphereinc.com) app main form view manager interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 29-Oct-2019 at 9:56:38p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "StdAfx.h"
#include "ebo.boo.frm.main.man.h"
#include "ebo.boo.res.h"

using namespace ebo::boo::gui::view;

/////////////////////////////////////////////////////////////////////////////

CViewHandler:: CViewHandler(CViewManager& _man) : m_man_ref(_man) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CViewHandler::~CViewHandler(void) {}

/////////////////////////////////////////////////////////////////////////////

bool          CViewHandler::Accept(const WORD _cmd_id) const {

	bool b_handled_ = false;
	if (!b_handled_) b_handled_ = m_man_ref.Report  ().CanAccept(_cmd_id);
	if (!b_handled_) b_handled_ = m_man_ref.Customer().CanAccept(_cmd_id);
	if (!b_handled_) b_handled_ = m_man_ref.Sales   ().CanAccept(_cmd_id);
	if (!b_handled_) b_handled_ = m_man_ref.Expenses().CanAccept(_cmd_id);
	if (!b_handled_) b_handled_ = m_man_ref.Employee().CanAccept(_cmd_id);
	if (!b_handled_) b_handled_ = m_man_ref.Supplier().CanAccept(_cmd_id);
	if (!b_handled_) b_handled_ = m_man_ref.Taxes   ().CanAccept(_cmd_id);
	if (!b_handled_) b_handled_ = m_man_ref.Accounts().CanAccept(_cmd_id);

	return b_handled_;
}

HRESULT       CViewHandler::Do    (const WORD _cmd_id) {
	m_error << __MODULE__ << S_OK;

	if (this->Accept(_cmd_id) == false)
		return (m_error = (DWORD)ERROR_INVALID_OPERATION);

	HRESULT hr_ = m_error; hr_;

	bool b_handled_ = m_man_ref.Report().CanAccept(_cmd_id);
	if ( b_handled_ ) {
		hr_ =  m_man_ref.Report().OnCommand(_cmd_id) ; if (FAILED(hr_)) m_error = m_man_ref.Report().Error();
		return m_error;
	}
	if (!b_handled_) b_handled_ = m_man_ref.Customer().CanAccept(_cmd_id);
	if ( b_handled_) {
		hr_ = m_man_ref.Customer().OnCommand(_cmd_id); if (FAILED(hr_)) m_error = m_man_ref.Customer().Error();
		return m_error;
	}
	if (!b_handled_) b_handled_ = m_man_ref.Sales   ().CanAccept(_cmd_id);
	if ( b_handled_) {
		hr_ = m_man_ref.Sales().OnCommand(_cmd_id); if (FAILED(hr_)) m_error = m_man_ref.Sales().Error();
		return m_error;
	}
	if (!b_handled_) b_handled_ = m_man_ref.Expenses().CanAccept(_cmd_id);
	if ( b_handled_) {
		return m_error;
	}
	if (!b_handled_) b_handled_ = m_man_ref.Employee().CanAccept(_cmd_id);
	if ( b_handled_) {
		return m_error;
	}
	if (!b_handled_) b_handled_ = m_man_ref.Supplier().CanAccept(_cmd_id);
	if ( b_handled_) {
		hr_ = m_man_ref.Supplier().OnCommand(_cmd_id); if (FAILED(hr_)) m_error = m_man_ref.Supplier().Error();
		return m_error;
	}
	if (!b_handled_) b_handled_ = m_man_ref.Taxes().CanAccept(_cmd_id);
	if ( b_handled_) {
		hr_ = m_man_ref.Taxes().OnCommand(_cmd_id)   ; if (FAILED(hr_)) m_error = m_man_ref.Taxes().Error();
		return m_error;
	}
	if (!b_handled_) b_handled_ = m_man_ref.Accounts().CanAccept(_cmd_id);
	if ( b_handled_) {
		return m_error;
	}
	return  m_error;
}

TErrorRef     CViewHandler::Error (void) const { return m_error; }

/////////////////////////////////////////////////////////////////////////////

CViewManager:: CViewManager(void) : m_h_obj(*this), m_active(NULL) { m_error << __MODULE__ << S_OK >> __MODULE__;
	m_views[CViewType::e_cust] = &m_v_cust;
	m_views[CViewType::e_emp ] = &m_v_emp ;
	m_views[CViewType::e_exp ] = &m_v_exp ;
	m_views[CViewType::e_rpt ] = &m_v_rpt ;
	m_views[CViewType::e_sale] = &m_v_sale;
	m_views[CViewType::e_supl] = &m_v_supl;
	m_views[CViewType::e_tax ] = &m_v_tax ;
	m_views[CViewType::e_acnt] = &m_v_acnt;
}
CViewManager::~CViewManager(void) {}

/////////////////////////////////////////////////////////////////////////////
CViewType::_tp
CViewManager::Active (void) const  {
	for (INT i_ = 0; i_ < _countof(m_views); i_++)
		if (m_views[i_]->IsVisible())
			return (CViewType::_tp)i_;
	return CViewType::e_none;
}
HRESULT       CViewManager::Active (const  CViewType::_tp _tp){
	m_error << __MODULE__ << S_OK;

	m_active = NULL;
	if (_tp != CViewType::e_none)
		m_active = m_views[_tp];
	

	if (m_active)
		m_active->IsVisible(true);
	for (INT i_ = 0; i_ < _countof(m_views); i_++) {
		if (m_views[i_] != m_active)
			m_views[i_]->IsVisible(false);
	}

	return m_error;
}

HRESULT       CViewManager::Create (const HWND hParent, const RECT& _area) {
	m_error << __MODULE__ << S_OK;

	if (NULL == hParent || !::IsWindow(hParent)) return (m_error = OLE_E_INVALIDHWND);
	if (::IsRectEmpty(&_area)) return (m_error = OLE_E_INVALIDRECT);

	HRESULT hr_ = m_error;

	for (INT i_ = 0; i_ < _countof(m_views); i_++) {
		hr_ = m_views[i_]->Create(hParent, _area, false); if (FAILED(hr_)) return (m_error = hr_);
	}

	return m_error;
}

HRESULT       CViewManager::Destroy(void) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = m_error;

	for (INT i_ = 0; i_ < _countof(m_views); i_++) {
		hr_ = m_views[i_]->Destroy(); if (FAILED(hr_)) m_views[i_]->Error().Show();
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////
const
CAccView&      CViewManager::Accounts(void)  const { return m_v_acnt; }
CAccView&      CViewManager::Accounts(void)        { return m_v_acnt; }
const
CCustView&     CViewManager::Customer(void)  const { return m_v_cust; }
CCustView&     CViewManager::Customer(void)        { return m_v_cust; }
TErrorRef      CViewManager::Error   (void)  const { return m_error ; }
const
CEmpView&      CViewManager::Employee(void)  const { return m_v_emp ; }
CEmpView&      CViewManager::Employee(void)        { return m_v_emp ; }
const
CExpView&      CViewManager::Expenses(void)  const { return m_v_exp ; }
CExpView&      CViewManager::Expenses(void)        { return m_v_exp ; }
const
TViewHandler&  CViewManager::Handler (void)  const { return m_h_obj ; }
TViewHandler&  CViewManager::Handler (void)        { return m_h_obj ; }
const
CRptView&      CViewManager::Report  (void)  const { return m_v_rpt ; }
CRptView&      CViewManager::Report  (void)        { return m_v_rpt ; }
const
CSaleView&     CViewManager::Sales   (void)  const { return m_v_sale; }
CSaleView&     CViewManager::Sales   (void)        { return m_v_sale; }
const
CSuplView&     CViewManager::Supplier(void)  const { return m_v_supl; }
CSuplView&     CViewManager::Supplier(void)        { return m_v_supl; }
const
CTaxView&      CViewManager::Taxes   (void)  const { return m_v_tax ; }
CTaxView&      CViewManager::Taxes   (void)        { return m_v_tax ; }

/////////////////////////////////////////////////////////////////////////////

HRESULT        CViewManager::Redraw  (void) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = m_error;

	for (INT i_ = 0; i_ < _countof(m_views); i_++) {
		hr_ = m_views[i_]->Refresh();
	}

	return m_error;
}

HRESULT        CViewManager::Trigger(const CViewType::_tp _view) {
	m_error << __MODULE__ << S_OK;
	// it is very important to keep a specific sequence in managing views' visibility:
	// a view being made visible must come first, otherwise, main window background can appear at a moment;
	return this->Active(_view);
}

bool           CViewManager::Visible (void)  const {
	return (m_active != NULL);
}