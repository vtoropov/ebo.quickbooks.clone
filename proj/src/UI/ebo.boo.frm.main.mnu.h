#ifndef _EBOBOOFRMMAINMNU_H_822EC824_BE86_4d40_A172_0DE7F2E95259_INCLUDED
#define _EBOBOOFRMMAINMNU_H_822EC824_BE86_4d40_A172_0DE7F2E95259_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-May-2016 at 10:16:52pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian Main Frame Manu Bar class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 16-Aug-2018 at 5:02:19p, UTC+7, Novosibirsk, Rodniki, Tulenina, Thursday;
	Adopted to Ebo Pack personal account app on 30-Oct-2019 at 9:09:31a, UTC+7, Novosibirsk, Tulenina, Wednesday;
*/
#include "ebo.boo.frm.main.h"
namespace ebo { namespace boo { namespace gui {
	//
	// adopted version does not use menu data for identifying a popup menu, because it does not work in regular window
	// in comparison with dialog based frame; moreover, in order to still identify a popup menu, new approach is applied:
	// menu cache is created for associating particular menu handle with identifier specified;
	//
    typedef ::std::map<HMENU, UINT> t_sub_menu_map;  // a key is actual handle of a sub-menu, a value is an associated identifier;
	class CMenuBar {
	private:
		CMainForm&     m_main; // a reference to main form object;

	public:
		CMenuBar(CMainForm&);

	public:
		VOID     Initialize(void);
		VOID     UpdateState(const HMENU _sub, const INT _ndx);

	public:
		operator HMENU(void)const;

	public:
		static WORD  CommandFirst(void);
		static WORD  CommandLast (void);
		static HMENU GetCtxMenu  (void);   // a caller is responsive for releasing/deleting menu handle;
		static HMENU GetExpMenu  (void);   // a caller is responsive for releasing/deleting menu handle;
		static HMENU GetPrintPrw (void);   // a caller is responsive for releasing/deleting menu handle;
		static HMENU GetRptMenu  (void);   // a caller is responsive for releasing/deleting menu handle;
	};
}}}

#endif/*_EBOBOOFRMMAINMNU_H_822EC824_BE86_4d40_A172_0DE7F2E95259_INCLUDED*/