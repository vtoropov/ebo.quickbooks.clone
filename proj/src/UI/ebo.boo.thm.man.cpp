/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Nov-2019 at 8:24:09a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app color theme manager interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.thm.man.h"
#include "ebo.boo.res.h"

using namespace ebo::boo::gui;

/////////////////////////////////////////////////////////////////////////////

CThemeMan:: CThemeMan(void) : m_them_ndx(0) { CCmpClrs_Enum clrs_; m_clrs = clrs_[m_them_ndx];}
CThemeMan::~CThemeMan(void) {}

/////////////////////////////////////////////////////////////////////////////
const
CCmpClrs&     CThemeMan::Colours (void) const { return m_clrs; }
eGridTheme    CThemeMan::GrdTheme(void) const {
	eGridTheme e_theme = eGridTheme::eSolidBlack;
	switch (m_them_ndx) {
	case  0: case  1: case  2: case  5: case  8: case  9: case 10: case 11: case 12: case 13:
	case  3: { e_theme = eGridTheme::eSolidBlack; } break;
	case  6:
	case  4: { e_theme = eGridTheme::eSolidBlue ; } break;
	case  7: { e_theme = eGridTheme::eSolidWhite; } break;
	case 15: case 16: case 17: case 14: case 18: case 19:
	case 20: { e_theme = eGridTheme::eSolidWhite; } break;
	}
	return e_theme;
}
const bool    CThemeMan::Is      (const WORD _w_cmd) const {
	if (_w_cmd < IDC_EBO_BOO_MNU_CMD_CLR_TH0 ||
		_w_cmd > IDC_EBO_BOO_MNU_CMD_CLR_THK)
		return false;
	const
	bool   b_result = ((static_cast<UINT>(_w_cmd) - IDC_EBO_BOO_MNU_CMD_CLR_TH0) == m_them_ndx);
	return b_result;
}
HRESULT       CThemeMan::Select  (const WORD _w_cmd) {
	if (IDC_EBO_BOO_MNU_CMD_CLR_TH0 <= _w_cmd &&
		IDC_EBO_BOO_MNU_CMD_CLR_THK >= _w_cmd) {
		m_them_ndx = _w_cmd - IDC_EBO_BOO_MNU_CMD_CLR_TH0;
		CCmpClrs_Enum clrs_;
		m_clrs = clrs_[m_them_ndx];
		return S_OK;
	}
	else
		return __DwordToHresult(ERROR_NOT_FOUND);
}

/////////////////////////////////////////////////////////////////////////////

CThemeMan& CThemeMan::operator =(const WORD _w_cmd) { this->Select(_w_cmd); return *this; }

/////////////////////////////////////////////////////////////////////////////

CThemeMan::operator const CCmpClrs& (void) const { return this->Colours(); }