#ifndef _EBOBOOFRMMAINMAN_H_28AD0080_1473_4063_871D_D8F1FD9D9AAC_INCLUDED
#define _EBOBOOFRMMAINMAN_H_28AD0080_1473_4063_871D_D8F1FD9D9AAC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Dec-2018 at 6:42:50p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is USB Drive Detective (bitsphereinc.com) desktop app main form view manager interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 29-Oct-2019 at 9:40:31p, UTC+7, Novosibirsk, Tulenina, Tuesday;
*/
#include "shared.gen.sys.err.h"
#include "ebo.boo.main.view.emp.h"
#include "ebo.boo.main.view.rpt.h"
#include "ebo.boo.main.view.cust.h"
#include "ebo.boo.main.view.sale.h"
#include "ebo.boo.main.view.exp.h"
#include "ebo.boo.main.view.supl.h"
#include "ebo.boo.main.view.tax.h"
#include "ebo.boo.main.view.acc.h"

namespace ebo { namespace boo { namespace gui { namespace view {

	using shared::sys_core::CError;

	class CViewType {
	public:
		enum _tp : INT {
			e_none   = -1 ,   // no view, default;
			e_cust   = 0x0,   // customer view;
			e_emp    = 0x1,   // employee view;
			e_exp    = 0x2,   // expenses view;
			e_rpt    = 0x3,   // analysis reports;
			e_sale   = 0x4,   // sales' view;
			e_supl   = 0x5,   // supplier view;
			e_tax    = 0x6,   // taxes view;
			e_acnt   = 0x7,   // account chart view;
		};
	};

	class CViewManager;

	class CViewHandler {
	private:
		CViewManager& m_man_ref;
		CError        m_error;

	public:
		 CViewHandler(CViewManager&);
		~CViewHandler(void);

	public:
		bool          Accept(const WORD _cmd_id) const;
		HRESULT       Do    (const WORD _cmd_id)      ;
		TErrorRef     Error (void) const;
	};

	typedef CViewHandler TViewHandler;

	class CViewManager  {
	private:
		CView_Base*   m_active;
		CView_Base*   m_views[CViewType::e_acnt + 1];

		CError        m_error ;
		CEmpView      m_v_emp ;
		CCustView     m_v_cust;
		CRptView      m_v_rpt ;
		CSaleView     m_v_sale;
		CExpView      m_v_exp ;
		CSuplView     m_v_supl;
		CTaxView      m_v_tax ;
		CAccView      m_v_acnt;
		TViewHandler  m_h_obj ;

	public:
		 CViewManager(void);
		~CViewManager(void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& _area);
		HRESULT       Destroy (void)       ;

	public: // accessor(s)
		const
		CAccView&     Accounts(void)  const;
		CAccView&     Accounts(void)       ;
		CViewType::_tp
		              Active  (void)  const;
		HRESULT       Active  (const CViewType::_tp);
		const
		CCustView&    Customer(void)  const;
		CCustView&    Customer(void)       ;
		TErrorRef     Error   (void)  const;
		const
		CEmpView&     Employee(void)  const;
		CEmpView&     Employee(void)       ;
		const
		CExpView&     Expenses(void)  const;
		CExpView&     Expenses(void)       ;
		const
		TViewHandler& Handler (void)  const;
		TViewHandler& Handler (void)       ;
		HRESULT       Redraw  (void)       ;
		const
		CRptView&     Report  (void)  const;
		CRptView&     Report  (void)       ;
		const
		CSaleView&    Sales   (void)  const;
		CSaleView&    Sales   (void)       ;
		const
		CSuplView&    Supplier(void)  const;
		CSuplView&    Supplier(void)       ;
		const
		CTaxView&     Taxes   (void)  const;
		CTaxView&     Taxes   (void)       ;
		HRESULT       Trigger (const CViewType::_tp);
		bool          Visible (void)  const;
	};

}}}}

#endif/*_EBOBOOFRMMAINMAN_H_28AD0080_1473_4063_871D_D8F1FD9D9AAC_INCLUDED*/