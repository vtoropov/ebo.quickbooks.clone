/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Nov-2018 at 5:44:25p, UTC+7, Novosibirsk, Tulenina, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop app setting dialog page base layout interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 10:40:56p, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 10:56:11a, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "StdAfx.h"
#include "ebo.boo.pag.lay.bas.h"

using namespace ebo::boo::gui;

/////////////////////////////////////////////////////////////////////////////

CPage_Layout_Base:: CPage_Layout_Base(const HWND _host) : m_active(0) { *this = _host; }
CPage_Layout_Base::~CPage_Layout_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

CPage_Layout_Base& CPage_Layout_Base::operator= (const HWND _v) { m_host = _v; return *this; }

/////////////////////////////////////////////////////////////////////////////

RECT        CPage_Layout_Base::operator=(const UINT _ctl_id) const {
	_ctl_id;
	RECT rec_ctl = {0};

	if (NULL == m_host.m_hWnd)
		return rec_ctl;

	CWindow ctl_itm = m_host.GetDlgItem(_ctl_id);
	if (NULL == ctl_itm.m_hWnd)
		return rec_ctl;
	else
		ctl_itm.GetWindowRect(&rec_ctl);

	::MapWindowPoints(HWND_DESKTOP, m_host, (LPPOINT)&rec_ctl, 0x2);

	return rec_ctl;
}

HWND        CPage_Layout_Base::operator<<(const UINT _ctl_id) { return m_host.GetDlgItem(m_active = _ctl_id); }

CAtlString  CPage_Layout_Base::operator<=(const UINT _ctl_id) const {
	CAtlString cs_cap;

	CWindow ctl_ = m_host.GetDlgItem(_ctl_id);
	if (ctl_)
		ctl_.GetWindowText(cs_cap);

	return cs_cap;
}

/////////////////////////////////////////////////////////////////////////////

CPage_Layout_Base::operator    RECT (void) const {
	RECT rec_cli = {0};

	if (NULL == m_host.m_hWnd)
		return rec_cli;

	m_host.GetWindowRect(&rec_cli);
	::MapWindowPoints(HWND_DESKTOP, m_host, (LPPOINT)&rec_cli, 0x2);

	return rec_cli;
}