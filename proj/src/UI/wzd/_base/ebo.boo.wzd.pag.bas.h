#ifndef _EBOBOOWZDPAGBAS_H_591600E9_7DB4_4ec1_BE04_755F53449EE0_INCLUDED
#define _EBOBOOWZDPAGBAS_H_591600E9_7DB4_4ec1_BE04_755F53449EE0_INCLUDED
/*
	Created by Tech_dog (VToropov) on 14-Jan-2016 at 11:44:33am, GMT+7, Phuket, Rawai, Thursday;
	This is USB Drive Detective (bitsphereinc.com) desktop app UI dialog page base interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 16-Aug-2018 at 7:02:21p, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 10:34:04p, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 10:56:11a, UTC+7, Novosibirsk, Tulenina, Friday;
*/

namespace ebo { namespace boo { namespace gui {

	interface ITabPageEvents
	{
		virtual LRESULT TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) PURE;
	};
	//
	// TODO: this interface must be re-viewed; it very looks like it is not used frequently;
	//
	interface ITabPageCmdHandler
	{
		virtual BOOL    TabPage_OnCommand(const WORD wCmdId) PURE;
	};
	//
	// TODO: this interface is intended for calling parent window/dialog panel functionality;
	//       it is also sent to each tab page, that may be unnecessary; it the reason for default implementation;
	//       the interface must be re-viewed;
	//
	interface ITabSetCallback
	{
		virtual bool    TabSet_IsNewMode(void) { return false; }
		virtual HRESULT TabSet_OnDataChanged (const UINT pageId, const bool bChanged    ) { pageId;  bChanged ; return E_NOTIMPL; }
		virtual HRESULT TabSet_OnDataChanged (const UINT pageId, const CAtlString& _data) { pageId;  _data    ; return E_NOTIMPL; }
		virtual HRESULT TabSet_OnDataComplete(const UINT pageId, const bool bAccepted   ) { pageId;  bAccepted; return E_NOTIMPL; }
		virtual HRESULT TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) {
				::SetRectEmpty(&_rc_ctrl); ctrlId; return E_NOTIMPL;
			}
	};
	#define WM_WINDOWVIS  WM_WINDOWPOSCHANGED
	class CTabPageBase:
		public  ::ATL::CDialogImpl<CTabPageBase>,
		public  ITabPageCmdHandler
	{
		typedef ::ATL::CDialogImpl<CTabPageBase> TBaseDialog;
	public:
		const UINT IDD;

	protected:
		bool                m_bInited;
		INT                 m_nIndex ;
		ITabPageEvents &    m_evt_snk;   // window messages sink that is used by page for event handling;
		ITabSetCallback&    m_set_snk;   // tab set container callback reference;
		bool                m_b_vis_1st_time;

	protected:
		CTabPageBase(const UINT RID, ITabPageEvents&, ITabSetCallback&);
		virtual ~CTabPageBase(void);

	public:
		BEGIN_MSG_MAP(CTabPageBase)
			MESSAGE_HANDLER (WM_DESTROY   ,  OnPageClose)
			MESSAGE_HANDLER (WM_INITDIALOG,  OnPageInit )
			MESSAGE_HANDLER (WM_PAINT     ,  OnPagePaint)
			MESSAGE_HANDLER (WM_SIZE      ,  OnPageSize )
			MESSAGE_HANDLER (WM_WINDOWVIS ,  OnShowHide )
			m_evt_snk.TabPage_OnEvent(uMsg, wParam, lParam, bHandled);
			if (bHandled)
				return TRUE;
		END_MSG_MAP()

	public:
		virtual bool        IsChanged   (void) const { return false; }   // checks for changing controls' state;
		virtual CAtlString  GetPageTitle(void) const PURE;
		virtual VOID        UpdateData  (const DWORD _opt = 0)     {_opt;}
		virtual VOID        UpdateLayout(void)       {}
		virtual HRESULT     Validate    (void) const { return E_NOTIMPL; };

	protected:
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		virtual LRESULT     OnPagePaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		virtual LRESULT     OnShowHide (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	protected: // ITabPageCmdHandler
		virtual BOOL        TabPage_OnCommand(const WORD wCmdId) override;
	public:
		INT                 Index(void)const;
		VOID                Index(const INT);
	public:
		static const HFONT  SectionCapFont (void);
	};
#if(0)
	VOID       AlignComboHeightTo(const CWindow& _host, const UINT _combo_id, const UINT _height);
	VOID       AlignCtrlsByHeight(const CWindow& _host, const UINT _ctrl_0, const UINT _ctrl_1, const SIZE& _shifts);

	class CWindowEx : public ATL::CWindow
	{
		typedef ATL::CWindow TWindow;
	public:
		CWindowEx(const CWindow&);
	public:
		CWindowEx& operator=(const CWindow&);
	public:
		RECT      GetDlgItemRect(const UINT _ctrl_id)const;
	};

	class CCtrlAutoState
	{
	private:
		const
		UINT      m_ctrlId;
		CWindow   m_host;
		bool      m_bEnabled; // the old enable status that will be restored on this class object destruction
	public:
		CCtrlAutoState(const CWindow& _host, const UINT ctrlId, const bool bEnabled);
		~CCtrlAutoState(void);
	};
#endif
}}}

#endif/*_EBOBOOWZDPAGBAS_H_591600E9_7DB4_4ec1_BE04_755F53449EE0_INCLUDED*/