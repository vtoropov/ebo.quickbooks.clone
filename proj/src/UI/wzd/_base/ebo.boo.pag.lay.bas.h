#ifndef EBOBOOPAGLAYBAS_H_9110A96E_7D32_45DC_9283_F1DA0C0839CD_INCLUDED
#define EBOBOOPAGLAYBAS_H_9110A96E_7D32_45DC_9283_F1DA0C0839CD_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Nov-2018 at 1:31:43p, UTC+7, Novosibirsk, Tulenina, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop app setting dialog page base layout interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 10:38:36p, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 10:56:11a, UTC+7, Novosibirsk, Tulenina, Friday;
*/

namespace ebo { namespace boo { namespace gui {

	class CPage_Layout_Base {
	protected:
		CWindow     m_host;
		UINT        m_active;    // identifier of currently active or selected control within this layer;
	public:
		 CPage_Layout_Base(const HWND _host);
		~CPage_Layout_Base(void);

	public:
		 CPage_Layout_Base& operator= (const HWND);  // assigns a host handle;

	public:
		//
		// TODO: operator signs must be reviewed for better readability;
		//
		RECT        operator =(const UINT _ctl_id) const; // gets a rectangle of a control in host client area coordinates;
		HWND        operator<<(const UINT _ctl_id);       // sets an active control and returns its window handle;
		CAtlString  operator<=(const UINT _ctl_id) const; // gets control window text;
	public:
		operator    RECT (void) const; // returns host window client rectangle;
	};

	typedef CPage_Layout_Base Lay_;

}}}

#endif/*EBOBOOPAGLAYBAS_H_9110A96E_7D32_45DC_9283_F1DA0C0839CD_INCLUDED*/