/*
	Created by Tech_dog (VToropov) on 14-Jan-2016 at 11:50:01am, GMT+7, Phuket, Rawai, Thursday;
	This is File Watcher desktop UI common definitions implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 16-Aug-2018 at 7:02:23p, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 10:34:04p, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 10:56:11a, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "StdAfx.h"
#include "ebo.boo.wzd.pag.bas.h"

using namespace ebo::boo::gui;

#include "shared.gen.app.obj.h"

using namespace shared::user32;

#include "shared.uix.gdi.provider.h"
#include "shared.uix.gen.theme.h"
#include "shared.uix.gdi.object.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

CTabPageBase::CTabPageBase(const UINT RID, ITabPageEvents& _evt_snk, ITabSetCallback& _set_snk) :
	IDD(RID), m_evt_snk(_evt_snk), m_bInited(false), m_nIndex(-1), m_set_snk(_set_snk), m_b_vis_1st_time(true) {
}

CTabPageBase::~CTabPageBase(void) { }

/////////////////////////////////////////////////////////////////////////////

LRESULT   CTabPageBase::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	return 0;
}

LRESULT   CTabPageBase::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	RECT rcArea = {0};

	WTL::CTabCtrl tab_ctl = TBaseDialog::GetParent();

	tab_ctl.GetWindowRect(&rcArea);
	tab_ctl.ScreenToClient(&rcArea);
	tab_ctl.AdjustRect(false, &rcArea);
	TBaseDialog::SetWindowPos(
		0, &rcArea, SWP_NOZORDER|SWP_NOACTIVATE
	);
	if (CTheme::IsEnabled())
	{
		HRESULT hr_ = ::EnableThemeDialogTexture(*this, ETDT_ENABLETAB);
		ATLVERIFY(SUCCEEDED(hr_));
	}
	m_evt_snk.TabPage_OnEvent(uMsg, wParam, lParam, bHandled);
	return 0;
}

LRESULT   CTabPageBase::OnPagePaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	::WTL::CPaintDC dc(TBaseDialog::m_hWnd);
	return 0;
}

LRESULT   CTabPageBase::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	return 0;
}

LRESULT   CTabPageBase::OnShowHide (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	// this routine must not handle this event,
	// otherwise, main window does not receive changing size message;
	uMsg; wParam; lParam; bHandled = FALSE;
	if (NULL != lParam)
	{
		LPWINDOWPOS const pPos = reinterpret_cast<LPWINDOWPOS>(lParam);
		if (NULL != pPos && (SWP_SHOWWINDOW & (*pPos).flags)) {
			if (m_b_vis_1st_time) {
				m_b_vis_1st_time = false;
			}
		}
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

BOOL      CTabPageBase::TabPage_OnCommand(const WORD wCmdId) {
	wCmdId;
	return FALSE; // not handled
}

/////////////////////////////////////////////////////////////////////////////

INT       CTabPageBase::Index(void)   const { return m_nIndex; }
VOID      CTabPageBase::Index(const INT _v) { m_nIndex   = _v; }

/////////////////////////////////////////////////////////////////////////////

const
HFONT     CTabPageBase::SectionCapFont (void) {
	ex_ui::draw::CFont fnt_(
		_T("verdana"),
		eCreateFontOption::eRelativeSize,
		+1
	);
	return fnt_.Detach();
}

/////////////////////////////////////////////////////////////////////////////
#if (0)
namespace udd { namespace ui {

	VOID       AlignComboHeightTo(const CWindow& _host, const UINT _combo_id, const UINT _height)
	{
		CWindow ctrl_ = _host.GetDlgItem(_combo_id);
		if (!ctrl_)
			return;

		static const INT _COMBOBOX_EDIT_MARGIN = 3;

		const INT pixHeight = _height - _COMBOBOX_EDIT_MARGIN * 2;
		LRESULT res__ = ERROR_SUCCESS;
		res__ = ctrl_.SendMessage(CB_SETITEMHEIGHT, (WPARAM)-1, pixHeight); ATLASSERT(res__ != CB_ERR);
		res__ = ctrl_.SendMessage(CB_SETITEMHEIGHT, (WPARAM) 0, pixHeight); ATLASSERT(res__ != CB_ERR);
	}

	VOID       AlignCtrlsByHeight(const CWindow& _host, const UINT _ctrl_0, const UINT _ctrl_1, const SIZE& _shifts)
	{
		RECT rc_0 = {0};
		CWindow ctrl_;
		ctrl_ = _host.GetDlgItem(_ctrl_0);
		if (ctrl_)
		{
			ctrl_.GetWindowRect(&rc_0);
			::MapWindowPoints(HWND_DESKTOP, _host, (LPPOINT)&rc_0, 0x2);
		}
		ctrl_ = _host.GetDlgItem(_ctrl_1);
		if (ctrl_ && !::IsRectEmpty(&rc_0))
		{
			RECT rc_1 = {0};
			ctrl_.GetWindowRect(&rc_1);
			::MapWindowPoints(HWND_DESKTOP, _host, (LPPOINT)&rc_1, 0x2);
			rc_1.top    = rc_0.top    + _shifts.cx;
			rc_1.bottom = rc_0.bottom + _shifts.cy;
			ctrl_.SetWindowPos(
					NULL,
					&rc_1,
					SWP_NOZORDER|SWP_NOACTIVATE
				);
		}
	}
}}

/////////////////////////////////////////////////////////////////////////////

namespace udd { namespace ui
{
	//
	// it is necessary to keep this class implementation in namespace, which the class belongs to;
	// otherwise, class name coincidence appears with ATL window class;
	//
	CWindowEx::CWindowEx(const CWindow& _wnd) { *this = _wnd; }

/////////////////////////////////////////////////////////////////////////////

	CWindowEx& CWindowEx::operator=(const CWindow& _wnd) { TWindow::m_hWnd = _wnd; return *this; }

/////////////////////////////////////////////////////////////////////////////

	RECT      CWindowEx::GetDlgItemRect(const UINT _ctrl_id)const
	{
		RECT rc_ = {0};
		if (!*this)
			return rc_;

		CWindow ctrl_ = TWindow::GetDlgItem(_ctrl_id);
		if (!ctrl_)
			return rc_;

		ctrl_.GetWindowRect(&rc_);
		::MapWindowPoints(HWND_DESKTOP, *this, (LPPOINT)&rc_, 0x2);

		return rc_;
	}

}}

/////////////////////////////////////////////////////////////////////////////

CCtrlAutoState::CCtrlAutoState(const CWindow& _host, const UINT ctrlId, const bool bEnabled):
m_host(_host), m_ctrlId(ctrlId), m_bEnabled(bEnabled)
{
	CWindow ctrl_ = m_host.GetDlgItem(m_ctrlId);
	if (ctrl_)
	{
		m_bEnabled = !!ctrl_.IsWindowEnabled();
		if (m_bEnabled != bEnabled)
			ctrl_.EnableWindow(static_cast<BOOL>(bEnabled));
	}
}

CCtrlAutoState::~CCtrlAutoState(void)
{
	CWindow ctrl_ = m_host.GetDlgItem(m_ctrlId);
	if (ctrl_)
		ctrl_.EnableWindow(static_cast<BOOL>(m_bEnabled));
}
#endif