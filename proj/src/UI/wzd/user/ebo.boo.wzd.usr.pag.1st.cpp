/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Sep-2018 at 11:44:40a, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
	This is USB Drive Detective (bitsphereinc.com) main view options dialog page interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 11:55:31p, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 11:15:37a, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "StdAfx.h"
#include "ebo.boo.wzd.usr.pag.1st.h"
#include "ebo.boo.res.h"
#include "ebo.boo.res.wzd.usr.h"
#include "ebo.boo.wzd.pag.lay.h"
#include "ebo.boo.usr.prf.dlg.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

#include "ebo.boo.data.cache.h"
#include "ebo.boo.stg.qry.usr.h"

using namespace ebo::boo::data;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace details
{
	class CPageUser_1st_Ctl {
	public:
		enum _ctl : WORD {
			e_ban_ava = IDC_EBO_BOO_USR_PAG_1ST_AVA,    // page header avatar/logo image control;
			e_ban_cap = IDC_EBO_BOO_USR_PAG_1ST_CAP,    // page header caption label control;
			e_ban_seq = IDC_EBO_BOO_USR_PAG_1ST_SEQ,    // page banner hype image control;
			e_inf_img = IDC_EBO_BOO_USR_PAG_1ST_IMG,    // hype information sign;
			e_inf_txt = IDC_EBO_BOO_USR_PAG_1ST_INF,    // hype information text;
			e_lst_lin = IDC_EBO_BOO_USR_PAG_1ST_GRD,    // showing column grid lines;
			e_lst_usr = IDC_EBO_BOO_USR_PAG_1ST_LST,    // registered user list view;
			e_btn_new = IDC_EBO_BOO_USR_PAG_1ST_NEW,    // adding new URL;
			e_btn_edt = IDC_EBO_BOO_USR_PAG_1ST_EDT,    // editing existing URL;
			e_btn_del = IDC_EBO_BOO_USR_PAG_1ST_DEL,    // deleting existing URL;
		};
	};
	typedef CPageUser_1st_Ctl This_Ctl;

	class CPageUser_1st_Res {
	public:
		enum _res : WORD {
			e_ban_ava = IDR_EBO_BOO_USR_PAG_1ST_AVA,
			e_ban_seq = IDR_EBO_BOO_USR_PAG_1ST_SEQ,
			e_pan_inf = IDR_EBO_BOO_USR_PAG_1ST_INF,
			e_pan_exc = IDR_EBO_BOO_USR_PAG_1ST_WRN,
			e_ban_cap = IDS_EBO_BOO_USR_PAG_1ST_CAP,
			e_tip_inf = IDS_EBO_BOO_USR_PAG_1ST_INF,
			e_tip_wrn = IDS_EBO_BOO_USR_PAG_1ST_WRN,
			e_tip_adm = IDS_EBO_BOO_USR_PAG_1ST_ADM
		};
	};
	typedef CPageUser_1st_Res This_Res;

	class CPageUser_1st_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageUser_1st_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(const HIMAGELIST& _lst_img) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ban_ava, This_Res::e_ban_ava));
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ban_seq, This_Res::e_ban_seq));
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_inf_img, This_Res::e_pan_inf));

			CWindow ctl_cap = (Lay_(m_page_ref) << This_Ctl::e_ban_cap);
			if (0!= ctl_cap)
				ctl_cap.SetFont(CTabPageBase::SectionCapFont());

			CWindow ctl_lst = (Lay_(m_page_ref) << This_Ctl::e_lst_usr);
			if (0!= ctl_lst) {
				CListViewCtrl usr_lst = ctl_lst;
				usr_lst.SetImageList(_lst_img, LVSIL_SMALL|LVSIL_NORMAL);
			}
		}

		VOID   OnSize(void) {
			CPage_Ban pan_(This_Ctl::e_inf_img, This_Ctl::e_inf_txt);
			pan_.Image().Resource() = This_Res::e_pan_inf;
			pan_.Label().Resource() = This_Res::e_tip_inf;
			CPage_Layout(m_page_ref).AdjustBan(
				pan_
			);
			CWindow ctl_lin = Lay_(m_page_ref) << This_Ctl::e_lst_lin;
			RECT rc_ctl = (Lay_(m_page_ref) = This_Ctl::e_lst_lin);
			::OffsetRect(&rc_ctl, 0, (Lay_(m_page_ref) = This_Ctl::e_inf_img).top - rc_ctl.top - __H(rc_ctl) - CPage_Layout::u_md_gap);
			ctl_lin.MoveWindow(&rc_ctl);
			// gets listview control rectangle;
			rc_ctl = (Lay_(m_page_ref) = This_Ctl::e_lst_usr);
			const DWORD dw_top = rc_ctl.bottom + CPage_Layout::u_sm_gap * 2;
			// aligns button(s);
			const WORD w_ids[] = {
				This_Ctl::e_btn_new, This_Ctl::e_btn_edt, This_Ctl::e_btn_del
			};
			const UINT w_prn[] = {
				IDOK, IDYES, IDCANCEL
			};
			for (INT i_ = 0; i_ < _countof(w_ids) && i_ < _countof(w_prn); i_++) {
				CWindow  but_ctl =  Lay_(m_page_ref) << w_ids[i_];
				if (0 == but_ctl)
					continue;
				RECT rc_btn   = (Lay_(m_page_ref) = This_Ctl::e_btn_new); const SIZE sz_btn = {__W(rc_btn), __H(rc_btn)};
				if (SUCCEEDED(m_page_snk.TabSet_OnDataRequest(w_prn[i_], rc_btn))) {
					::MapWindowPoints(HWND_DESKTOP, m_page_ref, (LPPOINT)&rc_btn, 0x2);
					rc_btn.top    = dw_top;
					rc_btn.bottom = rc_btn.top + sz_btn.cy;
				}
				but_ctl.MoveWindow(&rc_btn);
			}
		}

		void  OnUpdate (const CUser_Enum& _users) {
			CButton btn_del = (Lay_(m_page_ref) << This_Ctl::e_btn_del);
			CButton btn_edt = (Lay_(m_page_ref) << This_Ctl::e_btn_edt);
			if (0!= btn_del) btn_del.EnableWindow(_users.Ref().empty() == false);
			if (0!= btn_edt) btn_edt.EnableWindow(_users.Ref().empty() == false);
		}
	};

	class CPageUser_1st_Init
	{
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPageUser_1st_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (CUser_Enum& _users)  {
			m_error << __MODULE__ << S_OK;
			CWindow ban_cap = Lay_(m_page_ref) << This_Ctl::e_ban_cap;
			if (ban_cap) {
				CAtlString cs_cap; cs_cap.LoadString(IDS_EBO_BOO_USR_PAG_1ST_CAP);
				ban_cap.SetWindowText(
					(LPCTSTR) cs_cap
				);
			}
			WTL::CButton use_grid = m_page_ref.GetDlgItem(This_Ctl::e_lst_lin);
			if (NULL  != use_grid) {
				const bool b_use_grid = false;
				use_grid.SetCheck(static_cast<INT>(b_use_grid));
				use_grid.EnableWindow(FALSE);
			}
			CListViewCtrl usr_lst = m_page_ref.GetDlgItem(This_Ctl::e_lst_usr);
			if (usr_lst) {
				usr_lst.SetExtendedListViewStyle(LVS_EX_FULLROWSELECT|LVS_EX_DOUBLEBUFFER, 0);
				usr_lst.AddColumn(_T("")      , 0); usr_lst.SetColumnWidth(    0,  35);
				usr_lst.AddColumn(_T("User" ) , 1); usr_lst.SetColumnWidth(    1, 218);
				usr_lst.AddColumn(_T("Role" ) , 2); usr_lst.SetColumnWidth(    2, 116);
				usr_lst.AddColumn(_T("State") , 3); usr_lst.SetColumnWidth(    3, 100);

				this->UpdateData(_users);

				CAtlString cs_name;
				usr_lst.GetItemText(0, 1, cs_name);
				CData_Cache().User().Ref().Name((LPCTSTR)cs_name);
				CData_Cache().User().Action() = TActionType::e_edit;

				this->UpdateTips(_users);
			}
			return m_error;
		}

		HRESULT   UpdateData(CUser_Enum& _users) {
			m_error << __MODULE__ << S_OK;

			CQuery_Users  qry_;
			HRESULT hr_ = qry_.Get(_users);
			if (FAILED(hr_)) {
				return (m_error = qry_.Error());
			}
			
			CListViewCtrl usr_lst = m_page_ref.GetDlgItem(This_Ctl::e_lst_usr);
			if ( NULL  == usr_lst ) {
				return (m_error = OLE_E_INVALIDHWND);
			}
			usr_lst.DeleteAllItems();

			INT   n_iter_ = 0;
			const TUsers& recs_ = _users.Ref();
			for ( TUsers::const_iterator it_ = recs_.begin(); it_ != recs_.end(); ++it_) {
				const CUser& user_ = *it_;

				usr_lst.AddItem(n_iter_, 0, (LPCTSTR)_T(""), 0);
				usr_lst.AddItem(n_iter_, 1, (LPCTSTR)user_.Name ());
				usr_lst.AddItem(n_iter_, 2, (LPCTSTR)user_.Role ().Name());
				usr_lst.AddItem(n_iter_, 3, (LPCTSTR)user_.State().Name());

				const bool b_checked = user_.State().Is();
				n_iter_ += 1;
			}
			usr_lst.EnableWindow(recs_.size() > 0);
			if (recs_.empty()) {
				usr_lst.AddItem(n_iter_, 0, (LPCTSTR)_T(""));
				usr_lst.AddItem(n_iter_, 1, (LPCTSTR)_T("No users; Add new one;"));
				usr_lst.AddItem(n_iter_, 2, (LPCTSTR)_T(""));
				usr_lst.AddItem(n_iter_, 2, (LPCTSTR)_T(""));
			}
			else
				usr_lst.SelectItem(0);

			return m_error;
		}

		VOID      UpdateTips(const CUser_Enum& _users) {
			const bool b_wrn = (_users.Ref().empty());
			const WORD w_res = (b_wrn ? This_Res::e_tip_adm : This_Res::e_tip_inf);
			CAtlString cs_tip; cs_tip.LoadStringW(w_res);
			CWindow lab_ctl  = (Lay_(m_page_ref) << This_Ctl::e_inf_txt);
			if (0!= lab_ctl) {
				lab_ctl.SetWindowText((LPCTSTR)cs_tip);
			}
			CPage_Layout(m_page_ref).AdjustImg(CPage_Ava(This_Ctl::e_inf_img, (b_wrn ? This_Res::e_pan_exc : This_Res::e_pan_inf)));
		}

	private:
		bool   UseRowDividers(void) const {
			bool b_use_def = true;
			WTL::CButton use_def = m_page_ref.GetDlgItem(This_Ctl::e_lst_lin);
			if (NULL !=  use_def)
				b_use_def = !!use_def.GetCheck();
			return b_use_def;
		}
	};

	class CPageUser_1st_Handler
	{
	private:
		CWindow& m_page_ref;
		CUser_Enum& m_users;

	public:
		CPageUser_1st_Handler(CWindow& dlg_ref, CUser_Enum& _users) : m_page_ref(dlg_ref), m_users(_users) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL bHandled = TRUE;
			// list view update changes the data cache object via notification event;
			// TODO: this unintended change must be fixed;
			CUser target_;

			if (false){}
			else if (This_Ctl::e_btn_new == ctrlId) {

				CData_Cache().User().Action() = TActionType::e_new;

				CProfileDlg prf_dlg;
				HRESULT hr_ = prf_dlg.DoModal();
				if (S_OK == hr_) {
					target_ = TDataCache().User().Ref();
					CPageUser_1st_Init init_(m_page_ref);
					init_.UpdateData(m_users);
				}
			}
			else if (This_Ctl::e_btn_edt == ctrlId) {
				CData_Cache().User().Action() = TActionType::e_edit;

				CProfileDlg prf_dlg;
				HRESULT hr_ = prf_dlg.DoModal();
				if (S_OK == hr_) {
					target_ = TDataCache().User().Ref();
					CPageUser_1st_Init init_(m_page_ref);
					init_.UpdateData(m_users);
				}
			}
			else if (This_Ctl::e_btn_del == ctrlId) {
				CData_Cache().User().Action() = TActionType::e_delete;
				target_   = TDataCache().User().Ref();
				INT n_ndx = m_users.Find(target_); n_ndx -= 1;
				if (n_ndx < 0) n_ndx = 0;
				static LPCTSTR lp_sz_pat = _T("Do you really want to delete the user: %s?");
				CAtlString cs_msg; cs_msg.Format(
					lp_sz_pat, TDataCache().User().Ref().Name()
				);
				const INT n_res = AtlMessageBox(
					::GetActiveWindow(), (LPCTSTR)cs_msg, _T("Deleting User"), MB_ICONEXCLAMATION|MB_YESNO
				);
				if (IDYES == n_res) {
					CQuery_Users qry_;
					HRESULT hr_ = qry_.Rem(target_);
					if (SUCCEEDED(hr_)) {
						CPageUser_1st_Init init_(m_page_ref);
						init_.UpdateData(m_users);
						if (n_ndx < m_users.Ref().size())
						  target_ = m_users.Ref()[n_ndx];
					}
					else {
						qry_.Error().Show();
						return (bHandled = FALSE);
					}
				}
			}
			else
				bHandled = FALSE;

			if (bHandled == TRUE) {
				TDataCache().User().Ref() = target_;
				CListViewCtrl usr_lst = Lay_(m_page_ref) << This_Ctl::e_lst_usr;
				if ( NULL  != usr_lst ) {
					const INT n_ndx = m_users.Find(target_);
					if (n_ndx >= 0 && n_ndx < usr_lst.GetItemCount())
						usr_lst.SelectItem(n_ndx);
					usr_lst.SetFocus();
				}
			}

			return bHandled;
		}
		BOOL   OnNotify  (WPARAM _w_p, LPARAM _l_p) {
			_w_p; _l_p;
			const LPNMHDR p_header = reinterpret_cast<LPNMHDR>(_l_p);
			if (NULL == p_header)
				return FALSE;
			// TODO: someimes this case must be avoided;
			if (LVN_ITEMCHANGED == p_header->code) {

				LPNMLISTVIEW p_lvw = reinterpret_cast<LPNMLISTVIEW>(_l_p);
				if ( NULL == p_lvw )
					return FALSE;
				if (p_lvw->uNewState & LVIS_SELECTED) {
					CWindow usr_lst = Lay_(m_page_ref) << This_Ctl::e_lst_usr;
					if (0!= usr_lst) {
						INT n_pos = ListView_GetNextItem(usr_lst, -1, LVNI_SELECTED);
						if (n_pos>= 0 && n_pos < static_cast<INT>(m_users.Ref().size())) {
							const CUser& usr_ = m_users.Ref()[n_pos];
							CData_Cache().User().Ref() = usr_;
							CData_Cache().User().Action() = TActionType::e_edit;
						}
					}
				}
			}
			return TRUE;
		}
	};
}}}}

using namespace ebo::boo::gui::details;
/////////////////////////////////////////////////////////////////////////////

CPageUser_1st:: CPageUser_1st(ITabSetCallback& _set_snk):
	TBasePage(IDD_EBO_BOO_USR_MAN_1ST_PAG, *this, _set_snk), m_lvl_imgs(NULL)
{
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();

	HBITMAP h_bmp_ = NULL;
	HRESULT hr_ = CGdiPlusPngLoader::LoadResource(IDR_EBO_BOO_USR_PAG_1ST_LST, hInstance, h_bmp_);
	if (SUCCEEDED(hr_)) {
		const SIZE sz_frm = {
			16, 16 // predefined in source PSD file;
		};
		hr_ = CGdiPlusPngLoader::CreateImages(
			h_bmp_, m_lvl_imgs, sz_frm
		);
		::DeleteObject(h_bmp_); h_bmp_ = NULL;
	}
}

CPageUser_1st::~CPageUser_1st(void) {
	if (m_lvl_imgs) {
		::ImageList_Destroy(m_lvl_imgs); m_lvl_imgs = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageUser_1st::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
#if(0)
	details::CTabPageView_Init init_(*this, m_usr_list);
	details::CTabPageView_Handler hand_(*this, m_usr_list);
#endif
	return l_res;
}

LRESULT    CPageUser_1st::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	details::CPageUser_1st_Init init_(*this);
	init_.OnCreate(m_usr_enum);

	details::CPageUser_1st_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate(m_lvl_imgs);
	layout_.OnUpdate(m_usr_enum);
	
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageUser_1st::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	details::CPageUser_1st_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

LRESULT    CPageUser_1st::OnShowHide (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	if (TBasePage::m_b_vis_1st_time) {
		CWindow  lst_users = Lay_(*this) << This_Ctl::e_lst_usr;
		if (0 != lst_users)
			lst_users.SetFocus(); bHandled = FALSE;
		return 0;
	}
	return TBasePage::OnShowHide(uMsg, wParam, lParam, bHandled);
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageUser_1st::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPageUser_1st_Handler handler_(*this, m_usr_enum);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE)
			{}
		} break;
	case WM_NOTIFY : {
			const LPNMHDR p_header = reinterpret_cast<LPNMHDR>(lParam);
			if (NULL == p_header)
				return FALSE;

			if (TBasePage::m_bInited && LVN_ITEMCHANGED == p_header->code) {
				CPageUser_1st_Handler hand_(*this, m_usr_enum);
				bHandled = hand_.OnNotify(wParam, lParam);
			}

		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageUser_1st::IsChanged   (void) const {
#if(0)
	CWindow lvw_wnd(Lay_(*this) << This_Ctl::e_col_vis);
	CListViewColEnum cols_ex(lvw_wnd);
#endif
	return false;
}

CAtlString CPageUser_1st::GetPageTitle(void) const {
	static CAtlString cs_title(_T(" User Accounts ")); return cs_title;
}
VOID       CPageUser_1st::UpdateData  (const DWORD _opt) {
	_opt;
#if(0)
	details::CTabPageView_Handler hand(*this);
	hand.UpdateVis(m_lv_cls);
	CWindow lvw_wnd(Lay_(*this) << This_Ctl::e_col_vis);
	CListViewColEnum cols_ex(lvw_wnd);

	cols_ex = m_lv_cls;
	cols_ex.Update();
#endif
}
VOID       CPageUser_1st::UpdateLayout(void) {
	CWindow  lst_users = Lay_(*this) << This_Ctl::e_lst_usr;
	if (0 != lst_users)
		lst_users.SetFocus();
}