#ifndef _EBOBOOUSRMANWZD_H_C263FBA4_4D8A_40AE_B492_0DB4FBD4BAFE_INCLUDED
#define _EBOBOOUSRMANWZD_H_C263FBA4_4D8A_40AE_B492_0DB4FBD4BAFE_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Nov-2018 at 5:06:27p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is USB Drive Detective (www.bitsphereinc.com) desktop app data wizard main dialog interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 7-Sep-2019 at 7:57:54p, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 12:46:48p, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "shared.uix.frms.img.ban.h"
#include "ebo.boo.wzd.usr.tab.set.h"

namespace ebo { namespace boo { namespace gui {

	using ex_ui::frames::CImageBanner;

	using ex_ui::draw::defs::IRenderer;

	class CUserManWzd {
	private:
		class CUserManWzdImpl : public ::ATL::CDialogImpl<CUserManWzdImpl> , ITabSetCallback, IRenderer {
		                       typedef ::ATL::CDialogImpl<CUserManWzdImpl>   TDialog;
		friend class CUserManWzd;
		private:
			CImageBanner m_banner;
			CTabSet      m_tabset;

		public:
			UINT   IDD;

		public:
			BEGIN_MSG_MAP(CUserManWzdImpl)
				MESSAGE_HANDLER     (WM_COMMAND    ,  OnCommand  )
				MESSAGE_HANDLER     (WM_DESTROY    ,  OnDestroy  )
				MESSAGE_HANDLER     (WM_INITDIALOG ,  OnInitDlg  )
				MESSAGE_HANDLER     (WM_SYSCOMMAND ,  OnSysCmd   )
				NOTIFY_CODE_HANDLER (TCN_SELCHANGE ,  OnTabNotify)
			END_MSG_MAP()
		public:
			 CUserManWzdImpl(void);
			~CUserManWzdImpl(void);

		private:
			LRESULT OnCommand(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnDestroy(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnInitDlg(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnSysCmd (UINT , WPARAM , LPARAM , BOOL&);
		private:
			LRESULT OnTabNotify (INT  , LPNMHDR, BOOL&);
		private: // ITabSetCallback
			virtual HRESULT TabSet_OnDataChanged (const UINT pageId, const bool bChanged) override sealed;
			virtual HRESULT TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) override sealed;
		private: // IRenderer
			HRESULT DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const override sealed;
		};
	private:
		CUserManWzdImpl   m_dlg;
	public:
		 CUserManWzd(void);
		~CUserManWzd(void);

	public:
		HRESULT     DoModal(void);
	};
}}}

#endif/*_EBOBOOUSRMANWZD_H_C263FBA4_4D8A_40AE_B492_0DB4FBD4BAFE_INCLUDED*/