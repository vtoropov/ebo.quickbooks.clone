/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Nov-2018 at 5:11:27p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is USB Drive Detective (bitsphereinc.com) desktop app data wizard main dialog interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 7-Sep-2019 at 8:08:04p, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 12:46:50p, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "StdAfx.h"
#include "ebo.boo.usr.man.wzd.h"
#include "ebo.boo.res.h"
#include "ebo.boo.res.wzd.usr.h"

using namespace ebo::boo::gui;

#include "shared.gen.app.res.h"
#include "shared.uix.gdi.provider.h"

using namespace shared::user32;
using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl {

	class CUserManWzd_Initer {
	private:
		CWindow&   m_dlg_ref;
	public:
		CUserManWzd_Initer(CWindow& dlg_ref) : m_dlg_ref(dlg_ref) {}

	public:
		VOID   OnCreateEvent(void) {
			const UINT w_ids[] = {
				IDOK, IDYES, IDCANCEL
			};
			for (INT i_ = 0; i_ < _countof(w_ids); i_ ++) {
				CButton  btn_ctl = m_dlg_ref.GetDlgItem(w_ids[i_]);
				if (0 == btn_ctl)
					continue;
				::SendMessage(btn_ctl, WM_CHANGEUISTATE, MAKELONG(UIS_SET, UISF_HIDEFOCUS|UISF_HIDEACCEL), 0);
				btn_ctl.EnableWindow(IDCANCEL == w_ids[i_]);
			}
		}
	};

	class CUserManWzd_Layout {
	private:
		enum _e {
			e_gap = 0x4, // it is very hight for separator control: it gets empty space between top and bottom edges;
			e_sep = 0x2,
		};

	private:
		CWindow&   m_dlg_ref;
		RECT       m_dlg_rec;

	public:
		CUserManWzd_Layout(CWindow& dlg_ref) : m_dlg_ref(dlg_ref) {

			if (m_dlg_ref.GetClientRect(&m_dlg_rec) == FALSE)
				::SetRectEmpty(&m_dlg_rec);
		}

	public:
		// 
		// it does not work in intermediate cases when selected page is already has controls' state been changed;
		//
		VOID     AdjustButts(const INT _n_step) {
			CButton prev_ = m_dlg_ref.GetDlgItem(IDNO); if (prev_) prev_.EnableWindow(_n_step > 0);
			CButton next_ = m_dlg_ref.GetDlgItem(IDOK); if (next_) next_.EnableWindow(_n_step < 1);
		}
		VOID     AdjustCtrls(VOID) {
			CWindow div_hz = m_dlg_ref.GetDlgItem(IDC_EBO_BOO_USR_MAN_WZD_SEP);
			if (div_hz) {
				const RECT rc_hz = this->GetHozArea();
				div_hz.MoveWindow(&rc_hz);
			}
		}

		RECT     GetBanArea (VOID) {

			RECT rc_ban  = {0};
			CWindow ban_ = m_dlg_ref.GetDlgItem(IDC_EBO_BOO_USR_MAN_WZD_BAN);
			if (NULL == ban_)
				return rc_ban;

			if (FALSE == ban_.GetWindowRect(&rc_ban))
				return rc_ban;

			::MapWindowPoints(HWND_DESKTOP, m_dlg_ref, (LPPOINT)&rc_ban, 0x2);

			return rc_ban;
		}

		RECT     GetHozArea (VOID) {
			const
			RECT rc_ban = this->GetBanArea();
			RECT rc_hoz = {0};

			CWindow div_hz = m_dlg_ref.GetDlgItem(IDC_EBO_BOO_USR_MAN_WZD_SEP);
			if (NULL != div_hz) {
				rc_hoz         = m_dlg_rec;
				rc_hoz.top     = rc_ban.bottom + 1;
				rc_hoz.bottom  = rc_ban.bottom + 1 + _e::e_sep;
			}
			return rc_hoz;
		}

		RECT     GetTabArea (VOID) {
			RECT rc_tab   = m_dlg_rec;
			rc_tab.left   = this->GetBanArea().right;
			rc_tab.bottom = this->GetHozArea().top  ;

			return rc_tab;
		}
	};

}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CUserManWzd::CUserManWzdImpl:: CUserManWzdImpl(void) :
	IDD     (IDD_EBO_BOO_USR_MAN_WZD    ),
	m_banner(IDR_EBO_BOO_USR_MAN_WZD_BAN), m_tabset(*this){ m_banner.Renderer(this); }
CUserManWzd::CUserManWzdImpl::~CUserManWzdImpl(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CUserManWzd::CUserManWzdImpl::OnCommand(UINT, WPARAM wParam, LPARAM, BOOL& bHandled) {
	wParam; bHandled = FALSE;
	switch (wParam) {
	case IDCANCEL: {
			TDialog::EndDialog(static_cast<INT>(wParam));
		} break;
	}
	return 0;
}

LRESULT   CUserManWzd::CUserManWzdImpl::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	m_tabset.Destroy();
	m_banner.Destroy();
	return 0;
}

LRESULT   CUserManWzd::CUserManWzdImpl::OnInitDlg(UINT , WPARAM , LPARAM , BOOL&) {

	CUserManWzd_Layout layout_(*this);
	HRESULT hr_ = m_banner.Create(TDialog::m_hWnd, IDC_EBO_BOO_USR_MAN_WZD_BAN);
	if (FAILED(hr_)) {}
	else {
		layout_.AdjustCtrls();
	}
	CUserManWzd_Initer
	initer_(*this);
	initer_.OnCreateEvent();

	const RECT rc_tabs = layout_.GetTabArea();
	hr_ = m_tabset.Create(*this, rc_tabs);

	TDialog::CenterWindow();
	{
		CApplicationIconLoader loader_(IDR_EBO_BOO_USR_MAN_FRM_ICO);
		TDialog::SetIcon(loader_.DetachLargeIcon(), TRUE );
		TDialog::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	return 0;
}

LRESULT   CUserManWzd::CUserManWzdImpl::OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam) {
		case SC_CLOSE: {
			TDialog::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CUserManWzd::CUserManWzdImpl::OnTabNotify (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	idCtrl; pnmh; bHandled = TRUE;
	m_tabset.UpdateLayout();
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CUserManWzd::CUserManWzdImpl::TabSet_OnDataChanged (const UINT pageId, const bool bChanged ) {
	pageId; bChanged;
	HRESULT hr_ = S_OK;

	CWindow bt_app = TDialog::GetDlgItem(IDOK);
	if (NULL != bt_app) {
	}

	return  hr_;
}

HRESULT   CUserManWzd::CUserManWzdImpl::TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) {
	ctrlId; _rc_ctrl;
	HRESULT hr_ = S_OK;
	switch (ctrlId) {
	case IDYES   : case IDOK:
	case IDCANCEL:
		{
			CWindow ctl_btn = TDialog::GetDlgItem(ctrlId);
			if (NULL != ctl_btn) {
				ctl_btn.GetWindowRect(&_rc_ctrl);
			}
		} break;
	default:
		hr_ = DISP_E_PARAMNOTFOUND;
	}
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CUserManWzd::CUserManWzdImpl::DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& _rc_area) const {
	hChild;
	if (NULL == hSurface)
		return __DwordToHresult(ERROR_INVALID_HANDLE);
	if (::IsRectEmpty(&_rc_area))
		return OLE_E_INVALIDRECT;
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getsyscolor
	CZBuffer dc_(hSurface, _rc_area);
	dc_.FillSolidRect(
		&_rc_area, ::GetSysColor(COLOR_3DFACE)
		);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CUserManWzd:: CUserManWzd(void) {}
CUserManWzd::~CUserManWzd(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT CUserManWzd::DoModal(void) {

	HRESULT hr_ = S_OK;

	INT_PTR i_ptr = m_dlg.DoModal();
	switch (i_ptr) {
	case IDOK: {
		} break;
	default:
		hr_ = S_FALSE;
	}
	return  hr_;
}