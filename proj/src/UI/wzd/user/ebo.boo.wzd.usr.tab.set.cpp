/*
	Created by Tech_dog (VToropov) on 16-Jan-2016 at 3:58:22pm, GMT+7, Phuket, Rawai, Saturday;
	This is USB Drive Detective (bitsphereinc.com) app database option tab set interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 17-Aug-2018 at 6:57:00p, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 10:57:52p, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 12:20:12p, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "StdAfx.h"
#include "ebo.boo.wzd.usr.tab.set.h"
#include "ebo.boo.res.h"
#include "ebo.boo.res.wzd.usr.h"

using namespace ebo::boo::gui;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CTabSet_Layout
	{
	private:
		RECT    m_area;
	public:
		CTabSet_Layout(const RECT& rcArea) : m_area(rcArea) {}

	public:
		RECT    GetTabsArea(void)const {
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};
}}}}

using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CTabSet:: CTabSet(ITabSetCallback& _snk) : m_e_active(_pages::e_users), m_users(_snk), m_auth(_snk) { _snk; }
CTabSet::~CTabSet(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CTabSet::Create(const HWND hParent, const RECT& rcArea) {
	hParent; rcArea;
	HRESULT hr_ = S_OK;

	if(FALSE == ::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return (hr_ = E_INVALIDARG);

	CTabSet_Layout layout(rcArea);
	RECT rcTabs  = layout.GetTabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs ,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_EBO_BOO_USR_MAN_WZD_TAB
		);

	const SIZE szPadding = {
		5, 2
	};

	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());
	m_cTabCtrl.SetPadding(szPadding);
	m_cTabCtrl.SetItemSize(128,  24);

	INT nIndex = 0;
	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW; // auto-placement is made by default implementation of create/initialize page handler;

	m_cTabCtrl.AddItem(m_users.GetPageTitle());
	{
		m_users.Create(m_cTabCtrl.m_hWnd);
		m_users.SetWindowPos(
				HWND_TOP,
				0, 0, 0 , 0,
				dwFlags
			);
		if (m_users.IsWindow()) m_users.Index(nIndex++);
	}
	m_cTabCtrl.AddItem(m_auth.GetPageTitle());
	{
		m_auth.Create(m_cTabCtrl.m_hWnd);
		m_auth.SetWindowPos(
			HWND_TOP,
			0, 0, 0 , 0,
			dwFlags
		);
		if (m_auth.IsWindow()) m_auth.Index(nIndex++);
	}
	m_cTabCtrl.SetCurSel(m_e_active);

	this->UpdateLayout();

	return S_OK;
}

HRESULT       CTabSet::Destroy(void)
{
	if (m_auth .IsWindow()) { m_auth .DestroyWindow(); m_auth .m_hWnd = NULL; }
	if (m_users.IsWindow()) { m_users.DestroyWindow(); m_users.m_hWnd = NULL; }
	return S_OK;
}

CTabSet::_pages
              CTabSet::Selected(void) const      { return m_e_active; }
HRESULT       CTabSet::Selected(const _pages _v) {
	HRESULT hr_ = S_OK;
	const bool b_changed = (_v != m_e_active);
	m_e_active  = _v;
	if (b_changed && m_cTabCtrl.IsWindow()) {
		m_cTabCtrl.SetCurSel(m_e_active);
		this->UpdateLayout();
	}
	return  hr_;
}

void          CTabSet::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();
	
	if (m_auth .IsWindow()) {
		m_auth .ShowWindow(nTabIndex == m_auth .Index() ? SW_SHOW : SW_HIDE);
	if (m_auth .Index() == nTabIndex)
		m_auth .UpdateData  ();
		m_auth .UpdateLayout();
	}
	if (m_users.IsWindow()) {
		m_users.ShowWindow(nTabIndex == m_users.Index() ? SW_SHOW : SW_HIDE);
	if (m_users.Index() == nTabIndex)
		m_users.UpdateLayout();
	}
}