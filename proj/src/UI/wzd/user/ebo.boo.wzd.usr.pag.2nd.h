#ifndef _EBOBOOWZDUSRPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOBOOWZDUSRPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Sep-2018 at 11:38:45a, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
	This is USB Drive Detective (bitsphereinc.com) main view options dialog page interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 11:37:57p, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to Ebo Pack personal account app on 19-Oct-2019 at 10:35:46p, UTC+7, Novosibirsk, Light Coloured, Saturday;
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "shared.uix.ctrl.lvw.h"
#include "ebo.boo.stg.dat.acc.h"
namespace ebo { namespace boo { namespace gui {

	using ebo::boo::data::access::CArea_Enum;
	using ebo::boo::data::access::TAccAreas ;
	using ebo::boo::data::access::CArea     ;

	class CPageUser_2nd : public CTabPageBase, public  ITabPageEvents {
	                     typedef CTabPageBase  TBasePage;
	private:
		HIMAGELIST          m_tvw_imgs;
		CArea_Enum          m_acc_enum;

	public:
		 CPageUser_2nd(ITabSetCallback&);
		~CPageUser_2nd(void);

	private:
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnShowHide (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public:  // TBasePage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed; // temporarily disabled due to possible invocation after control(s) destroying;
		virtual VOID        UpdateLayout(void) ;
	};

}}}

#endif/*_EBOBOOWZDUSRPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/