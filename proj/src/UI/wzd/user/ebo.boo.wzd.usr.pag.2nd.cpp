/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Sep-2018 at 11:44:40a, UTC+7, Novosibirsk, Rodniki, Tulenina, Sunday;
	This is USB Drive Detective (bitsphereinc.com) main view options dialog page interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 11:55:31p, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to Ebo Pack personal account app on 20-Oct-2019 at 6:43:32a, UTC+7, Novosibirsk, Light Coloured, Sunday;
*/
#include "StdAfx.h"
#include "ebo.boo.wzd.usr.pag.2nd.h"
#include "ebo.boo.res.h"
#include "ebo.boo.res.wzd.usr.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;
using namespace ex_ui::controls::std;

#include "ebo.boo.stg.qry.acc.h"
#include "ebo.boo.data.cache.h"

using namespace ebo::boo::data;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageUser_2nd_Ctl {
	public:
		enum _ctl : WORD {
			e_ban_ava = IDC_EBO_BOO_USR_PAG_2ND_AVA,    // page header avatar/logo image control;
			e_ban_cap = IDC_EBO_BOO_USR_PAG_2ND_CAP,    // page header caption label control;
			e_ban_des = IDC_EBO_BOO_USR_PAG_2ND_DES,    // page banner of selected description area;
			e_inf_img = IDC_EBO_BOO_USR_PAG_2ND_IMG,    // hype information sign;
			e_inf_txt = IDC_EBO_BOO_USR_PAG_2ND_INF,    // hype information text;
		//	e_lst_lin = IDC_EBO_BOO_USR_PAG_1ST_GRD,    // showing column grid lines;
			e_tvw_are = IDC_EBO_BOO_USR_PAG_2ND_TVW,    // area tree view;
		//	e_btn_new = IDC_EBO_BOO_USR_PAG_1ST_NEW,    // adding new URL;
		//	e_btn_edt = IDC_EBO_BOO_USR_PAG_1ST_EDT,    // editing existing URL;
		//	e_btn_del = IDC_EBO_BOO_USR_PAG_1ST_DEL,    // deleting existing URL;
		};
	};
	typedef CPageUser_2nd_Ctl This_Ctl;

	class CPageUser_2nd_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageUser_2nd_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ban_ava, IDR_EBO_BOO_USR_PAG_2ND_CAP) );
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_inf_img, IDR_EBO_BOO_USR_PAG_2ND_IMG) );

			CWindow ctl_cap = (Lay_(m_page_ref) << This_Ctl::e_ban_cap);
			if (0!= ctl_cap)
				ctl_cap.SetFont(CTabPageBase::SectionCapFont());
		}

		VOID   OnSize(void) {
			CPage_Ban pan_(This_Ctl::e_inf_img, This_Ctl::e_inf_txt);
			pan_.Image().Resource() = IDR_EBO_BOO_USR_PAG_2ND_IMG;
			pan_.Label().Resource() = IDS_EBO_BOO_USR_PAG_2ND_INF;
#if (0)
			CPage_Layout(m_page_ref).AdjustBan(
				pan_
			);
#endif
			// correct more precise positioning;
			RECT rc_tvw =  (Lay_(m_page_ref) = This_Ctl::e_tvw_are);
			CWindow info_img = (Lay_(m_page_ref) << This_Ctl::e_inf_img);
			CWindow info_txt = (Lay_(m_page_ref) << This_Ctl::e_inf_txt);

			if (info_img) {
				RECT rc_img = (Lay_(m_page_ref) = This_Ctl::e_inf_img);
				SIZE sz_img = {
					__W(rc_img), __H(rc_img)
				};
				::SetRect(&rc_img, rc_tvw.left, rc_tvw.bottom, rc_tvw.left + sz_img.cx, rc_tvw.bottom + sz_img.cy);
				info_img.MoveWindow(&rc_img);
			}

			if (info_txt) {
				RECT rc_img = (Lay_(m_page_ref) = This_Ctl::e_inf_img);
				RECT rc_txt = (Lay_(m_page_ref) = This_Ctl::e_inf_txt);
				SIZE sz_img = {
					__W(rc_img), __H(rc_img)
				};
				SIZE sz_txt = {
					__W(rc_txt), __H(rc_txt)
				};
				const LONG l_top = rc_img.top + (sz_img.cy - sz_txt.cy) / 2;
				::SetRect(&rc_txt, rc_img.right, l_top, rc_img.right + sz_txt.cx, l_top + sz_txt.cy);
				info_txt.MoveWindow(&rc_txt);
			}
		}
	};

	class CPageUser_2nd_Init
	{
	private:
		CError     m_error   ;
		CWindow&   m_page_ref;
		
	public:
		CPageUser_2nd_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) { m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (const HIMAGELIST _tvw, CArea_Enum& _areas)  {
			CWindow ban_cap = Lay_(m_page_ref) << This_Ctl::e_ban_cap;
			if (ban_cap) {
				CAtlString cs_cap; cs_cap.LoadString(IDS_EBO_BOO_USR_PAG_2ND_CAP);
				ban_cap.SetWindowText(
					(LPCTSTR) cs_cap
				);
			}

			HRESULT hr_ = this->UpdateBanDesc();
			CTreeViewCtrl areas_ = m_page_ref.GetDlgItem(This_Ctl::e_tvw_are);
			if ( NULL ==  areas_ ) {
				return (hr_ = OLE_E_INVALIDHWND);
			}
			areas_.SetImageList(_tvw, TVSIL_NORMAL);
			areas_.SetExtendedStyle(TVS_EX_DOUBLEBUFFER, 0);
			::SetWindowTheme(areas_, _T("Explorer"), NULL);

			HTREEITEM h_root = areas_.InsertItem(_T("Access Areas"), 8, 8, TVI_ROOT, TVI_ROOT);
			if (NULL == h_root)
				return (hr_ = __LastErrToHresult());
			else
				areas_.SetItemData(h_root, 0);

			CQuery_Areas are_qry;
			hr_ = are_qry.Get(_areas);
			if (FAILED(hr_)){
				m_error = are_qry.Error(); m_error.Show();
				return hr_;
			}
			const TAccAreas& ref_ = _areas.Ref();
			for (size_t i_ = 0; i_ < ref_.size(); i_++) {
				const CArea& area_ = ref_[ i_ ];
				HTREEITEM h_node = areas_.InsertItem(
					area_.Name(), _get_img_ndx_from_id(area_.Id()), _get_img_ndx_from_id(area_.Id()), h_root, h_root
				);
				areas_.SetItemData(h_node, i_);
			}
			areas_.Expand(TVI_ROOT, TVE_EXPAND);
			return hr_;
		}

		HRESULT   UpdateBanDesc(void) {
			m_error << __MODULE__ << S_OK;
			CWindow ban_des = Lay_(m_page_ref) << This_Ctl::e_ban_des;
			if (ban_des) {
				LPCTSTR lp_sz_pat  = _T("by User: %s"); 
				CAtlString cs_user; cs_user.Format(
					lp_sz_pat, CData_Cache().User().Ref().Name()
				);
				ban_des.SetWindowText((LPCTSTR)cs_user);
			}
			return m_error;
		}

	private:
		INT    _get_img_ndx_from_id(INT _n_id) {
			INT img_ndx = 0;
			switch  (_n_id) {
			case 0: {} break; // No area
			case 1: { img_ndx =  6; } break; // Sales and Accounts Receivable
			case 2: { img_ndx =  6; } break; // Purchases and Accounts Payable
			case 3: { img_ndx = 11; } break; // Checking and Credit Cards
			case 4: { img_ndx =  3; } break; // Inventory
			case 5: { img_ndx =  4; } break; // Time Tracking
			case 6: { img_ndx =  2; } break; // Payroll and Employees
			case 7: { img_ndx =  9; } break; // Sensitive Accounting Activities
			case 8: { img_ndx =  1; } break; // Sensitive Financial Reporting
			case 9: { img_ndx =  5; } break; // Changing or Deleting Transactions
			
			}
			return img_ndx;
		}
	};

	class CPageUser_2nd_Handler
	{
	private:
		CWindow& m_page_ref;

	public:
		CPageUser_2nd_Handler(CWindow& dlg_ref) : m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL bHandled = TRUE;

			if (false){
			}
			else
				bHandled = FALSE;

			return bHandled;
		}
		BOOL   OnNotify  (const CArea_Enum& _areas, LPARAM _l_p) {
			CTreeViewCtrl tv_ctl = m_page_ref.GetDlgItem(This_Ctl::e_tvw_are);
			if (NULL == tv_ctl)
				return FALSE;
			const LPNMHDR p_header = reinterpret_cast<LPNMHDR>(_l_p);
			if (  NULL != p_header && TVN_GETINFOTIP == p_header->code  ) {
				LPNMTVGETINFOTIP p_tip = reinterpret_cast<LPNMTVGETINFOTIP>(_l_p);
				if (NULL == p_tip)
					return FALSE;
				CAtlString cs_tip;
				::wcscpy_s(p_tip->pszText, p_tip->cchTextMax, cs_tip.GetBuffer());
				return TRUE;
			}
			if (NULL == p_header || TVN_SELCHANGED != p_header->code)
				return FALSE;

			LPNMTREEVIEW p_tvw = reinterpret_cast<LPNMTREEVIEW>(_l_p);
			if (NULL ==  p_tvw)
				return FALSE;
			size_t ndx_ = static_cast<size_t>(p_tvw->itemNew.lParam);
			if (ndx_ < _areas.Ref().size()) {
				CWindow lab_ = m_page_ref.GetDlgItem(This_Ctl::e_inf_txt);
				if (lab_)
					lab_.SetWindowTextW(_areas.Ref()[ndx_].Desc());
			}

			return TRUE;
		}
	};
}}}}

using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageUser_2nd:: CPageUser_2nd(ITabSetCallback& _set_snk):
	TBasePage(IDD_EBO_BOO_USR_MAN_2ND_PAG, *this, _set_snk), m_tvw_imgs(NULL) {

	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();

	HBITMAP h_bmp_ = NULL;
	HRESULT hr_ = CGdiPlusPngLoader::LoadResource(IDR_EBO_BOO_USR_PAG_2ND_TVW, hInstance, h_bmp_);
	if (SUCCEEDED(hr_)) {
		const SIZE sz_frm = {
			26, 22 // predefined in source PSD file;
		};
		hr_ = CGdiPlusPngLoader::CreateImages(
			h_bmp_, m_tvw_imgs, sz_frm
		);
		::DeleteObject(h_bmp_); h_bmp_ = NULL;
	}
}

CPageUser_2nd::~CPageUser_2nd(void) {
	if (m_tvw_imgs) {
		::ImageList_Destroy(m_tvw_imgs); m_tvw_imgs = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageUser_2nd::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
#if(0)
	details::CTabPageView_Init init_(*this, m_usr_list);
	details::CTabPageView_Handler hand_(*this, m_usr_list);
#endif
	return l_res;
}

LRESULT    CPageUser_2nd::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);
	CPageUser_2nd_Init initer_(*this);
	initer_.OnCreate(m_tvw_imgs, m_acc_enum);

	CPageUser_2nd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageUser_2nd::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageUser_2nd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

LRESULT    CPageUser_2nd::OnShowHide (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	if (TBasePage::m_b_vis_1st_time) {
		CWindow  lst_areas = Lay_(*this) << This_Ctl::e_tvw_are;
		if (0 != lst_areas)
			lst_areas.SetFocus(); bHandled = FALSE;
		return 0;
	}
	return TBasePage::OnShowHide(uMsg, wParam, lParam, bHandled);
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageUser_2nd::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPageUser_2nd_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE)
			{}
		} break;
	case WM_NOTIFY : {
			const LPNMHDR p_header = reinterpret_cast<LPNMHDR>(lParam);
			if (NULL == p_header)
				return FALSE;

			if (TBasePage::m_bInited) {
				CPageUser_2nd_Handler hand_(*this);
				bHandled = hand_.OnNotify(m_acc_enum, lParam);
			}

		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageUser_2nd::IsChanged   (void) const {
	return false;
}

CAtlString CPageUser_2nd::GetPageTitle(void) const {
	static CAtlString cs_title(_T(" Authorization ")); return cs_title;
}
VOID       CPageUser_2nd::UpdateData  (const DWORD _opt) {
	_opt;
	CPageUser_2nd_Init(*this).UpdateBanDesc();
}
VOID       CPageUser_2nd::UpdateLayout(void) {
	WTL::CTreeViewCtrl  lst_areas = Lay_(*this) << This_Ctl::e_tvw_are;
	if (0 != lst_areas) {
		HTREEITEM h_root = lst_areas.GetRootItem();
		lst_areas.SetFocus();
		lst_areas.Expand(h_root, TVE_EXPAND);
	}
}


/////////////////////////////////////////////////////////////////////////////