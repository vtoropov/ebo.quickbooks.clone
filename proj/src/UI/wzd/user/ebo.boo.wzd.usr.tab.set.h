#ifndef _EBOBOOWZDUSRTAB_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _EBOBOOWZDUSRTAB_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Apr-2018 at 3:51:20p, UTC+7, Novosibirsk, Rodniki, Monday;
	This is USB Drive Detective (bitsphereinc.com) app database option tab set interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 17-Aug-2018 at 6:55:35p, UTC+7, Novosibirsk, Rodniki, Tulenina, Friday;
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 10:57:52p, UTC+7, Novosibirsk, Tulenina, Monday;
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 12:20:12p, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.wzd.usr.pag.1st.h"
#include "ebo.boo.wzd.usr.pag.2nd.h"

namespace ebo { namespace boo { namespace gui {

	class CTabSet
	{
	public:
		enum _pages : INT {
			e_users  = 0,  // user list page;
		};
	private:
		WTL::CTabCtrl m_cTabCtrl;
		_pages        m_e_active;

	private: //tab page(s)
		CPageUser_1st m_users;
		CPageUser_2nd m_auth ;

	public:
		 CTabSet (ITabSetCallback&);
		~CTabSet (void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy (void);
		_pages        Selected(void) const;
		HRESULT       Selected(const _pages);
		VOID          UpdateLayout(void);
	public:
	};
}}}

#endif/*_EBOBOOWZDUSRTAB_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/