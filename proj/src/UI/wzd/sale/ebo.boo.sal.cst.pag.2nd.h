#ifndef _EBOBOOSALCSTPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOBOOSALCSTPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Jan-2020 at 9:16:59a, UTC+7, Novosibirsk, Rodniki, Friday;
	This is Ebo Pack personal account app sale document wizard the 2nd page interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"

namespace ebo { namespace boo { namespace gui {

	class CPageSale_2nd : public CTabPageBase, public  ITabPageEvents {
	                     typedef CTabPageBase  TBasePage;
	private:
	public :
		 CPageSale_2nd(ITabSetCallback&);
		~CPageSale_2nd(void);

	private:
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TBasePage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed;
		virtual VOID        UpdateLayout(void)       override sealed;
	};

}}}

#endif/*_EBOBOOSALCSTPAG2ND_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/