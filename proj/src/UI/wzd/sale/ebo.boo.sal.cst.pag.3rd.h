#ifndef _EBOBOOSALCSTPAG3RD_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _EBOBOOSALCSTPAG3RD_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Jan-2020 at 11:57:15a, UTC+7, Novosibirsk, Rodniki, Sunday;
	This is Ebo Pack personal account app sale document wizard the 3rd page interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"

namespace ebo { namespace boo { namespace gui {

	class CPageSale_3rd : public CTabPageBase, public  ITabPageEvents {
	                     typedef CTabPageBase  TBasePage;
	private:
	public :
		 CPageSale_3rd(ITabSetCallback&);
		~CPageSale_3rd(void);

	private:
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageEvents
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public : // TBasePage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CAtlString  GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed;
		virtual VOID        UpdateLayout(void)       override sealed;
	};

}}}

#endif/*_EBOBOOSALCSTPAG3RD_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/