#ifndef _EBOBOOSALCSTTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _EBOBOOSALCSTTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Dec-2019 at 9:55:33p, UTC+7, Novosibirsk, Rodniki, Tuesday;
	This is Ebo Pack personal account app sale document wizard tab set interface declaration file.
*/
#include "ebo.boo.wzd.pag.bas.h"
#include "ebo.boo.sal.cst.pag.1st.h"
#include "ebo.boo.sal.cst.pag.2nd.h"
#include "ebo.boo.sal.cst.pag.3rd.h"

namespace ebo { namespace boo { namespace gui {

	class CSaleTabSet {
	public:
		enum _pages : INT {
			e_doc_type  = 0,  // sale document type;
			e_doc_invc  = 1,  // invoice document page;
			e_doc_auxd  = 2,  // auxiliary data page;
		};
	private:
		WTL::CTabCtrl m_cTabCtrl;
		_pages        m_e_active;

	private: //tab page(s)
		CPageSale_1st m_doc_type;
		CPageSale_2nd m_doc_invc;
		CPageSale_3rd m_doc_auxd;

	public:
		 CSaleTabSet (ITabSetCallback&);
		~CSaleTabSet (void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy (void);
	public:
		_pages        Back    (void)      ;
		_pages        Next    (void)      ;
		_pages        Selected(void) const;
		HRESULT       Selected(const _pages);
	public:
		VOID          UpdateLayout(void);
	};

	typedef CSaleTabSet::_pages TSalePages;

}}}

#endif/*_EBOBOOSALCSTTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/