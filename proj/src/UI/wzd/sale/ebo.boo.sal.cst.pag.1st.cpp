/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Jan-2020 at 8:07:50a, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is Ebo Pack personal account app sale document wizard the 1st page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.sal.cst.pag.1st.h"
#include "ebo.boo.res.wzd.sal.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageSale_1st_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_SAL_CST_PAG_1ST_AVA,    // document type page avatar ;
			e_ava_cap = IDC_EBO_SAL_CST_PAG_1ST_CAP,    // document type page caption;
			e_opt_inv = IDC_EBO_SAL_CST_PAG_1ST_TP0,    // radio button for selecting invoice document;
			e_inf_img = IDC_EBO_SAL_CST_PAG_1ST_AV1,    // document type page info image control ;
			e_inf_txt = IDC_EBO_SAL_CST_PAG_1ST_CA1,    // document type page info text control ;
		};
	};
	typedef CPageSale_1st_Ctl This_Ctl;

	class CPageSale_1st_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageSale_1st_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_inf_img, This_Ctl::e_inf_img) );

			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_cap;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}

			CWindow  cap_inf = Lay_(m_page_ref) << This_Ctl::e_inf_txt;
			if (0 != cap_inf) {
				CAtlString cs_desc; cs_desc.LoadStringW(IDS_EBO_SAL_CST_PAG_1ST_INF_0);
				cap_inf.SetWindowTextW(cs_desc);
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPageSale_1st_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPageSale_1st_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;

			CButton opt_inv = Lay_(m_page_ref) << This_Ctl::e_opt_inv;
			if (0!= opt_inv)
				opt_inv.SetCheck(true);

			return m_error;
		}

	};

	class CPageSale_1st_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPageSale_1st_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageSale_1st:: CPageSale_1st(ITabSetCallback& _set_snk):
       TBasePage(IDD_EBO_BOO_SAL_CST_1ST_PAG, *this, _set_snk) {
}

CPageSale_1st::~CPageSale_1st(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageSale_1st::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageSale_1st::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageSale_1st_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPageSale_1st_Init initer_(*this);
	initer_.OnCreate();
	
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageSale_1st::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageSale_1st_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageSale_1st::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPageSale_1st_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE ) {
				TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageSale_1st::IsChanged   (void) const {
	return false;
}

CAtlString CPageSale_1st::GetPageTitle(void) const {  static CAtlString cs_title(_T("Document Type")); return cs_title; }

VOID       CPageSale_1st::UpdateData  (const DWORD _opt) {
	_opt;
}