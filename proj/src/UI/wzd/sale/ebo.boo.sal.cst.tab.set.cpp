/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Dec-2019 at 10:03:36p, UTC+7, Novosibirsk, Rodniki, Tuesday;
	This is Ebo Pack personal account app sale document wizard tab set interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.sal.cst.tab.set.h"
#include "ebo.boo.res.wzd.sal.h"

using namespace ebo::boo::gui;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CTabSet_Layout
	{
	private:
		RECT    m_area;
	public:
		CTabSet_Layout(const RECT& rcArea) : m_area(rcArea) {}

	public:
		RECT    GetTabsArea(void)const {
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};
}}}}

using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CSaleTabSet:: CSaleTabSet(ITabSetCallback& _snk) :
	m_e_active(_pages::e_doc_type), m_doc_type(_snk), m_doc_invc(_snk), m_doc_auxd(_snk) { _snk; }
CSaleTabSet::~CSaleTabSet(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CSaleTabSet::Create(const HWND hParent, const RECT& rcArea) {
	hParent; rcArea;
	HRESULT hr_ = S_OK;

	if(FALSE == ::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return (hr_ = E_INVALIDARG);

	CTabSet_Layout layout(rcArea);
	RECT rcTabs  = layout.GetTabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs ,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_EBO_BOO_SAL_CST_WZD_TAB
		);

	const SIZE szPadding = {
		5, 2
	};

	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());
	m_cTabCtrl.SetPadding(szPadding);
	m_cTabCtrl.SetItemSize(  0,   0);

	INT nIndex = 0; nIndex;
	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW; // auto-placement is made by default implementation of create/initialize page handler;

	{
		m_doc_type.Create(m_cTabCtrl.m_hWnd);
		m_doc_type.SetWindowPos(
				HWND_TOP,
				0, 0, 0 , 0,
				dwFlags
			);
		if (m_doc_type.IsWindow()) m_doc_type.Index(nIndex++);
	}
	{
		m_doc_invc.Create(m_cTabCtrl.m_hWnd);
		m_doc_invc.SetWindowPos(
			HWND_TOP,
			0, 0, 0 , 0,
			dwFlags
		);
		if (m_doc_invc.IsWindow()) m_doc_invc.Index(nIndex++);
	}
	{
		m_doc_auxd.Create(m_cTabCtrl.m_hWnd);
		m_doc_auxd.SetWindowPos(
			HWND_TOP,
			0, 0, 0 , 0,
			dwFlags
		);
		if (m_doc_auxd.IsWindow()) m_doc_auxd.Index(nIndex++);
	}

	m_cTabCtrl.SetCurSel(m_e_active);

	this->UpdateLayout();

	return S_OK;
}

HRESULT       CSaleTabSet::Destroy(void)
{
	if (m_doc_type .IsWindow()) { m_doc_type .DestroyWindow(); m_doc_type .m_hWnd = NULL; }
	if (m_doc_invc .IsWindow()) { m_doc_invc .DestroyWindow(); m_doc_invc .m_hWnd = NULL; }
	if (m_doc_auxd .IsWindow()) { m_doc_auxd .DestroyWindow(); m_doc_auxd .m_hWnd = NULL; }
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

TSalePages    CSaleTabSet::Back    (void) {
	switch (this->Selected()) {
	case TSalePages::e_doc_type: {} break;
	case TSalePages::e_doc_invc: { m_e_active = TSalePages::e_doc_type; } break;
	case TSalePages::e_doc_auxd: { m_e_active = TSalePages::e_doc_invc; } break;
	}
	this->UpdateLayout();
	return  this->Selected();
}
TSalePages    CSaleTabSet::Next    (void) {
	switch (this->Selected()) {
	case TSalePages::e_doc_type: { m_e_active = TSalePages::e_doc_invc; } break;
	case TSalePages::e_doc_invc: { m_e_active = TSalePages::e_doc_auxd; } break;
	case TSalePages::e_doc_auxd: {} break;
	}
	this->UpdateLayout();
	return  this->Selected();
}
TSalePages    CSaleTabSet::Selected(void) const      { return m_e_active; }
HRESULT       CSaleTabSet::Selected(const _pages _v) {
	HRESULT hr_ = S_OK;
	const bool b_changed = (_v != m_e_active);
	m_e_active  = _v;
	if (b_changed) {
		this->UpdateLayout();
	}
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////
void          CSaleTabSet::UpdateLayout(void)
{
	const INT nTabIndex = m_e_active;
	
	if (m_doc_type.IsWindow()) {
		m_doc_type.ShowWindow(nTabIndex == m_doc_type.Index() ? SW_SHOW : SW_HIDE);
	if (m_doc_type.Index() == nTabIndex)
		m_doc_type.UpdateData  ();
		m_doc_type.UpdateLayout();
	}
	if (m_doc_invc.IsWindow()) {
		m_doc_invc.ShowWindow(nTabIndex == m_doc_invc.Index() ? SW_SHOW : SW_HIDE);
	if (m_doc_invc.Index() == nTabIndex)
		m_doc_invc.UpdateData  ();
		m_doc_invc.UpdateLayout();
	}
	if (m_doc_auxd.IsWindow()) {
		m_doc_auxd.ShowWindow(nTabIndex == m_doc_auxd.Index() ? SW_SHOW : SW_HIDE);
	if (m_doc_auxd.Index() == nTabIndex)
		m_doc_auxd.UpdateData  ();
		m_doc_auxd.UpdateLayout();
	}
}