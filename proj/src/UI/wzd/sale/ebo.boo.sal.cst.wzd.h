#ifndef _EBOBOOSALCSTWZD_H_C263FBA4_4D8A_40AE_B492_0DB4FBD4BAFE_INCLUDED
#define _EBOBOOSALCSTWZD_H_C263FBA4_4D8A_40AE_B492_0DB4FBD4BAFE_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Dec-2019 at 9:01:36p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app sale document wizard main dialog interface declaration file.
*/
#include "shared.uix.frms.img.ban.h"
#include "ebo.boo.sal.cst.tab.set.h"

namespace ebo { namespace boo { namespace gui {

	using ex_ui::frames::CImageBanner;

	using ex_ui::draw::defs::IRenderer;

	class CSaleDocWzd {
	private:
		class CSaleDocWzdImpl : public ::ATL::CDialogImpl<CSaleDocWzdImpl> , ITabSetCallback, IRenderer {
		                       typedef ::ATL::CDialogImpl<CSaleDocWzdImpl>   TDialog;
		friend class CSaleDocWzd;
		private:
			CImageBanner    m_banner;
			CSaleTabSet     m_tabset;

		public:
			UINT   IDD;

		public:
			BEGIN_MSG_MAP(CSaleDocWzdImpl)
				MESSAGE_HANDLER     (WM_COMMAND    ,  OnCommand  )
				MESSAGE_HANDLER     (WM_DESTROY    ,  OnDestroy  )
				MESSAGE_HANDLER     (WM_INITDIALOG ,  OnInitDlg  )
				MESSAGE_HANDLER     (WM_SYSCOMMAND ,  OnSysCmd   )
				NOTIFY_CODE_HANDLER (TCN_SELCHANGE ,  OnTabNotify)
			END_MSG_MAP()
		public:
			 CSaleDocWzdImpl(void);
			~CSaleDocWzdImpl(void);

		private:
			LRESULT OnCommand(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnDestroy(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnInitDlg(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnSysCmd (UINT , WPARAM , LPARAM , BOOL&);
		private:
			LRESULT OnTabNotify (INT  , LPNMHDR, BOOL&);
		private: // ITabSetCallback
			virtual HRESULT TabSet_OnDataChanged (const UINT pageId, const bool bChanged) override sealed;
			virtual HRESULT TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) override sealed;
		private: // IRenderer
			HRESULT DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) const override sealed;
		};
	private:
		CSaleDocWzdImpl   m_dlg;
	public:
		 CSaleDocWzd(void);
		~CSaleDocWzd(void);

	public:
		HRESULT     DoModal(void);
	};
}}}

#endif/*_EBOBOOSALCSTWZD_H_C263FBA4_4D8A_40AE_B492_0DB4FBD4BAFE_INCLUDED*/