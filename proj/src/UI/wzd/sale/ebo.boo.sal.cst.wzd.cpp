/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Dec-2019 at 9:24:22p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app sale document wizard main dialog interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.sal.cst.wzd.h"
#include "ebo.boo.res.h"
#include "ebo.boo.res.wzd.sal.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui;

#include "shared.gen.app.res.h"
#include "shared.uix.gdi.provider.h"

using namespace shared::user32;
using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl {

	class CSaleDocWzd_1st_Ctl {
	public:
		enum _ctl : WORD {
			e_but_prv = IDNO,    // wizard  back button ;
			e_but_nxt = IDOK,    // wizard  next button ;
			e_but_can = IDCANCEL // wizard close button ;
		};
	};
	typedef CSaleDocWzd_1st_Ctl This_Ctl;

	class CSaleDocWzd_Initer {
	private:
		CWindow&   m_dlg_ref;
	public:
		CSaleDocWzd_Initer(CWindow& dlg_ref) : m_dlg_ref(dlg_ref) {}

	public:
		VOID   OnCreateEvent(void) {
			const UINT w_ids[] = {
				CSaleDocWzd_1st_Ctl::e_but_prv, CSaleDocWzd_1st_Ctl::e_but_nxt, CSaleDocWzd_1st_Ctl::e_but_can
			};
			for (INT i_ = 0; i_ < _countof(w_ids); i_ ++) {
				CButton  btn_ctl = m_dlg_ref.GetDlgItem(w_ids[i_]);
				if (0 == btn_ctl)
					continue;
				::SendMessage(btn_ctl, WM_CHANGEUISTATE, MAKELONG(UIS_SET, UISF_HIDEFOCUS|UISF_HIDEACCEL), 0);
				btn_ctl.EnableWindow(IDCANCEL == w_ids[i_]);
			}
		}

		VOID   UpdateButtonState(const CSaleTabSet::_pages _current) {

			CButton but_prv = Lay_(m_dlg_ref) << This_Ctl::e_but_prv;
			CButton but_nxt = Lay_(m_dlg_ref) << This_Ctl::e_but_nxt;

			bool b_states[2] = {false, false};

			switch (_current) {
			case CSaleTabSet::e_doc_type: {
					b_states[0] = false; b_states[1] = true ;
				} break;
			case CSaleTabSet::e_doc_invc: {
					b_states[0] = true ; b_states[1] = true ;
				} break;
			case CSaleTabSet::e_doc_auxd: {
					b_states[0] = true ; b_states[1] = false;
				} break;
			}

			if (but_prv.IsWindow()) but_prv.EnableWindow(b_states[0]);
			if (but_nxt.IsWindow()) but_nxt.EnableWindow(b_states[1]);

			if (CSaleTabSet::e_doc_auxd == _current) {
				if (but_nxt.IsWindow()) {
					but_nxt.EnableWindow(true); but_nxt.SetWindowText(_T("Save"));
				}
			}
			else {
				if (but_nxt.IsWindow()) {
					but_nxt.EnableWindow(true); but_nxt.SetWindowText(_T("Next"));
				}
			}
		}
	};

	class CSaleDocWzd_Layout {
	private:
		enum _e {
			e_gap = 0x4, // it is very hight for separator control: it gets empty space between top and bottom edges;
			e_sep = 0x2,
		};

	private:
		CWindow&   m_dlg_ref;
		RECT       m_dlg_rec;

	public:
		CSaleDocWzd_Layout(CWindow& dlg_ref) : m_dlg_ref(dlg_ref) {

			if (m_dlg_ref.GetClientRect(&m_dlg_rec) == FALSE)
				::SetRectEmpty(&m_dlg_rec);
		}

	public:
		// 
		// it does not work in intermediate cases when selected page is already has controls' state been changed;
		//
		VOID     AdjustButts(const INT _n_step) {
			CButton prev_ = m_dlg_ref.GetDlgItem(IDNO); if (prev_) prev_.EnableWindow(_n_step > 0);
			CButton next_ = m_dlg_ref.GetDlgItem(IDOK); if (next_) next_.EnableWindow(_n_step < 1);
		}
		VOID     AdjustCtrls(VOID) {
			CWindow div_hz = m_dlg_ref.GetDlgItem(IDC_EBO_BOO_SAL_CST_WZD_SEP);
			if (div_hz) {
				const RECT rc_hz = this->GetHozArea();
				div_hz.MoveWindow(&rc_hz);
			}
		}

		RECT     GetBanArea (VOID) {

			RECT rc_ban  = {0};
			CWindow ban_ = m_dlg_ref.GetDlgItem(IDC_EBO_BOO_SAL_CST_WZD_BAN);
			if (NULL == ban_)
				return rc_ban;

			if (FALSE == ban_.GetWindowRect(&rc_ban))
				return rc_ban;

			::MapWindowPoints(HWND_DESKTOP, m_dlg_ref, (LPPOINT)&rc_ban, 0x2);

			return rc_ban;
		}

		RECT     GetHozArea (VOID) {
			const
			RECT rc_ban = this->GetBanArea();
			RECT rc_hoz = {0};

			CWindow div_hz = m_dlg_ref.GetDlgItem(IDC_EBO_BOO_SAL_CST_WZD_SEP);
			if (NULL != div_hz) {
				rc_hoz         = m_dlg_rec;
				rc_hoz.top     = rc_ban.bottom + 1;
				rc_hoz.bottom  = rc_ban.bottom + 1 + _e::e_sep;
			}
			return rc_hoz;
		}

		RECT     GetTabArea (VOID) {
			RECT rc_tab   = m_dlg_rec;
			rc_tab.left   = this->GetBanArea().right;
			rc_tab.bottom = this->GetHozArea().top  ;

			return rc_tab;
		}
	};

}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CSaleDocWzd::CSaleDocWzdImpl:: CSaleDocWzdImpl(void) :
	IDD     (IDD_EBO_BOO_SAL_CST_WZD    ),
	m_banner(IDR_EBO_BOO_SAL_CST_WZD_BAN), m_tabset(*this) { m_banner.Renderer(this); }
CSaleDocWzd::CSaleDocWzdImpl::~CSaleDocWzdImpl(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CSaleDocWzd::CSaleDocWzdImpl::OnCommand(UINT, WPARAM wParam, LPARAM, BOOL& bHandled) {
	wParam; bHandled = FALSE;
	TSalePages current_ = m_tabset.Selected();
	switch (wParam) {
	case This_Ctl::e_but_nxt: {
		current_ = m_tabset.Next(); CSaleDocWzd_Initer(*this).UpdateButtonState(current_); } break;
	case This_Ctl::e_but_prv: {
		current_ = m_tabset.Back(); CSaleDocWzd_Initer(*this).UpdateButtonState(current_); } break;
	case This_Ctl::e_but_can: {
			TDialog::EndDialog(static_cast<INT>(wParam));
		} break;
	}

	return 0;
}

LRESULT   CSaleDocWzd::CSaleDocWzdImpl::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	m_tabset.Destroy();
	m_banner.Destroy();
	return 0;
}

LRESULT   CSaleDocWzd::CSaleDocWzdImpl::OnInitDlg(UINT , WPARAM , LPARAM , BOOL&) {

	CSaleDocWzd_Layout layout_(*this);
	HRESULT hr_ = m_banner.Create(TDialog::m_hWnd, IDC_EBO_BOO_SAL_CST_WZD_BAN);
	if (FAILED(hr_)) {}
	else {
		layout_.AdjustCtrls();
	}
	CSaleDocWzd_Initer
	initer_(*this);
	initer_.OnCreateEvent();

	const RECT rc_tabs = layout_.GetTabArea();
	hr_ = m_tabset.Create(*this, rc_tabs);
	hr_ = m_tabset.Selected(CSaleTabSet::e_doc_type);

	initer_.UpdateButtonState(m_tabset.Selected());

	TDialog::CenterWindow();
	{
		CApplicationIconLoader loader_(IDR_EBO_BOO_SAL_CST_FRM_ICO);
		TDialog::SetIcon(loader_.DetachLargeIcon(), TRUE );
		TDialog::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	return 0;
}

LRESULT   CSaleDocWzd::CSaleDocWzdImpl::OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam) {
		case SC_CLOSE: {
			TDialog::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CSaleDocWzd::CSaleDocWzdImpl::OnTabNotify (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled) {
	idCtrl; pnmh; bHandled = TRUE;
	m_tabset.UpdateLayout();
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CSaleDocWzd::CSaleDocWzdImpl::TabSet_OnDataChanged (const UINT pageId, const bool bChanged ) {
	pageId; bChanged;
	HRESULT hr_ = S_OK;

	CWindow bt_app = TDialog::GetDlgItem(IDOK);
	if (NULL != bt_app) {
	}

	return  hr_;
}

HRESULT   CSaleDocWzd::CSaleDocWzdImpl::TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) {
	ctrlId; _rc_ctrl;
	HRESULT hr_ = S_OK;
	switch (ctrlId) {
	case This_Ctl::e_but_prv : case This_Ctl::e_but_nxt :
	case This_Ctl::e_but_can : {
			CWindow ctl_btn = TDialog::GetDlgItem(ctrlId);
			if (NULL != ctl_btn) {
				ctl_btn.GetWindowRect(&_rc_ctrl);
			}
		} break;
	default:
		hr_ = DISP_E_PARAMNOTFOUND;
	}
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CSaleDocWzd::CSaleDocWzdImpl::DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& _rc_area) const {
	hChild;
	if (NULL == hSurface)
		return __DwordToHresult(ERROR_INVALID_HANDLE);
	if (::IsRectEmpty(&_rc_area))
		return OLE_E_INVALIDRECT;
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getsyscolor
	CZBuffer dc_(hSurface, _rc_area);
	dc_.FillSolidRect(
		&_rc_area, ::GetSysColor(COLOR_3DFACE)
		);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CSaleDocWzd:: CSaleDocWzd(void) {}
CSaleDocWzd::~CSaleDocWzd(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT CSaleDocWzd::DoModal(void) {

	HRESULT hr_ = S_OK;

	INT_PTR i_ptr = m_dlg.DoModal();
	switch (i_ptr) {
	case This_Ctl::e_but_nxt: {
		} break;
	default:
		hr_ = S_FALSE;
	}
	return  hr_;
}