/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Jan-2020 at 12:00:04p, UTC+7, Novosibirsk, Rodniki, Sunday;
	This is Ebo Pack personal account app sale document wizard the 3rd page interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.sal.cst.pag.3rd.h"
#include "ebo.boo.res.wzd.sal.h"
#include "ebo.boo.wzd.pag.lay.h"

using namespace ebo::boo::gui::layout;
using namespace ebo::boo::gui;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl
{
	class CPageSale_3rd_Ctl {
	public:
		enum _ctl : WORD {
			e_ava_ctl = IDC_EBO_SAL_CST_PAG_3RD_AVA,
			e_ava_cap = IDC_EBO_SAL_CST_PAG_3RD_CAP,    // invoice document page caption;
			e_but_can = IDCANCEL,
			e_but_nxt = IDOK    ,
			e_but_bak = IDNO    ,
			e_msg_inv = IDC_EBO_SAL_CST_PAG_3RD_MSG,    // displayed invoice message text box;
			e_msg_sta = IDC_EBO_SAL_CST_PAG_3RD_STA,    // displayed statement message text box;
			e_sub_ctl = IDC_EBO_SAL_CST_PAG_3RD_AV1,
			e_sub_cap = IDC_EBO_SAL_CST_PAG_3RD_CA1,
			e_att_lst = IDC_EBO_SAL_CST_PAG_3RD_ATT,
			e_but_add = IDC_EBO_SAL_CST_PAG_3RD_ADD,
			e_but_edt = IDC_EBO_SAL_CST_PAG_3RD_EDT,
			e_but_del = IDC_EBO_SAL_CST_PAG_3RD_DEL,
		};
	};
	typedef CPageSale_3rd_Ctl This_Ctl;

	class CPageSale_3rd_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageSale_3rd_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_ava_ctl, This_Ctl::e_ava_ctl) );
			CPage_Layout(m_page_ref).AdjustImg( CPage_Ava(This_Ctl::e_sub_ctl, This_Ctl::e_sub_ctl) );
			
			CWindow  cap_ava = Lay_(m_page_ref) << This_Ctl::e_ava_cap;
			if (0 != cap_ava) {
				cap_ava.SetFont(CTabPageBase::SectionCapFont());
			}

			CWindow  cap_sub = Lay_(m_page_ref) << This_Ctl::e_sub_cap;
			if (0 != cap_sub) {
				cap_sub.SetFont(CTabPageBase::SectionCapFont());
			}

			RECT  rc_inv_msg  = Lay_(m_page_ref)  = This_Ctl::e_msg_inv;
			CWindow  inv_msg  = Lay_(m_page_ref) << This_Ctl::e_msg_inv;

			RECT  rc_sta_msg  = Lay_(m_page_ref)  = This_Ctl::e_msg_sta;
			CWindow  sta_msg  = Lay_(m_page_ref) << This_Ctl::e_msg_sta;

			RECT  rc_but_add  = Lay_(m_page_ref)  = This_Ctl::e_but_add;
			CWindow  but_add  = Lay_(m_page_ref) << This_Ctl::e_but_add;

			RECT  rc_but_edt  = Lay_(m_page_ref)  = This_Ctl::e_but_edt;
			CWindow  but_edt  = Lay_(m_page_ref) << This_Ctl::e_but_edt;

			RECT  rc_but_del  = Lay_(m_page_ref)  = This_Ctl::e_but_del;
			CWindow  but_del  = Lay_(m_page_ref) << This_Ctl::e_but_del;

			CWindow  dlg_main = m_page_ref.GetTopLevelParent();
			if (0 != dlg_main) {
				CWindow  but_can = Lay_(dlg_main) << This_Ctl::e_but_can;
				if (0 != but_can) {
					RECT  rc_but = Lay_(dlg_main) =  This_Ctl::e_but_can;
					::MapWindowPoints ( dlg_main,  m_page_ref, (LPPOINT)&rc_but, 0x2 );
					rc_but_del.left   = rc_but.left ;
					rc_but_del.right  = rc_but.right;
					but_del.MoveWindow(&rc_but_del) ;
				}
				CWindow  but_nxt = Lay_(dlg_main) << This_Ctl::e_but_nxt;
				if (0 != but_nxt) {
					RECT  rc_but = Lay_(dlg_main) =  This_Ctl::e_but_nxt;
					::MapWindowPoints ( dlg_main,  m_page_ref, (LPPOINT)&rc_but, 0x2 );
					rc_inv_msg.right  = rc_but.right;
					rc_sta_msg.right  = rc_but.right;
					rc_but_edt.left   = rc_but.left ;
					rc_but_edt.right  = rc_but.right;
					inv_msg.MoveWindow(&rc_inv_msg) ;
					sta_msg.MoveWindow(&rc_sta_msg) ;
					but_edt.MoveWindow(&rc_but_edt) ;
				}
				CWindow  but_bak = Lay_(dlg_main) << This_Ctl::e_but_bak;
				if (0 != but_bak) {
					RECT  rc_but = Lay_(dlg_main) =  This_Ctl::e_but_bak;
					::MapWindowPoints ( dlg_main,  m_page_ref, (LPPOINT)&rc_but, 0x2 );
					rc_but_add.left   = rc_but.left ;
					rc_but_add.right  = rc_but.right;
					but_add.MoveWindow(&rc_but_add) ;
				}
			}
		}
		VOID   OnSize  (void) {
		}
	};

	class CPageSale_3rd_Init {
	private:
		CWindow&   m_page_ref;
		CError     m_error   ;
		
	public:
		CPageSale_3rd_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
			m_error << __MODULE__ << S_OK >> __MODULE__;
		}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;

			static const INT n_col_w[] = {
				20, 415, 90
			};
			static LPCTSTR lp_sz_cap[] = {
				_T("#"), _T("URI"), _T("Type")
			};

			CListViewCtrl att_lst = m_page_ref.GetDlgItem(This_Ctl::e_att_lst);
			if (att_lst) {
				att_lst.SetExtendedListViewStyle(LVS_EX_FULLROWSELECT|LVS_EX_DOUBLEBUFFER ,   0);
				for (INT i_ = 0; i_ < _countof(n_col_w) && i_ < _countof(lp_sz_cap); i_++) {
					att_lst.AddColumn(lp_sz_cap[i_],i_); att_lst.SetColumnWidth( i_, n_col_w[i_]);
				}
			}

			return m_error;
		}

	};

	class CPageSale_3rd_Handler {
	private:
		CWindow&     m_page_ref;

	public:
		CPageSale_3rd_Handler(CWindow& dlg_ref)
			: m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			return bHandled;
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CPageSale_3rd:: CPageSale_3rd(ITabSetCallback& _set_snk):
       TBasePage(IDD_EBO_BOO_SAL_CST_3RD_PAG, *this, _set_snk) {
}

CPageSale_3rd::~CPageSale_3rd(void) {
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageSale_3rd::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageClose(uMsg, wParam, lParam, bHandled);
	return l_res;
}

LRESULT    CPageSale_3rd::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageSale_3rd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnCreate();

	CPageSale_3rd_Init initer_(*this);
	initer_.OnCreate();
	
	TBasePage::m_bInited = true;
	return l_res;
}

LRESULT    CPageSale_3rd::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TBasePage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageSale_3rd_Layout layout_(*this, TBasePage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageSale_3rd::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TBasePage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPageSale_3rd_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE ) {
				TBasePage::m_set_snk.TabSet_OnDataChanged(0, true);
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageSale_3rd::IsChanged   (void) const {
	return false;
}

CAtlString CPageSale_3rd::GetPageTitle(void) const {  static CAtlString cs_title(_T("Invoice Aux")); return cs_title; }

VOID       CPageSale_3rd::UpdateData  (const DWORD _opt) {
	_opt;
}

VOID       CPageSale_3rd::UpdateLayout(void) {
}