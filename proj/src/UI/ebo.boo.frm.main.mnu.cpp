/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-May-2016 at 10:24:36pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian Main Frame Manu Bar class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 16-Aug-2018 at 5:31:41p, UTC+7, Novosibirsk, Rodniki, Tulenina, Thursday;
	Adopted to Ebo Pack personal account app on 30-Oct-2019 at 9:17:01a, UTC+7, Novosibirsk, Tulenina, Wednesday;
*/
#include "StdAfx.h"
#include "ebo.boo.frm.main.mnu.h"
#include "ebo.boo.res.mnu.bar.h"

using namespace ebo::boo::gui;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace gui { namespace _impl {

	CMenuHandle& CMenuBar_Handle(void) {
		static CMenuHandle m_bar;  // menu bar handle is only one for this version of the software;
		return m_bar;
	}

	t_sub_menu_map& CMenuBar_Map(void) {
		static t_sub_menu_map m_map;  // sub-menu map;
		return m_map;
	}
	class CMenuBar_Spec {
	public:

		static const UINT* GetMenuBarIds(INT& _size) {
			static UINT ids_[] = {
				IDC_EBO_BOO__MNU_BAR_FILE,
				IDC_EBO_BOO__MNU_BAR_REPS,
				IDC_EBO_BOO__MNU_BAR_TOOL,
				IDC_EBO_BOO__MNU_BAR_VIEW,
				IDC_EBO_BOO__MNU_BAR_HELP, NULL
			};
			_size = _countof(ids_);
			return ids_;
		}

		static const UINT* GetRptMenuSubIds  (INT& _size) {
			static UINT ids_[] = {
				IDC_EBO_BOO_MNU_CMD_RPT_PAL
			};
			_size = _countof(ids_);
			return ids_;
		}

		static const UINT* GetToolMenuSubIds (INT& _size) {
			static UINT ids_[] = {
				IDC_EBO_BOO_SUB_MNU_CLR_THM
			};
			_size = _countof(ids_);
			return ids_;
		}

		static const UINT* GetViewMenuSubIds (INT& _size) {
			static UINT ids_[] = {
				IDC_EBO_BOO_SUB_MNU_SEL_POS, IDC_EBO_BOO_SUB_MNU_TLS_SIZ
			};
			_size = _countof(ids_);
			return ids_;
		}

		static const UINT* GetViewMenuSideIds(INT& _size) {
			static UINT ids_[] = {
				IDC_EBO_BOO_MNU_CMD_SEL_LFT, IDC_EBO_BOO_MNU_CMD_SEL_RIT
			};
			_size = _countof(ids_);
			return ids_;
		}

		static const UINT* GetViewMenuSizeIds(INT& _size) {
			static UINT ids_[] = {
				IDC_EBO_BOO_MNU_CMD_TLS_EXT, IDC_EBO_BOO_MNU_CMD_TLS_LRG,
				IDC_EBO_BOO_MNU_CMD_TLS_MED, IDC_EBO_BOO_MNU_CMD_TLS_SML
			};
			_size = _countof(ids_);
			return ids_;
		}
	};

	class CMenuBar_Update {
	public:

		static VOID   UpdateMnuFile(const HMENU _mnu) { _mnu; }
		static VOID   UpdateMnuHelp(const HMENU _mnu) { _mnu; }
		static VOID   UpdateMnuReps(const HMENU _mnu, CMainForm& _frm) {
			_mnu;
			const bool bIsRepVisible = _frm.Views().Report().IsVisible();
			CMenuHandle mnu_ = _mnu;
			mnu_.CheckMenuItem(IDC_EBO_BOO_MNU_CMD_RPT_PAL, MF_BYCOMMAND | (bIsRepVisible ? MF_CHECKED : MF_UNCHECKED));
		}
		static VOID   UpdateMnuThem(const HMENU _mnu, CMainForm& _frm) {
			_mnu; _frm;
			CMenuHandle mnu_ = _mnu;
			for (UINT i_ = IDC_EBO_BOO_MNU_CMD_CLR_TH0; i_ <= IDC_EBO_BOO_MNU_CMD_CLR_THK; i_++) {
				mnu_.CheckMenuItem(i_, MF_BYCOMMAND | (_frm.IsTheme(static_cast<WORD>(i_)) ? MF_CHECKED : MF_UNCHECKED));
			}
		}
		static VOID   UpdateMnuTool(const HMENU _mnu) { _mnu; }
		static VOID   UpdateMnuView(const HMENU _mnu, CMainForm& _frm) { _mnu; _frm; }
		static VOID   UpdateMnuSide(const HMENU _mnu, CMainForm& _frm) {
			_mnu;
			const  bool  bIsSelVisible = _frm.Selector().Is() && _frm.Selector().Visible(); bIsSelVisible;
			const  ex_ui::controls::eHorzAlign::_e
				e_side = _frm.Selector().Layout().HAlign();
		
			CMenuHandle mnu_ = _mnu;

			INT n_count = 0;
			const UINT* p_ids = CMenuBar_Spec::GetViewMenuSideIds(n_count);

			for (INT i_ = 0; i_ < n_count && p_ids != NULL; i_ ++) {

				bool bChecked = false;

				switch (p_ids[i_]) {
				case IDC_EBO_BOO_MNU_CMD_SEL_LFT: { bChecked = (ex_ui::controls::eHorzAlign::eLeft  == e_side); } break;
				case IDC_EBO_BOO_MNU_CMD_SEL_RIT: { bChecked = (ex_ui::controls::eHorzAlign::eRight == e_side); } break;
				}
				mnu_.CheckMenuItem(
					p_ids[i_], MF_BYCOMMAND | (bChecked ? MF_CHECKED : MF_UNCHECKED)
				);
			}
		}
		static VOID   UpdateMnuSize(const HMENU _mnu, CMainForm& _frm) {
			_mnu;
			const  bool  bIsRepVisible = _frm.Views().Report().IsVisible(); bIsRepVisible;

			CMenuHandle mnu_ = _mnu;

			INT n_count = 0;
			const UINT* p_ids = CMenuBar_Spec::GetViewMenuSizeIds(n_count);

			for (INT i_ = 0; i_ < n_count && p_ids != NULL; i_ ++) {

				bool bChecked = _frm.Views().Report().IsChecked(p_ids[i_]);
				mnu_.CheckMenuItem(
					p_ids[i_], MF_BYCOMMAND | (bChecked ? MF_CHECKED : MF_UNCHECKED)
				);
			}
		}
	};
}}}}
using namespace ebo::boo::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CMenuBar::CMenuBar(CMainForm& _main) : m_main(_main) {}

/////////////////////////////////////////////////////////////////////////////

VOID     CMenuBar::Initialize(void) {

	CMenuBar_Handle() = ::LoadMenu(_Module.GetResourceInstance(), MAKEINTRESOURCE(IDC_EBO_BOO__MNU_BAR));
	if (NULL == CMenuBar_Handle()) 
		return;

	m_main.Window().SetMenu(CMenuBar_Handle());

	const INT n_sub_cnt = CMenuBar_Handle().GetMenuItemCount();

	INT sz_ = 0;
	const UINT* p_ids = CMenuBar_Spec::GetMenuBarIds(sz_);

	for (INT i_ = 0; p_ids != NULL && i_ < n_sub_cnt && i_ < sz_; i_++) {

		::WTL::CMenuHandle menu_ = CMenuBar_Handle().GetSubMenu(i_);
		if (NULL == menu_)
			continue;

		CMenuBar_Map().insert(
			::std::make_pair(menu_.m_hMenu, p_ids[i_])
		);

		const INT n_itm_cnt = menu_.GetMenuItemCount();
		INT n_sub_ptr = 0;

		for (INT j_ = 0; j_ < n_itm_cnt; j_++) {

			::WTL::CMenuHandle sub_menu_ = menu_.GetSubMenu(j_);
			if (NULL == sub_menu_)
				continue;
			switch (p_ids[i_]) {
			case IDC_EBO_BOO__MNU_BAR_REPS: {
				} break;
			case IDC_EBO_BOO__MNU_BAR_TOOL: {
					INT sz_sub = 0;
					const UINT* pSubs = CMenuBar_Spec::GetToolMenuSubIds(sz_sub);
					if (n_sub_ptr < sz_sub) {
						CMenuBar_Map().insert(
							::std::make_pair(sub_menu_.m_hMenu, pSubs[n_sub_ptr])
						);
					}
				} break;
			case IDC_EBO_BOO__MNU_BAR_VIEW: {
					INT sz_sub = 0;
					const UINT* pSubs = CMenuBar_Spec::GetViewMenuSubIds(sz_sub);
					if (n_sub_ptr < sz_sub) {
						CMenuBar_Map().insert(
							::std::make_pair(sub_menu_.m_hMenu, pSubs[n_sub_ptr])
						);
					}
				} break;
			}
			n_sub_ptr += 1;
		}
	}
}

VOID     CMenuBar::UpdateState(const HMENU _sub_menu, const INT _sub_ndx)
{
	_sub_ndx;
	if (NULL == CMenuBar_Handle())
		return;

	t_sub_menu_map::const_iterator it_ = CMenuBar_Map().find(_sub_menu);
	if (it_ == CMenuBar_Map().end())
		return;
	
	switch (it_->second)
	{
	case IDC_EBO_BOO__MNU_BAR_FILE  : CMenuBar_Update::UpdateMnuFile(_sub_menu); break ;
	case IDC_EBO_BOO__MNU_BAR_HELP  : CMenuBar_Update::UpdateMnuHelp(_sub_menu); break ;
	case IDC_EBO_BOO__MNU_BAR_REPS  : CMenuBar_Update::UpdateMnuReps(_sub_menu, m_main); break ;
	case IDC_EBO_BOO__MNU_BAR_TOOL  : CMenuBar_Update::UpdateMnuTool(_sub_menu); break ;
	case IDC_EBO_BOO__MNU_BAR_VIEW  : CMenuBar_Update::UpdateMnuView(_sub_menu, m_main); break ;
	case IDC_EBO_BOO_SUB_MNU_SEL_POS: CMenuBar_Update::UpdateMnuSide(_sub_menu, m_main); break ;
	case IDC_EBO_BOO_SUB_MNU_TLS_SIZ: CMenuBar_Update::UpdateMnuSize(_sub_menu, m_main); break ;
	case IDC_EBO_BOO_SUB_MNU_CLR_THM: CMenuBar_Update::UpdateMnuThem(_sub_menu, m_main); break ;
	}
}

/////////////////////////////////////////////////////////////////////////////

CMenuBar::operator HMENU(void) const { return CMenuBar_Handle(); }

/////////////////////////////////////////////////////////////////////////////

WORD  CMenuBar::CommandFirst(void) { return IDC_EBO_BOO_MNU_CMD_APP_EXIT;  }
WORD  CMenuBar::CommandLast (void) { return IDC_EBO_BOO_MNU_CMD_HLP_ABU ; }

HMENU CMenuBar::GetRptMenu (void) {

	CMenuHandle mnu_loader;
	mnu_loader.LoadMenu(IDC_EBO_BOO__MNU_BAR_REPS);
	if (mnu_loader.IsMenu() == FALSE)
		return NULL;

	CMenuHandle mnu_popup = mnu_loader.GetSubMenu(0);

	return mnu_popup.m_hMenu;
}

HMENU CMenuBar::GetCtxMenu (void) {
	CMenuHandle mnu_loader;
	if (mnu_loader.IsMenu() == FALSE)
		return NULL;

	CMenuHandle mnu_ctx  = mnu_loader.GetSubMenu(0);
	CMenuHandle mnu_view = mnu_ctx.GetSubMenu(0)   ;

	return mnu_ctx.m_hMenu;
}

HMENU CMenuBar::GetExpMenu (void) {

	CMenuHandle mnu_loader;
	if (mnu_loader.IsMenu() == FALSE)
		return NULL;

	CMenuHandle mnu_popup = mnu_loader.GetSubMenu(0);
	return mnu_popup.m_hMenu;
}

HMENU CMenuBar::GetPrintPrw (void) {

	CMenuHandle mnu_loader;
	if (mnu_loader.IsMenu() == FALSE)
		return NULL;

	CMenuHandle mnu_popup = mnu_loader.GetSubMenu(0);
	return mnu_popup.m_hMenu;
}