#ifndef _EBOBOOTHMMAN_H_E20361D2_3181_4303_9E72_BDCFEC316925_INCLUDED
#define _EBOBOOTHMMAN_H_E20361D2_3181_4303_9E72_BDCFEC316925_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Nov-2019 at 8:24:09a, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack personal account app color theme manager interface declaration file.
*/
#include "shared.uix.gen.clrs.h"
#include "IGridInterface.thm.h"

namespace ebo { namespace boo { namespace gui {

	using ex_ui::draw::CCmpClrs;
	using ex_ui::draw::CCmpClrs_Enum;

	typedef UILayer::ext::ePredefinedTheme eGridTheme;

	class CThemeMan {
	private:
		CCmpClrs      m_clrs;
		UINT          m_them_ndx;

	public:
		 CThemeMan (void);
		~CThemeMan (void);

	public:
		const
		CCmpClrs&     Colours (void) const;
		eGridTheme    GrdTheme(void) const;
		const bool    Is      (const WORD _w_cmd) const;
		HRESULT       Select  (const WORD _w_cmd);

	public:
		CThemeMan& operator =(const WORD _w_cmd);
	public:
		operator const CCmpClrs& (void) const;
	};

}}}

#endif/*_EBOBOOTHMMAN_H_E20361D2_3181_4303_9E72_BDCFEC316925_INCLUDED*/