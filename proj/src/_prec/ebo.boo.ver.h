#ifndef _EBOSVCGOOPANVER_H_6ECEFB93_ABCD_4677_85B0_CEFB85902C6D_INCLUDED
#define _EBOSVCGOOPANVER_H_6ECEFB93_ABCD_4677_85B0_CEFB85902C6D_INCLUDED
/*
	The following macros define the minimum required platform.  The minimum required platform
	is the earliest version of Windows, Internet Explorer etc. that has the necessary features to run 
	your application.  The macros work by enabling all features available on platform versions up to and 
	including the version specified.

	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:16:29p, UTC+7, Phuket, Rawai, Tuesday;
	This is communication data exchange receiver desktop console application version declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack virtual printer console on 5-Sep-2019 at 3:02:45p, UTC+7, Novosibirsk, Tulenina, Thursday;
	Adopted to Ebo Pack notify Google service system tray app on 7-Sep-2019 at 1:31:59p, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Pack accounting app on 13-Oct-2019 at 9:19:02a, UTC+7, Novosibirsk, Light Coloured, Sunday;
*/

#ifndef WINVER                 // Specifies that the minimum required platform is Windows Vista.
#define WINVER         0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT           // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT   0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS         // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600  // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE              // Specifies that the minimum required platform is Internet Explorer 9.0 (Windows Vista, service pack 2).
#define _WIN32_IE      0x0900  // Change this to the appropriate value to target other versions of IE.
#endif

#endif/*_EBOSVCGOOPANVER_H_6ECEFB93_ABCD_4677_85B0_CEFB85902C6D_INCLUDED*/