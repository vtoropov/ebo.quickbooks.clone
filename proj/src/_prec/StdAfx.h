#ifndef _STDAFX_H_A2AD1503_5555_ABCD_9B38_341E6E697B7E_INCLUDED
#define _STDAFX_H_A2AD1503_5555_ABCD_9B38_341E6E697B7E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:17:57p, UTC+7, Phuket, Rawai, Tuesday;
	This is communication data exchange receiver desktop console app precompiled header declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack virtual printer console project on 5-Sep-2019 at 3:07:06p, UTC+7, Novosibirsk, Tulenina, Thursday;
	Adopted to Ebo Pack notify Google service system tray app on 7-Sep-2019 at 1:31:59p, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Pack personal account app on 13-Oct-2019 at 9:23:29a, UTC+7, Novosibirsk, Light Coloured, Sunday;
*/
#include "ebo.boo.ver.h"

#ifndef STRICT
#define STRICT
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe
#pragma warning(disable: 4458)  // declaration of '{func_name}' hides class member (GDI+)

#include <atlbase.h>
#include <atlstr.h>             // important order, this file must be included before any includes of the WTL headers

#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>
#include <atlctrls.h>
#include <atldlgs.h>

#include <comdef.h>
#include <comutil.h>

using namespace ATL;

#ifdef _DEBUG
	#define _ATL_DEBUG_INTERFACES
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

#include <vector>
#include <map>
#include <time.h>
#include <typeinfo>

#if (0)
#if defined WIN64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined WIN32
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

#include "shared.uix.ctrl.sta.h"
#include "ebo.boo.frm.main.sta.h"
#include "ebo.boo.thm.man.h"

namespace _out {

	using ebo::boo::gui::CMainForm_Sta;
	using ebo::boo::gui::CThemeMan;

	CMainForm_Sta& MainFrmSta(void);     // main frame status bar control reference;
	CThemeMan&     ThemeMan  (void);
	VOID   SetIdleMessage(VOID);
}

#if (0)
#else
#define __nothing
#define __empty_ln
#define __no_args
#endif

#pragma comment(lib, "_ntfs_v15.lib"    )
#pragma comment(lib, "_registry_v15.lib")
#pragma comment(lib, "_uix.draw_v15.lib")
#pragma comment(lib, "_uix.ctrl_v15.lib")
#pragma comment(lib, "_uix.frms_v15.lib")
#pragma comment(lib, "_user.32_v15.lib" )
#pragma comment(lib, "_web_v15.lib"     )
#pragma comment(lib, "_xml_v15.lib"     )
#pragma comment(lib, "_generic.sql_v15.lib"   )
#pragma comment(lib, "_generic.stg_v15.lib"   )
#pragma comment(lib, "ebo.boo.acc.rpt_v15.lib")
#pragma comment(lib, "ebo.boo.acc.stg_v15.lib")
#pragma comment(lib, "ebo.boo.flex.grid.prior.lib")

#endif/*_STDAFX_H_A2AD1503_5555_ABCD_9B38_341 E6E697B7E_INCLUDED*/