/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:19:45p, UTC+7, Phuket, Rawai, Tuesday;
	This is communication data exchange receiver desktop console app precompiled header creation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack virtual printer console on 5-Sep-2019 at 3:10:17p, UTC+7, Novosibirsk, Tulenina, Thursday;
	Adopted to Ebo Pack notify Google service system tray app on 7-Sep-2019 at 1:31:59p, UTC+7, Novosibirsk, Tulenina, Saturday;
	Adopted to Ebo Pack personal account app on 13-Oct-2019 at 9:25:48a, UTC+7, Novosibirsk, Light Coloured, Sunday;
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

namespace _out {

/////////////////////////////////////////////////////////////////////////////
	CMainForm_Sta& MainFrmSta(void)   { static CMainForm_Sta bar_; return bar_; }
	CThemeMan&     ThemeMan  (void)   { static CThemeMan  the_man; return the_man; }
/////////////////////////////////////////////////////////////////////////////

	VOID   SetIdleMessage(VOID) {
		MainFrmSta().SetInfo(NULL);
	}

}

::WTL::CMessageLoop* get_MessageLoop(){ return _Module.GetMessageLoop(); }
HINSTANCE get_HINSTANCE() { return _Module.GetResourceInstance(); }