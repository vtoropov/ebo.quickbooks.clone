//
// Created by Tech_dog (ebontrop@gmail.com) on 21-Dec-2019 at 5:01:42a, UTC+7, Novosibirsk, Tulenina, Saturday;
// This is Ebo Pack personal account app product and service info dialog identifiers file;
//

#pragma region __prod_and_svc_dlg

#define IDD_EBO_BOO_PRO_SVC_DLG          3401
#define IDR_EBO_BOO_PRO_SVC_DLG_ICO      3403
#define IDC_EBO_BOO_PRO_SVC_DLG_SAV      3405
#define IDC_EBO_BOO_PRO_SVC_DLG_BAN      3407
#define IDR_EBO_BOO_PRO_SVC_DLG_BAN_0    IDC_EBO_BOO_PRO_SVC_DLG_BAN
#define IDR_EBO_BOO_PRO_SVC_DLG_BAN_1    3408
#define IDC_EBO_BOO_PRO_SVC_DLG_TAB      3409
#define IDC_EBO_BOO_PRO_SVC_DLG_SEP      3411

#pragma region __the_1st_page
#define IDD_EBO_BOO_PRO_SVC_1ST_GEN      3421

#define IDC_EBO_BOO_PRO_SVC_1ST_AVA      3423
#define IDR_EBO_BOO_PRO_SVC_1ST_AVA      IDC_EBO_BOO_PRO_SVC_1ST_AVA
#define IDC_EBO_BOO_PRO_SVC_1ST_CAP      3425
#define IDC_EBO_BOO_PRO_SVC_1ST_SEP      3427

#define IDC_EBO_BOO_PRO_SVC_1ST_NAM      3429
#define IDC_EBO_BOO_PRO_SVC_1ST_SKU      3431
#define IDC_EBO_BOO_PRO_SVC_1ST_CAT      3433
#define IDC_EBO_BOO_PRO_SVC_1ST_DSC      3435

#define IDC_EBO_BOO_PRO_SVC_1ST_AVA_1    3437
#define IDR_EBO_BOO_PRO_SVC_1ST_AVA_1    IDC_EBO_BOO_PRO_SVC_1ST_AVA_1
#define IDC_EBO_BOO_PRO_SVC_1ST_CAP_1    3439
#define IDC_EBO_BOO_PRO_SVC_1ST_SEP_1    3441

#define IDC_EBO_BOO_PRO_SVC_1ST_RAT      3443
#define IDC_EBO_BOO_PRO_SVC_1ST_ACC      3445

#pragma endregion

#pragma region __the_2nd_page

#define IDD_EBO_BOO_PRO_SVC_2ND_THU      3451

#define IDC_EBO_BOO_PRO_SVC_2ND_AVA      3453
#define IDR_EBO_BOO_PRO_SVC_2ND_AVA      IDC_EBO_BOO_PRO_SVC_2ND_AVA
#define IDC_EBO_BOO_PRO_SVC_2ND_CAP      3455
#define IDC_EBO_BOO_PRO_SVC_2ND_SEP      3457

#define IDC_EBO_BOO_PRO_SVC_2ND_BUT      3459

#pragma endregion


#pragma region __the_3rd_page
#define IDD_EBO_BOO_PRO_SVC_3RD_GEN      3461

#define IDC_EBO_BOO_PRO_SVC_3RD_AVA      3463
#define IDR_EBO_BOO_PRO_SVC_3RD_AVA      IDC_EBO_BOO_PRO_SVC_3RD_AVA
#define IDC_EBO_BOO_PRO_SVC_3RD_CAP      3465
#define IDC_EBO_BOO_PRO_SVC_3RD_SEP      3467

#define IDC_EBO_BOO_PRO_SVC_3RD_NAM      3469
#define IDC_EBO_BOO_PRO_SVC_3RD_CAT      3471
#define IDC_EBO_BOO_PRO_SVC_3RD_DSC      3473

#define IDC_EBO_BOO_PRO_SVC_3RD_AVA_1    3475
#define IDR_EBO_BOO_PRO_SVC_3RD_AVA_1    IDC_EBO_BOO_PRO_SVC_3RD_AVA_1
#define IDC_EBO_BOO_PRO_SVC_3RD_CAP_1    3477
#define IDC_EBO_BOO_PRO_SVC_3RD_SEP_1    3479

#define IDC_EBO_BOO_PRO_SVC_3RD_RAT      3481
#define IDC_EBO_BOO_PRO_SVC_3RD_ACC      3483

#pragma endregion


#pragma region __the_4th_page

#define IDD_EBO_BOO_PRO_SVC_4TH_THU      3491

#define IDC_EBO_BOO_PRO_SVC_4TH_AVA      3493
#define IDR_EBO_BOO_PRO_SVC_4TH_AVA      IDC_EBO_BOO_PRO_SVC_4TH_AVA
#define IDC_EBO_BOO_PRO_SVC_4TH_CAP      3495
#define IDC_EBO_BOO_PRO_SVC_4TH_SEP      3497

#define IDC_EBO_BOO_PRO_SVC_4TH_BUT      3499

#pragma endregion

#define IDS_EBO_BOO_PRO_SVC_TYP_INV      3501
#define IDS_EBO_BOO_PRO_SVC_TYP_NON      3503
#define IDS_EBO_BOO_PRO_SVC_TYP_SVC      3505
#define IDS_EBO_BOO_PRO_SVC_TYP_BUN      3507

#pragma endregion


