//
// Created by Tech_dog (ebontrop@gmail.com) on 30-Oct-2019 at 4:32:34p, UTC+7, Novosibirsk, Tulenina, Wednesday;
// This is Ebo Pack personal account app report tool bar resource identifiers declaration script file.
//
#pragma once

#pragma region __main_frm_tool_bar

#define IDC_EBO_BOO_TOO_RPT         1201

#define IDR_EBO_BOO_RPT_IMG_x24     1201
#define IDR_EBO_BOO_RPT_IMG_x32     1203
#define IDR_EBO_BOO_RPT_IMG_x48     1205
#define IDR_EBO_BOO_RPT_IMG_x64     1207
#define IDR_EBO_BOO_RPT_IMG_x24_d   1209
#define IDR_EBO_BOO_RPT_IMG_x32_d   1211
#define IDR_EBO_BOO_RPT_IMG_x48_d   1213
#define IDR_EBO_BOO_RPT_IMG_x64_d   1215

#define IDS_EBO_BOO_CMD_VIEW_MAIL   1211
#define IDS_EBO_BOO_CMD_VIEW_XCEL   1213
#define IDS_EBO_BOO_CMD_VIEW_PRN    1217
#define IDS_EBO_BOO_CMD_VIEW_PRE    1219

#pragma endregion


