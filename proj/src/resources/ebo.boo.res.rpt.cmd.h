//
// Created by Tech_dog (ebontrop@gmail.com) on 31-Oct-2019 at 8:13:25a, UTC+7, Novosibirsk, Tulenina, Thursday;
// This is Ebo Pack personal account app report view command identifiers declaration script file.
//
#pragma once

#pragma region __rpt_view_cmds
#define IDC_EBO_BOO_CMD_VIEW_MAIL   1201
#define IDC_EBO_BOO_CMD_VIEW_XCEL   1203
#define IDC_EBO_BOO_CMD_VIEW_PRN    1205
#define IDC_EBO_BOO_CMD_VIEW_PRE    1207
#define IDC_EBO_BOO_RPT_CTRL_RBAR   1209
#pragma endregion
