//
//   Created by Tech_dog (ebontrop@gmail.com) on 7-Jan-2020 at 11:42:07a, UTC+7, Novosibirsk, Tulenina, Tuesday;
//   This is Ebo Pack personal account app user password dialog UI control identifier declaration file.
//

#pragma region __pwd_dlg

#define IDD_EBO_BOO_PWD_DLG_0        4000
#define IDD_EBO_BOO_PWD_DLG_1        4001
#define IDR_EBO_BOO_PWD_DLG_ICO      4003
#define IDC_EBO_BOO_PWD_DLG_SAV      4005
#define IDC_EBO_BOO_PWD_DLG_BAN      4007
#define IDR_EBO_BOO_PWD_DLG_BAN      IDC_EBO_BOO_PWD_DLG_BAN

#pragma endregion

#pragma region __pwd_dlg_1st_page

#define IDD_EBO_OUT_PWD_1ST_PAG      4009

#define IDC_EBO_BOO_PWD_1ST_PWD      4011
#define IDC_EBO_BOO_PWD_1ST_WRN      4013
#define IDR_EBO_BOO_PWD_1ST_WRN      IDC_EBO_BOO_PWD_1ST_WRN
#define IDC_EBO_BOO_PWD_1ST_MSG      4015

#pragma endregion

#pragma region __pwd_dlg_2nd_page

#define IDD_EBO_OUT_PWD_2ND_PAG      4017

#define IDC_EBO_BOO_PWD_2ND_PWD_0    4019
#define IDC_EBO_BOO_PWD_2ND_PWD_1    4020
#define IDC_EBO_BOO_PWD_2ND_WRN      4021
#define IDR_EBO_BOO_PWD_2ND_WRN      IDC_EBO_BOO_PWD_2ND_WRN
#define IDC_EBO_BOO_PWD_2ND_MSG      4023

#pragma endregion


