//
// Created by Tech_dog (ebontrop@gmail.com) on 9-Dec-2019 at 8:48:28p, UTC+7, Novosibirsk, Tulenina, Monday;
// This is Ebo Pack personal account desktop app tax dialog UI controls' identifier declaration file.
//

#pragma region __tax_dlg

#define IDD_EBO_BOO_TAX_DLG          3201
#define IDR_EBO_BOO_TAX_DLG_ICO      3203
#define IDC_EBO_BOO_TAX_DLG_SAV      3205
#define IDC_EBO_BOO_TAX_DLG_BAN      3207
#define IDR_EBO_BOO_TAX_DLG_BAN      IDC_EBO_BOO_TAX_DLG_BAN
#define IDC_EBO_BOO_TAX_DLG_TAB      3209
#define IDC_EBO_BOO_TAX_DLG_SEP      3211

#pragma region __tax_1st_page
#define IDD_EBO_BOO_TAX_PAG_1ST      3251
#define IDC_EBO_BOO_TAX_PAG_1ST_AVA  3253
#define IDR_EBO_BOO_TAX_PAG_1ST_AVA  IDC_EBO_BOO_TAX_PAG_1ST_AVA
#define IDC_EBO_BOO_TAX_PAG_1ST_CAP  3255
#define IDC_EBO_BOO_TAX_PAG_1ST_SEP  3257

#define IDC_EBO_BOO_TAX_PAG_1ST_NAM  3259
#define IDC_EBO_BOO_TAX_PAG_1ST_DSC  3261
#define IDC_EBO_BOO_TAX_PAG_1ST_AGY  3263
#define IDC_EBO_BOO_TAX_PAG_1ST_SAL  3265
#define IDC_EBO_BOO_TAX_PAG_1ST_PUR  3267
#pragma endregion

#pragma region __tax_2nd_page
#define IDD_EBO_BOO_TAX_PAG_2ND      3281

#define IDC_EBO_BOO_TAX_PAG_2ND_AVA  3283
#define IDR_EBO_BOO_TAX_PAG_2ND_AVA  IDC_EBO_BOO_TAX_PAG_2ND_AVA
#define IDC_EBO_BOO_TAX_PAG_2ND_CAP  3285
#define IDC_EBO_BOO_TAX_PAG_2ND_SEP  3287

#define IDC_EBO_BOO_TAX_PAG_2ND_NAM  3289
#define IDC_EBO_BOO_TAX_PAG_2ND_DSC  3291

#define IDC_EBO_BOO_TAX_PAG_2ND_RAT  3299
#define IDC_EBO_BOO_TAX_PAG_2ND_APP  3301

#pragma endregion

#pragma region __tax_3rd_page
#define IDD_EBO_BOO_TAX_PAG_3RD      3341
#define IDC_EBO_BOO_TAX_PAG_3RD_AVA  3343
#define IDR_EBO_BOO_TAX_PAG_3RD_AVA  IDC_EBO_BOO_TAX_PAG_3RD_AVA
#define IDC_EBO_BOO_TAX_PAG_3RD_CAP  3345
#define IDC_EBO_BOO_TAX_PAG_3RD_SEP  3347

#define IDC_EBO_BOO_TAX_PAG_3RD_NAM  3349
#define IDC_EBO_BOO_TAX_PAG_3RD_DSC  3351
#define IDC_EBO_BOO_TAX_PAG_3RD_AGY  3353
#define IDC_EBO_BOO_TAX_PAG_3RD_REG  3355
#define IDC_EBO_BOO_TAX_PAG_3RD_PRD  3357
#define IDC_EBO_BOO_TAX_PAG_3RD_FRQ  3359
#define IDC_EBO_BOO_TAX_PAG_3RD_RPT  3361
#define IDC_EBO_BOO_TAX_PAG_3RD_SAL  3363
#define IDC_EBO_BOO_TAX_PAG_3RD_PUR  3365
#pragma endregion

#pragma endregion


