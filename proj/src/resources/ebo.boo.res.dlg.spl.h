//
// Created by Tech_dog (ebontrop@gmail.com) on 8-Dec-2019 at 4:30:22p, UTC+7, Novosibirsk, Tulenina, Sunday;
// This is Ebo Pack personal account desktop app supplier dialog UI controls' identifier declaration file.
//

#pragma region __supplier_dlg

#define IDD_EBO_BOO_SPL_DLG          2901
#define IDR_EBO_BOO_SPL_DLG_ICO      2903
#define IDC_EBO_BOO_SPL_DLG_SAV      2905
#define IDC_EBO_BOO_SPL_DLG_BAN      2907
#define IDR_EBO_BOO_SPL_DLG_BAN      IDC_EBO_BOO_SPL_DLG_BAN
#define IDC_EBO_BOO_SPL_DLG_TAB      2909
#define IDC_EBO_BOO_SPL_DLG_SEP      2911

#pragma region __supplier_1st_page
#define IDD_EBO_BOO_SPL_PAG_1ST      2931
#define IDC_EBO_BOO_SPL_PAG_1ST_AVA  2933
#define IDR_EBO_BOO_SPL_PAG_1ST_AVA  IDC_EBO_BOO_SPL_PAG_1ST_AVA
#define IDC_EBO_BOO_SPL_PAG_1ST_CAP  2935
#define IDC_EBO_BOO_SPL_PAG_1ST_SEP  2937

#define IDC_EBO_BOO_SPL_PAG_1ST_TTL  2939
#define IDC_EBO_BOO_SPL_PAG_1ST_FNM  2941
#define IDC_EBO_BOO_SPL_PAG_1ST_MNM  2943
#define IDC_EBO_BOO_SPL_PAG_1ST_LNM  2945
#define IDC_EBO_BOO_SPL_PAG_1ST_SFX  2947

#define IDC_EBO_BOO_SPL_PAG_1ST_SP1  2949
#define IDC_EBO_BOO_SPL_PAG_1ST_AV1  2951
#define IDR_EBO_BOO_SPL_PAG_1ST_AV1  IDC_EBO_BOO_SPL_PAG_1ST_AV1
#define IDC_EBO_BOO_SPL_PAG_1ST_CA1  2953

#define IDC_EBO_BOO_SPL_PAG_1ST_COM  2955
#define IDC_EBO_BOO_SPL_PAG_1ST_NIC  2957

#pragma endregion

#pragma region __supplier_2nd_page
#define IDD_EBO_BOO_SPL_PAG_2ND      2971
#define IDC_EBO_BOO_SPL_PAG_2ND_AVA  2973
#define IDR_EBO_BOO_SPL_PAG_2ND_AVA  IDC_EBO_BOO_SPL_PAG_2ND_AVA
#define IDC_EBO_BOO_SPL_PAG_2ND_CAP  2975
#define IDC_EBO_BOO_SPL_PAG_2ND_SEP  2977

#define IDC_EBO_BOO_SPL_PAG_2ND_ACC  2979
#define IDC_EBO_BOO_SPL_PAG_2ND_BIZ  2981

#pragma endregion

#pragma region __supplier_3rd_page

#define IDD_EBO_BOO_SPL_PAG_3RD      3001
#define IDC_EBO_BOO_SPL_PAG_3RD_AVA  3003
#define IDR_EBO_BOO_SPL_PAG_3RD_AVA  IDC_EBO_BOO_SPL_PAG_3RD_AVA
#define IDC_EBO_BOO_SPL_PAG_3RD_CAP  3005
#define IDC_EBO_BOO_SPL_PAG_3RD_SEP  3007

#define IDC_EBO_BOO_SPL_PAG_3RD_EML  3009
#define IDC_EBO_BOO_SPL_PAG_3RD_PHN  3011
#define IDC_EBO_BOO_SPL_PAG_3RD_MOB  3013
#define IDC_EBO_BOO_SPL_PAG_3RD_FAX  3015
#define IDC_EBO_BOO_SPL_PAG_3RD_OTH  3017
#define IDC_EBO_BOO_SPL_PAG_3RD_WEB  3019

#pragma endregion

#pragma region __supplier_4th_page

#define IDD_EBO_BOO_SPL_PAG_4TH      3031
#define IDC_EBO_BOO_SPL_PAG_4TH_AVA  3033
#define IDR_EBO_BOO_SPL_PAG_4TH_AVA  IDC_EBO_BOO_SPL_PAG_4TH_AVA
#define IDC_EBO_BOO_SPL_PAG_4TH_CAP  3035
#define IDC_EBO_BOO_SPL_PAG_4TH_SEP  3037

#define IDC_EBO_BOO_SPL_PAG_4TH_SE0  3039
#define IDC_EBO_BOO_SPL_PAG_4TH_CT0  3041
#define IDC_EBO_BOO_SPL_PAG_4TH_ST0  3043
#define IDC_EBO_BOO_SPL_PAG_4TH_PS0  3045
#define IDC_EBO_BOO_SPL_PAG_4TH_CN0  3047

#pragma endregion

#pragma region __supplier_5th_page

#define IDD_EBO_BOO_SPL_PAG_5TH      3081
#define IDC_EBO_BOO_SPL_PAG_5TH_AVA  3083
#define IDR_EBO_BOO_SPL_PAG_5TH_AVA  IDC_EBO_BOO_SPL_PAG_5TH_AVA
#define IDC_EBO_BOO_SPL_PAG_5TH_CAP  3085
#define IDC_EBO_BOO_SPL_PAG_5TH_SEP  3087

#define IDC_EBO_BOO_SPL_PAG_5TH_MTH  3089
#define IDC_EBO_BOO_SPL_PAG_5TH_DLV  3091
#define IDC_EBO_BOO_SPL_PAG_5TH_TRM  3093

#define IDC_EBO_BOO_SPL_PAG_5TH_AV1  3095
#define IDR_EBO_BOO_SPL_PAG_5TH_AV1  IDC_EBO_BOO_SPL_PAG_5TH_AV1
#define IDC_EBO_BOO_SPL_PAG_5TH_CA1  3097
#define IDC_EBO_BOO_SPL_PAG_5TH_SP1  3099

#define IDC_EBO_BOO_SPL_PAG_5TH_BAL  3101
#define IDC_EBO_BOO_SPL_PAG_5TH_DAT  3103

#define IDC_EBO_BOO_SPL_PAG_5TH_AV2  3105
#define IDR_EBO_BOO_SPL_PAG_5TH_AV2  IDC_EBO_BOO_SPL_PAG_5TH_AV2
#define IDC_EBO_BOO_SPL_PAG_5TH_CA2  3107
#define IDC_EBO_BOO_SPL_PAG_5TH_SP2  3109

#define IDC_EBO_BOO_SPL_PAG_5TH_LNG  3110

#pragma endregion

#pragma region __supplier_6th_page

#define IDD_EBO_BOO_SPL_PAG_6TH      3131
#define IDC_EBO_BOO_SPL_PAG_6TH_AVA  3133
#define IDR_EBO_BOO_SPL_PAG_6TH_AVA  IDC_EBO_BOO_SPL_PAG_6TH_AVA
#define IDC_EBO_BOO_SPL_PAG_6TH_CAP  3135
#define IDC_EBO_BOO_SPL_PAG_6TH_SEP  3137

#define IDC_EBO_BOO_SPL_PAG_6TH_NOT  3139
#define IDC_EBO_BOO_SPL_PAG_6TH_LST  3141
#define IDR_EBO_BOO_SPL_PAG_6TH_LST  IDC_EBO_BOO_SPL_PAG_6TH_LST
#define IDC_EBO_BOO_SPL_PAG_6TH_ADD  3143
#define IDC_EBO_BOO_SPL_PAG_6TH_EDT  3145
#define IDC_EBO_BOO_SPL_PAG_6TH_DEL  3147

#pragma endregion

#pragma endregion



