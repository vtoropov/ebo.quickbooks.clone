//
// Created by Tech_dog (ebontrop@gmail.com) on 25-Nov-2019 at 10:21:29a, UTC+7, Novosibirsk, Tulenina, Monday;
// This is Ebo Pack personal account desktop app employee dialog UI controls' identifier declaration file.
//

#pragma region __employee_dlg

#define IDD_EBO_BOO_EMP_DLG          2401
#define IDR_EBO_BOO_EMP_DLG_ICO      2403
#define IDC_EBO_BOO_EMP_DLG_SAV      2405
#define IDC_EBO_BOO_EMP_DLG_BAN      2407
#define IDR_EBO_BOO_EMP_DLG_BAN      IDC_EBO_BOO_EMP_DLG_BAN
#define IDC_EBO_BOO_EMP_DLG_TAB      2409
#define IDC_EBO_BOO_EMP_DLG_SEP      2411

#pragma region __employee_1st_page
#define IDD_EBO_BOO_EMP_PAG_1ST      2451
#define IDC_EBO_BOO_EMP_PAG_1ST_AVA  2453
#define IDR_EBO_BOO_EMP_PAG_1ST_AVA  IDC_EBO_BOO_EMP_PAG_1ST_AVA
#define IDC_EBO_BOO_EMP_PAG_1ST_CAP  2455
#define IDC_EBO_BOO_EMP_PAG_1ST_SEP  2457
#define IDC_EBO_BOO_EMP_PAG_1ST_TTL  2459
#define IDC_EBO_BOO_EMP_PAG_1ST_FNM  2461
#define IDC_EBO_BOO_EMP_PAG_1ST_LNM  2463
#define IDC_EBO_BOO_EMP_PAG_1ST_DSP  2465
#define IDC_EBO_BOO_EMP_PAG_1ST_SP2  2467
#define IDC_EBO_BOO_EMP_PAG_1ST_AV2  2469
#define IDR_EBO_BOO_EMP_PAG_1ST_AV2  IDC_EBO_BOO_EMP_PAG_1ST_AV2
#define IDC_EBO_BOO_EMP_PAG_1ST_NIC  2471
#pragma endregion

#pragma region __employee_2nd_page
#define IDD_EBO_BOO_EMP_PAG_2ND      2501

#define IDC_EBO_BOO_EMP_PAG_2ND_AVA  2503
#define IDR_EBO_BOO_EMP_PAG_2ND_AVA  IDC_EBO_BOO_EMP_PAG_2ND_AVA
#define IDC_EBO_BOO_EMP_PAG_2ND_CAP  2505
#define IDC_EBO_BOO_EMP_PAG_2ND_SEP  2507

#define IDC_EBO_BOO_EMP_PAG_2ND_IDN  2509
#define IDC_EBO_BOO_EMP_PAG_2ND_IDE  2511

#define IDC_EBO_BOO_EMP_PAG_2ND_AV1  2513
#define IDR_EBO_BOO_EMP_PAG_2ND_AV1  IDC_EBO_BOO_EMP_PAG_2ND_AV1
#define IDC_EBO_BOO_EMP_PAG_2ND_CA1  2515
#define IDC_EBO_BOO_EMP_PAG_2ND_SE1  2517

#define IDC_EBO_BOO_EMP_PAG_2ND_HIR  2519
#define IDC_EBO_BOO_EMP_PAG_2ND_FIR  2521

#define IDC_EBO_BOO_EMP_PAG_2ND_AV2  2523
#define IDR_EBO_BOO_EMP_PAG_2ND_AV2  IDC_EBO_BOO_EMP_PAG_2ND_AV2
#define IDC_EBO_BOO_EMP_PAG_2ND_CA2  2525
#define IDC_EBO_BOO_EMP_PAG_2ND_SE2  2527

#define IDC_EBO_BOO_EMP_PAG_2ND_GEN  2529
#define IDC_EBO_BOO_EMP_PAG_2ND_DOB  2531
#define IDC_EBO_BOO_EMP_PAG_2ND_NOT  2533
#pragma endregion

#pragma region __employee_3rd_page
#define IDD_EBO_BOO_EMP_PAG_3RD      2541
#define IDC_EBO_BOO_EMP_PAG_3RD_AVA  2543
#define IDR_EBO_BOO_EMP_PAG_3RD_AVA  IDC_EBO_BOO_EMP_PAG_3RD_AVA
#define IDC_EBO_BOO_EMP_PAG_3RD_CAP  2545
#define IDC_EBO_BOO_EMP_PAG_3RD_SEP  2547
#define IDC_EBO_BOO_EMP_PAG_3RD_EMA  2549
#define IDC_EBO_BOO_EMP_PAG_3RD_PHO  2551
#define IDC_EBO_BOO_EMP_PAG_3RD_MOB  2553
#define IDC_EBO_BOO_EMP_PAG_3RD_IMG  2555
#define IDR_EBO_BOO_EMP_PAG_3RD_BAN  IDC_EBO_BOO_EMP_PAG_3RD_IMG
#define IDC_EBO_BOO_EMP_PAG_3RD_INF  2557
#pragma endregion

#pragma region __employee_4th_page
#define IDD_EBO_BOO_EMP_PAG_4TH      2561
#define IDC_EBO_BOO_EMP_PAG_4TH_AVA  2563
#define IDR_EBO_BOO_EMP_PAG_4TH_AVA  IDC_EBO_BOO_EMP_PAG_4TH_AVA
#define IDC_EBO_BOO_EMP_PAG_4TH_CAP  2565
#define IDC_EBO_BOO_EMP_PAG_4TH_SEP  2567
#define IDC_EBO_BOO_EMP_PAG_4TH_BUT  2569
#pragma endregion

#pragma region __employee_5th_page
#define IDD_EBO_BOO_EMP_PAG_5TH      2581
#define IDC_EBO_BOO_EMP_PAG_5TH_AVA  2583
#define IDR_EBO_BOO_EMP_PAG_5TH_AVA  IDC_EBO_BOO_EMP_PAG_5TH_AVA
#define IDC_EBO_BOO_EMP_PAG_5TH_CAP  2585
#define IDC_EBO_BOO_EMP_PAG_5TH_SEP  2587
#define IDC_EBO_BOO_EMP_PAG_5TH_STR  2599
#define IDC_EBO_BOO_EMP_PAG_5TH_CIT  2591
#define IDC_EBO_BOO_EMP_PAG_5TH_STA  2593
#define IDC_EBO_BOO_EMP_PAG_5TH_POS  2595
#define IDC_EBO_BOO_EMP_PAG_5TH_COU  2597
#pragma endregion

#pragma endregion


