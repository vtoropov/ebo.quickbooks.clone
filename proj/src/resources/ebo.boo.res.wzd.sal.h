//
//   Created by Tech_dog (ebontrop@gmail.com) on 31-Dec-2019 at 3:38:47p, UTC+7, Novosibirsk, Tulenina, Tuesday;
//   This is Ebo Pack personal account app sale data wizard UI control identifier declaration file.
//

#pragma region __wzd_dlg

#define IDD_EBO_BOO_SAL_CST_WZD          3601
#define IDC_EBO_BOO_SAL_CST_WZD_SEP      3603

#pragma region __wzd_left_ban
#define IDC_EBO_BOO_SAL_CST_WZD_BAN      3605
#define IDR_EBO_BOO_SAL_CST_WZD_BAN      IDC_EBO_BOO_SAL_CST_WZD_BAN
#pragma endregion

#define IDR_EBO_BOO_SAL_CST_FRM_ICO      3607
#define IDC_EBO_BOO_SAL_CST_WZD_TAB      3609

#pragma endregion

#pragma region __wzd_1st_page

#define IDD_EBO_BOO_SAL_CST_1ST_PAG      3621
#define IDC_EBO_SAL_CST_PAG_1ST_AVA      3623
#define IDR_EBO_SAL_CST_PAG_1ST_AVA      IDC_EBO_SAL_CST_PAG_1ST_AVA
#define IDC_EBO_SAL_CST_PAG_1ST_CAP      3625

#define IDC_EBO_SAL_CST_PAG_1ST_TP0      3627
#define IDC_EBO_SAL_CST_PAG_1ST_TP1      3629
#define IDC_EBO_SAL_CST_PAG_1ST_TP2      3631
#define IDC_EBO_SAL_CST_PAG_1ST_TP3      3633

#define IDC_EBO_SAL_CST_PAG_1ST_AV1      3635
#define IDR_EBO_SAL_CST_PAG_1ST_AV1      IDC_EBO_SAL_CST_PAG_1ST_AV1
#define IDC_EBO_SAL_CST_PAG_1ST_CA1      3637
#define IDS_EBO_SAL_CST_PAG_1ST_INF_0    3639

#pragma endregion

#pragma region __wzd_2nd_page

#define IDD_EBO_BOO_SAL_CST_2ND_PAG      3651

#define IDC_EBO_SAL_CST_PAG_2ND_AVA      3653
#define IDR_EBO_SAL_CST_PAG_2ND_AVA      IDC_EBO_SAL_CST_PAG_2ND_AVA
#define IDC_EBO_SAL_CST_PAG_2ND_CAP      3655

#define IDC_EBO_SAL_CST_PAG_2ND_NUM      3657
#define IDC_EBO_SAL_CST_PAG_2ND_LST      3659
#define IDC_EBO_SAL_CST_PAG_2ND_NEW      3661
#define IDC_EBO_SAL_CST_PAG_2ND_TRM      3663
#define IDC_EBO_SAL_CST_PAG_2ND_DAT      3665
#define IDC_EBO_SAL_CST_PAG_2ND_DUE      3667

#define IDC_EBO_SAL_CST_PAG_2ND_AV1      3669
#define IDR_EBO_SAL_CST_PAG_2ND_AV1      IDC_EBO_SAL_CST_PAG_2ND_AV1
#define IDC_EBO_SAL_CST_PAG_2ND_CA1      3671

#define IDC_EBO_SAL_CST_PAG_2ND_ITM      3673
#define IDC_EBO_SAL_CST_PAG_2ND_ADD      3675
#define IDC_EBO_SAL_CST_PAG_2ND_EDT      3677
#define IDC_EBO_SAL_CST_PAG_2ND_DEL      3679

#define IDC_EBO_SAL_CST_PAG_2ND_TTL      3681

#pragma endregion

#pragma region __wzd_3rd_page

#define IDD_EBO_BOO_SAL_CST_3RD_PAG      3691

#define IDC_EBO_SAL_CST_PAG_3RD_AVA      3693
#define IDR_EBO_SAL_CST_PAG_3RD_AVA      IDC_EBO_SAL_CST_PAG_3RD_AVA
#define IDC_EBO_SAL_CST_PAG_3RD_CAP      3695

#define IDC_EBO_SAL_CST_PAG_3RD_MSG      3697
#define IDC_EBO_SAL_CST_PAG_3RD_STA      3699
#define IDC_EBO_SAL_CST_PAG_3RD_SND      3700

#define IDC_EBO_SAL_CST_PAG_3RD_AV1      3701
#define IDR_EBO_SAL_CST_PAG_3RD_AV1      IDC_EBO_SAL_CST_PAG_3RD_AV1
#define IDC_EBO_SAL_CST_PAG_3RD_CA1      3703

#define IDC_EBO_SAL_CST_PAG_3RD_ATT      3705
#define IDC_EBO_SAL_CST_PAG_3RD_ADD      3707
#define IDC_EBO_SAL_CST_PAG_3RD_EDT      3709
#define IDC_EBO_SAL_CST_PAG_3RD_DEL      3711

#pragma endregion



