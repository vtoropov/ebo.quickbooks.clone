//
// Created by Tech_dog (ebontrop@gmail.com) on 30-Oct-2019 at 9:27:17a, UTC+7, Novosibirsk, Tulenina, Wednesday;
// This is Ebo Pack personal account app menu bar identifiers declaration script file.
//
#pragma once

#define IDC_EBO_BOO__MNU_BAR           1001
#define IDC_EBO_BOO__MNU_BAR_FILE      1003
#define IDC_EBO_BOO__MNU_BAR_CUST      1005
#define IDC_EBO_BOO__MNU_BAR_EXPS      1007
#define IDC_EBO_BOO__MNU_BAR_REPS      1009
#define IDC_EBO_BOO__MNU_BAR_SALS      1013
#define IDC_EBO_BOO__MNU_BAR_SUPL      1015
#define IDC_EBO_BOO__MNU_BAR_TAXS      1017
#define IDC_EBO_BOO__MNU_BAR_TOOL      1019
#define IDC_EBO_BOO__MNU_BAR_VIEW      1021
#define IDC_EBO_BOO__MNU_BAR_HELP      1023

#pragma region __main_frm_mnu_file
#define IDC_EBO_BOO_MNU_CMD_APP_EXIT   1103
#pragma endregion

#pragma region __main_frm_mnu_cust
#define IDC_EBO_BOO_MNU_CMD_CST_LST    1151
#define IDC_EBO_BOO_MNU_CMD_CST_ADD    1153
#pragma endregion

#pragma region __main_frm_mnu_exps
#define IDC_EBO_BOO_MNU_CMD_EXP_LST    1161
#define IDC_EBO_BOO_MNU_CMD_EXP_ADD    1163
#pragma endregion

#pragma region __main_frm_mnu_reps
#define IDC_EBO_BOO_MNU_CMD_RPT_PAL    1213
#pragma endregion

#pragma region __main_frm_mnu_sale
#define IDC_EBO_BOO_MNU_CMD_SAL_ADD    1271
#define IDC_EBO_BOO_MNU_CMD_SAL_LST    1273
#define IDC_EBO_BOO_MNU_CMD_PRO_ADD    1275
#define IDC_EBO_BOO_MNU_CMD_SVC_ADD    1277
#pragma endregion

#pragma region __main_frm_mnu_supl
#define IDC_EBO_BOO_MNU_CMD_SUP_ADD    1301
#define IDC_EBO_BOO_MNU_CMD_SUP_LST    1303
#pragma endregion

#pragma region __main_frm_mnu_taxs
#define IDC_EBO_BOO_MNU_CMD_TAX_RAT    1351
#define IDC_EBO_BOO_MNU_CMD_TAX_GRP    1353
#define IDC_EBO_BOO_MNU_CMD_TAX_CST    1355
#define IDC_EBO_BOO_MNU_CMD_TAX_ALL    1357
#define IDC_EBO_BOO_MNU_CMD_TAX_LST    1359
#pragma endregion

#pragma region __main_frm_mnu_tool
#define IDC_EBO_BOO_MNU_CMD_USR_MAN    1413
#define IDC_EBO_BOO_MNU_CMD_USR_PWD    1415

#define IDC_EBO_BOO_SUB_MNU_CLR_THM    1423 // this is important to increase menu item identifier by one for theme index calculation;
#define IDC_EBO_BOO_MNU_CMD_CLR_TH0    1424
#define IDC_EBO_BOO_MNU_CMD_CLR_TH1    1425
#define IDC_EBO_BOO_MNU_CMD_CLR_TH2    1426
#define IDC_EBO_BOO_MNU_CMD_CLR_TH3    1427
#define IDC_EBO_BOO_MNU_CMD_CLR_TH4    1428
#define IDC_EBO_BOO_MNU_CMD_CLR_TH5    1429
#define IDC_EBO_BOO_MNU_CMD_CLR_TH6    1430
#define IDC_EBO_BOO_MNU_CMD_CLR_TH7    1431
#define IDC_EBO_BOO_MNU_CMD_CLR_TH8    1432
#define IDC_EBO_BOO_MNU_CMD_CLR_TH9    1433
#define IDC_EBO_BOO_MNU_CMD_CLR_THA    1434
#define IDC_EBO_BOO_MNU_CMD_CLR_THB    1435
#define IDC_EBO_BOO_MNU_CMD_CLR_THC    1436
#define IDC_EBO_BOO_MNU_CMD_CLR_THD    1437
#define IDC_EBO_BOO_MNU_CMD_CLR_THE    1438
#define IDC_EBO_BOO_MNU_CMD_CLR_THF    1439
#define IDC_EBO_BOO_MNU_CMD_CLR_THG    1440
#define IDC_EBO_BOO_MNU_CMD_CLR_THH    1441
#define IDC_EBO_BOO_MNU_CMD_CLR_THI    1442
#define IDC_EBO_BOO_MNU_CMD_CLR_THJ    1443
#define IDC_EBO_BOO_MNU_CMD_CLR_THK    1444
#pragma endregion

#pragma region __main_frm_mnu_view

#define IDC_EBO_BOO_SUB_MNU_SEL_POS    1451 // selector panel position sub menu identifier;
#define IDC_EBO_BOO_MNU_CMD_SEL_LFT    1453
#define IDC_EBO_BOO_MNU_CMD_SEL_RIT    1455

#define IDC_EBO_BOO_SUB_MNU_TLS_SIZ    1461 // icons size of print toolbar sub menu identifier;
#define IDC_EBO_BOO_MNU_CMD_TLS_EXT    1463
#define IDC_EBO_BOO_MNU_CMD_TLS_LRG    1465
#define IDC_EBO_BOO_MNU_CMD_TLS_MED    1467
#define IDC_EBO_BOO_MNU_CMD_TLS_SML    1469
#pragma endregion

#pragma region __main_frm_mnu_help
#define IDC_EBO_BOO_MNU_CMD_HLP_ABU    1503
#pragma endregion



