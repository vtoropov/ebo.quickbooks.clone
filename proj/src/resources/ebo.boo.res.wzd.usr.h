//
// Created by Tech_dog (ebontrop@gmail.com) on 3-Dec-2018 at 6:50:00a, UTC+7, Novosibirsk, Tulenina, Monday;
// This is USB Drive Detective (bitsphereinc.com) desktop app data wizard UI control identifier declaration file.
// -----------------------------------------------------------------------------
// Adopted to Ebo Pack Google push notification app on 7-Sep-2019 at 7:45:35p, UTC+7, Novosibirsk, Tulenina, Saturday;
// Adopted to Ebo Pack personal account app on 18-Oct-2019 at 11:32:12a, UTC+7, Novosibirsk, Tulenina, Friday;
//

#pragma region __wzd_dlg

#define IDD_EBO_BOO_USR_MAN_WZD          1101
#define IDC_EBO_BOO_USR_MAN_WZD_SEP      1103
#pragma region __wzd_left_ban
#define IDC_EBO_BOO_USR_MAN_WZD_BAN      1105
#define IDR_EBO_BOO_USR_MAN_WZD_BAN      IDC_EBO_BOO_USR_MAN_WZD_BAN
#pragma endregion
#define IDR_EBO_BOO_USR_MAN_FRM_ICO      1107
#define IDC_EBO_BOO_USR_MAN_WZD_TAB      1109

#pragma endregion

#pragma region __pag_usr_lst

#define IDD_EBO_BOO_USR_MAN_1ST_PAG      2301
#pragma region __pag_usr_top_ban
#define IDC_EBO_BOO_USR_PAG_1ST_AVA      2303
#define IDC_EBO_BOO_USR_PAG_1ST_CAP      2305
#define IDS_EBO_BOO_USR_PAG_1ST_CAP      IDC_EBO_BOO_USR_PAG_1ST_CAP
#define IDR_EBO_BOO_USR_PAG_1ST_AVA      IDC_EBO_BOO_USR_PAG_1ST_AVA
#define IDC_EBO_BOO_USR_PAG_1ST_SEQ      2307
#define IDR_EBO_BOO_USR_PAG_1ST_SEQ      IDC_EBO_BOO_USR_PAG_1ST_SEQ
#pragma endregion 
#define IDC_EBO_BOO_USR_PAG_1ST_LST      2309
#define IDC_EBO_BOO_USR_PAG_1ST_GRD      2311
#pragma region __pag_usr_buts
#define IDC_EBO_BOO_USR_PAG_1ST_NEW      2313
#define IDC_EBO_BOO_USR_PAG_1ST_EDT      2315
#define IDC_EBO_BOO_USR_PAG_1ST_DEL      2317
#pragma endregion
#pragma region __pag_usr_inf_ban
#define IDC_EBO_BOO_USR_PAG_1ST_IMG      2319
#define IDR_EBO_BOO_USR_PAG_1ST_INF      IDC_EBO_BOO_USR_PAG_1ST_IMG
#define IDC_EBO_BOO_USR_PAG_1ST_INF      2321
#define IDS_EBO_BOO_USR_PAG_1ST_INF      2323
#define IDR_EBO_BOO_USR_PAG_1ST_WRN      2325
#define IDS_EBO_BOO_USR_PAG_1ST_ADM      2327
#define IDS_EBO_BOO_USR_PAG_1ST_WRN      2329
#pragma endregion
#define IDR_EBO_BOO_USR_PAG_1ST_LST      2331

#pragma region __pag_usr_2nd
#define IDD_EBO_BOO_USR_MAN_2ND_PAG      2351
#define IDC_EBO_BOO_USR_PAG_2ND_AVA      2353
#define IDR_EBO_BOO_USR_PAG_2ND_CAP      2355
#define IDC_EBO_BOO_USR_PAG_2ND_CAP      2357
#define IDS_EBO_BOO_USR_PAG_2ND_CAP      IDC_EBO_BOO_USR_PAG_2ND_CAP
#define IDC_EBO_BOO_USR_PAG_2ND_TVW      2359
#define IDR_EBO_BOO_USR_PAG_2ND_TVW      IDC_EBO_BOO_USR_PAG_2ND_TVW
#define IDC_EBO_BOO_USR_PAG_2ND_DES      2361
#define IDC_EBO_BOO_USR_PAG_2ND_IMG      2363
#define IDR_EBO_BOO_USR_PAG_2ND_IMG      IDC_EBO_BOO_USR_PAG_2ND_IMG
#define IDC_EBO_BOO_USR_PAG_2ND_INF      2365
#define IDS_EBO_BOO_USR_PAG_2ND_INF      2367
#pragma endregion

#pragma endregion


