//
//   Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:21:36p, UTC+7, Phuket, Rawai, Tuesday;
//   This is communication data exchange receiver desktop console application resource declaration file.
//   -----------------------------------------------------------------------------
//   Adopted to Ebo Pack virtual printer console on 5-Sep-2019 at 3:14:50p, UTC+7, Novosibirsk, Tulenina, Thursday;
//   Adopted to Ebo Pack notify Google service system tray app on 7-Sep-2019 at 1:32:02p, UTC+7, Novosibirsk, Tulenina, Saturday;
//   Adopted to Ebo Pack personal account app on 13-Oct-2019 at 9:27:52a, UTC+7, Novosibirsk, Light Coloured, Sunday;
//

#pragma region __generic

#define IDR_EBO_BOO_ICO                1001

#pragma region __main_frm_sta
#define IDC_EBO_BOO_MAIN_FRM_STA       1003
#define IDR_EBO_BOO_MAIN_FRM_STAx16    1005
#define IDR_EBO_BOO_MAIN_FRM_STAx16_pc 1007
#define IDR_EBO_BOO_MAIN_FRM_STAx16_rs 1009
#pragma endregion

#define IDC_EBO_BOO_MAIN_FRM_GND_EMP   1011
#define IDR_EBO_BOO_MAIN_FRM_GND_EMP   IDC_EBO_BOO_MAIN_FRM_GND_EMP

#define IDC_EBO_BOO_MAIN_FRM_SEL       1013
#define IDR_EBO_BOO_MAIN_FRM_SEL       IDC_EBO_BOO_MAIN_FRM_SEL
#define IDC_EBO_BOO_SEL_CMD_HOME       1015
#define IDC_EBO_BOO_SEL_CMD_CUST       1017
#define IDC_EBO_BOO_SEL_CMD_SALE       1019
#define IDC_EBO_BOO_SEL_CMD_EXPS       1021
#define IDC_EBO_BOO_SEL_CMD_EMPL       1023
#define IDC_EBO_BOO_SEL_CMD_REPS       1025
#define IDC_EBO_BOO_SEL_CMD_TAXS       1027
#define IDC_EBO_BOO_SEL_CMD_ACNT       1029
#define IDC_EBO_BOO_SEL_CMD_SETS       1031
#define IDC_EBO_BOO_SEL_CMD_SUPL       1033

#pragma endregion

#include "ebo.boo.res.mnu.bar.h"    // 1001-1011;1151-1155;1253;1303-1334;1401-1419;1503;
#include "ebo.boo.res.rpt.cmd.h"    // 1201-1209
#include "ebo.boo.res.too.bar.h"    // 1201-1219
#if (0)
#include "ebo.boo.res.dlg.prf.h"    // 2101-2227
#include "ebo.boo.res.wzd.usr.h"    // 1101-1109;2301-2367;
#include "ebo.boo.res.dlg.emp.h"    // 2401-2597
#include "ebo.boo.res.dlg.cst.h"    // 2601-2847
#include "ebo.boo.res.dlg.spl.h"    // 2901-3147
#include "ebo.boo.res.dlg.tax.h"    // 3201-3357
#include "ebo.boo.res.dlg.pas.h"    // 3401-3507
#include "ebo.boo.res.wzd.sal.h"    // 3601-3711
#include "ebo.boo.res.dlg.pwd.h"    // 4001-4051
#endif

#include "..\..\..\proj.rpt\interface\templates\ebo.boo.temp.rpt.res.h" // 6661;

#define IDC_EBO_BOO_FLX_GRD_CUST       5001
#define IDC_EBO_BOO_FLX_GRD_SALE       5003
#define IDC_EBO_BOO_FLX_GRD_EXPS       5005
#define IDC_EBO_BOO_FLX_GRD_EMPS       5007
#define IDC_EBO_BOO_FLX_GRD_SUPL       5009
#define IDC_EBO_BOO_FLX_GRD_TAXS       5011
#define IDC_EBO_BOO_FLX_GRD_ACNT       5013

#define IDC_EBO_BOO_VIEW_EMP_NEW       5301
#define IDR_EBO_BOO_VIEW_EMP_NEW       IDC_EBO_BOO_VIEW_EMP_NEW
#define IDC_EBO_BOO_VIEW_EMP_EDT       5303
#define IDR_EBO_BOO_VIEW_EMP_EDT       IDC_EBO_BOO_VIEW_EMP_EDT
#define IDC_EBO_BOO_VIEW_EMP_REM       5305
#define IDR_EBO_BOO_VIEW_EMP_REM       IDC_EBO_BOO_VIEW_EMP_REM
#define IDR_EBO_BOO_VIEW_EMP_EDH       5307





