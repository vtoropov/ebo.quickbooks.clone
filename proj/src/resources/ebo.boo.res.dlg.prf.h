//
// Created by Tech_dog (ebontrop@gmail.com) on 3-Dec-2018 at 6:50:00a, UTC+7, Novosibirsk, Tulenina, Monday;
// This is USB Drive Detective (bitsphereinc.com) desktop app data wizard UI control identifier declaration file.
// -----------------------------------------------------------------------------
// Adopted to Ebo Pack Google push notification app on 14-Sep-2019 at 3:46:28p, UTC+7, Novosibirsk, Tulenina, Saturday;
// Adopted to Ebo Pack personal account app on 22-Oct-2019 at 7:40:18a, UTC+7, Novosibirsk, Light Coloured, Tuesday;
//

#pragma region __profile_dlg

#define IDD_EBO_BOO_USR_PRF_DLG          2101
#define IDR_EBO_BOO_USR_PRF_DLG_ICO      2103
#define IDC_EBO_BOO_USR_PRF_DLG_SAV      2105
#define IDC_EBO_BOO_USR_PRF_DLG_BAN      2107
#define IDR_EBO_BOO_USR_PRF_DLG_BAN      IDC_EBO_BOO_USR_PRF_DLG_BAN
#define IDC_EBO_BOO_USR_PRF_DLG_TAB      2109
#define IDC_EBO_BOO_USR_PRF_DLG_SEP      2111

#pragma region __profile_1st_page
#define IDD_EBO_OUT_USR_PRF_1ST_REG      2201
#define IDC_EBO_BOO_USR_PRF_1ST_NAM      2203
#define IDC_EBO_BOO_USR_PRF_1ST_PWD      2205
#define IDC_EBO_BOO_USR_PRF_1ST_IMG      2207
#define IDR_EBO_BOO_USR_PRF_1ST_IMG      IDC_EBO_BOO_USR_PRF_1ST_IMG
#define IDC_EBO_BOO_USR_PRF_1ST_INF      2209
#define IDS_EBO_BOO_USR_PRF_1ST_INF      IDC_EBO_BOO_USR_PRF_1ST_INF
#define IDC_EBO_BOO_USR_PRF_1ST_CFM      2211
#define IDC_EBO_BOO_USR_PRF_1ST_AVA      2213
#define IDR_EBO_BOO_USR_PRF_1ST_AVA      IDC_EBO_BOO_USR_PRF_1ST_AVA
#define IDC_EBO_BOO_USR_PRF_1ST_CAP      2215
#define IDC_EBO_BOO_USR_PRF_1ST_SEP      2217
#define IDC_EBO_BOO_USR_PRF_1ST_SHW      2219
#define IDC_EBO_BOO_USR_PRF_1ST_ROL      2221
#define IDC_EBO_BOO_USR_PRF_1ST_SP1      2223
#define IDC_EBO_BOO_USR_PRF_1ST_CP1      2225
#define IDC_EBO_BOO_USR_PRF_1ST_STA      2227
#pragma endregion

#pragma endregion


