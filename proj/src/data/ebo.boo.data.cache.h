#ifndef _EBOBOODATACACHE_H_948E0617_A017_43FD_900A_5AFFB1758448_INCLUDED
#define _EBOBOODATACACHE_H_948E0617_A017_43FD_900A_5AFFB1758448_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Oct-2019 at 10:01:29p, UTC+7, Novosibirsk, Light Coloured, Monday;
	This is Ebo Pack personal account app data cache interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.boo.stg.dat.usr.h"

namespace ebo { namespace boo { namespace data {

	using ebo::boo::data::profile::CUser;

	class CUser_Current {
	protected:
		CUser          m_user;   // currently selected user in the wizard;
		CData_Action   m_act ;

	public:
		 CUser_Current (void);
		~CUser_Current (void);

	public:
		const
		CData_Action& Action(void) const;
		CData_Action& Action(void)      ;
		const
		CUser&   Ref (void) const;
		CUser&   Ref (void)      ;
	};

	class CData_Cache {
	public:
		CUser_Current&  User(void) ; // needs to review simultaneous access to the object;
	};
}}}

typedef ebo::boo::data::CData_Cache   TDataCache;

#endif/*_EBOBOODATACACHE_H_948E0617_A017_43FD_900A_5AFFB1758448_INCLUDED*/