#ifndef _EBOBOODATABAS_H_C0BA7739_3236_49AB_9562_6941A451757C_INCLUDED
#define _EBOBOODATABAS_H_C0BA7739_3236_49AB_9562_6941A451757C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Oct-2019 at 2:51:01p, UTC+7, Novosibirsk, Light Coloured, Sunday;
	This is Ebo Pack personal account app data provider base interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "ebo.boo.stg.acc.h"

namespace ebo { namespace boo { namespace data {

	using shared::sys_core::CError;
	using ebo::boo::data::CDbConnector;

	class CProvider_Base {
	protected:
		CError     m_error;
		CDbConnector
		           m_conn ;
	public:
		 CProvider_Base (void) ;
		~CProvider_Base (void) ;

	public:
		HRESULT     Close(void) ;
		TErrorRef   Error(void) const;
		HRESULT     Open (const TDbLocator&) ;
	};

	class CProvider_Master : public CProvider_Base {
	                        typedef CProvider_Base TBase;
	private:
		CDbLocator_Master  m_locator;
	public:
		 CProvider_Master (void);
		~CProvider_Master (void);

	public:
		HRESULT    Open (void); // uses master locator;
	};

}}}

#endif/*_EBOBOODATABAS_H_C0BA7739_3236_49AB_9562_6941A451757C_INCLUDED*/