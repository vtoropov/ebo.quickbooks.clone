/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Oct-2019 at 10:17:05p, UTC+7, Novosibirsk, Light Coloured, Monday;
	This is Ebo Pack personal account app data cache interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.data.cache.h"

using namespace ebo::boo::data;

#include "shared.gen.syn.obj.h"

using namespace shared::sys_core;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace boo { namespace data { namespace _impl {

	TSyncRef CShared_guard(void) {
		static shared::sys_core::CSyncObject guard;
		return guard ;
	}

}}}}
using namespace ebo::boo::data::_impl;
/////////////////////////////////////////////////////////////////////////////

CUser_Current:: CUser_Current(void) {}
CUser_Current::~CUser_Current(void) {}

/////////////////////////////////////////////////////////////////////////////
const
CData_Action&  CUser_Current::Action(void) const { return m_act; }
CData_Action&  CUser_Current::Action(void)       { return m_act; }
const
CUser&   CUser_Current::Ref (void) const { return m_user; }
CUser&   CUser_Current::Ref (void)       { return m_user; }

/////////////////////////////////////////////////////////////////////////////

CUser_Current& CData_Cache::User (void) {
	SAFE_LOCK(CShared_guard());
	static CUser_Current m_current;
	return m_current;
}

/////////////////////////////////////////////////////////////////////////////