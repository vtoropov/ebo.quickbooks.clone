/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Oct-2019 at 4:00:06p, UTC+7, Novosibirsk, Light Coloured, Sunday;
	This is Ebo Pack personal account app data provider base interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.boo.data.bas.h"

using namespace ebo::boo::data;

/////////////////////////////////////////////////////////////////////////////

CProvider_Base:: CProvider_Base(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CProvider_Base::~CProvider_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CProvider_Base::Close(void) {
	m_error << __MODULE__ << S_OK;

	if (m_conn.IsOpen() == false)
		return ((m_error = __DwordToHresult(ERROR_INVALID_STATE)) = _T("Database is not open;"));

	HRESULT hr_ = m_conn.Close();
	if (FAILED(hr_))
		m_error = m_conn.Error();

	return m_error;
}
TErrorRef   CProvider_Base::Error (void) const { return m_error; }

HRESULT     CProvider_Base::Open  (const TDbLocator& _loc) {
	m_error << __MODULE__ << S_OK;

	if (_loc.IsValid() == false)
		return ((m_error = E_INVALIDARG) = _T("Database path is empty or invalid;"));

	if (m_conn.IsOpen() == true)
		return ((m_error = __DwordToHresult(ERROR_INVALID_STATE)) = _T("Database is already open;"));

	HRESULT hr_ = m_conn.Open(_loc);
	if (FAILED(hr_))
		return (m_error = m_conn.Error());

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CProvider_Master:: CProvider_Master(void) { TBase::m_error << __MODULE__ << S_OK >> __MODULE__; }
CProvider_Master::~CProvider_Master(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CProvider_Master::Open (void) {
	m_error << __MODULE__ << S_OK;

	TBase::Open(m_locator);

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////