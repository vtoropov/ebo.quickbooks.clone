#ifndef _IGRIDINTERFACECELL_H_04C105CB_84E7_40B8_A949_AA68853A2A0A_INCLUDED
#define _IGRIDINTERFACECELL_H_04C105CB_84E7_40B8_A949_AA68853A2A0A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Nov-2019 at 11:11:19a, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack flex grid control cell interface declaration file.
*/
#include <string>
typedef ::std::wstring  string_t;

#include "IGridInterface.fmt.h"
#include "IGridInterface.thm.h"
namespace UILayer { namespace ext {

	struct POSITION {
		UINT uRow;
		UINT uCol;
		POSITION(void) : uCol(0), uRow(0) {}
	};

	enum eCellOption {
	     eNone = 0,
	     eSingleBorder = 1
	};
/////////////////////////////////////////////////////////////////////////////
	interface IActiveCellTheme {
		virtual IColorTheme&    Customize(const ePredefinedTheme)      PURE ;    // returns a reference to object that can be customized;
	}; 
/////////////////////////////////////////////////////////////////////////////
	interface ICell {
		virtual VOID           ApplyTheme  (const IColorTheme&)        PURE ;
		virtual UINT           GetColIndex (void)                        = 0;
		virtual IFormatObject* GetFormat   (void)                        = 0;
		virtual DWORD          GetOptions  (void)                const PURE ;
		virtual VOID           GetPlacement(LPRECT lpRect)               = 0;
		virtual VOID           GetPosition (POSITION* pPos)              = 0;
		virtual UINT           GetRowIndex (void)                        = 0;
		virtual VOID           GetText(TCHAR* lpText, UINT iBufferLen)   = 0;
		virtual string_t       GetText(void)                             = 0;
		virtual IActiveCellTheme& GetTheme (void)                      PURE ;
		virtual bool           IsChanged   (ICell* pCell)                = 0;
		virtual bool           IsChanged   (const POSITION& refPos)      = 0;
		virtual bool           IsChanged   (UINT uRow, UINT uCol)        = 0;
		virtual VOID           SetColIndex (UINT uCol)                   = 0;
		virtual VOID           SetFormat   (IFormatObject* pFormatter)   = 0;
		virtual VOID           SetOptions  (const DWORD _value )       PURE ;
		virtual VOID           SetPosition (const POSITION& pos)         = 0;
		virtual VOID           SetPosition (UINT uRow, UINT uCol)        = 0;
		virtual VOID           SetPlacement(const RECT& rect)            = 0;
		virtual VOID           SetRowIndex (UINT uRow)                   = 0;
		virtual VOID           SetText     (const TCHAR* lpText)         = 0;
	};

}}

#endif/*_IGRIDINTERFACECELL_H_04C105CB_84E7_40B8_A949_AA68853A2A0A_INCLUDED*/