#ifndef _IGRIDINTERFACEROW_H_24F1167B_AF85_4FD0_9E55_1739A5447AD9_INCLUDED
#define _IGRIDINTERFACEROW_H_24F1167B_AF85_4FD0_9E55_1739A5447AD9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Nov-2019 at 8:11:07p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack flex grid control row interface declaration file.
*/

namespace UILayer { namespace ext {

	interface IRow
	{
		virtual IRow* Clone    (void)         = 0;
		virtual UINT  GetHeight(void)         = 0;
		virtual UINT  GetID    (void)         = 0;
		virtual UINT  GetIndex (void)         = 0;
		virtual int   GetTop   (void)         = 0;
		virtual void  SetHeight(UINT iHeight) = 0;
		virtual void  SetID    (UINT uID)     = 0;
		virtual void  SetIndex (UINT uIndex)  = 0;
		virtual void  SetTop  (int iTop)      = 0;
	};

}}


#endif/*_IGRIDINTERFACEROW_H_24F1167B_AF85_4FD0_9E55_1739A5447AD9_INCLUDED*/