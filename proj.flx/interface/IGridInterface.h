#ifndef _IGRIDINTERFACEDEF_H_DD819C22_D166_48EE_9ED0_71255526BFE6_INCLUDED
#define _IGRIDINTERFACEDEF_H_DD819C22_D166_48EE_9ED0_71255526BFE6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-May-2008 at 07:50:23p, UTC+3, Rostov-on-Don, Tuesday;
	This is an abstraction layer for Flex Grid control;
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Flex grid control on 4-Nov-2019 at 4:42:42p, UTC+7, Novosibirsk, Tulenina, Monday;
*/
#include <limits>
#include <ComDef.h>
#include "IGridInterface.enu.h"
#include "IGridInterface.fmt.h"
#include "IGridInterface.thm.h"
#include "IGridInterface.col.h"
#include "IGridInterface.cell.h"
#include "IGridInterface.row.h"
#include "IGridInterface.row.sel.h"
#include "IGridInterface.stg.h"
#include "IGridInterface.shared.h"

extern ::WTL::CMessageLoop* get_MessageLoop();
extern HINSTANCE get_HINSTANCE();

namespace data_table
{

enum ESortOrder
{
	SORT_ORDER_ASCENDING  = 1,
	SORT_ORDER_NONE       = 0,
	SORT_ORDER_DESCENDING = -1,
};

enum EFieldStatus
{
	STATUS_NOT_USE         = -5,
	STATUS_EMPTY           = -4,
	STATUS_ERROR           = -3,
	STATUS_NOT_ENOUGH_DATA = -2,
	STATUS_NOT_VERIFIED    = -1,
	STATUS_OK              =  0,
};

enum eCellAttribute
{
	e2_CELL_ATT_NONE                    = 0x00,  // no attribute
	e2_CELL_ATT_BLINK                   = 0x01,  // blinking
	e2_CELL_ATT_FORECOLOR_DEFAULT       = 0x02,  // used a default forecolor
	e2_CELL_ATT_FORECOLOR_FROM_PALETTE  = 0x04,  // used a forecolor from palette
};
}

namespace UILayer
{
	using namespace UILayer::ext;

#pragma region "CONSTANTS"
	
	static const double _NULL_DOUBLE                    = (std::numeric_limits<double>::max)();
	static const long   _NULL_LONG                      = 0xfffffffa;
	static const UINT _NULL_PARAM                       = 0xfffffffe;
	static const UINT _PERSISTENT_DATA                  = 0xfffffffb;
	static const UINT _SUB_COLUMN_ID                    = 1000000;
	static const UINT _UPDATE_FQ_AUTO                   = 0xffffffff;
	static const UINT _COLUMN_WIDTH_AUTO                = 0xffffffff;
	static const UINT _CLMN_DEF_WIDTH                   = 85; // 80 bars x 1px width + left & right margins by 4px - 1px grid

	
	static const UINT _MK_LBUTTON_GRID                  = 0x0100;
	static const UINT _MK_LBUTTON_HEADER                = 0x0200;
	static const UINT _MK_LBUTTON_ROWSEL                = 0x0400;
	static const UINT _MK_LBUTTON_INVCOL                = 0x1000; // mouse click is outside of columns
	static const UINT _MK_LBUTTONDBLCLK_GRID            = 0x1100;

	static const UINT WM_COLUMN_WIDTH_CHANGED           = (WM_USER + 0x10);
	static const UINT WM_COLUMN_WIDTH_AUTOFIT           = (WM_USER + 0x11);
	static const UINT WM_COLUMN_LBUTTONDBLCLK           = (WM_USER + 0x12);
	static const UINT WM_COLUMN_DRAG_AND_DROP           = (WM_USER + 0x13);
	static const UINT WM_COLUMN_BUTTONPRESSED           = (WM_USER + 0x14);
	static const UINT WM_COLUMN_BEFORECTXMENU           = (WM_USER + 0x15);
	static const UINT WM_COLUMN_MENUCOMMANDEX           = (WM_USER + 0x16);

	static const UINT WM_GRID_BEFORE_EDIT               = (WM_USER + 0x17);
	static const UINT WM_GRID_AFTER_EDIT                = (WM_USER + 0x18);
	static const UINT WM_GRID_DELETE                    = (WM_USER + 0x19);
	static const UINT WM_GRID_GET_INPLACE_TOOLTIP       = (WM_USER + 0x20);
	static const UINT WM_DATA_NOT_FOUND                 = (WM_USER + 0x21);

	static const UINT _uiToolTipBufLen = 80;
	static const UINT _uiTextBufLen = 80;
	
	namespace HD_menu
	{
		static const UINT CMD_HALIGN_LEFT               = 0xff00;  // aligns a text to the left side
		static const UINT CMD_HALIGN_CENTER             = 0xff01;  // aligns a text on the column center
		static const UINT CMD_HALIGN_RIGHT              = 0xff02;  // aligns a text to the right side
		static const UINT CMD_F2T                       = 0xff03;  // fits column width to the longest text in it
		static const UINT CMD_HALIGN                    = 0xff04;  // 'Alignment' sub-menu identifier
	};
	namespace RS_menu
	{
		static const UINT CMD_HIDE                      = 0xff20;
		static const UINT CMD_CUT                       = 0xff21;
		static const UINT CMD_COPY                      = 0xff22;
		static const UINT CMD_PASTE                     = 0xff23;
		static const UINT CMD_INSERT                    = 0xff24;
		static const UINT CMD_CLEAR                     = 0xff25;
		static const UINT CMD_DELETE                    = 0xff26;
		static const UINT CMD_NUMBER                    = 0xff27;
	};
#pragma endregion
/////////////////////////////////////////////////////////////////////////////
	
	struct RANGE
	{
		INT iStart;
		INT iEnd;
		RANGE(void) : iStart(0), iEnd(0){}
		RANGE(INT i_min, INT i_max) : iStart(i_min), iEnd(i_max) {}
		__inline bool Has    (INT i_) const { return (iStart <= i_ && i_ <= iEnd); }
		__inline bool IsEmpty(void  ) const { return (0==(iEnd - iStart)); }
	};
/////////////////////////////////////////////////////////////////////////////
	enum eShortcutMenus {
		e2_shortcutToHeader      = 0x1,
		e2_shortcutToRowSelector = 0x2
	};
/////////////////////////////////////////////////////////////////////////////
	interface IGrid;
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
	enum eGridGroupStyles
	{
		e2_GroupStyleNone         = 0, // no style, default
		e2_GroupStyleImmovable    = 1, // group object is not movable
		e2_GroupStyleNotUpdatable = 2  // group object cannot be updated
	};
/////////////////////////////////////////////////////////////////////////////
	interface IColumnGroup
	{
		virtual IColumn* AddColumn(LPCTSTR lpCaption, UINT CID)                     = 0;
		virtual void     Clear(void)                                                = 0;
		virtual void     Destroy(void)                                              = 0;
		virtual IColumn* GetColumn(UINT iColIndex)                                  = 0;
		virtual IColumn* GetColumn(UINT iColID, int iReserved)                      = 0;
		virtual UINT     GetColumnCount(void)                                       = 0;
		virtual UINT     GetID(void)                                                = 0;
		virtual UINT     GetIndex(void)                                             = 0;
		virtual IColumn* GetLast(void)                                              = 0;
		virtual UINT     GetLeft(void)                                              = 0;
		virtual UINT     GetStyle(void)                                             = 0;
		virtual void     GetTitle(TCHAR* lpTitle, UINT iBufferLen)                  = 0;
		virtual LPCTSTR  GetTitle(void)                                             = 0;
		virtual ULONG    GetTag(void)                                               = 0;
		virtual UINT     GetWidth(UINT iFrom = 0)                                   = 0;
		virtual bool     HasStyle(DWORD dwStyle)                                    = 0;
		virtual void     RemoveColumn(IColumn* pColumn)                             = 0;
		virtual void     RemoveColumn(UINT iColumnID)                               = 0;
		virtual void     RemoveColumn(UINT iColIndex, UINT iReserved)               = 0;
		virtual void     SetID(UINT uID)                                            = 0;
		virtual void     SetIndex(UINT uIndex)                                      = 0;
		virtual void     SetLeft(UINT iLeft)                                        = 0;
		virtual void     SetStyle(UINT uStyle, bool bSet = true)                    = 0;
		virtual void     SetTitle(const TCHAR* lpTitle)                             = 0;
		virtual void     SetTag(ULONG ulTag)                                        = 0;
		virtual void     Update(void)                                               = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	interface IColumnHeader
	{
		virtual IColumnGroup* AddGroup(LPCTSTR lpTitle)                             = 0; // creates a new group, adds into internal array and returns its poiter
		virtual void          Clear(void)                                           = 0;
		virtual IColumn*      GetColumn(int uCol)                                   = 0;
		virtual IColumn*      GetColumn(UINT iColID, int nReserved)                 = 0;
		virtual IColumn*      GetColumn(const POINT& pt)                            = 0;
		virtual UINT          GetColumnCount(void)                                  = 0;
		virtual UINT          GetColumnWidth(int iFromIndex=-1, int iMaxWidth = -1) = 0;
		virtual UINT          GetFixedCols(void)                                    = 0;
		virtual IColumnGroup* GetGroup(int iGroupIndex)                             = 0;
		virtual IColumnGroup* GetGroup(UINT iGroupID, int nReserved)                = 0;
		virtual UINT          GetGroupCount(void)                                   = 0;
		virtual UINT          GetHeight(void)                                       = 0;
		virtual UINT          GetLeftCol(void)                                      = 0;
		virtual UINT          GetLines(void)                                        = 0;
		virtual HWND          GetSafeHwnd(void)                                     = 0;
		virtual UINT          GetVisibleCols(void)                                  = 0;
		virtual bool          IsScrollable(void)                                    = 0;
		virtual bool          MoveColumnGroup(IColumnGroup* pWhat, IColumnGroup* pBefore) = 0;
		virtual void          Refresh(void)                                         = 0;
		virtual void          RecalcLayout(bool bWithRefreshOption = true)          = 0;
		virtual void          RemoveGroup(IColumnGroup* pGroup)                     = 0;
		virtual bool          ReplaceColumnGroup(IColumnGroup* pWith, IColumnGroup* pWhat) = 0;
		virtual void          SetFixedCols(UINT _val)                               = 0;
		virtual void          SetHeight(UINT iHeight)                               = 0;
		virtual bool          SetLeftCol(UINT uCol)                                 = 0;
		virtual void          SetTextSize(eTextSizes eSize, bool bWithForceOption=false) = 0;
		virtual void          SetLinesCount(UINT iLinesCount)                       = 0;
		virtual void          UpdateLayout(LPRECT lpRect=0)                         = 0;
	};
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
	interface IRowGroup
	{
		virtual void           Collapse(void)                                             = 0;
		virtual void           Expand(void)                                               = 0;
		virtual long           GetID(void)                                                = 0;
		virtual LPCTSTR        GetName(void)                                              = 0;
		virtual RANGE&         GetRange(void)                                             = 0;
		virtual bool           IsExpanded(void)                                           = 0;
		virtual void           SetID(long lID)                                            = 0;
		virtual void           SetName(LPCTSTR lpName)                                    = 0;
		virtual void           SetRange(RANGE& refRange)                                  = 0;
		virtual void           Toggle(void)                                               = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	interface IRowGroupCollection
	{
		virtual void            Clear(int uRow)     = 0;
		virtual bool            CollapseAll(void)   = 0;
		virtual UINT            Count(void)         = 0;
		virtual bool            ExpandAll(void)     = 0;
		virtual IRowGroup*      GetAt(int uRow)     = 0;
		virtual IRowGroup*      GetAtByID(long lID) = 0;
		virtual IFormatObject4* GetFormat(void)     = 0;
		virtual bool            Insert(int uRow)    = 0;
		virtual bool            IsGroup(int uRow)   = 0;
		virtual bool            Remove(int uRow)    = 0;
		virtual bool            ToggleAll(void)     = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	interface IEventSink
	{
		virtual LRESULT OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)=0;
	};
/////////////////////////////////////////////////////////////////////////////
	interface ICellEditor
	{
		virtual void CancelEdit(void)                             = 0;
		virtual HWND GetSafeHwnd(void)                            = 0;
		virtual bool IsEditMode(void)                             = 0;
		virtual void SetStartTooltip(wchar_t *szTooltipTextW)     = 0;
		virtual void StartEditRowGroup(bool bGetCellText = false) = 0;
		virtual void StartEdit(ICell *pICell, bool bSelectAll)    = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	enum eGridScrollBarShow
	{
		e2_ScrollBarShowNever  = 0,
		e2_ScrollBarShowAuto   = 1,
		e2_ScrollBarShowAlways = 2
	};
/////////////////////////////////////////////////////////////////////////////
	interface ICellRenderer
	{
		virtual void DrawCell(HDC hDC, const RECT& rcCell, UINT uiRowID, bool bHovered = false) = 0;
		virtual IFormatObject* GetFormat(void)                                                  = 0;
		virtual void SetFormat(IFormatObject* pFormat)                                          = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	enum eCellLongParams
	{
		e2_PARAM_NONE            = 0,
		e2_PARAM_ERROR           = 1,
		e2_PARAM_NOT_VERIFIED    = 2,
		e2_PARAM_NOT_ENOUGH_DATA = 3,
		e2_PARAM_RTD_RECEIVED    = 4,
		e2_PARAM_RTD_DISMISS     = 5,
		e2_PARAM_FORECOLOR_DEF   = 6,	// used default color for text
		e2_PARAM_FORECOLOR_PALETTE = 7	// used default color for text
	};
/////////////////////////////////////////////////////////////////////////////
	interface IFind
	{
		virtual void Find(IColumn* pColumn, bool bShow = true, bool bDestroy = false) = 0;
		virtual bool IsActiveWnd(void)                                                = 0;
		virtual bool IsFind(bool& bIsVisible)                                         = 0;
		virtual void SetHWNDOwner(HWND hWndOwner)                                     = 0;
		virtual void SetLockFindWnd(bool bLock)                                       = 0;
		virtual void SetTitle(LPCTSTR lpTitle)                                        = 0;
	};
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
	enum eCmdMenuGroup
	{
		e2_ExportToExcel   = 0,
		e2_CopyToClipboard = 1,
		e2_SelectAll       = 2,
		e2_TextSize        = 3,
		e2_Themes          = 4
	};
	interface IMenuBuilder
	{
		virtual bool		   AppendCommandToContextMenu(const HMENU, const HINSTANCE hResModule, const eCmdMenuGroup* pCmdGroup, const int cGroup) = 0;   // appends to the end necessary menu items for built-in command
		virtual bool           UpdateAllMenuState(const HMENU)                            = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	interface ICommandDefaultHandler
	{
		virtual  bool             IsRegistertedCommand(const UINT) const                     = 0;
		virtual  bool             IsTextSizeRegisteredCommand(const UINT) const              = 0;
		virtual  bool             IsThemeRegisteredCommand(const UINT) const                 = 0;
		virtual  bool             IsExportToExcelRegisterCommand(const UINT) const           = 0;
		virtual  bool             IsCopyToClipboardRegisterCommand(const UINT) const         = 0;
		virtual  bool             IsSelectAllRegisterCommand(const UINT) const               = 0;
		virtual  HRESULT          OnRegisteredCommand(const UINT)                            = 0;
		virtual  HRESULT          OnTextSizeRegisteredCommand(const UINT)                    = 0;
		virtual  HRESULT          OnThemeRegisteredCommand(const UINT)                       = 0;
		virtual  HRESULT          OnExportToExcelRegisterCommand(const UINT)                 = 0;
		virtual  HRESULT		  OnCopyToClipboardRegisterCommand(const UINT)               = 0;
		virtual  HRESULT          OnSelectAllRegisterCommand(const UINT)                     = 0;
		virtual  eTextSizes       RegisteredCommandToTextSize(const UINT) const              = 0;
		virtual  ePredefinedTheme RegisteredCommandToPredefinedTheme(const UINT) const    = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	enum eNotifyPriority
	{
		eNotifyPriorityLow = 0,
		eNotifyPriorityHigh = 1,
	};
/////////////////////////////////////////////////////////////////////////////
	enum eAcceleratorGroup
	{
		e2_None = 0,
		e2_NoneModifyCmd = 1,
		e2_All = 2
	};
	interface IAcceleratorManager
	{
		virtual bool SetAcceleratorGroup(const eAcceleratorGroup eGroup) = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	interface IGrid : public IEventSink
	{
		virtual void           AddRow(void)                                               = 0;
		virtual void           Advise(IEventSink* const pSink, const eNotifyPriority = eNotifyPriorityLow) = 0;
		virtual void           ApplyObject(IColumn* pColumn, int nReserved = _NULL_PARAM) = 0;
		virtual void           ApplyObject(IColumnGroup* pGroup)                          = 0;
		virtual void           ApplyObject(IRow* pRow)                                    = 0;
		virtual void           ApplyObject(IColorTheme2* pTheme)                          = 0;
		virtual void           ClearColumn(UINT iColIndex)                                = 0;
		virtual void           ClearColumn(UINT iColID, int nReserved)                    = 0;
		virtual void           ClearLine(void)                                            = 0;
		virtual void           ClearRow(UINT iRowIndex)                                   = 0;
		virtual void           ClearRow(UINT iRowID, int nReserved)                       = 0;
		virtual void           ClearRowByIndex(UINT iRowID)                               = 0;
		virtual void           ClearSort(void)                                            = 0;
		virtual bool           CopyToClipboard(void)                                      = 0;
		virtual void           DeleteAllLine(void)                                        = 0;
		virtual void           DeleteLine(void)                                           = 0; 
		virtual void           DeleteObject(ICell* pCell)                                 = 0;
		virtual void           DeleteObject(IRow* pRow)                                   = 0;
		virtual void           DeleteRow(void)                                            = 0;
		virtual void           Destroy(void)                                              = 0;
		virtual void           EnableRowGrouping(bool)                                    = 0; // enables/disables grouping mode in the grid
		virtual void           EnsureVisible(void)                                        = 0; // makes current cell visible (auto-scroll)
		virtual HRESULT        Export2Excel(void)                                         = 0;
		virtual IAcceleratorManager& GetAcceleratorManager(void)                          = 0;
		virtual ICell*         GetActiveCell(void)                                        = 0;
		virtual ICell*         GetActiveCell(int nReserved)                               = 0;
		virtual ICell*         GetCell(UINT uRow, UINT uCol)                              = 0;
		virtual ICellEditor*   GetCellEditor(void)                                        = 0;
		virtual long           GetCellLong(UINT uRow, UINT uCol)                          = 0;
		virtual IColorTheme2*  GetColorTheme(void)const                                   = 0;
		virtual IColumn*       GetColumn(UINT iColIndex)                                  = 0;
		virtual IColumn*       GetColumn(UINT iColID, int iReserved)                      = 0;
		virtual UINT           GetColumnCount(void)                                       = 0;
		virtual IColumnGroup*  GetColumnGroup(int iGroupIndex)                            = 0;
		virtual IColumnGroup*  GetColumnGroup(UINT iGroupID, int iReserved)               = 0;
		virtual UINT           GetColumnGroupCount(DWORD dwStyle=0)                       = 0;
		virtual IColumnHeader* GetColumnHeader(void)                                      = 0;
		virtual IColumnHeader& GetColumnHeader_Ref(void)                                  = 0;
		virtual HMENU          GetCtxMenuHandle(eShortcutMenus eShortcut) const           = 0;
		virtual ICommandDefaultHandler& GetCommandDefaultHandlerRef(void)                 = 0;
		virtual IFind*         GetFind(void)                                              = 0;
		virtual IRow*          GetLastRow(void)                                           = 0;
		virtual IMenuBuilder&  GetMenuBuilderRef(void)                                    = 0;
		virtual bool           GetPersistData(ePersistDataTypes eType, VARIANT* pvData)   = 0;
		virtual IPersistent*   GetPersistent(void)                                        = 0;
		virtual IPersistent&   GetPersistent_Ref(void)                                    = 0;
		virtual IRow*          GetRow(UINT iRowIndex)                                     = 0;
		virtual IRow*          GetRow(UINT iRowID, int iReserved)                         = 0;
		virtual UINT           GetRowCount(void)                                          = 0;
		virtual IThemeManager& GetThemeManagerRef(void)                                   = 0;
		virtual UINT           GetTotalRowCount(void)                                     = 0;
		virtual IRowGroupCollection* GetRowGroupCollection(void)                          = 0;
		virtual IRowGroupCollection& GetRowGroupCollection_Ref(void)                      = 0;
		virtual IRowSelector*  GetRowSelector(void)                                       = 0;
		virtual const RANGE&   GetRowRange(void)                                          = 0; // returns row available range (iStart - minimal row number, iEnd - maximum row number)
		virtual HWND           GetSafeHwnd(void)                                          = 0;
		virtual IColumn*       GetSortIndex(int iter)                                     = 0;
		virtual int            GetSortIndexCount(void)                                    = 0;
		virtual void           GetText(UINT uRow, UINT uCol, TCHAR* lpBuffer, UINT iBufferLen) = 0;
		virtual bool           GetTextInverse(void) const                                 = 0;
		virtual eTextSizes     GetTextSize(void) const                                    = 0;
		virtual void           GetValue(UINT uRow, UINT uCol, _variant_t& vtValue, int* pPrecision=0, long* pclrFore=0, long* pclrBack=0) = 0;
		virtual IRow*          InsertRow(UINT iRowID, UINT iBefore)                       = 0;
		virtual bool           IsDataInSelectRowOrActiveCell(void)                        = 0;
		virtual bool           IsSupported(DWORD dwAttributes)                            = 0;
		virtual bool           ModifyCellLong(UINT iRowID, UINT iColID, long eParams, bool bSet)=0;
		virtual bool           MoveColumnGroup(IColumnGroup* pGroup, UINT iBefore)        = 0;
		virtual void           MoveToClipboard(void)                                      = 0;
		virtual void           PasteFromClipboard(void)                                   = 0;
		virtual void           Refresh(void)                                              = 0;
		virtual void           RemoveColumnGroup(IColumnGroup* pGroup)                    = 0;
		virtual void           RemoveColumnGroup(UINT iGroupIndex)                        = 0;
		virtual void           RemoveRow(UINT iRowIndex)                                  = 0;
		virtual void           RemoveRow(UINT iRowID, int nReserved)                      = 0;
		virtual void           RemoveLastRowInDefaultGroup()							  = 0;
		virtual bool           SetActiveCell(ICell* pCell)                                = 0;
		virtual bool           SetActiveCell(UINT uRow, UINT uCol)                        = 0;
		virtual void           SetCellForeColor(UINT uRow, UINT uCol, COLORREF clrFore)   = 0;
		virtual void           SetCellLong(UINT iRowID, UINT iColID, long eParams)        = 0;
		virtual void           SetCellRenderer(ICellRenderer* pRenderer)                  = 0;
		virtual void           SetRowRange(const RANGE&)                                  = 0; // sets flexible row available range (iStart - minimal row count, iEnd - maximum row count)
		virtual void           SetRows(UINT nCount)                                       = 0; // sets fixed row available range (internally iStart & iEnd are assigned to nCount)
		virtual void           SetText(UINT uRow, UINT uCol, const TCHAR* lpText)         = 0;
		virtual void           SetText(UINT iRowID, UINT iColID, LPCTSTR lpText, long clrFore) = 0;
		virtual void           SetText(UINT iRowID, UINT iColID, const _variant_t& vtValue, int iPrecision = 5, long clrFore = CLR_NONE, long clrBack = CLR_NONE) = 0;
		virtual void           SetTextInverse(bool bOn)                                   = 0;
		virtual void           SetTextSize(eTextSizes eSize)                              = 0;
		virtual void           ShowRowSelector(bool bShow)                                = 0;
		virtual void           ShowScrollBars(eGridScrollBarShow eMode)                   = 0;
		virtual void           Sort(void)                                                 = 0;
		virtual void           StopSort(bool bStop, bool bLockDataChange = true)          = 0;
		virtual void           Unadvise(IEventSink* pSink)                                = 0;
		virtual void           Update(ICell* pCell)                                       = 0;
		virtual void           UpdateBatch(int iFreq = _UPDATE_FQ_AUTO/*in millisec*/)    = 0;
		virtual void           UpdateLayout(HWND hwndFrame)                               = 0;
		virtual void           UpdateVisibleColumn(void)                                  = 0;
	};
/////////////////////////////////////////////////////////////////////////////
	IGrid*                     CreateGridInstance(HWND hFrame, const DWORD _max_columns = 100, const WORD _ctrl_id = 0);
	HRESULT                    DestroyGrid_Safe(IGrid**);
	const ICommandDefaultHandler&   GetDefaultHandler_Ref(void);
/////////////////////////////////////////////////////////////////////////////
}

typedef UILayer::SCellPersistData  TCellPersistData;
typedef UILayer::SColPersistData   TColPersistData;
typedef UILayer::SGroupPersistData TGroupPersistData;
typedef UILayer::SRowPersistData   TRowPersistData;

#endif/*_IGRIDINTERFACEDEF_H_DD819C22_D166_48EE_9ED0_71255526BFE6_INCLUDED*/