#ifndef _IGRIDINTERFACEFMT_H_99767FE3_949A_4AD1_9325_72A502A81450_INCLUDED
#define _IGRIDINTERFACEFMT_H_99767FE3_949A_4AD1_9325_72A502A81450_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Nov-2019 at 6:20:09p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack flex grid control format interface declaration file.
*/
#include "IGridInterface.enu.h"
#include "IGridInterface.col.h"

namespace UILayer { namespace ext {

	interface IFormatObject
	{
		virtual COLORREF           GetBackColor(void)                      const = 0; // used as first gradient color in derived objects
		virtual HFONT              GetFont     (void)                      const = 0;
		virtual COLORREF           GetForeColor(void)                      const = 0;
		virtual eColumnHAlignTypes GetHAlign   (void)                      const = 0;
		virtual LPCTSTR            GetPattern  (void)                      const = 0;
		virtual void               SetBackColor(const COLORREF)                  = 0;
		virtual void               SetFont     (const HFONT)                     = 0;
		virtual void               SetForeColor(const COLORREF)                  = 0;
		virtual void               SetHAlign   (const eColumnHAlignTypes)        = 0;
		virtual void               SetPattern  (LPCTSTR)                         = 0;
	};
/////////////////////////////////////////////////////////////////////////////
// interface is to format gradient drawing
	interface IFormatObject2 : public IFormatObject
	{
		virtual COLORREF       GetBackColor2     (void)          const = 0; // gets background gradient color (second)
		virtual UINT           GetColPerGradient (void)          const = 0; // gets column count for gradient transition (0 - all visible cols)
		virtual eGradientType  GetGradientType   (void)          const = 0;
		virtual COLORREF       GetGridLineHColor (void)          const = 0; // gets grid horizontal line (row divider) color
		virtual COLORREF       GetGridLineHColor2(void)          const = 0; // gets grid horizontal line (row divider) color (second)
		virtual COLORREF       GetGridLineVColor (void)          const = 0; // gets grid vertical line (column divider) color
		virtual COLORREF       GetGridLineVColor2(void)          const = 0; // gets grid vertical line (column divider) color (second)
		virtual UINT           GetRowPerGradient (void)          const = 0; // gets row count for gradient transition (0 - all visible rows)
		virtual bool           IsValid           (void)          const = 0; // validate an object
		virtual void           SetBackColor2     (const COLORREF)      = 0; // sets background gradient color (second)
		virtual void           SetColPerGradient (const UINT iCols)    = 0; // sets column count for gradient transition (0 - all visible cols)
		virtual void           SetGradientType   (const eGradientType) = 0;
		virtual void           SetGradientMode   (const bool bEnable)  = 0; // sets gradient mode availability
		virtual void           SetGridLineHColor (const COLORREF)      = 0; // sets grid horizontal line (row divider) color
		virtual void           SetGridLineHColor2(const COLORREF)      = 0; // sets grid horizontal line (row divider) color (second)
		virtual void           SetGridLineVColor (const COLORREF)      = 0; // sets grid vertical line (column divider) color
		virtual void           SetGridLineVColor2(const COLORREF)      = 0; // sets grid vertical line (column divider) color (second)
		virtual void           SetHorzLineMode   (const bool bEnable)  = 0; // sets horizontal (row divider) lines drawing availability
		virtual void           SetRowPerGradient (const UINT iRows)    = 0; // sets row count for gradient transition (0 - all visible rows)
		virtual void           SetVertLineMode   (const bool bEnable)  = 0; // sets vertical (col divider) lines drawing availability
		virtual bool           UseGradient       (void)          const = 0; // returns gradient mode availability
		virtual bool           UseHorzLine       (void)          const = 0; // returns horizontal (row divider) lines drawing availability
		virtual bool           UseVertLine       (void)          const = 0; // returns vertical (col divider) lines drawing availability
	};
/////////////////////////////////////////////////////////////////////////////
// format object for row group
	interface IFormatObject4 : public IFormatObject
	{
		virtual void            Apply            (void)                   = 0;
		virtual COLORREF        GetBackColor2    (void)             const = 0; // gets background gradient color (second)
		virtual COLORREF        GetFacetColor    (const eFacetType) const = 0; // gets a facet color of specified facet type
		virtual eGradientEffect GetGradientEffect(void)             const = 0;
		virtual void            SetGradientEffect(const eGradientEffect)  = 0;
		virtual void            SetBackColor2    (const COLORREF)         = 0;
		virtual void            SetFacetColor    (const eFacetType, const COLORREF) = 0;
	};
/////////////////////////////////////////////////////////////////////////////
// format object for selection
	interface IFormatObject6
	{
		virtual void            Apply           (void)                    = 0;
		virtual COLORREF        GetColor        (void)              const = 0;
		virtual int             GetTransparency (void)              const = 0; // 0% - complete transparent, 100% - opaque
		virtual void            SetColor        (const COLORREF clr)      = 0;
		virtual void            SetTransparency (const int t)             = 0; // from 0% to 100%
	};
}}

#endif/*_IGRIDINTERFACEFMT_H_99767FE3_949A_4AD1_9325_72A502A81450_INCLUDED*/