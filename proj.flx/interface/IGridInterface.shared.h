#ifndef __FLEXGRIDSHAREDDEFINITIONS_H_574E33B7_5409_45d1_B4CD_39E366A2ECE4_INCLUDED
#define __FLEXGRIDSHAREDDEFINITIONS_H_574E33B7_5409_45d1_B4CD_39E366A2ECE4_INCLUDED
/*
	Created by VToropov on 20-Feb-2012 at 1:21:36pm, GMT+3, Rostov-on-Don, Monday;
	This is Flex Grid Shared Definitions file.
*/

#define ID_CMD_EXPORT_CSV_DATA                        0x61A0
#define ID_CMD_SELECT_ALL                             0x61A1
////////////////////////////////////////////////////////////////////////////////////
#define ID_SCNR_TXT_FIRST________                     0x61A9
#define ID_SCNR_TEXT_SIZE_LARGEST                     0x61A9
#define ID_SCNR_TEXT_SIZE_LARGER                      0x61AA
#define ID_SCNR_TEXT_SIZE_MEDIUM                      0x61AB
#define ID_SCNR_TEXT_SIZE_SMALLER                     0x61AC
#define ID_SCNR_TEXT_SIZE_SMALLEST                    0x61AD
#define ID_SCNR_TXT_LAST_________                     ID_SCNR_TEXT_SIZE_SMALLEST
////////////////////////////////////////////////////////////////////////////////////
#define ID_THEMES_FIRST__________                     0x61AE
#define ID_THEMES_BLUE__GRADIENT                      0x61AE
#define ID_THEMES_BROWN_GRADIENT                      0x61AF
#define ID_THEMES_GREEN_GRADIENT                      0x61B0
#define ID_THEMES_TEAL_GRADIENT                       0x61B1
#define ID_THEMES_WHITE_GRADIENT                      0x61B2
#define ID_THEMES_BLACK                               0x61B3
#define ID_THEMES_WHITE                               0x61B4
#define ID_THEMES_BLUE                                0x61B5
#define ID_THEMES_GREEN                               0x61B6
#define ID_THEMES_BROWN                               0x61B7
#define ID_THEMES_WA_STRIPES                          0x61B8
#define ID_THEMES_LAST___________                     0x61B8

namespace UILayer { namespace Grid_menu
{
	static const UINT CMD_EXPORT_CSV_DATA           = ID_CMD_EXPORT_CSV_DATA;
	static const UINT CMD_SELECT_ALL                = ID_CMD_SELECT_ALL;
	///////////////////////////////////////////////////////////////////////////////////
	static const UINT CMD_TXT_SIZE_SMALLEST         = ID_SCNR_TEXT_SIZE_SMALLEST;
	static const UINT CMD_TXT_SIZE_SMALLER          = ID_SCNR_TEXT_SIZE_SMALLER;
	static const UINT CMD_TXT_SIZE_MEDIUM           = ID_SCNR_TEXT_SIZE_MEDIUM;
	static const UINT CMD_TXT_SIZE_LARGER           = ID_SCNR_TEXT_SIZE_LARGER;
	static const UINT CMD_TXT_SIZE_LARGEST          = ID_SCNR_TEXT_SIZE_LARGEST;
	///////////////////////////////////////////////////////////////////////////////////
	static const UINT CMD_THEME_BLUE_GRADIENT       = ID_THEMES_BLUE__GRADIENT;
	static const UINT CMD_THEME_BROWN_GRADIENT      = ID_THEMES_BROWN_GRADIENT;
	static const UINT CMD_THEME_GREEN_GRADIENT      = ID_THEMES_GREEN_GRADIENT;
	static const UINT CMD_THEME_TEAL_GRADIENT       = ID_THEMES_TEAL_GRADIENT;
	static const UINT CMD_THEME_WHITE_GRADIENT      = ID_THEMES_WHITE_GRADIENT;
	static const UINT CMD_THEME_BLACK_SOLID         = ID_THEMES_BLACK;
	static const UINT CMD_THEME_WHITE_SOLID         = ID_THEMES_WHITE;
	static const UINT CMD_THEME_BLUE_SOLID          = ID_THEMES_BLUE;
	static const UINT CMD_THEME_GREEN_SOLID         = ID_THEMES_GREEN;
	static const UINT CMD_THEME_BROWN_SOLID         = ID_THEMES_BROWN;
	static const UINT CMD_THEME_WET_ASPHALT_STRIPES = ID_THEMES_WA_STRIPES;
}}

#endif/*__FLEXGRIDSHAREDDEFINITIONS_H_574E33B7_5409_45d1_B4CD_39E366A2ECE4_INCLUDED*/
