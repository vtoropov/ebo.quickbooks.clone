#ifndef _IGRIDINTERFACECOL_H_CEC16E4D_54E3_48F5_A2AA_E0AF4EFE1B79_INCLUDED
#define _IGRIDINTERFACECOL_H_CEC16E4D_54E3_48F5_A2AA_E0AF4EFE1B79_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Nov-2019 at 5:31:19p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack flex grid control column interface declaration file.
*/
#include "IGridInterface.enu.h"

namespace UILayer {
	interface IColumnGroup;
}

namespace UILayer { namespace ext {

	using UILayer::IColumnGroup;

	enum eColumnStyles
	{
		e2_clmnStyleNone     = 0x0,
		e2_clmnStyleDropDown = 0x1,
		e2_clmnStyleFixed    = 0x2,      // cannot be sized
		e2_clmnStyleNumbered = 0x4
	};

	enum eColumnHAlignTypes
	{
		e2_clmnHAlignGeneral = 0,
		e2_clmnHAlignLeft    = 1,
		e2_clmnHAlignRight   = 2,
		e2_clmnHAlignCenter  = 3,
		e2_clmnHAlignNA      = 0xff     // non-applicable
	};

	enum eColumnDataTypes
	{
		e2_clmnDTypeText     = 0,
		e2_clmnDTypeLong     = 1,
		e2_clmnDTypeDouble   = 2,
		e2_clmnDTypePicture  = 3
	};

	enum eColumnDataSortOrders
	{
		e2_clmnDSortNone     = 0,
		e2_clmnDSortAsc      = 1,
		e2_clmnDSortDesc     = 2,
		e2_clmnDSortDisable  = 3
	};
/////////////////////////////////////////////////////////////////////////////
	interface IColumnButton
	{
		virtual COLORREF    GetBackColor(void)                             = 0;
		virtual const RECT& GetRect_Ref (void)                             = 0;
		virtual void        GetText     (TCHAR* lpText, UINT iBufferLen)   = 0;
		virtual void        GetToolText (TCHAR* lpToolTip, UINT iBufferLen)= 0;
		virtual int         GetWidth    (void)                             = 0;
		virtual bool        HasSeparator(void)                             = 0;
		virtual void        SetBackColor(COLORREF clr)                     = 0;
		virtual void        SetRect     (const RECT& refRect)              = 0;
		virtual void        SetSeparator(bool bHasSep)                     = 0;
		virtual void        SetText     (const TCHAR* lpText)              = 0;
		virtual void        SetToolText (const TCHAR* lpToolTip)           = 0;
		virtual void        SetWidth    (int uWidth)                       = 0;
	};	
/////////////////////////////////////////////////////////////////////////////
	interface IColumn
	{
		virtual __OverlayData&        AccessOverlay(void)                  = 0;
		virtual void                  Destroy      (void)                  = 0;
		virtual IColumnButton*        GetButton    (void)                  = 0;
		virtual void                  GetCaption   (TCHAR* lpCaption, UINT iBufferLen) = 0;
		virtual LPCTSTR               GetCaption   (void)                  = 0;
		virtual eColumnDataTypes      GetDataType  (void)                  = 0;
		virtual UINT                  GetDefWidth  (void)                  = 0;
		virtual eColumnHAlignTypes    GetHAlign    (void)                  = 0;
		virtual UINT                  GetID        (void)                  = 0;
		virtual UINT                  GetIndex     (void)                  = 0;
		virtual  INT                  GetLeft      (bool bInPx=true)       = 0;
		virtual UINT                  GetMinWidth  (void)                  = 0;
		virtual const __OverlayData&  GetOverlay   (void) const            = 0;
		virtual IColumnGroup*         GetParent    (void)                  = 0;
		virtual eColumnDataSortOrders GetSortOrder (void)                  = 0;
		virtual DWORD                 GetStyle     (void)                  = 0;
		virtual ULONG                 GetTag       (void)                  = 0;
		virtual UINT                  GetWidth     (void)                  = 0;
		virtual bool                  HasStyle     (DWORD dwStyle)         = 0;
		virtual bool                  IsLocked     (void)                  = 0;
		virtual bool                  IsNumber     (void)                  = 0;
		virtual bool                  IsReadOnly   (void)                  = 0;
		virtual bool                  IsVisible    (void)                  = 0;
		virtual void                  ModifyStyle  (DWORD dwStyle, bool bRemove)  = 0;
		virtual void                  SetCaption   (const TCHAR* lpCaption)= 0;
		virtual void                  SetDataType  (eColumnDataTypes eType)= 0;
		virtual void                  SetDefWidth  (UINT uWidth)           = 0;
		virtual void                  SetHAlign    (eColumnHAlignTypes eType)     = 0;
		virtual void                  SetID        (UINT uID)              = 0;
		virtual void                  SetIndex     (UINT iColIndex)        = 0;
		virtual void                  SetLeft      (INT iLeft, bool bInPx=true)   = 0;
		virtual void                  SetLock      (bool bState)           = 0;
		virtual void                  SetNumber    (bool bNumber)          = 0;
		virtual void                  SetMinWidth  (UINT iMWidth)          = 0;
		virtual void                  SetMinWidth  (const TCHAR* lpFromText)      = 0;
		virtual void                  SetParent    (IColumnGroup* pGroup)  = 0;
		virtual void                  SetReadOnly  (bool bState)           = 0;
		virtual void                  SetSortOrder (eColumnDataSortOrders eOrder) = 0;
		virtual void                  SetStyle     (DWORD dwStyle)         = 0;
		virtual void                  SetVisible   (bool bState)           = 0;
		virtual void                  SetTag       (ULONG ulTag)           = 0;
		virtual void                  SetWidth     (UINT uWidth)           = 0;
		virtual void                  SetWidth     (const TCHAR* lpFromText)      = 0;
	};
}}

#endif/*_IGRIDINTERFACECOL_H_CEC16E4D_54E3_48F5_A2AA_E0AF4EFE1B79_INCLUDED*/